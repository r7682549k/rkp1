<page>1</page><title>¡NOP!</title>
<microhd>NA</microhd>
<fullhd>6cc957393eda2c516d27a41013fba53e50d1b5ed</fullhd>
<tresd>NA</tresd>
<cuatrok>423189a314b37e22a3e430aec65d6e3c51a7a55a</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/kI6MCuN69AH1xc4ABnFRnCaTxCV.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/tXRl6ijm4cnLp47icO9SPp1PT5g.jpg</fanart>
<date>2022</date>
<genre>Intriga. Fantástico. Terror</genre>
<extra>NA</extra>
<info>Dos rancheros de un remoto pueblo del interior de California hacen un descubrimiento tan insólito como escalofriante.</info>
 
<page>1</page><title>¡SHAZAM! LA FURIA DE LOS DIOSES</title>
<microhd>NA</microhd>
<fullhd>a47a64b38f5d9d957f6016a503528684d707d8bb</fullhd>
<tresd>NA</tresd>
<cuatrok>2a3334f5eac96b9a848896c69e4e67c8599f101f</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/igFLHxab9zG0M89OmEpnOM6TPXn.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/vn4vwKXiqpcmThenluyWhxMvTQK.jpg</fanart>
<date>2023</date>
<genre>Fantástico. Comedia. Acción. Aventuras. Saga</genre>
<extra>NA</extra>
<info>Billy Batson y sus compañeros adoptados han recibido los poderes de los dioses pero aún están aprendiendo a compaginar sus vidas de adolescentes con sus alter-egos superheróicos. Pero cuando las Hijas de Atlas, un vengativo trío de antiguos dioses, llegan a la Tierra en busca de la magia que les robaron hace mucho tiempo, Billy -alias Shazam- y su familia se ven envueltos en una batalla por sus superpoderes, sus vidas y el destino de su mundo. </info>
 
<page>1</page><title>¿NO ES ROMANTICO?</title>
<microhd>3660d201fe3a086c1d75e7dfce6f85af0d049db4</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/AvLtW2LPRYbyaXgoC0UA2RsK68T.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/fPwJmIg6CVNNXUJvL1wgeexzwEp.jpg</fanart>
<date>2019</date>
<genre>Comedia. Romance</genre>
<extra>NA</extra>
<info>Natalie (Rebel Wilson) es una arquitecta de Nueva York que se esfuerza por hacerse notar en su trabajo, pero es más probable que le pidan café y panecillos que diseñe el próximo rascacielos de la ciudad. Por si las cosas no son lo suficientemente malas, Natalie, una cínica de toda la vida cuando se trata del amor, tiene un encuentro con un asaltante que la deja inconsciente y, al despertar, descubre que su vida se ha convertido de repente en su peor pesadilla: está inmersa en una comedia romántica. Y es la protagonista...</info>
 
<page>1</page><title>12 MONOS</title>
<microhd>NA</microhd>
<fullhd>788ecbc90b9533530a23f8bb5ac196bf0263e7fc</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/rLygRoznX4Rycwcs6ENh4msMt3z.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/1IWaKG7AWiYMhADxhGtnElDJAGI.jpg</fanart>
<date>1995</date>
<genre>Ciencia ficción. Fantástico. Intriga </genre>
<extra>Culto</extra>
<info>Año 2035. Tras la epidemia provocada por un virus asesino que ha matado a millones de personas, los supervivientes se refugian en comunidades subterráneas, húmedas y frías. El prisionero James Cole se ofrece como voluntario para viajar al pasado y conseguir una muestra del virus, gracias a la cual los científicos podrán elaborar un antídoto. Durante el viaje conoce a una bella psiquiatra y a Jeffrey Goines, un excepcional enfermo mental. Cole tratará de encontrar al ”Ejército de los 12 Monos”, un grupo radical vinculado a la mortal enfermedad.</info>
 
<page>1</page><title>12 VALIENTES</title>
<microhd>de2b15fb8025c99ad80a8c9f4ff8809b739c5c7c</microhd>
<fullhd>75cf493c9c064af33dca51397a97fb6b486ada7a</fullhd>
<tresd>NA</tresd>
<cuatrok>1f7b4c39694d11b7eefc265f383842ac4e6b940d</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/8YcjGJtW4AEHyNH8yrs8FAaPBM.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/x5EUEv1N9g4BCX9GQXKvv3nqMVo.jpg</fanart>
<date>2018</date>
<genre>Belico</genre>
<extra>NA</extra>
<info>Año 2001. Poco después de los atentados del 11-S, un equipo de soldados de élite de las Fuerzas Especiales, en colaboración con la C.I.A. y operativos de la Fuerza Aérea, unen sus fuerzas con la Alianza del Norte de Afganistán para derrotar a los talibanes, gobernantes del país. Tras conseguir introducirse en secreto en territorio afgano, este grupo de hombres, encabezado por Mitch Nelson (Chris Hemsworth), será el encargado de poner en práctica una peligrosa misión. En las escarpadas montañas afganas deberán convencer al general Dostum (Navid Negahban), de la Alianza del Norte, de la necesidad de unirse y combatir juntos a los talibanes y Al Qaeda. En esta arriesgada tarea no dispondrán de tanques, ya que será una batalla a caballo...</info>
 
<page>1</page><title>14 DIAS, 12 NOCHES</title>
<microhd>c6697d432382a8feb7f757188d49de47e0b845b4</microhd>
<fullhd>efa7c62d5855d04c73d9a1dc6a61462835e571f4</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/4jRl5en1AHzPOj2S41Ch6CTiQ2J.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/rFVrq8xfuMIuWrxVlin1CiF1d6X.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>La adolescente hija adoptada de Isabelle muere en un trágico accidente. Un año después, y aún conmocionada por la experiencia, Isabelle regresa al orfanato vietnamita con la esperanza de volver a conectarse emocionalmente con su hija Clara a través de su tierra natal. Inmersa y abrumada, Isabelle inesperadamente se encuentra con la madre biológica de su hija y juntas buscan la forma de seguir adelante.</info>
 
<page>1</page><title>1917</title>
<microhd>5757bf736e179b4a648d2bf42f1e69594d66ee36</microhd>
<fullhd>a3582b7ebe66550615eed096421a7635e6a678f3</fullhd>
<tresd>NA</tresd>
<cuatrok>2fb02c0a2f86e1476b3ce59b350ae4dc6dc051fd</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/axdhbcZeOsfX3ZpwtgdgPIjc06l.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ds9GKAMhxv5AbprmZ2xCpK8FiEG.jpg</fanart>
<date>2019</date>
<genre>Belico. Drama </genre>
<extra>NA</extra>
<info>En lo más crudo de la Primera Guerra Mundial, dos jóvenes soldados británicos, Schofield (George MacKay) y Blake (Dean-Charles Chapman) reciben una misión aparentemente imposible. En una carrera contrarreloj, deberán atravesar el territorio enemigo para entregar un mensaje que evitará un mortífero ataque contra cientos de soldados, entre ellos el propio hermano de Blake.</info>
 
<page>1</page><title>1942 LA GRAN OFENSIVA</title>
<microhd>6c0a52133816918f73793bbdb4ef796eb4d333df</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/sgUaucXoVmNdI2SyeJyPCxm0Psr.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/cpjGunibNPz0K75nzpLEXX5a5GE.jpg</fanart>
<date>2019</date>
<genre>Bélico. Drama</genre>
<extra>NA</extra>
<info>Basada en las memorias de Vyacheslav Kondratyev, aborda la batalla de la ciudad de Rzhev durante la segunda guerra mundial de 1942-1943. Dicha batalla fue clave en la lucha para romper el devastador asedio naSi. Se produjo un baño de sangre, murieron más de 400 mil personas y hubo 900 mil heridos.</info>
 
<page>1</page><title>2001 UNA ODISEA EN EL ESPACIO</title>
<microhd>0ff31d6b8396fe0cf713db829c62dd73308c2bbc</microhd>
<fullhd>00bb8d9afb1f811f49a1284683731efc7ebe2cbf</fullhd>
<tresd>NA</tresd>
<cuatrok>8d5a20db24c0c8352c8918609479de9513a5946a</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/sln50uQYigvu5AvN72JfYfu1ckq.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/spoZUN4X1KiOc5S0plOyGAXLNtb.jpg</fanart>
<date>1968</date>
<genre>Ciencia ficción</genre>
<extra>NA</extra>
<info>Tras su descubrimiento en África hace varias generaciones, los misteriosos monolitos conducen a la humanidad a un impresionante viaje a Júpiter, con la ayuda del superordenador HAL 9000..</info>
 
 
<page>1</page><title>300</title>
<microhd>NA</microhd>
<fullhd>d52e8e0fc587a1663c5c00e1c9b22216d397f7ef</fullhd>
<tresd>NA</tresd>
<cuatrok>c831823122bb345e15e7f62af3ff36baa753ec04</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/lGv19gokQvgC7jgjWqapIachnxU.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/lgBZlJ1LHQel5nneNQMoesmvc7l.jpg</fanart>
<date>2006</date>
<genre>Acción. Bélico. Aventuras. Fantástico</genre>
<extra>Mis</extra>
<info>Adaptación del cómic de Frank Miller (autor del cómic 'Sin City') sobre la famosa batalla de las Termópilas (480 a.C.). El objetivo de Jerjes, emperador de Persia, era la conquista de Grecia, lo que desencadenó las Guerras Médicas. Dada la gravedad de la situación, el rey Leónidas de Esparta (Gerard Butler) y 300 espartanos se enfrentaron a un ejército persa que era inmensamente superior.</info>
 
<page>1</page><title>365 DIAS</title>
<microhd>280cfccd13e252f2aec0055acb50500d06f808c1</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/jz8T3hrU6GuMqSuQ4Rbd4MJUeaq.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/e8b2F4eg6ansZhaQQN8iXfzZtz7.jpg</fanart>
<date>2020</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Massimo es miembro de la mafia siciliana y Laura es una directora de ventas. Exitosa en el trabajo pero un tanto aburrida en el amor, ella decide viajar con su novio y un grupo de amigos a Sicilia, pero allí se cruza con Massimo Torricelli, quien acaba de heredar el negocio de la mafia. Decidido a que ella se quede con él, la encierra durante 365 días, para que se enamore de él. </info>
 
<page>1</page><title>6 EN LA SOMBRA</title>
<microhd>caa8cc739bfbc7bc0515d7fd73515ff3c131aaf1</microhd>
<fullhd>b62e9df0db8a180bd7849280c95a6dc89bdf6fc2</fullhd>
<tresd>NA</tresd>
<cuatrok>8511ecfb0b21fcfab3bbb71e923db0412c92d2bc</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/49z88ri8gToqfVodW3GYQJCDmPx.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/eFw5YSorHidsajLTayo1noueIxI.jpg</fanart>
<date>2019</date>
<genre>Acción. Comedia</genre>
<extra>NA</extra>
<info>Conoce a un nuevo tipo de héroe de acción. Seis agentes imposibles de rastrear, totalmente fuera de la red. Han enterrado su pasado para poder cambiar el futuro.</info>
 
<page>1</page><title>70 BINLADENS</title>
<microhd>bc718ac2a9fe0671e849a15942e3418b1a744530</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/AeMBbW0XSpdpcEiyKkm5SvZZYs6.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/1ry2HTEKpJYzMmBL23D6Eg7DvJV.jpg</fanart>
<date>2018</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>Raquel (Emma Suárez) es una mujer con una complicada situación personal que le lleva a necesitar conseguir con urgencia, en 24 horas, 35.000 euros. Su última esperanza es un préstamo bancario. La irrupción de dos atracadores (Nathalie Poza y Hugo Silva) cuando está a punto de cerrar la transacción complica aún más su situación, pero Raquel sabe que no tiene otra opción que salir de allí con el dinero, al precio que sea.</info>
 
<page>1</page><title>A CIEGAS</title>
<microhd>NA</microhd>
<fullhd>4cc81fe836e8f1afdb696c828fdfbda26c5ea044</fullhd>
<tresd>NA</tresd>
<cuatrok>12728a819178421be4d454dcc35303350468456c</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/k9BJG9t2iaxbTo2fEngKyuIc343.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/A4xNxrRodvXzJWJs2GbBKo1IBk2.jpg</fanart>
<date>2018</date>
<genre>Fantastico. Thriller. Terror. Drama</genre>
<extra>NA</extra>
<info>Un lustro después de que una misteriosa presencia sobrenatural llevara al suicidio a una gran parte de la sociedad, una de las supervivientes, Malorie Hayes (Sandra Bullock), y sus dos hijos, buscan desesperadamente el modo de salvarse río abajo, en una pequeña barca, hacia un lugar seguro.</info>
 
<page>1</page><title>A DESCUBIERTO</title>
<microhd>d5b6d70d5221654231ee731bdf698413289a9e6d</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/e6SK2CAbO3ENy52UTzP3lv32peC.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/lOSdUkGQmbAl5JQ3QoHqBZUbZhC.jpg</fanart>
<date>2021</date>
<genre>Ciencia ficción. Acción</genre>
<extra>NA</extra>
<info>Un piloto de drones se une a un oficial androide para localizar un dispositivo que amenaza con acabar con la vida sobre el planeta. El dispositivo en cuestión se encuentra oculto en una zona altamente militarizada. </info>
 
<page>1</page><title>A NIGHT OF HORROR: NIGHTMARE RADIO</title>
<microhd>4f67065963daa7e3c802e0baf644f6b705286c5f</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/g7EYDfGgNEF1IQymS7EvDLKnpxT.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/bs7mYckE4BeVRtxK9MRYhNZTTqg.jpg</fanart>
<date>2019</date>
<genre>Terror. Intriga</genre>
<extra>NA</extra>
<info>Rod conduce un programa de radio dedicado a historias de terror. Hasta que de pronto, comienza a recibir llamadas extrañas de un niño que pide desesperadamente ayuda. Al principio Rod piensa que es un chiste de mal gusto hasta que descubre que no así. Estas llamadas esconden un oscuro secreto...</info>
 
<page>1</page><title>A TODO TREN 2: SI, LES HA PASADO OTRA VEZ</title>
<microhd>NA</microhd>
<fullhd>9cd79ecd76b4ea877ac920768a57c00cbc0aee62</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/998jFDIyVPanolKlKLT5iJCjdDb.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/fNGcWUXdo9F7fZAb4R1ehPW34vD.jpg</fanart>
<date>2022</date>
<genre>Comedia. Saga</genre>
<extra>NA</extra>
<info>Hace un año Ricardo (Santiago Segura) y Felipe (Leo Harlem), encargados de llevar a los niños a un campamento, se quedaron fuera del tren dejando a los niños dentro. Clara (Paz Vega) no se fía ya de ellos y decide este año llevar a los niños personalmente con ayuda de su amiga Susana (Paz Padilla). Sin embargo, ahora serán ellas las que a raíz de un accidente se separen de los niños. Secuela de \"A todo tren\". Destino Asturias.</info>
 
<page>1</page><title>A TODO TREN: DESTINO ASTURIAS</title>
<microhd>2d4c767576542d3f97e86a23ffc1e3fae7fca9d7</microhd>
<fullhd>f1fd674b879acab196e5474eaf0198a4e4d25ac9</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/wEbBaFX7DBSrv0GbLvJFZXQ7Sdw.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/wFjgLgKG4AuiqDr7d1U7ct0DmZx.jpg</fanart>
<date>2021</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Cuando Ricardo, padre responsable volcado en su hijo, decide llevarle a un campamento en Asturias en tren nocturno, algunos padres proponen que sea él quien lleve a varios de sus hijos Sin embargo, no cuentan que en el último minuto le acompañe Felipe, abuelo de dos de los niños, un tipo extravagante e irresponsable. Cuando el tren arranca sin Ricardo ni Felipe pero con los niños solos dentro, comenzará una disparatada persecución por parte del padre y del abuelo para alcanzar al tren, y un alocado viaje por parte de los niños donde harán todas las travesuras que no se atrevían a hacer delante de los mayores.</info>
 
<page>1</page><title>A TRAICION</title>
<microhd>e91492835eca6d594ae7508792025e5b06485c94</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/fkjZy0YxNIGJ1VhVevbLzr2co6r.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/5O8gvY97QOQrbDMiATBQtsuRW0c.jpg</fanart>
<date>2020</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>Después de la infidelidad de su exmarido, Grace Waters (Crystal Fox) recupera la ilusión con un nuevo amor. Hasta que salen a la luz los secretos que sacan su cara más violenta.</info>
 
<page>1</page><title>A TRAVES DE MI VENTANA</title>
<microhd>a1e096edfcdb7e694176cb31e80031c562e3cef2</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/zGnfnPkQRUC1W1R5A8ldI4AfSJu.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/4rsomWxlqnHt3muGYK06auhOib6.jpg</fanart>
<date>2022</date>
<genre>Romance. Comedia</genre>
<extra>NA</extra>
<info>Raquel lleva toda la vida loca por Ares, su atractivo y misterioso vecino. Lo observa sin ser vista y es que, muy a su pesar, no han intercambiado ni una palabra. Raquel tiene muy claro su objetivo: conseguir que Ares se enamore de ella. Pero ella no es una niña inocente y no está dispuesta a perderlo todo por el camino, y mucho menos a sí misma.</info>
 
<page>1</page><title>ABOMINABLE</title>
<microhd>887fc74b2c925fb3d9db8f242d803207febe882d</microhd>
<fullhd>abb8c6252e124ca00b3e316040261f4bb111abfc</fullhd>
<tresd>NA</tresd>
<cuatrok>8b36f6b400cae966cddea9c652242940bb3e2338</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/gp7d1OGYpBGyG5E6fr1fMi01bvK.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/pBJdJ3sjdJYQ1wRJJDOSZSGpazh.jpg</fanart>
<date>2019</date>
<genre>Fantastico. Aventuras. Comedia. Infantil</genre>
<extra>NA</extra>
<info>Yi es una adolescente más en la enorme ciudad de Shanghai. Un día, se encuentra a un joven yeti en la azotea de su edificio. La supuestamente “abominable” criatura, que se ha escapado del laboratorio donde estaba encerrado, está siendo buscada por toda la ciudad. Junto con sus ingeniosos amigos Jin y Peng, Yi decide ayudarle a huir, y los cuatro se embarcan en una épica aventura para reunir a la mítica criatura con su familia en el pico más alto del mundo, el Everest. </info>
 
<page>1</page><title>ABRE LOS OJOS</title>
<microhd>64222027da893f1ac01f4fb2083e74a46512c0e1</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/tzEeEEHnCc8tXe4qE8LhaosFq6g.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/2ILGrVCOQyiClPQkS6qSqp7FFvY.jpg</fanart>
<date>1997</date>
<genre>Intriga. Drama. Romance. Ciencia ficción. Thriller</genre>
<extra>Culto</extra>
<info>César, un atractivo joven que ha heredado de sus padres una gran fortuna, vive en una espléndida casa en la que organiza lujosas fiestas. Cuando una noche conoce a Sofía y se enamora de ella, Nuria, su antigua amante, se muere de celos. Al día siguiente, yendo en coche con César, intenta suicidarse. Cuando César se despierta en el hospital, descubre que su rostro ha quedado horriblemente desfigurado.</info>
 
<page>1</page><title>AD ASTRA</title>
<microhd>ea63feac72e2ce9987da9bcb5ae5dd916a91d982</microhd>
<fullhd>f2ace66edd02a25c6daff6cf392ae94ea411ccad</fullhd>
<tresd>NA</tresd>
<cuatrok>1d295bc5d6f145cf3c935de70201f3d55c5a0a53</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/zZKauX8RNzT8VCV2XN78Wc9dVIH.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/elvVHhtKYFLoGGhfyKhhA0wQ4kc.jpg</fanart>
<date>2019</date>
<genre>Ciencia ficcion. Drama. Thriller</genre>
<extra>NA</extra>
<info>El astronauta Roy McBride (Brad Pitt) viaja a los límites exteriores del sistema solar para encontrar a su padre perdido y desentrañar un misterio que amenaza la supervivencia de nuestro planeta. Su viaje desvelará secretos que desafían la naturaleza de la existencia humana y nuestro lugar en el cosmos.</info>
 
<page>1</page><title>ADIOS</title>
<microhd>e4663696f038ecf89f6ec3c4d39a76350a1f5e67</microhd>
<fullhd>ff1ef603a4e398fc4cc0b7fe37c91f8e9048933a</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/A1pSmpQOexCjUQokW3JK4KSe9I7.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/A1Cyp3iNv4fjV4HZ6tvc0GEHQ9c.jpg</fanart>
<date>2019</date>
<genre>Thriller. Acción</genre>
<extra>NA</extra>
<info>Juan (Mario Casas), preso en tercer grado y padre de familia, logra un permiso para asistir a la comunión de su hija en Sevilla. Pero la muerte accidental de la niña destapa todo un entramado de corrupción policial y narcotráfico, y el caso acaba en manos de Eli, una inspectora que tendrá que lidiar con los recelos de un sector de la policía y del padre de la pequeña, que quiere tomarse la justicia por su cuenta.</info>
 
<page>1</page><title>ADIOS,  SEÑOR HAFFMANN</title>
<microhd>NA</microhd>
<fullhd>38268f31511540f65eb7d0343630a5ba884a4f90</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/5Lxsln5naXidztPQQJbAcqA76dz.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/onCRpQaTwP9771uLhUbOprlMbGG.jpg</fanart>
<date>2021</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>París, 1942. François Mercier es un hombre corriente que solo aspira a formar una familia con la mujer que ama, Blanche. También es el empleado de un talentoso joyero, el señor Haffmann. Pero ante la ocupación alemana, los dos hombres no tendrán más remedio que concluir un acuerdo cuyas consecuencias, a lo largo de los meses, alterarán el destino de nuestros tres personajes. </info>
 
<page>1</page><title>ADU</title>
<microhd>5fc15bc996a4bceb4495470b163af005d13ce6bd</microhd>
<fullhd>f925127f442c0c385efaced119b1558a9ed7de41</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/4EBIHSIefpgl6LGq973NizW5k9x.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/tfsU13aIgSCkTZiFWZbNJB2o1is.jpg</fanart>
<date>2020</date>
<genre>Drama </genre>
<extra>NA</extra>
<info>En un intento desesperado por alcanzar Europa y agazapados ante una pista de aterrizaje en Camerún, un niño de seis años y su hermana mayor esperan para colarse en las bodegas de un avión. No demasiado lejos, un activista medioambiental contempla la terrible imagen de un elefante, muerto y sin colmillos. No solo tiene que luchar contra la caza furtiva, sino que también tendrá que reencontrarse con los problemas de su hija recién llegada de España. Miles de kilómetros al norte, en Melilla, un grupo de guardias civiles se prepara para enfrentarse a la furibunda muchedumbre de subsaharianos que ha iniciado el asalto a la valla. Tres historias unidas por un tema central, en las que ninguno de sus protagonistas sabe que sus destinos están condenados a cruzarse y que sus vidas ya no volverán a ser las mismas.</info>
 
<page>1</page><title>AGENTE OCULTO</title>
<microhd>NA</microhd>
<fullhd>f5e8f0dad110c3b51bd4eefb32f7b5b7ee9fcf06</fullhd>
<tresd>NA</tresd>
<cuatrok>b42148f5332cbec46b65cf0dc5b56ee411ffbf76</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/xXKVwk2wbcmZoY1oqa483YGbwwk.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/23G1QhZuS8oPfHbkcK2jYXCN5KZ.jpg</fanart>
<date>2022</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>La extraordinaria y escalofriante historia del estafador profesional Robert Freegard, que se hizo pasar por un agente del MI5.</info>
 
<page>1</page><title>AGENTE STONE</title>
<microhd>NA</microhd>
<fullhd>107487c6b5f547bde04a23bd0eaaa04365548510</fullhd>
<tresd>NA</tresd>
<cuatrok>44ce40ab50d10a63091727e04635792c0a3ced63</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/pfrBdyalwwSEVrqDX8FXLtpwSLE.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/cuJ8IxabMEQVpF5TWnmVTvitHIm.jpg</fanart>
<date>2023</date>
<genre>Acción. Thriller</genre>
<extra>NA</extra>
<info>Rachel Stone es una agente de inteligencia del MI6 que a su vez trabaja de incógnito para una organización mundial ajena a gobiernos dedicada a mantener la paz. Tras una operación en los Alpes italianos, Stone tratará de evitar que una hacker robe su recurso más valioso... y peligroso.</info>
 
<page>1</page><title>AGENTES 355</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>c934f75c5f6abf3400e945274553bb8b073e2ea9</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/kgU2aIuDdPyKtEi8oZ6yzohpfVF.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/8pgKccb5PfE1kWB9qqiXJem83VC.jpg</fanart>
<date>2022</date>
<genre>Thriller. Intriga. Acción</genre>
<extra>NA</extra>
<info>Cuando un arma ultra secreta cae en manos de unos peligrosos mercenarios, la agente especial de la CIA, Mason "Mace" Brown (Jessica Chastain) debe unir fuerzas con la agente alemana Marie (Diane Kruger), la ex aliada del MI6 y especialista en informática Khadijah (Lupita Nyong'o), y la experta psicóloga colombiana Graciela (Penélope Cruz), en una misión letal y vertiginosa para recuperarla. Mientras, una misteriosa mujer, Lin Mi Sheng (Fan Bingbing), sigue todos sus pasos. Juntas deberán superar sus conflictos personales y utilizar sus talentos y experiencia para salvar el mundo. En el camino, se convertirán en camaradas y amigas, formando un nuevo grupo letal: 355. Nombre que adoptan de la primera mujer espía en la Revolución Americana.</info>
 
<page>1</page><title>AGORA</title>
<microhd>8b522c78380af312314b7fcd4c2661b04f7c727a</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/9geLqKZ87tgFBS7GuivL5UWelQu.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/eeFdes8lDTx05DRtqHklZepDRqK.jpg</fanart>
<date>2009</date>
<genre>Aventuras. Romance</genre>
<extra>Mis</extra>
<info>En el siglo IV, Egipto era una provincia del Imperio Romano. La ciudad más importante, Alejandría, se había convertido en el último baluarte de la cultura frente a un mundo en crisis, dominado por la confusión y la violencia. En el año 391, hordas de fanáticos se ensañaron con la legendaria biblioteca de Alejandría. Atrapada tras sus muros, la brillante astrónoma Hypatia (Rachel Weisz), filósofa y atea, lucha por salvar la sabiduría del mundo antiguo, sin percibir que su joven esclavo Davo se debate entre el amor que le profesa en secreto y la libertad que podría alcanzar uniéndose al imparable ascenso del Cristianismo.</info>
 
<page>1</page><title>AGUAS OSCURAS</title>
<microhd>5e9bb9877aeb3f789f0245a19f44e0dcb64a1841</microhd>
<fullhd>bb2ef8e3828bafcfe802ffbd3398b96a879f06fc</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/yMe6JWb3C5ntdxlXtkbOVg6URUo.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/4ZSlTfkHtgTTupCaLbseXQQzZha.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Inspirada en una impactante historia real. Un tenaz abogado (Mark Ruffalo) descubre el oscuro secreto que conecta un número creciente de muertes y enfermedades con una de las corporaciones más grandes del mundo. En el proceso arriesga su futuro, su trabajo y hasta su propia familia para sacar a la luz la verdad.</info>
 
<page>1</page><title>AHORA ME VES</title>
<microhd>5c8f2434cba6ee741099aedb46eaff80fcf8595e</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/iN3fPrPLozgPDrHO1IILh1Cx0C3.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/lYz0QBxmJPKJSFAagsSwDEOMfBp.jpg</fanart>
<date>2013</date>
<genre>Thriller. Saga</genre>
<extra>NA</extra>
<info>Un equipo del FBI debe enfrentarse a una banda de criminales expertos en magia que se dedican a atracar bancos. Son "los cuatro jinetes”, un grupo formado por los mejores ilusionistas del mundo. Durante los atracos, siempre contra hombres de negocios corruptos, hacen llover el dinero robado sobre los espectadores, ante la atónita mirada de un equipo de élite del FBI que les sigue la pista.</info>
 
<page>1</page><title>AHORA ME VES 2</title>
<microhd>c245afdcc869e29202fc5fa2a12d42068719cd81</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/pjGt13AXXTLxvkmdF6SpA1feFOw.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/oFQilRMEq6yQbtMPxIYWpXeQ5ZN.jpg</fanart>
<date>2016</date>
<genre>Thriller. Comedia. Saga</genre>
<extra>NA</extra>
<info>n año después de despistar al FBI y conseguir la admiración del público con sus espectáculos mentales, los cuatro jinetes vuelven a la luz pública, pero un nuevo enemigo se propone arruinar su golpe más espectacular y peligroso hasta la fecha... Secuela de "Now You See Me" de 2013.</info>
 
<page>1</page><title>AIR FORCE ONE</title>
<microhd>7973608b4a62d6721522cc09bddac465502bff75</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>4fea5f82d633dcf4a8cd864b28e7e4c147742c3a</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/30UqJ92nDBB4BKaHwrtDk6LNMt9.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/8IFCWQ0fQEukqNY7LAljl7DuFZf.jpg</fanart>
<date>1997</date>
<genre>Acción. Thriller</genre>
<extra>Mis</extra>
<info>El Presidente de los EE.UU., James Marshall (Harrison Ford), regresa de un viaje oficial a Rusia a bordo del Boing 747 Presidencial, el famoso Air Force One. Un grupo terrorista ruso se apodera del avión y toma como rehenes a la familia y a los miembros del equipo presidencial.</info>
 
<page>1</page><title>AK-47 KALASHNIKOV</title>
<microhd>d22b9232f205389a5735c51f00b3d94e2e7fe167</microhd>
<fullhd>fd1f358acfaa791ce99d98bf9b3c63943e435fc7</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/sNOvlEsBPHpI3mekQ90ItaNzelu.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/dUasP5TGFk1dhTgtNhk5uLRPRx6.jpg</fanart>
<date>2020</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Película biográfica sobre el inventor y diseñador de armas pequeñas Mikhail Timofeevich Kalashnikov.</info>
 
<page>1</page><title>AKELARRE</title>
<microhd>NA</microhd>
<fullhd>38de84dc91e2d976b0ee475c14bd92321bf99d6f</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/4ZmlEn9fBR6syM5JNmiUoiYhXov.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/imgc14olSDlAlf1z3nC11cL8Zyn.jpg</fanart>
<date>2020</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>País Vasco, 1609. Los hombres de la región se han ido a la mar. Ana participa en una fiesta en el bosque con otras chicas de la aldea. El juez Rostegui, encomendado por el Rey para purificar la región, las hace arrestar y las acusa de brujería. Decide hacer lo necesario para que confiesen lo que saben sobre el akelarre, ceremonia mágica durante la cual supuestamente el Diablo inicia a sus servidoras y se aparea con ellas. </info>
 
<page>1</page><title>AL ESTE DEL EDEN</title>
<microhd>9c6908a85c757c2cbbd6b9874232dbb5ab2238fc</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>a85c2204dbbc074dc1146f6b4a2f64cd8806b267</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/3SxXS5cEqOH9QhtW6qQkldi5MRJ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ogL3cNMaj61ToePxxWtHvsXXSlr.jpg</fanart>
<date>1955</date>
<genre>Drama</genre>
<extra>Mis</extra>
<info>Un granjero californiano tiene dos hijos, Cal y Aron, de caracteres muy diferentes, pero ambos compiten por el cariño de su padre. La situación de Cal se complica cuando averigua que, en realidad, su madre no sólo no está muerta, sino que además regenta un local de alterne.</info>
 
<page>1</page><title>ALADDIN</title>
<microhd>f0885d2ac169af2187df0afa25bdef3b73eb0fa1</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/trnyoKkkvvjZvRvCMrNDtSf25nH.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/v4yVTbbl8dE1UP2dWu5CLyaXOku.jpg</fanart>
<date>2019</date>
<genre>Infantil.Fantastico. Musical. Romance. Aventuras</genre>
<extra>NA</extra>
<info>Aladdin (Mena Massoud) es un adorable pero desafortunado ladronzuelo enamorado de la hija del Sultán, la princesa Jasmine (Naomi Scott). Para intentar conquistarla, acepta el desafío de Jafar (Marwan Kenzari), que consiste en entrar a una cueva en mitad del desierto para dar con una lámpara mágica que le concederá todos sus deseos. Allí es donde Aladdín conocerá al Genio (Will Smith), dando inicio a una aventura como nunca antes había imaginado.</info>
 
<page>1</page><title>ALCARRÀS</title>
<microhd>NA</microhd>
<fullhd>61af4b9f21036e4b3f4f53354b5bfa2bf7fcc3f5</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/uQYR7MNLtLWN1WdbITOqzA28eH9.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/nA1MwlqoMr79Qdhwng9K8vtdHi1.jpg</fanart>
<date>2022</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Durante generaciones, la familia Solé, cultiva una gran extensión de melocotoneros en Alcarràs, una pequeña localidad rural de Cataluña. Pero este verano, después de ochenta años cultivando la misma tierra, puede que sea su última cosecha.</info>
 
<page>1</page><title>ALERTA ROJA</title>
<microhd>FDEEB49251A0DF07F4BE2EB3AC0CFE15E160EB33</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/tRO3xmdFO4qnDzRk2h4U6hw72Ho.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/5uVhMGsps81CN0S4U9NF0Z4tytG.jpg</fanart>
<date>2021</date>
<genre>Acción. Aventuras. Thriller. Comedia</genre>
<extra>NA</extra>
<info>Cuando la Interpol envía una "Alerta roja", significa que los departamentos de Policía de todo el mundo deben estar alerta para capturar a los criminales más buscados. Todas las alarmas saltan cuando un temerario robo une al mejor agente del FBI (Dwayne Johnson) con dos criminales rivales entre sí (Gal Gadot y Ryan Reynolds). Una coincidencia que hará que suceda lo impredecible.</info>
 
<page>1</page><title>ALGUIEN QUE CUIDE DE MI</title>
<microhd>NA</microhd>
<fullhd>81db11e2bc2e32f9893b385bfd0c10a22410d135</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/2DkjqLckutZDNeY87CRrwjvYbd8.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/vaYoUeXukptPuzLBp2EoYqSTYOp.jpg</fanart>
<date>2023</date>
<genre>Comedia. Drama</genre>
<extra>NA</extra>
<info>Nora es una joven actriz con un futuro prometedor, los dos pilares de su vida: su abuela Magüi y su madre Cecilia tuvieron grandes carreras en el mundo del espectáculo. Pero Nora descubre que su madre guarda un secreto que la ha marcado de por vida.</info>
 
<page>1</page><title>ALGUIEN VOLO SOBRE EL NIDO DEL CUCO</title>
<microhd>845f3f1bd23e919a4adb0f56347b729862f00442</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/yU7bnfzfhZoxkZRcJfTbhxfFJhg.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/hb3FLfyoFcm47l2KDyBtKP7x3eF.jpg</fanart>
<date>1975</date>
<genre>Drama</genre>
<extra>Culto</extra>
<info>Randle McMurphy (Jack Nicholson), un hombre condenado por asalto, y un espíritu libre que vive contracorriente, es recluido en un hospital psiquiátrico. La inflexible disciplina del centro acentúa su contagiosa tendencia al desorden, que acabará desencadenando una guerra entre los pacientes y el personal de la clínica con la fría y severa enfermera Ratched (Louise Fletcher) a la cabeza. La suerte de cada paciente del pabellón está en juego.</info>
 
<page>1</page><title>ALGUNOS HOMBRES BUENOS</title>
<microhd>8349c1c5da78ddd587f60a7002ca56c2db3a05a9</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>54aef8ed0b53aff19a41d78a1131c04a60eb1095</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/fPDeGYYHN3Un7xK3qrpXMSOp1t8.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/2iU9NMQZgUzco5fIzEI9wuoguWv.jpg</fanart>
<date>1992</date>
<genre>Intriga. Drama</genre>
<extra>Mis</extra>
<info>El teniente Daniel Kaffee es un joven y prometedor abogado de la Marina que tiene una excelente reputación. Sus superiores le confían la defensa de dos marines acusados de asesinato. A primera vista, el caso no parece complicado. Pero cuando tenga que vérselas con el Coronel Nathan R. Jessup, Comandante en Jefe de la base de Guantánamo, saldrán a la luz nuevas pistas que harán que el caso adquiera dimensiones insospechadas. </info>
 
<page>1</page><title>ALIEN: COVENANT</title>
<microhd>52320c4b9b656954532fd4e826e6ba1a2c5de5e0</microhd>
<fullhd>51a37e6d108cc90e07d349e9366a20ed08ba2a38</fullhd>
<tresd>NA</tresd>
<cuatrok>a9cca8b1357f4d94502be275a4e5fdb2527a3f72</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/245tgZ3mTHWs0NRiPSwPjKobKHf.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/6jajFcaY2YsfGQstJ5HaqZNVseX.jpg</fanart>
<date>2017</date>
<genre>Ciencia ficcion. Terror.Saga</genre>
<extra>NA</extra>
<info>Rumbo a un remoto planeta al otro lado de la galaxia, la tripulación de la nave colonial Covenant descubre lo que creen que es un paraíso inexplorado, pero resulta tratarse de un mundo oscuro y hostil cuyo único habitante es un "sintético" llamado David (Michael Fassbender), superviviente de la malograda expedición Prometheus. Secuela de "Prometheus" (2012), a su vez precuela de "Alien, el octavo pasajero" (1979).</info>
 
<page>1</page><title>ALIEN: EL OCTAVO PASAJERO</title>
<microhd>NA</microhd>
<fullhd>9e3af9233fde06b116b458f1b2f15764f84afc80</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/pZ9cAe5FxexJjpCaeiETbXzz3Fs.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/AmR3JG1VQVxU8TfAvljUhfSFUOx.jpg</fanart>
<date>1979</date>
<genre>Ciencia ficción. Terror.Saga</genre>
<extra>Culto</extra>
<info>De regreso a la Tierra, la nave de carga Nostromo interrumpe su viaje y despierta a sus siete tripulantes. El ordenador central, MADRE, ha detectado la misteriosa transmisión de una forma de vida desconocida, procedente de un planeta cercano aparentemente deshabitado. La nave se dirige entonces al extraño planeta para investigar el origen de la comunicación. </info>
 
<page>1</page><title>ALIEN: PROMETHEUS</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>eb2ecab8f7266256a60dda1055989736e292c976</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/haYckth3UrCVd1WdUMTl0PJCX3q.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/bgc28hV0Rxkw4RqtxzjsfY4vfqO.jpg</fanart>
<date>2012</date>
<genre>Ciencia ficción. Saga</genre>
<extra>NA</extra>
<info>Finales del siglo XXI. Un grupo de científicos y exploradores emprende un viaje espacial de más de dos años en la nave Prometheus a un remoto planeta recién descubierto, donde su capacidad física y mental será puesta a prueba. El objetivo de la misión es encontrar respuesta al mayor de los misterios: el origen de la vida en la Tierra.</info>
 
<page>1</page><title>ALIEN: RESURRECCION</title>
<microhd>NA</microhd>
<fullhd>3f0c9332211137cd5c8258936a4e7701d3104a8c</fullhd>
<tresd>NA</tresd>
<cuatrok>cdda506cbc42cc5a99f25e5041d9ccaeaca40c7c</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/8TydIuhzm6cDxuHuQNFNy7bajFh.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/4hqxQQsBJpktbFA7ERZxAeqMVYH.jpg</fanart>
<date>1997</date>
<genre>Ciencia ficción. Terror.Saga</genre>
<extra>NA</extra>
<info>Más de doscientos años después de su muerte, Ripley (Sigourney Weaver) vuelve a la vida gracias al empleo de técnicas avanzadas de clonación. Pero, durante el proceso, el ADN de Ripley se ha mezclado con el de la Reina Alien, por lo que Ripley empieza a desarrollar ciertas características de la peligrosa alienígena.</info>
 
<page>1</page><title>ALIEN³</title>
<microhd>NA</microhd>
<fullhd>57dbba63ec4d25c359a8c226b920f9d60e6c3307</fullhd>
<tresd>NA</tresd>
<cuatrok>ba0aa29c37dc9537cd6199031ae8940a37f769d1</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/pvT2FoGChCeifaMwx6Ubc8Loixp.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/337gURu2PgkPYZE4CVZFX9cMFGu.jpg</fanart>
<date>1992</date>
<genre>Ciencia ficción. Terror.Saga</genre>
<extra>NA</extra>
<info>Tras conseguir escapar con Newt y Bishop de un planeta alienígena, la teniente Ellen Ripley (Sigourney Weaver) recala accidentalmente en Fiorna 161, una remota cárcel galáctica cuyos peligrosos reclusos están absolutamente abandonados a su suerte.</info>
 
<page>1</page><title>ALIENS: EL REGRESO</title>
<microhd>NA</microhd>
<fullhd>f667415de21f7ed5d2d33d9bca147581691eeefa</fullhd>
<tresd>NA</tresd>
<cuatrok>0c0b550d1a816102dd911bf2e82894c92d0600a8</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/3QU9EP8BFLnTh6w9ycDSCvhqbRU.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/gXUdQLbo6eqaTby2cEjFQW1jyVV.jpg</fanart>
<date>1986</date>
<genre>Ciencia ficción. Acción.Saga</genre>
<extra>NA</extra>
<info>Alien es un organismo perfecto, una máquina de matar cuya superioridad física sólo puede competir con su agresividad. La oficial Ripley y la tripulación de la nave “Nostromo” se habían enfrentado, en el pasado, a esa monstruosa criatura. Y sólo Ripley sobrevivió a la masacre. Después de vagar por el espacio durante varios años, Ripley fue rescatada. Durante ese tiempo, el planeta de Alien ha sido colonizado. Pero, de repente, se pierde la comunicación con la colonia y, para investigar los motivos, se envía una expedición de marines espaciales, capitaneados por Ripley. Allí les esperan miles de espeluznantes criaturas. Alien se ha reproducido y esta vez la lucha es por la supervivencia de la Humanidad.</info>
 
<page>1</page><title>ALITA: ANGEL DE COMBATE</title>
<microhd>94cb2bd23087c79a9e2d633753bc84685e85c2ad</microhd>
<fullhd>NA</fullhd>
<tresd>537ec4e156f638cc77a7e70e303247dfa5c7c3f6</tresd>
<cuatrok>66bf4a272f216bcc768d6533dcd3a22fff74221c</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/quWP5VIzTUf0Hr8AJZLloM6js8I.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/aQXTw3wIWuFMy0beXRiZ1xVKtcf.jpg</fanart>
<date>2019</date>
<genre>Ciencia ficcion. Acción. Romance. Thriller </genre>
<extra>NA</extra>
<info>Cuando Alita (Rosa Salazar) se despierta sin recordar quién es en un mundo futuro que no reconoce, Ido (Christoph Waltz), un médico compasivo, se da cuenta de que en algún lugar de ese caparazón de cyborg abandonado, está el corazón y alma de una mujer joven con un pasado extraordinario. Mientras Alita toma las riendas de su nueva vida y aprende a adaptarse a las peligrosas calles de Iron City, Ido tratará de protegerla de su propio pasado, mientras que su nuevo amigo Hugo (Keean Johnson) se ofrecerá, en cambio, a ayudarla a desenterrar sus recuerdos. Cuando las fuerzas mortales y corruptas que manejan la ciudad comienzan a perseguir a Alita, ella descubre una pista crucial sobre su pasado: posee habilidades de combate únicas que los que ostentan el poder querrán controlar a toda costa. Sólo manteniéndose fuera de su alcance, podrá salvar a sus amigos, a su familia y el mundo que ha aprendido a amar. Remake del clásico anime de 1993 "Alita, Ángel del Combate".</info>
 
<page>1</page><title>ALMAS DE METAL</title>
<microhd>d57c2766b9f7c9ab062870b1f527bf1a5f3230b9</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/wSk1lLjY479xtTC9GXCBb5ljZb.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/1smZ8NV0jMaJuobzMwXE8sG3ZSz.jpg</fanart>
<date>1973</date>
<genre>Ciencia ficción. Thriller. Acción. Western</genre>
<extra>Culto</extra>
<info>En un futuro próximo existe un gigantesco parque de vacaciones dividido en tres zonas: la Roma Imperial, el Oeste americano y la Europa Medieval. Cada una de ellas reproduce con total fidelidad las características de cada época. Existe, además, la posibilidad de que quien pueda permitírse el lujo viva sus propias aventuras en la época elegida. Al parque han ido de vacaciones un par de amigos, Peter Martin (Richard Benjamin) y John Blane (James Brolin). Todo está perfectamente controlado hasta que los robots que hacen de figurantes empiezan a fallar, en concreto un implacable pistolero (Yul Brynner) diseñado para perseguir sin descanso a sus víctimas. </info>
 
<page>1</page><title>ALMAS EN PENA DE INISHERIN</title>
<microhd>NA</microhd>
<fullhd>36b6ea6116663f3b33f0da6a63cc6fac51a27111</fullhd>
<tresd>NA</tresd>
<cuatrok>e2e95d6ac86bab43b3759c195e691de16635d963</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ucuuM51Kw3IoTXRUQLnKAz9jhip.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/1vXD5HXqkhvsXFHE7KmCPZGPR1e.jpg</fanart>
<date>2023</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Ambientada en una isla remota frente a la costa oeste de Irlanda en 1923, narra la historia de dos amigos de toda la vida, Pádraic y Colm, quienes se encuentran en un callejón sin salida cuando Colm pone fin a su amistad de un modo abrupto. Un Pádraic atónito, con la única comprensión de su hermana Siobhán y del simple Dominic, se esfuerza por reconstruir la relación, negándose a aceptar las negativas de su amigo de siempre. Cuando Colm le plantea a Pádraic un ultimátum desesperado, los acontecimientos se precipitan y provocan consecuencias traumáticas.</info>
 
<page>2</page><title>ALPHA</title>
<microhd>c63449f72826a339eaa4d90d640aad27d3075f76</microhd>
<fullhd>b6379c147680ebefc8e72caba4441b9f7719b413</fullhd>
<tresd>3e5a3386649fc9ffddc719437ee13e406964ca19</tresd>
<cuatrok>e587b5212aa0c9adfbbd055a93f7a5e9a58b7f15</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ujWmzBgVZLyTiEbFPgA50qSJVGl.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/hBkPmNA04BYggZQqzzvVEBVnj9m.jpg</fanart>
<date>2018</date>
<genre>Aventuras</genre>
<extra>NA</extra>
<info>Una aventura épica y una historia de supervivencia ambientada en la Europa de hace 20.000 años, durante la última glaciación. En mitad de su primera cacería con el grupo de élite de su tribu, un joven es herido y dado por muerto. Al despertar se encontrará débil y solo, y deberá aprender a sobrevivir y abrirse camino ante la dura y cruel naturaleza. Acompañado a regañadientes de un lobo abandonado por su manada, los dos aprenderán a confiar el uno en el otro, convertirse en aliados y superar los innumerables peligros para intentar encontrar el camino a casa antes de que llegue el letal invierno.</info>
 
<page>2</page><title>AMBULANCE: PLAN DE HUIDA</title>
<microhd>66cbf1e3dd6c0a147c0b186c38eeac8015b615bc</microhd>
<fullhd>dea3317eb1275a27a56e7b3eefea2a6f601f8db6</fullhd>
<tresd>NA</tresd>
<cuatrok>6415296542ebb5ebf0320207b0213dd5b34d86a4</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/hUbgg3mMSbY9PlpTxBo4IFUVSd6.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/192vHmAPbk5esL34XMKZ1YLGFjr.jpg</fanart>
<date>2022</date>
<genre>Acción. Thriller. Drama</genre>
<extra>NA</extra>
<info>El veterano Will Sharp, en un acto desesperado por conseguir dinero para cubrir las deudas médicas de su esposa, recurre a la única persona que sabe que no debería, su hermano adoptivo Danny. Danny le ofrece participar en un robo a un banco, el más grande en la historia de la ciudad. Will no puede decir que no. Cuando su intento de escape sale mal, los hermanos secuestran una ambulancia con un policía herido y una paramédica. Ahora deberán huir de un inmenso dispositivo de fuerzas de seguridad desplegado por toda la ciudad, mantener a sus rehenes con vida y de alguna forma tratar de no matarse entre ellos.</info>
 
<page>2</page><title>AMELIE</title>
<microhd>c2258e4ca4e323113fcb8b0bb7fca12744f0caed</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/mYvUib00miJWxWkiXhy3QnrCj96.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/4JMHILK0kcqv2mkAkC1nmdMeiuS.jpg</fanart>
<date>2001</date>
<genre>Comedia. Drama</genre>
<extra>Culto</extra>
<info>Amelie no es una chica como las demás. Ha visto a su pez de colores deslizarse hacia las alcantarillas municipales, a su madre morir en la plaza de Nôtre-Dame y a su padre dedicar todo su afecto a un gnomo de jardín. De repente, a los veintidós años, descubre su objetivo en la vida: arreglar la vida de los demás. A partir de entonces, inventa toda clase de estrategias para intervenir en los asuntos de los demás: su portera, que se pasa los días bebiendo vino de Oporto; Georgette, una estanquera hipocondríaca, o "el hombre de cristal", un vecino que sólo ve el mundo a través de la reproducción de un cuadro de Renoir. </info>
 
<page>2</page><title>AMERICAN ASSASSIN</title>
<microhd>91ebd876f28cdd6eca8a0034bf33b1532ba2ba4a</microhd>
<fullhd>b6799c222673202760c979f61b8402b8dab89cba</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/1UFz4TrbVdwBaJTz0rZn3PFI5kp.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/puKZWmBIpuEMwGCn2hZkublG1rO.jpg</fanart>
<date>2017</date>
<genre>Thriller. Accion</genre>
<extra>NA</extra>
<info>Adaptación cinematográfica de la novela homónima de Vince Flynnm, cuya trama presenta a Mitch Rapp (Dylan O'Brien), un estudiante ejemplar que, tras la terrible muerte de su novia en una playa a manos de unos terorristas, decide cambiar el rumbo de su vida y dedicarse a la caza de esta clase de delincuentes. Un año más tarde, entra en las filas de la CIA, institución en la que estará a las órdenes de su mentor, Stan Hurley (Michael Keaton).</info>
 
<page>2</page><title>AMERICAN BEAUTY</title>
<microhd>35755c97715ab9d820689400335118955e0c5be4</microhd>
<fullhd>ae4cf8b437ca643711bbe8234978b22f8eb5243a</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/6MNwC101wSEsDyxTJBGXsgIMlmh.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/sARGXcSdC1nbjegY6aGk0eujfRP.jpg</fanart>
<date>1999</date>
<genre>Drama. Comedia</genre>
<extra>Culto</extra>
<info>Lester Burnham (Kevin Spacey), un cuarentón en crisis, cansado de su trabajo y de su mujer Carolyn (Annette Bening), despierta de su letargo cuando conoce a la atractiva amiga (Mena Suvari) de su hija (Thora Birch), a la que intentará impresionar a toda costa. </info>
 
<page>2</page><title>AMERICAN GANGSTER</title>
<microhd>NA</microhd>
<fullhd>3514688c10ff03eed70227862c58c133187df3db</fullhd>
<tresd>NA</tresd>
<cuatrok>657e57a0e46b5da33e9e8b964431e9d463440ac8</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/g8kCZy9In4Ujg8kmNP6IKxTiQho.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/uPSd1R7eGp0QV8CD6V6S3MYKyRC.jpg</fanart>
<date>2007</date>
<genre>Thriller. Drama</genre>
<extra>Mis</extra>
<info>Nueva York, 1968. Frank Lucas (Denzel Washington) es el taciturno chófer de un importante mafioso negro de Harlem. Cuando su jefe muere inesperadamente, Frank aprovecha la oportunidad para construir su propio imperio. Gracias a su talento, se convierte no sólo en el principal narcotraficante de la ciudad, inundando las calles con productos de mejor calidad y precio, sino también en un hombre público muy respetado. Richie Roberts (Russell Crowe), un policía incorruptible marginado por su honradez que conoce bien las calles, se da cuenta de que una persona ajena a los clanes trepa por la escalera del poder. Tanto Roberts como Lucas comparten un estricto código ético que les aparta de los demás y los convierte en dos figuras solitarias en lados opuestos de la ley. Cuando se encuentren, el enfrentamiento entre ellos será inevitable.</info>
 
<page>2</page><title>AMERICAN HISTORY X</title>
<microhd>65499e39262d36801201a485e5fac68834c56ff3</microhd>
<fullhd>f640bd7b252712684d9a964e21dbfa5c992ae8a3</fullhd>
<tresd>NA</tresd>
<cuatrok>de1ab440de9b8c4dee116fa8169e28b1ebd33ec5</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/jQIrsqZlEMY3r9VneqGjYjGOFYT.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/o4Thn9ooTZAZOYrlbWgVB20weSf.jpg</fanart>
<date>1998</date>
<genre>Drama </genre>
<extra>Culto</extra>
<info>Derek (Edward Norton), un joven "skin head" californiano de ideología neonazi, es encarcelado por asesinar a un negro que pretendía robarle su furgoneta. Cuando sale de prisión y regresa a su barrio dispuesto a alejarse del mundo de la violencia, se encuentra con que su hermano pequeño (Edward Furlong), para quien Derek es el modelo a seguir, sigue el mismo camino que a él lo condujo a la cárcel.</info>
 
<page>2</page><title>AMERICAN PSYCHO</title>
<microhd>cefc62207105d1cbce56b0824b4709419fea83e5</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/fKxg5lEQ64QdwINhYllI26vlyr2.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/xMnGZBGeR12qYjhOmg5WeWkUxdU.jpg</fanart>
<date>2000</date>
<genre>Drama. Terror</genre>
<extra>Culto</extra>
<info>En un mundo moralmente plano en el que la ropa tiene más sentido que la piel, Patrick Bateman es un espécimen soberbiamente elaborado que cumple todos los requisitos de Master del Universo, desde el diseño de su vestuario hasta el de sus productos químicos. Es prácticamente perfecto, como casi todos en su mundo e intenta desesperadamente encajar en él. Cuando más intenta ser como cualquier otro hombre adinerado de Wall Street, más anónimo se vuelve y menos control tiene sobre sus terribles instintos y su insaciable sed de sangre, que lo arrastra a una vorágine en la que los objetos valen más que el cuerpo y el alma de una persona.</info>
 
<page>2</page><title>AMITYVILLE EL DESPERTAR</title>
<microhd>df29c2f6ae7743adc7d051b9c666df992b54bdaf</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/yPDde99dGGaTmSfZCSpTNSoSQQT.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/lA8r57ye1Kg30RO8PesaajzAWAM.jpg</fanart>
<date>2017</date>
<genre>Terror. Thriller</genre>
<extra>NA</extra>
<info>Belle y su familia se mudan a una nueva casa, con el fin de ahorrar dinero para ayudar a pagar el costoso tratamiento de su hermano gemelo que se encuentra en coma. Pero cuando fenómenos extraños empiezan a ocurrir en la casa, incluyendo la recuperación milagrosa de su hermano y pesadillas cada vez más espeluznantes, Belle comienza a sospechar que su madre no le está contando toda la verdad y pronto se da cuenta de que acaban de mudarse a la terrorífica casa de Amityville.</info>
 
<page>2</page><title>AMOR DE MADRE</title>
<microhd>1597d02aa3adf25868f95acdffb1fc8f6cc9f48b</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/mJnldu7WjrGdZnFZMacaoguKX48.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/abbFJvlNGPIIywT3VsktG3Rzgia.jpg</fanart>
<date>2022</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>A José Luis (Quim Gutiérrez) acaban de dejarlo plantado en el altar, y por si esto fuera poco, Mari Carmen (Carmen Machi), su madre, se ha empeñado en acompañarlo a la luna de miel con la excusa de no perder el dinero. Cada minuto que pasan en Mauricio, José Luis se siente más infeliz y fracasado, mientras que Mari Carmen se lo pasa como nunca, viviendo todas las experiencias que siempre había deseado y revelándose como la maravillosa mujer que es en realidad, y que su familia no ve.</info>
 
<page>2</page><title>AMSTERDAM</title>
<microhd>NA</microhd>
<fullhd>871ca1333f4a3d34d2160ec2c5a9c09652ad0db2</fullhd>
<tresd>NA</tresd>
<cuatrok>2b5bc48299704e2e8ba81c305cf2c745031ebbfb</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/2SnvKZ47BRq56O4qIXqaGXSGohD.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/79PcXPpbDWql74h8Y00mNwbYMbS.jpg</fanart>
<date>2022</date>
<genre>Drama. Romance. Comedia. Intriga</genre>
<extra>NA</extra>
<info>Epopeya romántica sobre tres amigos que se ven envueltos en una de las tramas secretas más impactantes de la historia de Estados Unidos. Basada en hechos que se mezclan con ficción.</info>
 
<page>2</page><title>AMUNDSED</title>
<microhd>f23d23324004b6569eb4bf22e8ea1ecd2c10b7a1</microhd>
<fullhd>f963cd081eaada0d73299c440900ebf527bb513f</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/k8xu1BZywZMktdewQK5N8scLvoT.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/36u1hw0twlAGpl1K5EHNVrS44Ff.jpg</fanart>
<date>2019</date>
<genre>Aventuras. Drama</genre>
<extra>NA</extra>
<info>Narra la vida del legendario explorador noruego Roald Amundsen (1872-1928), un hombre que estuvo obsesionado con conquistar las zonas polares. Dirigió la expedición a la Antártida que por primera vez alcanzó el Polo Sur.</info>
 
<page>2</page><title>ANIQUILACION</title>
<microhd>b18415e58e2423430a499e2112de6fadfb76d868</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/zTiKrS1e38XKwiisZpyjNJb7CnI.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/5zfVNTrkhMu673zma6qhFzG01ig.jpg</fanart>
<date>2018</date>
<genre>Ciencia ficcion. Thriller</genre>
<extra>NA</extra>
<info>Cuando su marido desaparece durante una misión secreta para regresar sin recordar nada, la bióloga Lena se une a una expedición a una misteriosa región acordonada por el gobierno de los Estados Unidos. El grupo, compuesto por 5 mujeres científicas, investiga la zona X, un intrigante lugar controlado por una poderosa fuerza alienígena. La zona X es un lugar al que han ido otras expediciones, pero del que ninguna ha vuelto.</info>
 
<page>2</page><title>ANNA</title>
<microhd>ef65d29623cd5db15eb01a920e19118f166b5183</microhd>
<fullhd>a8159a5c9238311614f826a41bd656ab788f0ef6</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/eLBq1ANzAHgVfQSK6urU0HaTGPS.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/pIL9e5cN6RNugDCCLwn3stfb9HE.jpg</fanart>
<date>2019</date>
<genre>Accion. Thriller</genre>
<extra>NA</extra>
<info>Bajo la hipnotizante belleza de Anna Poliatova (Sasha Luss) se esconde un secreto que la lleva a poder desatar una imparable agilidad y fuerza, convirtiéndose así en una de las asesinas a sueldo más temidas por los gobiernos de todo el planeta.</info>
 
<page>2</page><title>ANTEBELLUM</title>
<microhd>ed89eb38fdb7f8c7c3aef16a89cfa295cf7cc6e3</microhd>
<fullhd>037f2b80c0942c72e7b83483a3e25fac0a14eea5</fullhd>
<tresd>NA</tresd>
<cuatrok>262536ffdfd4fa60ca892a6cdc35999ea646c35b</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/kxekf7Ufxhubi00h02oWq6gbkWX.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/3aefwvGlKL9Z6e7boxk40VF7Ggs.jpg</fanart>
<date>2020</date>
<genre>Intriga. Thriller. Terror</genre>
<extra>NA</extra>
<info>Verónica es una escritora de éxito que queda atrapada en una terrorífica realidad cuyo misterio debe resolver antes de que sea demasiado tarde.</info>
 
<page>2</page><title>APOCALYPSE NOW</title>
<microhd>0a502e6629a9b35e94b68007571b19bbdfceac7a</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>c95752e434842113f7cd5d601951a19218505df9</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/gQB8Y5RCMkv2zwzFHbUJX3kAhvA.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/9Qs9oyn4iE8QtQjGZ0Hp2WyYNXT.jpg</fanart>
<date>1979</date>
<genre>Bélico. Drama</genre>
<extra>Culto</extra>
<info>Durante la guerra de Vietnam, al joven Capitán Willard, un oficial de los servicios de inteligencia del ejército estadounidense, se le ha encomendado entrar en Camboya con la peligrosa misión de eliminar a Kurtz, un coronel renegado que se ha vuelto loco. El capitán deberá ir navegar por el río hasta el corazón de la selva, donde parece ser que Kurtz reina como un buda despótico sobre los miembros de la tribu Montagnard, que le adoran como a un dios.</info>
 
<page>2</page><title>APOLO 13</title>
<microhd>e9a634c226bbd95a9941cad6ceb7fde1562251b9</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/xyHoI6hEd6uO8BpZwgQ9z5w2w.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/32zua2oKjn2EaRk7am3qK8zAlEj.jpg</fanart>
<date>1995</date>
<genre>Drama. Aventuras</genre>
<extra>Mis</extra>
<info>El Apolo XIII inicia su viaje a la Luna en abril de 1970. Cuando está a punto de llegar a su destino, se produce una explosión en el espacio que les hace perder oxígeno y, además, cambia el rumbo de la nave. La situación de los tripulantes se hace desesperada cuando el oxígeno empieza a agotarse. Mientras tanto, el mundo entero vive pendiente del desenlace de tan angustiosa aventura.</info>
 
<page>2</page><title>AQUAMAN</title>
<microhd>690654b00393c5f6cc13fbcbb5ddcd7b45662e2c</microhd>
<fullhd>a2112625109efe99fff4fbac527a961ef7e856cf</fullhd>
<tresd>c8752cc57097adcf4f94bea0e6e7a768cce5c7d6</tresd>
<cuatrok>8da939c32df2a5861237690581dd3dbfd739ca8e</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/z5gqW3fffzQ16Jv5vDScpypq1FA.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/9QusGjxcYvfPD1THg6oW3RLeNn7.jpg</fanart>
<date>2018</date>
<genre>Fantastico. Accion</genre>
<extra>NA</extra>
<info>uando Arthur Curry (Jason Momoa) descubre que es mitad humano y mitad atlante, emprenderá el viaje de su vida en esta aventura que no sólo le obligará a enfrentarse a quién es en realidad, sino también a descubrir si es digno de cumplir con su destino: ser rey, y convertirse en Aquaman.</info>
 
<page>2</page><title>AQUELLOS QUE DESEAN MI MUERTE</title>
<microhd>3a8d5ff001d2a05ee867ae846d04babb411ec764</microhd>
<fullhd>b2fb013bc18fa019a1b61cdfba2f85c1f9b44122</fullhd>
<tresd>NA</tresd>
<cuatrok>805533f93a367ec1aadf4b08ddd3125af29fe508</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/spiKpaVilWKxLDfjIuBkKhpaRkR.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/ouOojiypBE6CD1aqcHPVq7cJf2R.jpg</fanart>
<date>2021</date>
<genre>Thriller </genre>
<extra>NA</extra>
<info>Connor (Finn Little), un niño testigo de un asesinato, es perseguido por dos asesinos a través de las tierras salvajes de Montana. Aunque cuenta con Hannah (Angelina Jolie), una bombero y experta en supervivencia, para evitar que los secuaces le den caza, un peligroso incendio cercano amenaza con acabar con la vida de todos los implicados.</info>
 
<page>2</page><title>ARCHENEMY</title>
<microhd>8f28b93dff8b480cc09d6e5a0977c6b93e4673c7</microhd>
<fullhd>4204a655c9b05e4589629d59da6edddd2eeb64e6</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/4H54bQrCM6NBm9sqw911scGd4hb.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/9j7VovzODcb1xQ1kKm68i8XC2bj.jpg</fanart>
<date>2020</date>
<genre>Acción. Aventuras</genre>
<extra>NA</extra>
<info>Max Fist asegura ser un héroe de otra dimensión que cayó por accidente en la Tierra, donde no puede emplear sus poderes, a través de una brecha en el espacio-tiempo. Sólo un adolescente local a quien todos conocen por el sobrenombre de "Hamster" cree en él. </info>
 
<page>2</page><title>ARCHIVE</title>
<microhd>eef113b30cd263f9b15dcb1cbdf6af4c9840e1d7</microhd>
<fullhd>4572775bd4688dd35f80b0ce56cbf9a5a1432397</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/zNLVWauiqW3I7nJ5lRE1GLDjlHw.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/u9YEh2xVAPVTKoaMNlB5tH6pXkm.jpg</fanart>
<date>2020</date>
<genre>Ciencia ficción</genre>
<extra>NA</extra>
<info>Año 2038: George Almore trabaja en una inteligencia artificial que parezca verdaderamente humana. Su último prototipo ya está en su fase más delicada y arriesgada, en especial, porque el objetivo de Almore debe permanecer oculto a cualquier precio: reunirse con su mujer fallecida.</info>
 
<page>2</page><title>ARKANSAS</title>
<microhd>0f5ff26ba269a0279fb9d401c62334142865189d</microhd>
<fullhd>4ed96353defaefbb3315fd0804c40e59e5ee67e2</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ieNGttYsxunAANK9LRZa4LdkEqi.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/1Q3cxQmZcU7lqwWlruKL2yCC5j2.jpg</fanart>
<date>2020</date>
<genre>Thriller </genre>
<extra>NA</extra>
<info>Un par de jóvenes camellos de poca monta trabajan para un capo de la droga de Arkansas al que nunca han visto. Un día una de sus operaciones sale terriblemente mal, con terribles consecuencias.</info>
 
<page>2</page><title>ARMA LETAL</title>
<microhd>0b8b81b920a188da81b9d9c74917caaed550f233</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>ebf2e725994f73ae72b7b3dd9d1108a07119437b</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/bOmKVohi6yjxBnoP7XvX6tpXR56.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/guTNnSWS3CaH71jasY8W1FMptjG.jpg</fanart>
<date>1987</date>
<genre>Acción. Comedia.Saga</genre>
<extra>Culto</extra>
<info>Martin Riggs es un policía de Los Ángeles con tendencias suicidas. Su compañero Roger Murtaugh es un veterano y responsable policía y padre de familia. Juntos intentan frustrar una operación de contrabando de droga. Al mismo tiempo, a pesar de sus diferentes caracteres, se harán buenos amigos.</info>
 
<page>2</page><title>ARMA LETAL 2</title>
<microhd>16247a3313c070fbc29b643ee97be11a3e74699a</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>https://bt4g.org/magnet/8adbbd7207ac07e884576ba4eb5073e8519f8275</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/aSEqIMal3ZjFgYi9OE9y8zucJfp.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/zMyTdJZBk7FSFG9xDhIWLceo1xe.jpg</fanart>
<date>1989</date>
<genre>Acción . Saga</genre>
<extra>NA</extra>
<info>La pareja de policías formada por Martin Riggs (Mel Gibson) y su compañero Roger Murtaugh (Danny Glover) se enfrenta ahora a una poderosa organización surafricana de traficantes de droga que opera en Estados Unidos. Su misión principal es la de proteger al testigo protegido Leo Getz (Joe Pesci), un alocado contable que ha blanqueado dinero para la banda y que en unos días va a testificar contra ellos. </info>
 
<page>2</page><title>ARMA LETAL 3</title>
<microhd>fea4606cf131a6b36592e60bafb0d7c80dcd7815</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>da54fd9dbafc0e7c836e9b735b33e934481e1544</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/6AYVXfpC4tE7pYNpa6Zf6wKu6f2.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/mpStVqLPiVndPdERAo91rB66As9.jpg</fanart>
<date>1992</date>
<genre>Acción. Saga</genre>
<extra>NA</extra>
<info>Martin Riggs y Roger Murtaugh, la incombustible pareja de policías de Los Ángeles, se enfrentan a un difícil caso: armas confiscadas y a punto de ser destruidas han desaparecido de las dependencias policiales y están llegando a manos de los delincuentes más peligrosos de la ciudad</info>
 
<page>2</page><title>ARMA LETAL 4</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>b32e8ddc7ca31cbeab329d788613df05fd491347</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/o1oLMCHkxrqXHGNCEgUosnwlLuV.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/q4K2Gl5IXe7uG2ritExXf9zcg8K.jpg</fanart>
<date>1998</date>
<genre>Acción. Comedia. Saga</genre>
<extra>NA</extra>
<info>La pareja de policías formada por Martin Riggs y Roger Murtaugh vuelve a la carga para enfrentarse a un capo de la mafia asiática que ha extendido sus actividades a Los Ángeles. En la investigación les ayudará un joven policía responsable de los quebraderos de cabeza familiares de Murtaugh, ya que ha dejado embarazada a su hija. En casa de Riggs, las cosas también se complican cuando su compañera se queda embarazada y amenaza con hacerlo pasar por el altar. </info>
 
<page>2</page><title>ARMAGEDDON</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>72f6fa36e08aa5bcf5f36b1ce471103cd6c74dfc</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/yYtlk1E28L1kQG6VcazkHhv29Ur.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/kh3KoeyQpzqScweErLqEmBjNDLr.jpg</fanart>
<date>1998</date>
<genre>Ciencia ficción. Fantástico. Drama. Acción. Romance</genre>
<extra>Mis</extra>
<info>Un asteroide del tamaño del estado de Tejas apunta directamente hacia la Tierra. Los expertos de la NASA tan sólo encuentran una posible solución: enviar a un equipo de astronautas al espacio para que destruya el meteorito antes de que colisione con nuestro planeta. Para ello recurren a Harry S. Stamper, el mayor experto en perforaciones petrolíferas, y a su cualificado equipo de perforadores, para que aterricen en la superficie del asteroide, lo perforen e introduzcan un dispositivo nuclear que al estallar consiga desviar su trayectoria y poder salvar el planeta, evitando así el Armageddon. </info>
 
<page>2</page><title>ARTEMIS FOWL</title>
<microhd>0fe32f6aa824adea478e940e840c95c4844fd7ae</microhd>
<fullhd>79db7e63699a953256a755a817534089fb63bc69</fullhd>
<tresd>NA</tresd>
<cuatrok>fb230efe9b0979b56f5858978965602fca333191</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/w58ohFF2YeEEVYRWiiMjJZ1hk9R.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/3CIae0GrhKTIzNS3FYYvT8P9D3w.jpg</fanart>
<date>2020</date>
<genre>Fantástico. Aventuras.Infantil</genre>
<extra>NA</extra>
<info>Artemis Fowl, un joven criminal irlandés, rapta a Holly Short, un hada, con la intención de pedir un rescate que le permita salvar a su padre. Una vez logra cumplir sus intenciones, se embarca en una aventura que le llevará a luchar contra el maléfico hada Opal Koboi. Adaptación del libro de Eoin Colfer.</info>
 
<page>2</page><title>ARTICO</title>
<microhd>2e913269428e406d92f386eabc65ced5f8a71659</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/hPuhXR5rYiHf5soWL8jZSw6AqGr.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/s75CDPm61dDM7C5STW884J6uFk1.jpg</fanart>
<date>2018</date>
<genre>Aventuras. Drama</genre>
<extra>NA</extra>
<info>En el Ártico, la temperatura puede bajar hasta -70° C. En este desierto helado y hostil alejado de todo, un hombre lucha por sobrevivir. A su alrededor, la inmensidad blanca, y los restos de un avión que le ha servido de refugio, vestigio de un accidente ya muy lejano. Con el tiempo, ha aprendido a luchar contra el frío y las tormentas, a cuidarse de los osos polares y a buscar comida. </info>
 
<page>2</page><title>AS BESTAS</title>
<microhd>NA</microhd>
<fullhd>af8fe7b293fff9b4608a6498e2b83a1d605dcc6d</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/dV9kPhRJWsQI38IpjoC4AdAsONt.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/yhL6JbgpeId1N8wmJz3t3fHnM1W.jpg</fanart>
<date>2022</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Antoine y Olga son una pareja francesa que se instaló hace tiempo en una aldea del interior de Galicia. Allí llevan una vida tranquila, aunque su convivencia con los lugareños no es tan idílica como desearían. Un conflicto con sus vecinos, los hermanos Anta, hará que la tensión crezca en la aldea hasta alcanzar un punto de no retorno.</info>
 
<page>2</page><title>ASESINATO EN EL ORIENT EXPRESS</title>
<microhd>7b9e90aae1948c85e38b4be2d04b991052ce4f8c</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/v5F94xCSBrSDorvOQNHU4sqAcqZ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/2J283YNxKhxAqHeVegUJ5mzLfGb.jpg</fanart>
<date>2017</date>
<genre>Intriga. Drama</genre>
<extra>NA</extra>
<info>Durante un viaje en el legendario tren Orient Express, el detective belga Hercules Poirot investiga un asesinato cometido en el trayecto, y a resultas del cual todos los pasajeros del tren son sospechosos del mismo.</info>
 
<page>2</page><title>ASTERIX EL SECRETO DE LA POCION MAGICA</title>
<microhd>b785cd0d5b7b9780d7dbdcc3deca0ea79fb6b65d</microhd>
<fullhd>NA</fullhd>
<tresd>af5b628c60c1f016a90307bd8a3cb39e67f1f192</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/aXop79u2iLVqz1GuSAxgtRUcJHZ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/9cPMIuB2tv4IP0LbeNhrK5OyDy5.jpg</fanart>
<date>2018</date>
<genre>Infantil. Aventuras. Comedia</genre>
<extra>NA</extra>
<info>Tras sufrir una caída recogiendo muérdago, Panoramix decide que ha llegado la hora de asegurar el futuro del pueblo. Acompañado por Asterix y Obelix, emprende la búsqueda de un talentoso druida al que quiere transmitirle el secreto de la poción mágica.</info>
 
<page>2</page><title>ATENEA</title>
<microhd>09bc21c200a5e426d5299968667effc4618d97ff</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/fpmQbL4oCta0BiIgLDasJDbQdrY.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/55jDl8vtuB8dJBH8L2tdNMUcziH.jpg</fanart>
<date>2022</date>
<genre>Drama. Thriller. Acción</genre>
<extra>NA</extra>
<info>Horas después de la trágica muerte de su hermano menor en circunstancias inexplicables, la vida de tres hermanos se ve sumida en el caos.</info>
 
<page>2</page><title>ATOMICA</title>
<microhd>NA</microhd>
<fullhd>26229e312d6167465af5560f1059e51f60aab8a9</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/9DRFozlq1IMD7aMLzdRzWEEH6SH.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/9fhv5HaZs2pDBPc1y9ATNBqCbwo.jpg</fanart>
<date>2017</date>
<genre>Accion. Thriller</genre>
<extra>NA</extra>
<info>Año 1989, el muro de Berlín está a punto de caer. Un agente del MI6 encubierto aparece muerto y la espía Lorraine Broughton (Charlize Theron) debe encontrar por todos los medios una lista que el agente estaba intentando hacer llegar a Occidente, y en la que figuran los nombres de todos los agentes encubiertos que trabajan en Berlín oriental. Lorraine no se detendrá ante nada para conseguir dar con esa lista, enfrentándose a varios asesinos y sumergiéndose en un mundo en el que nadie parece ser quien dice ser.</info>
 
<page>2</page><title>AVA</title>
<microhd>3de36fb7587edd8bb73d83779de6b195dd2acc0f</microhd>
<fullhd>cebe87b9c94d222f9a1b5d09ee81c4e1bb5afd77</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/b9oHpPdcntkIP3zSp3Zc7o5a5wf.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/fXRQl0oN1jR0pdCqIvUqKDb2ss2.jpg</fanart>
<date>2020</date>
<genre>Acción. Drama </genre>
<extra>NA</extra>
<info>Ava (Chastain) es una asesina que trabaja para una organización de operaciones secretas. Cuando un peligroso encargo sale mal, Ava se verá obligada a luchar por su propia supervivencia.</info>
 
<page>2</page><title>AVATAR</title>
<microhd>62aa088ad00f3c4b12a1305ea006cf2af478b112</microhd>
<fullhd>a660cbaa636990357759b5b12c7103f98623c50c</fullhd>
<tresd>27824dd0364c8d6dd3f77a0930ba9368e10cffea</tresd>
<cuatrok>9b1d1776a52c2a799a7be19172ab9060d99fd00a</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/7paC3UJ0P2P3DeykgQhUD1hnQJO.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ruziOM4OlILvyrOdChvvFqy4Ggw.jpg</fanart>
<date>2009</date>
<genre>Ciencia ficción. Aventuras. Bélico. Acción. Fantástico. Romance </genre>
<extra>Mis</extra>
<info>Año 2154. Jake Sully (Sam Worthington), un ex-marine condenado a vivir en una silla de ruedas, sigue siendo, a pesar de ello, un auténtico guerrero. Precisamente por ello ha sido designado para ir a Pandora, donde algunas empresas están extrayendo un mineral extraño que podría resolver la crisis energética de la Tierra. Para contrarrestar la toxicidad de la atmósfera de Pandora, se ha creado el programa Avatar, gracias al cual los seres humanos mantienen sus conciencias unidas a un avatar: un cuerpo biológico controlado de forma remota que puede sobrevivir en el aire letal. Esos cuerpos han sido creados con ADN humano, mezclado con ADN de los nativos de Pandora, los Na'vi. Convertido en avatar, Jake puede caminar otra vez. Su misión consiste en infiltrarse entre los Na'vi, que se han convertido en el mayor obstáculo para la extracción del mineral. Pero cuando Neytiri, una bella Na'vi (Zoe Saldana), salva la vida de Jake, todo cambia: Jake, tras superar ciertas pruebas, es admitido en su clan. Mientras tanto, los hombres esperan los resultados de la misión de Jake. </info>
 
<page>2</page><title>AVATAR: EL SENTIDO DEL AGUA</title>
<microhd>NA</microhd>
<fullhd>3975b0d3a36fd6ce553bc4e5d8199eb7045dcdfc</fullhd>
<tresd>NA</tresd>
<cuatrok>9c9f89ea0a80f4979f9e2f54bf704de4713bc592</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/oIVSTKNyKvjm4NmmJi4zjQqHJ02.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/zaapQ1zjKe2BGhhowh5pM251Gpl.jpg</fanart>
<date>2022</date>
<genre>Ciencia ficción. Aventuras. Fantástico. Acción </genre>
<extra>NA</extra>
<info>Más de una década después de los acontecimientos de 'Avatar', los Na'vi Jake Sully, Neytiri y sus hijos viven en paz en los bosques de Pandora hasta que regresan los hombres del cielo. Entonces comienzan los problemas que persiguen sin descanso a la familia Sully, que decide hacer un gran sacrificio para mantener a su pueblo a salvo y seguir ellos con vida.</info>
 
<page>2</page><title>AVES DE PRESA</title>
<microhd>9ae5265f9334f6fd8f29de98050fef3d60a3d5ce</microhd>
<fullhd>cef7aec6f9d08b57200199ef5ad56a5c213d49b6</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/4GCqF17I2ST5M8iSkwh1jwqvi7F.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/uozb2VeD87YmhoUP1RrGWfzuCrr.jpg</fanart>
<date>2020</date>
<genre>Accion. Aventuras </genre>
<extra>NA</extra>
<info>Después de separarse de Joker, Harley Quinn y otras tres heroínas (Canario Negro, Cazadora y Renée Montoya) unen sus fuerzas para salvar a una niña (Cassandra Cain) del malvado rey del crimen Máscara Negra.</info>
 
<page>2</page><title>AWARENESS: LA REALIDAD ES UNA ILUSION</title>
<microhd>NA</microhd>
<fullhd>e6f36f8838ffd42c4857a2714a7c9ba7119eb4c6</fullhd>
<tresd>NA</tresd>
<cuatrok>36a8d1ad738912be32f97d39582cb67ca5e8f8d1</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/gPSnanVAnKgycgc3SJGcutXCe9A.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/hFpEKj9BcWYDlEAz3Td2J5htknf.jpg</fanart>
<date>2023</date>
<genre>Ciencia ficción. Acción. Thriller</genre>
<extra>Estreno</extra>
<info>Ian es un adolescente rebelde que vive con su padre al margen de la sociedad. Sobreviven a base de pequeños timos gracias a la extraordinaria habilidad de Ian para proyectar ilusiones visuales en la mente de los demás. Tras perder el control sobre sus poderes en público, una misteriosa agencia secreta comienza a perseguirle. En su huida, Ian descubrirá que no es el único y que toda su vida ha sido una mentira. </info>
 
<page>2</page><title>BABY</title>
<microhd>NA</microhd>
<fullhd>d20c5eb991aa0ca953abf7f117f6824a6c09bfec</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://i.imgur.com/AhH9wlf.jpg</thumbnail>
<fanart>https://i.imgur.com/JO0D6jj.jpg</fanart>
<date>2020</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Una joven drogadicta embarazada da a luz en medio de una de sus crisis. Incapaz de ocuparse del bebé, lo vende a una matrona dedicada al tráfico de niños. Arrepentida, la joven tratará de recuperarlo.</info>
 
<page>2</page><title>BABY DRIVER</title>
<microhd>ba36c2dc1a6e50b79d794beef660083dab7c6f8e</microhd>
<fullhd>11e213ec346981da497d918e627ac6bbdb3242c1</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/5FFkWMuLntzxqdpLT12avTxRpPE.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/yir5430WblZSZuIMd9C6pIZerfA.jpg</fanart>
<date>2017</date>
<genre>Thriller. Accion. Comedia</genre>
<extra>NA</extra>
<info>Baby (Ansel Elgort), un joven y talentoso conductor especializado en fugas, depende del ritmo de su banda sonora personal para ser el mejor en lo suyo. Cuando conoce a la chica de sus sueños (Lily James), Baby ve una oportunidad para abandonar su vida criminal y realizar una huida limpia. Pero después de ser forzado a trabajar para un jefe de una banda criminal (Kevin Spacey), deberá dar la cara cuando un golpe malogrado amenace su vida, su amor y su libertad.</info>
 
<page>2</page><title>BABYLON</title>
<microhd>NA</microhd>
<fullhd>7110731f82b06ef899fa478f9d0a94737a529bb6</fullhd>
<tresd>NA</tresd>
<cuatrok>1dfee437110ed5829f44d3eac966185c3d7e477f</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/oLZULW6Kn5imFM427tWfqIPMLmP.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/5fxTB08O7CW1hAcN2MWOKodp1h1.jpg</fanart>
<date>2022</date>
<genre>Drama. Comedia</genre>
<extra>NA</extra>
<info>Ambientada en Los Ángeles durante los años 20, cuenta una historia de ambición y excesos desmesurados que recorre la ascensión y caída de múltiples personajes durante una época de desenfrenada decadencia y depravación en los albores de Hollywood.</info>
 
<page>2</page><title>BAD BOYS FOR LIFE</title>
<microhd>8afa5a12a8a28074826a4a6c54248f6c766319ae</microhd>
<fullhd>2babc4bcd3b6e504b8915bab7942e570aceda293</fullhd>
<tresd>NA</tresd>
<cuatrok>bf089db1e009ae45a35ad3b66320af339ed2cc4e</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/chhwNdkN4n2oIEjz736HPjTJg1h.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/upUy2QhMZEmtypPW3PdieKLAHxh.jpg</fanart>
<date>2020</date>
<genre>Accion. Thriller. Comedia</genre>
<extra>NA</extra>
<info>En esta tercera entrega de la franquicia, los polícía Mike Lowrey (Will Smith) y Marcus Burnett (Martin Lawrence) vuelven a patrullar juntos en un último viaje.</info>
 
<page>2</page><title>BAJO EL MISMO TECHO</title>
<microhd>3dfb4092167e2a118a18e1606b53dd6e98c3443d</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/10RERo66cnzQ1W6tU3TnVZcaHII.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/hSW0xyqnP9g0BE6t7XeeP9KUQVE.jpg</fanart>
<date>2019</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Los dos miembros de un matrimonio, tras divorciarse, tendrán que seguir viviendo bajo el mismo techo al no encontrar comprador de su casa de casados y no tener ninguno de ellos dinero suficiente para irse a vivir a otro sitio.</info>
 
<page>2</page><title>BAJO SOSPECHA</title>
<microhd>d1d3c62cbee7604bbb854e9ff7747758538b42b9</microhd>
<fullhd>6c21e5e4329448cfda81f0afe34e1efe00c39c91</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/u3IZqv1SzNkE97DSGKqhI8v8Fhv.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/np8F1uaawPO58wuBE8PgSXKo5F7.jpg</fanart>
<date>2019</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Un joven miembro del FBI, recién casado, es enviado a un pueblo de montaña de los Apalaches en el estado de Kentucky. Allí inicia una relación con una joven de la zona, quien se convierte en su principal informadora. Ella además verá en él un medio para escapar de su vida.</info>
 
<page>2</page><title>BALLERINA</title>
<microhd>cd43f6f25079860b1ac38c4174cd2f69f218c20d</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/w8wbS7YRoi4kmlrigRHOaATsX6G.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/wlLdcK3Sm7zp0w1ZuVvnFlmcchM.jpg</fanart>
<date>2016</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Felicia es una niña que, tras perder a sus padres, vive en un orfanato en su Bretaña natal. Su pasión es la danza y sueña con convertirse en una bailarina profesional. Para conseguirlo se escapa con la ayuda de su amigo Víctor y viaja hasta el París de 1879. Allí se hará pasar por otra persona para conseguir entrar como alumna de la Grand Opera House y así luchar por tener la vida que desea.</info>
 
<page>2</page><title>BAMBI</title>
<microhd>0478d136915e5cd8927f338dca08b93de1d5e0c0</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/q9LI5Uloz1WRqaJjr8Tq2aOeSeH.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/a9kv63wV8wXAfSAcaJjLehGWAgG.jpg</fanart>
<date>1942</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Con los primeros rayos del sol iluminando la pradera, un nuevo príncipe ha nacido en el bosque. Tan pronto como Bambi aprende a dar sus primeros pasos, comienza a jugar con sus nuevos amigos, Tambor, el conejo juguetón, y Flor, la tímida y adorable mofeta. Pero la diversión de patinar sobre el lago helado, de mordisquear las florecillas y de juguetear entre los árboles del bosque será sólo el principio de un largo aprendizaje. Guiado por su sabio amigo el Búho, Bambi aprenderá lecciones sobre el valor del amor, la pérdida de los seres queridos, la madurez; en definitiva, aprenderá a seguir el camino de la vida.</info>
 
<page>2</page><title>BAMBY 2. EL PRINCIPE DEL BOSQUE</title>
<microhd>1534f3e8f5b7eac9fd6a03b3626be7d3d4798989</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/enl6RjnQEriJqMyGcMb72qgT8Ei.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/tyKOUk8qVvwCtfMD2eKRNiiEhcM.jpg</fanart>
<date>2006</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>La película retoma la historia de Disney desde el momento en que la madre de Bambi es capturada por los cazadores y el pequeño cervatillo tiene que aprender a valerse por si mismo con la ayuda de un padre poco habituado a tratar con pequeños. </info>
 
<page>2</page><title>BARBARIAN</title>
<microhd>986b7487c1b7f14928588fe9736ae83f213d6e21</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>7162b55b9df1fe986c407eae47c36712097f7d19</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/rHDL6LTRgc9h3oGkeoWVJpnkhTI.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/kSeU4uk19t82k6Cao7hc6ktDYhF.jpg</fanart>
<date>2022</date>
<genre>Terror. Thriller. Intriga</genre>
<extra>NA</extra>
<info>Una joven que viaja a Detroit para una entrevista de trabajo alquila una casa para pernoctar. Pero cuando llega a altas horas de la noche, descubre que la casa está doblemente reservada y que un hombre extraño ya se está quedando allí. En contra de su buen juicio, decide pasar la noche allí, pero pronto descubre que hay mucho más que temer que un invitado inesperado. </info>
 
<page>2</page><title>BARBIE</title>
<microhd>NA</microhd>
<fullhd>7705efd01bfdff77367aa1a7c4463c78c3049c9d</fullhd>
<tresd>NA</tresd>
<cuatrok>6f5a443c32bf6901b8d11f09dc0142bc164f34bc</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/u5kboZR4OMi4QdbOhawCZuzMVWJ.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/nHf61UzkfFno5X1ofIhugCPus2R.jpg</fanart>
<date>2023</date>
<genre>Comedia. Fantástico</genre>
<extra>Estreno</extra>
<info>Barbie (Margot Robbie) lleva una vida ideal en Barbieland, allí todo es perfecto, con chupi fiestas llenas de música y color, y todos los días son el mejor día. Claro que Barbie se hace algunas preguntas, cuestiones bastante incómodas que no encajan con el mundo idílico en el que ella y las demás Barbies viven. Cuando Barbie se dé cuenta de que es capaz de apoyar los talones en el suelo, y tener los pies planos, decidirá calzarse unos zapatos sin tacones y viajar hasta el mundo real.</info>
 
<page>3</page><title>BARRY SEAL EL TRAFICANTE</title>
<microhd>2c341de116229bfb1dc611933bb955143d860ce3</microhd>
<fullhd>dec3e7053ac1f70c328099d76d486b3b8586caed</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/i9jahD3uR2B4CPmJAsfeRvmzV0U.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/jcKzxRGYiXuS7ctTHLdw9wH8d7V.jpg</fanart>
<date>2017</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>Basada en la vida real de Barry Seal, un expiloto que se convirtió en un importante narcotraficante en el cartel de Medellín y que acabó siendo reclutado por la CIA y el departamento de inteligencia de la DEA.</info>
 
<page>3</page><title>BASADA EN HECHOS REALES</title>
<microhd>NA</microhd>
<fullhd>70b67821789ff2040d2208c9424970a4e4ce6a53</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/y4nbcV8ckxcb2j1GgTiRL0gyGKB.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/4N3ZAv55NSPttXSEVXByKYDyICL.jpg</fanart>
<date>2017</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Delphine (Emmanuelle Seigner) es una sensible y atormentada novelista de éxito, paralizada ante la idea de tener que comenzar a escribir una nueva novela. Su camino se cruza entonces con el de Elle (Eva Green), una joven encantadora, inteligente e intuitiva. Elle comprende a Delphine mejor que nadie, y pronto se convierte en su confidente. Delphine confía en Elle y le abre las puertas de su vida. Pero ¿quién es Elle en realidad? ¿Qué pretende? ¿Ha venido para darle un nuevo impulso a la vida de Delphine o para arrebatársela?</info>
 
<page>3</page><title>BATMAN</title>
<microhd>cbe23155f211a6508a2cff97b71b3e595f23cbd6</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>4658b4cf8a3873ea2251e7b76c25660b5e1ee1ca</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/k0f0FNZx0eBUCYiVpun9WUh0BFk.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/frDS8A5vIP927KYAxTVVKRIbqZw.jpg</fanart>
<date>1989</date>
<genre>Saga. Fantastico. Thriller. Accion</genre>
<extra>NA</extra>
<info>Cinta basada en el cómic homónimo. La oscura y peligrosa ciudad de Gotham tan sólo se halla protegida por su corrupto cuerpo de policía. A pesar de los esfuerzos del fiscal del distrito Harvey Dent y el comisionado de policía Jim Gordon, la ciudad es cada vez más insegura hasta que aparece Batman, el Señor de la Noche. La reputada periodista Vicky Vale intentará descubrir el secreto que se oculta tras el hombre murciélago.</info>
 
<page>3</page><title>BATMAN BEGINS</title>
<microhd>B4C3440773AAA9D17733F9F4AC1DF22037F463D0</microhd>
<fullhd>991cc25c0c1e856340c435691ec11ab807548083</fullhd>
<tresd>NA</tresd>
<cuatrok>e88d18b1cc8a3f7598c9d1f1ff67eae5a5a8f48c</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/z9iIvDrc4A6WqBg3qzmfQmRFLwe.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/q3f5eNjozyliGIuNrUFsL6Yc1i9.jpg</fanart>
<date>2005</date>
<genre>Saga. Thriller. Accion. Drama. Fantastico</genre>
<extra>NA</extra>
<info>Nueva adaptación del famoso cómic. Narra los orígenes de la leyenda de Batman y los motivos que lo convirtieron en el representante del Bien en la ciudad de Gotham. Bruce Wayne vive obsesionado con el recuerdo de sus padres, muertos a tiros en su presencia. Atormentado por el dolor, se va de Gotham y recorre el mundo hasta que encuentra a un extraño personaje que lo adiestra en todas las disciplinas físicas y mentales que le servirán para combatir el Mal. Por esta razón, la Liga de las Sombras, una poderosa y subversiva sociedad secreta, dirigida por el enigmático Ra's Al Ghul, intenta reclutarlo. Cuando Bruce vuelve a Gotham, la ciudad está dominada por el crimen y la corrupción. Con la ayuda de su leal mayordomo Alfred, del detective de la policía Jim Gordon y de Lucius Fox, su colega en una Sociedad de Ciencias Aplicadas, Wayne libera a su alter ego: Batman, un justiciero enmascarado que utiliza la fuerza, la inteligencia y la más alta tecnología para luchar contra las siniestras fuerzas que amenazan con destruir la ciudad.</info>
 
<page>3</page><title>BATMAN FOREVER</title>
<microhd>f7ed60cb4b6d007986a104437cb5ea2afe37aab2</microhd>
<fullhd>f83c75b0620e912d3339e15bab615d72e244fb98</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/7kKUspSxDoQ88Dot3mXnoe3lcu0.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/snlu32RmjldF9b068UURJg8sQtn.jpg</fanart>
<date>1995</date>
<genre>Saga. Ciencia ficcion. Fantastico. Accion</genre>
<extra>NA</extra>
<info>La ciudad de Gotham está amenazada por un hombre que intenta vengarse de la Humanidad; su rostro está desfigurado por el ácido y se hace llamar El Caras. Por otra parte, un psicópata con intención de dominar el mundo ha inventado un estrafalario aparato que permite absorber las ondas cerebrales de los seres humanos. Ambos se unirán para lograr sus propósitos.</info>
 
<page>3</page><title>BATMAN LA LEGO PELICULA</title>
<microhd>477b2f9c22e05230ad6f5983b262497d00f27cd7</microhd>
<fullhd>01d48cc34cf01d7cd0dc516abfe06fa8f63f774d</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/9F7OQVDMmvUJVwI7s0yzGNftC2F.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/jqnbac3oind5VOcAN0mTXyapOGh.jpg</fanart>
<date>2017</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Spin-off de "La LEGO Película", protagonizado por Batman. En esta ocasión, el irreverente Batman, que también tiene algo de artista frustrado, intentará salvar la ciudad de Gotham de un peligroso villano, el Joker. Pero no podrá hacerlo solo, y tendrá que aprender a trabajar con sus demás aliados.</info>
 
<page>3</page><title>BATMAN VUELVE</title>
<microhd>5de1bd7fcd74fd4fa21a34d2c9263c366a57a83a</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>35119c10761d1db59fc69c9029f98f102a8594ef</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/gTdEE0qGuagjMnzKNGYR6uaBITA.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/dlnqLQwtK0CgSf7QDVfTAkNDf1y.jpg</fanart>
<date>1992</date>
<genre>Saga. Fantastico. Thriller. Accion</genre>
<extra>NA</extra>
<info>Gotham City se enfrenta a dos nuevos y peculiares criminales: el diabólico y siniestro Pingüino, una criatura solitaria y extrañamente deformada, y la hermosa y seductora -aunque letalmente peligrosa- Catwoman. Batman se deberá enfrentar a Pingüino, que quiere convertirse en el amo de la ciudad.</info>
 
<page>3</page><title>BATMAN Y ROBIN</title>
<microhd>c044570eb56044aff51835dfbd67034de5cf5ed6</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>c8d4123cc00db2b902b4bf755dff6ab3b0caaeb7</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/gJAvyXHdIjeKHS07Dlim7LETBEK.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/rpOqHZMNIaI4qP4r7nw1B7oA0mx.jpg</fanart>
<date>1997</date>
<genre>Saga. Fantastico. Accion</genre>
<extra>NA</extra>
<info>Los experimentos del doctor Victor Fries para conservar la vida de su esposa con técnicas criogénicas originaron un error fatal que lo convirtió en Míster Frío, un villano que necesita mantener su cuerpo a temperaturas glaciales y que intentará congelar a todos los habitantes de la ciudad de Gotham. Además, la tímida botánica Pamela Isley se transforma en el transcurso de un experimento en la terrible Hiedra Venenosa que querrá asesinar a todos los seres humanos para que las plantas recuperen su hegemonía sobre la Tierra. Batman y su compañero Robin deberán enfrentarse a ellos para hacer fracasar sus siniestros planes.</info>
 
<page>3</page><title>BATMAN:  EL CABALLERO OSCURO</title>
<microhd>8C0A3D030251C2B8000D5C5B67F41E52F2F7A374</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>adebf7eaeee43cbab6f3d59a5b3efcc4de4275a3</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/8QDQExnfNFOtabLDKqfDQuHDsIg.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/6SfX1p5G4EEEDnEulFJeSxgbtSV.jpg</fanart>
<date>2008</date>
<genre>Saga. Thriller. Accion. Drama</genre>
<extra>Culto</extra>
<info>Batman/Bruce Wayne (Christian Bale) regresa para continuar su guerra contra el crimen. Con la ayuda del teniente Jim Gordon (Gary Oldman) y del Fiscal del Distrito Harvey Dent (Aaron Eckhart), Batman se propone destruir el crimen organizado en la ciudad de Gotham. El triunvirato demuestra su eficacia, pero, de repente, aparece Joker (Heath Ledger), un nuevo criminal que desencadena el caos y tiene aterrados a los ciudadanos.</info>
 
<page>3</page><title>BATMAN:  EL CABALLERO OSCURO: LA LEYENDA RENACE</title>
<microhd>8CCAC24C3ECC5B690EA5E5853688280881BB1399</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>659c415f020fbd7bc72da60c56976d81135e4fa5</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/rrS7K8tXVFUBliIKWaRuSq65nWr.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/2yubt6LkLfmuv8Z7sbhmjZUYom7.jpg</fanart>
<date>2012</date>
<genre>Saga. Thriller. Accion. Drama</genre>
<extra>NA</extra>
<info>Hace ocho años que Batman desapareció, dejando de ser un héroe para convertirse en un fugitivo. Al asumir la culpa por la muerte del fiscal del distrito Harvey Dent, el Caballero Oscuro decidió sacrificarlo todo por lo que consideraba, al igual que el Comisario Gordon, un bien mayor. La mentira funciona durante un tiempo, ya que la actividad criminal de la ciudad de Gotham se ve aplacada gracias a la dura Ley Dent. Pero todo cambia con la llegada de una astuta gata ladrona que pretende llevar a cabo un misterioso plan. Sin embargo, mucho más peligrosa es la aparición en escena de Bane, un terrorista enmascarado cuyos despiadados planes obligan a Bruce a regresar de su voluntario exilio.</info>
 
<page>3</page><title>BATMAN:  EL REGRESO DEL CABALLERO OSCURO . PARTE 1</title>
<microhd>D50473DE010B8BABE54E2FDDB6BE28FFDD284942</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/yplFdLSePwRdVeiw3xfnumeTvsH.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/1YaG52CwGY3pKh76OBlwX0ymncg.jpg</fanart>
<date>2012</date>
<genre>Animación. Thriller. Acción. Saga</genre>
<extra>NA</extra>
<info>Bruce Wayne hace más de diez años que abandonó la capa de murciélago atormentado por la muerte de Robín. La ciudad de Gotham esta hundida en la decadencia y la anarquía. Ahora, cuando su ciudad más lo necesita, El Regreso del Caballero Oscuro es un resplandor de gloria. Apoyado por Carrie Kelly, una adolescente que asumirá el puesto de Robin, el murciélago sale nuevamente a la calle para poner fi n a la amenaza de pandillas mutantes que han invadido la ciudad. Y después estará frente a frente contra sus dos grandes enemigos, el Joker y Dos Caras. Primera parte de la adaptación del aclamado cómic de Frank Miller "El retorno del señor de la noche".</info>
 
<page>3</page><title>BATMAN:  EL REGRESO DEL CABALLERO OSCURO . PARTE 2</title>
<microhd>00EC0C059F53BF1428A1648B46A21310A2C3C068</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/vTpZl5n7wxyxhZZINd3gDVaEGhk.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/odnIGBD99tTaWfeubIIAyJtapwL.jpg</fanart>
<date>2031</date>
<genre>Animación. Thriller. Acción. Saga</genre>
<extra>NA</extra>
<info>Siguiendo con los sucesos que nos hicieron vibrar en El Regreso del Caballero Oscuro: Primera Parte, un Batman odiado y querido a partes iguales por los ciudadanos de Ciudad Gotham será llevado hasta sus límites. Teniendo que enfrentarse de nuevo a un Joker más loco que nunca, se encontrará inesperadamente luchando a muerte con su antiguo aliado, Superman. Solo uno de los dos podrá sobrevivir al combate definitivo. Segunda parte de la adaptación del aclamado cómic de Frank Miller "El retorno del señor de la noche"</info>
 
<page>3</page><title>BATTLE ROYALE 2 REQUIEM</title>
<microhd>8B85E139BC8B4224842859F5221D009CC9AE47DB</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/v32LuSHdMEn25rmKeDvxV9tK30u.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/7zgvMgv3IVbBPA1mvz2QplNtGD9.jpg</fanart>
<date>2003</date>
<genre>Acción. Saga</genre>
<extra>NA</extra>
<info>Tres años después de los sucesos de “Battle Royale”, Shuya Nanahara es un famoso terrorista que está empeñado en derrocar al gobierno. Como respuesta, ellos ordenan la creación del programa “Battle Royale 2” y enviar a una clase de estudiantes de secundaria para buscarlo y matarlo. </info>
 
<page>3</page><title>BATTLE ROYALE DIRECTOR CUT</title>
<microhd>C3D6909E490E47031487F6E187B461E970FB860C</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>edf758533980ff2ef1b0b4eb3cff883c96fb546f</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/e9UQGoX2ZFNdrjLwEejMSMDSxmR.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/tSbZky1KLzqOSGPzYtqnxTQzNFy.jpg</fanart>
<date>2020</date>
<genre>Thriller. Acción.Saga</genre>
<extra>Culto</extra>
<info>En el amanecer de un nuevo milenio, el país está al borde del colapso. Millones de personas vagan sin empleo. La violencia en la escuela está descontrolada y adolescentes rebeldes protagonizan boicots masivos. El gobierno contrataca con “Battle Royale”. Cada año, una clase es escogida al azar para que se enfrente, en una isla abandonada, a un cruel juego de supervivencia.</info>
 
<page>3</page><title>BEING THE RICARDOS</title>
<microhd>d34f73bc6ce00e4862a0cbec7ae7c867007fe6f0</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>2b472cc2698c1cedcdc4bb28b4438329d7abeecb</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/oztBLWdRk5gApYmNdADXvXkLT5m.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/emo7xgyCvlmbeTHTlNofKjhBE7F.jpg</fanart>
<date>2021</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Película sobre la actriz, pionera de la televisión, Lucille Ball (1911-1989). La historia se desarrolla durante una semana concreta de la producción de la exitosa serie de la época 'I Love Lucy' cuando Lucy y su marido, Desi Arnaz, se enfrentan a una crisis que podría terminar con sus carreras y también con su matrimonio.</info>
 
<page>3</page><title>BELFAST</title>
<microhd>NA</microhd>
<fullhd>715ab88715c81b95537828e07e9399b4983af517</fullhd>
<tresd>NA</tresd>
<cuatrok>e15bc8659ce3ae422d21de6a2ef13d8500747661</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/7eUyUe3yLWsvx9yYfqwg4wQXeng.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/4KLlHifB1kgMtWl3YnY8JinOj65.jpg</fanart>
<date>2021</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Drama ambientado en la tumultuosa Irlanda del Norte de finales de los años 60. Sigue al pequeño Buddy mientras crece en un ambiente de lucha obrera, cambios culturales, odio interreligioso y violencia sectaria. Buddy sueña con un futuro que le aleje de los problemas, pero, mientras tanto, encuentra consuelo en su pasión por el cine, en la niña que le gusta de su clase, y en sus carismáticos padres y abuelos.</info>
 
<page>3</page><title>BENEDETTA</title>
<microhd>9fb45d802d4a30c769ae52cef5e58b94b34d390f</microhd>
<fullhd>6cee2647c61ae03c82d1c36a7240a4fd92b48d0c</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/gFromMkeTlIMGqGbYikD0k8ZaRg.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/fLZshCIFoLm3IypozhQ9Tnj0cd8.jpg</fanart>
<date>2021</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Adaptación de la novela "Immodest Acts: The Life of a Lesbian Nun in Renaissance Italy (Studies in the History of Sexuality)" de Judith C. Brown, que gira en torno a la sexualidad en un convento y la homosexualidad de una de sus monjas, que desde joven comenzó a tener visiones sin conocer el motivo. Ambientada en el siglo XVII e inspirada en hechos reales, narra la vida de la monja Benedetta Carlini, abadesa en el Convento de la Madre de Dios en Pescia, Italia, y de cómo empezó a tener extrañas visiones que la llevaron a recluirse en sí misma. Es cuando la jerarquía católica decide investigar y pronto encontrará que la abadesa Benedetta está teniendo relaciones sexuales con otra monja del convento, la hermana Bartolomea. Esto provocará un gran enfrentamiento con la Iglesia, en un momento muy complicado de la historia, en el que el protestantismo avanza con fuerza.</info>
 
<page>3</page><title>BEN-HUR</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>cd9d727e30d1e4f9306e63061bce3dd430fd037d</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/mcNBBxFrikb8ad8s5mDQp1TGcaU.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/vAUJooPGm7mNypJbBFYuzayruyI.jpg</fanart>
<date>1959</date>
<genre>Aventuras. Drama</genre>
<extra>Culto</extra>
<info>Antigua Roma, bajo el reinado de los emperadores Augusto y Tiberio (s. I d.C.). Judá Ben-Hur (Charlton Heston), hijo de una familia noble de Jerusalén, y Mesala (Stephen Boyd), tribuno romano que dirige los ejércitos de ocupación, son dos antiguos amigos, pero un accidente involuntario los convierte en enemigos irreconciliables: Ben-Hur es acusado de atentar contra la vida del nuevo gobernador romano, y Mesala lo encarcela a él y a su familia. Mientras Ben-Hur es trasladado a galeras para cumplir su condena, un hombre llamado Jesús de Nazaret se apiada de él y le da de beber. En galeras conocerá al comandante de la nave (Jack Hawkins) y más tarde a un jeque árabe (Hugh Griffith) que participa con sus magníficos caballos en carreras de cuadrigas. </info>
 
<page>3</page><title>BIG FISH</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>https://bt4g.org/magnet/14d8abd7bfba61f9a5bc68ce4d155d3ce610f863</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/m4gYCeV4GfGDw4PpMgU9kFIaNxP.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/3PcNoBZ4ZgUrESe1LmZcANEbEV.jpg</fanart>
<date>2003</date>
<genre>Drama. Fantástico. Aventuras </genre>
<extra>Mis</extra>
<info>William Bloom (Billy Crudup) no tiene muy buena relación con su padre (Albert Finney), pero tras enterarse de que padece una enfermedad terminal, regresa a su hogar para estar a su lado en sus últimos momentos. Una vez más, William se verá obligado a escucharlo mientras cuenta las interminables historias de su juventud. Pero, en esta ocasión, tratará de averiguar cosas que le permitan conocer mejor a su padre, aunque para ello tendrá que separar claramente realidad y fantasía, elementos que aparecen siempre mezclados en los relatos de su progenitor.</info>
 
<page>3</page><title>BIG GEORGE FOREMAN</title>
<microhd>NA</microhd>
<fullhd>2157cdfa7b0860d475d044422d71ab28b4c85c5a</fullhd>
<tresd>NA</tresd>
<cuatrok>4b5e796d4c7f2e96306b289037d18e79355b64dc</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/fdVd6thTstt0MQ4dUC1IXiOXpxv.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/pkuJFY5icxzr2uG9JMRoK5AvnMk.jpg</fanart>
<date>2023</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Impulsado por una infancia empobrecida, George Foreman canalizó su ira para convertirse en medallista de oro olímpico y campeón mundial de peso pesado, seguido de una experiencia cercana a la muerte que lo llevó del ring de boxeo al púlpito. Pero cuando ve que su comunidad lucha espiritual y financieramente, Foreman regresa al ring y hace historia al reclamar su título, convirtiéndose en el campeón mundial de boxeo de peso pesado más antiguo e improbable de todos los tiempos.</info>
 
<page>3</page><title>BILL Y TED SALVAN EL UNIVERSO</title>
<microhd>ef9f3a8d7e6f5454eb8da0e94cdc60df4b08750e</microhd>
<fullhd>939c2c02137f64988732db71bc4b131bf110523e</fullhd>
<tresd>NA</tresd>
<cuatrok>878b632dbe77c94ecfe1e42ac97faf112b999b10</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/wg7zMOc3DtTJyEazjHCojIBKSrD.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/oazPqs1z78LcIOFslbKtJLGlueo.jpg</fanart>
<date>2020</date>
<genre>Comedia. Ciencia ficción </genre>
<extra>NA</extra>
<info>A dos aspirantes a rockero de San Dimas, California, les aseguraron una vez no hace mucho tiempo que serían los salvadores del mundo después de protagonizar un viaje en el tiempo, pero la realidad es que hoy son padres de mediana edad que pasan los días tratando de componer una canción que valga la pena y de hacer que su destino se haga realidad.</info>
 
<page>3</page><title>BIRD BOX BARCELONA</title>
<microhd>NA</microhd>
<fullhd>93f4d6fda42221a153c4346b1ec8155edd7522e6</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/11n6tatb6NsrP6Ay4cEIWc297Eb.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/xWm2HGW6p1bziJ12rLlwOU4rmUa.jpg</fanart>
<date>2023</date>
<genre>Terror. Ciencia ficción</genre>
<extra>NA</extra>
<info>Tras la llegada de una misteriosa presencia que está destruyendo la población mundial, Sebastián comienza un viaje de supervivencia a través de las desiertas calles de Barcelona. Por el camino se encontrará con otros supervivientes con los que formará alianzas inciertas para intentar escapar de la ciudad. Pronto se darán cuenta de que hay una nueva e inesperada amenaza aún más peligrosa.</info>
 
<page>3</page><title>BITELCHUS</title>
<microhd>6dd82f617831d5f0f694a50fb8a934a9609e1572</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/Ap36MvDZbfX0vzPsNeYqYv29rrK.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/46fc09qRnuWkFiY11t2Eu63muvU.jpg</fanart>
<date>1988</date>
<genre>Fantástico. Comedia</genre>
<extra>Culto</extra>
<info>Un matrimonio de fantasmas (Geena Davis y Alec Baldwin) contrata los servicios de Bitelchus (Michael Keaton), un especialista en asustar mortales, para que ahuyente a los nuevos propietarios de su querida casa Victoriana. (</info>
 
<page>3</page><title>BLACK ADAM</title>
<microhd>NA</microhd>
<fullhd>07669d5d4ac958f32ff3243ff95d79a4d36a8b0e</fullhd>
<tresd>NA</tresd>
<cuatrok>832bba4d8f4366119118bdb2b4f9c860724ddade</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/moDLTCdLx38kMhN53KOTw0LdWMh.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/vTFLEVeoF84aI5fuESrLmHerTK4.jpg</fanart>
<date>2022</date>
<genre>Acción. Fantástico. Ciencia ficción. Comedia</genre>
<extra>NA</extra>
<info>Casi 5.000 años después de haber sido dotado de los poderes omnipotentes de los antiguos dioses -y encarcelado con la misma rapidez-, Black Adam (Johnson) es liberado de su tumba terrenal, listo para desatar su forma única de justicia en el mundo moderno.</info>
 
<page>3</page><title>BLACK AND BLUE</title>
<microhd>4020627b2d39ddb3af3a3018f9ef785309128d8e</microhd>
<fullhd>944dfbd7c95adff7f942163d7ca361a5261bd1b4</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/xkConpJqAQ15Z7hbfNgUoowEnTB.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/70HOyyuL7vmrLjxwDiSaUJTxwCm.jpg</fanart>
<date>2019</date>
<genre>Accion. Drama</genre>
<extra>NA</extra>
<info>Una policía afroamericana novata de Detroit sorprende a unos policías corruptos asesinando a un narcotraficante, hechos que graba con su cámara. Ellos la perseguirán con el fin de destruir esas imágenes.</info>
 
<page>3</page><title>BLACK BEACH</title>
<microhd>NA</microhd>
<fullhd>6455ed441d6915e70e5217b1e77b4dd3f227d135</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/a2Sw3SG3Oi9lg0PLv8v9Jz1P3dN.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/q4JREf8dYSlZesQZ6LJNqpudaDm.jpg</fanart>
<date>2020</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Carlos, un alto ejecutivo a punto de convertirse en socio de una gran empresa, recibe el encargo de mediar en el secuestro del ingeniero de una petrolera americana en África. El incidente está poniendo en peligro la firma de un contrato millonario.</info>
 
<page>3</page><title>BLACK HAWK DERRIBADO</title>
<microhd>NA</microhd>
<fullhd>59C5E715D934565CCD956A02976691F268317002</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/5chWdaHyaHWd5KaVxK94lbTDCdC.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/5czqI9tmuLjJTSs3e22cdmjNKD3.jpg</fanart>
<date>2001</date>
<genre>Bélico. Acción </genre>
<extra>Mis</extra>
<info>Octubre de 1993. Soldados americanos de élite son enviados a Mogadiscio (Somalia) en una misión de paz de las Naciones Unidas, pero su principal objetivo es capturar al caudillo Aidid y acabar de raíz con la guerra. La misión se complica cuando dos helicópteros Black Hawk son derribados. Entonces lo único que importa es rescatar a los soldados que han quedado atrapados o han resultado heridos en el accidente. </info>
 
<page>3</page><title>BLACK PANTHER</title>
<microhd>40574a5c68ce501ffa76235c021b1ad3d05f2111</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/udd8VinUWwLIiTYn3wdOEpCk9Fq.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ykKKcW2wSlazLd2Zc5AslSBFhJH.jpg</fanart>
<date>2018</date>
<genre>Accion. Fantastico. Aventuras</genre>
<extra>NA</extra>
<info>“Black Panther" cuenta la historia de T'Challa quien, después de los acontecimientos de "Capitán América: Civil War", vuelve a casa, a la nación de Wakanda, aislada y muy avanzada tecnológicamente, para ser proclamado Rey. Pero la reaparición de un viejo enemigo pone a prueba el temple de T'Challa como Rey y Black Panther ya que se ve arrastrado a un conflicto que pone en peligro todo el destino de Wakanda y del mundo.</info>
 
<page>3</page><title>BLACK PANTHER: WAKANDA FOREVER</title>
<microhd>99f1a60af6ad972864b86ea71a5a93daaa9281de</microhd>
<fullhd>bd5dd4f73e118b7a3df83f05b678d3a6e6f5ba10</fullhd>
<tresd>NA</tresd>
<cuatrok>9e15dff2225893475f60a0c208aebeb040059b5f</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/1AHvtMQOsOaJuesgXXJRQIBMlVk.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/fSfWloWi5rmqbmC7GhO0HY2TMZW.jpg</fanart>
<date>2022</date>
<genre>Fantástico. Acción. Aventuras. Ciencia ficción. Drama</genre>
<extra>NA</extra>
<info>La reina Ramonda (Angela Bassett), Shuri (Letitia Wright), M’Baku (Winston Duke), Okoye (Danai Gurira) y las Dora Milaje (incluida Florence Kasumba), luchan para proteger su nación de la injerencia de potencias mundiales a raíz de la muerte del rey T’Challa. Mientras los wakandianos se esfuerzan por adaptarse a su nueva etapa, los héroes deben actuar unidos, con la ayuda del Perro de la Guerra Nakia (Lupita Nyong’o) y Everett Ross (Martin Freeman), y forzar un nuevo destino para el reino de Wakanda.</info>
 
<page>3</page><title>BLACK PHONE</title>
<microhd>NA</microhd>
<fullhd>747c75459f5f66f17f593c8d483e1beaf1b294a6</fullhd>
<tresd>NA</tresd>
<cuatrok>e304706e2645b498a97d66b26a0ac83f667c23a4</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/p9ZUzCyy9wRTDuuQexkQ78R2BgF.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/wAKN3MbzvuQb4136GmYKdi36TzY.jpg</fanart>
<date>2021</date>
<genre>Terror. Thriller. Fantástico</genre>
<extra>NA</extra>
<info>En una ciudad de Colorado, en los años 70, un enmascarado secuestra a Finney Shaw, un chico tímido e inteligente de 13 años, y le encierra en un sótano insonorizado donde de nada sirven sus gritos. Cuando un teléfono roto y sin conexión empieza a sonar, Finney descubre que a través de él puede oír las voces de las anteriores víctimas, las cuales están decididas a impedir que Finney acabe igual que ellas.</info>
 
<page>3</page><title>BLACKWOOD</title>
<microhd>ef6da4488072c9d33820e7e1914f8f9bad31f2ea</microhd>
<fullhd>4faad59972d3f12858af530cfb3e82bdee60e71e</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/yBKD12N8P78jLO6GtcVgp96Yfky.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/2lhZxEhCNLSYPCD20JE8W24gUp5.jpg</fanart>
<date>2018</date>
<genre>Intriga. Fantastico</genre>
<extra>NA</extra>
<info>Cinco chicas problemáticas se ven obligadas a acogerse a un programa experimental de enseñanza, impartido por la enigmática Madame Duret (Uma Thurman) en el internado Blackwood. Pronto empiezan a mostrar talentos singulares que no sabían que poseían, y a tener extraños sueños, visiones y lagunas de memoria. Cuando la frontera entre realidad y sueño comienza a hacerse demasiado difusa, todas comprenden al fin el motivo por el que han sido llamadas a Blackwood. Aunque puede que ya sea tarde...</info>
 
<page>3</page><title>BLADE</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>9121cceb064662905ff034d09d4dbab3d5c4246c</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/dCsTsFX4PT9wewpKYaddwPHYI3A.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/bMhkCnATDPztcxFJULfnBqvzkyd.jpg</fanart>
<date>1998</date>
<genre>Fantástico. Terror. Acción</genre>
<extra>Mis</extra>
<info>En un mundo en guerra, en el que cohabitan hombres y vampiros, el caso de Blade es extraordinario: es un vampiro que no se alimenta de sangre humana y que, además, pretende acabar con la raza de "seres superiores" que mordieron a su madre cuando estaba embarazada.</info>
 
<page>3</page><title>BLADE II</title>
<microhd>NA</microhd>
<fullhd>9d4668efa1151f3bc534cdd54f4a63e773bd6119</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/4upKg9wQ6P86CjSlqI7wQ3ZM3Dz.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/g8cjJHZFeZQ5GRbFiDp5jXgfOZW.jpg</fanart>
<date>2002</date>
<genre>Fantástico. Acción. Terror</genre>
<extra>Mis</extra>
<info>Una nueva raza de vampiros, los Reapers, surge para atacar tanto a los humanos como a los de su propia raza, causando el terror. El Consejo de la Sombra, un grupo de vampiros que teme por su supervivencia y que tiene su sede en Praga, contacta con Blade para que acabe con ellos.</info>
 
<page>3</page><title>BLADE RUNNER</title>
<microhd>NA</microhd>
<fullhd>f054a25f0a9946f8afe400d4ae933325f6df4c7f</fullhd>
<tresd>NA</tresd>
<cuatrok>73a8d7262f330ef5e3653a531bab2e28bdcb3c0b</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/k4rBPLRRnw2t1exdSxyXFjeYWPj.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/eIi3klFf7mp3oL5EEF4mLIDs26r.jpg</fanart>
<date>1982</date>
<genre>Ciencia ficcion. Accion</genre>
<extra>Culto</extra>
<info>Noviembre de 2019. A principios del siglo XXI, la poderosa Tyrell Corporation creó, gracias a los avances de la ingeniería genética, un robot llamado Nexus 6, un ser virtualmente idéntico al hombre pero superior a él en fuerza y agilidad, al que se dio el nombre de Replicante. Estos robots trabajaban como esclavos en las colonias exteriores de la Tierra. Después de la sangrienta rebelión de un equipo de Nexus-6, los Replicantes fueron desterrados de la Tierra. Brigadas especiales de policía, los Blade Runners, tenían órdenes de matar a todos los que no hubieran acatado la condena. Pero a esto no se le llamaba ejecución, se le llamaba "retiro". Tras un grave incidente, el ex Blade Runner Rick Deckard es llamado de nuevo al servicio para encontrar y "retirar" a unos replicantes rebeldes.</info>
 
<page>3</page><title>BLADE RUNNER 2049</title>
<microhd>7894efa5c2330d277784b6b96ed35ee768a5f1db</microhd>
<fullhd>596774ba34b9eb40cc2e8ec3621086bcc9a25979</fullhd>
<tresd>47ccad3e25ed35ba2733958eff2da7eb6d1097a8</tresd>
<cuatrok>798a809bb4ceec092d2f8835c9993d494b9d10b2</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/2szDBSbKFa3n1ybMKzIR7O9kM6R.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/mVr0UiqyltcfqxbAUcLl9zWL8ah.jpg</fanart>
<date>2017</date>
<genre>Ciencia ficcion</genre>
<extra>NA</extra>
<info>Treinta años después de los eventos del primer film, un nuevo blade runner, K (Ryan Gosling) descubre un secreto profundamente oculto que podría acabar con el caos que impera en la sociedad. El descubrimiento de K le lleva a iniciar la búsqueda de Rick Deckard (Harrison Ford), un blade runner al que se le perdió la pista hace 30 años.</info>
 
<page>3</page><title>BLANCANIEVES Y LOS SIETE ENANITOS</title>
<microhd>0982e07a275bcd1ed7baac3135f4fb3ed8de33b5</microhd>
<fullhd>df524e51493a4db2ef0de07d8bad82c3117e03b4</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/jDpFqbbB2tQ1dPmPf1sV4sWqwvm.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/pmZiChyJs5Y0tz7sMAiZNNIo9z4.jpg</fanart>
<date>1937</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>La malvada madrastra de Blancanieves decide deshacerse de ella porque no puede soportar que la belleza de la joven sea superior a la suya. Sin embargo, Blancanieves consigue salvarse y se refugia en la cabaña de los siete enanitos. A pesar de todo, su cruel madrastra consigue encontrarla y la envenena con una manzana. Pero la princesa no está muerta, sólo dormida, a la espera de que un Príncipe Azul la rescate.</info>
 
<page>3</page><title>BLISS</title>
<microhd>NA</microhd>
<fullhd>145d6cc13e370240147e68eda065a189bb05610d</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/b7BllsQkD6nTkAdtd28UVCmKqVi.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/eSl9MRMvWJ0Hm5yePHgsRhFtPTw.jpg</fanart>
<date>2021</date>
<genre>Ciencia ficción. Drama</genre>
<extra>NA</extra>
<info>Tras divorciarse y ser despedido, Greg (Owen Wilson) conoce a Isabel (Salma Hayek), una mujer que vive en las calles y está convencida de que el mundo contaminado y destrozado que les rodea es solo una simulación informática. Tras sus dudas iniciales, Greg descubre que quizá haya algo de verdad en la delirante historia de Isabel</info>
 
<page>3</page><title>BLONDE</title>
<microhd>NA</microhd>
<fullhd>efd048f01a2bc64bb25f12bc63ebda63d72ffa14</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/jOgbnL5FB30pprEjZaY1E1iPtPM.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/o5Uy3zM1SNxzoMwy5H04GQAmfNF.jpg</fanart>
<date>2022</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Basada en el bestseller de la cinco veces finalista al Premio Pulitzer, Joyce Carol Oates, BLONDE es la historia personal audazmente reinventada de la sex symbol más famosa del mundo, Marilyn Monroe. La película es un retrato ficticio de la modelo, actriz y cantante durante los años 50 y 60, contada a través de la mirada moderna de la cultura de las celebridades.</info>
 
<page>3</page><title>BLOODSHOT</title>
<microhd>325963163f40c517cdaccb421fc37371f4efafb3</microhd>
<fullhd>f0cc2cec07bde6c88be973f30a62635aee90d4be</fullhd>
<tresd>NA</tresd>
<cuatrok>e426966b00b49c685b742f45e3a7c328b350e2a3</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/qdwpe2rk2xJ0MwO64ouVTNOyTzD.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/lP5eKh8WOcPysfELrUpGhHJGZEH.jpg</fanart>
<date>2020</date>
<genre>Accion. Fantastico</genre>
<extra>NA</extra>
<info>Ray Garrison (Vin Diesel), también conocido como Bloodshot, es resucitado por la compañía Rising Spirit Technologies a través del uso de nanotecnología. Mientras lucha por recuperar la memoria, Ray trata de volver al ser el que era antes de sufrir el accidente que le dejó amnésico... Película basada en el cómic "Valiant".</info>
 
<page>3</page><title>BLUE BEETLE</title>
<microhd>NA</microhd>
<fullhd>2bf87c0908e65458da1d2db92ee44aeea07fa633</fullhd>
<tresd>NA</tresd>
<cuatrok>34fa9ef13e70691e2b0b9ee872a77cd7fdfe5b9c</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/z5mkvXYNRauSzHdZgxAj6MzrLTY.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/H6j5smdpRqP9a8UnhWp6zfl0SC.jpg</fanart>
<date>2023</date>
<genre>Fantástico. Acción. Ciencia ficción</genre>
<extra>NA</extra>
<info>Jaime Reyes, recién licenciado, regresa a casa lleno de aspiraciones para su futuro, pero descubre que su hogar no es exactamente como lo dejó. En su búsqueda por encontrar su propósito en el mundo, el destino interviene cuando Jaime se encuentra inesperadamente en posesión de una antigua reliquia de biotecnología alienígena: el Escarabajo. Cuando el escarabajo elige a Jaime como huésped simbiótico, éste recibe una increíble armadura con poderes extraordinarios e impredecibles que cambiarán para siempre su destino y le convertirán en el superhéroe BLUE BEETLE.</info>
 
<page>3</page><title>BODY BROKERS ( EL TRAFICANTE )</title>
<microhd>d41fa53ec39d4c21c1e7359f47f541694c3a10b6</microhd>
<fullhd>21efe0dc15d756360cf163e929161b33d7446f7a</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/rLUHMHWZzFN1g2EXOWDLAUKeTCF.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/caWb61PBLJOdnnVYnL8WU6I80kE.jpg</fanart>
<date>2021</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>Utah (Jack Kilmer) y Opal (Alice Englert) son dos yonkis que viven en las calles de una zona rural de Ohio hasta que conocen al enigmático Wood (Michael Kenneth Williams), un hombre que les ofrece la posibilidad de viajar a Los Ángeles para ingresar en una clínica de rehabilitación. Pese a sus ilusiones, no tardan en descubrir que la clínica es en realidad un pretexto para un oscuro negocio.</info>
 
<page>3</page><title>BOHEMIAN RHAPSODY</title>
<microhd>ca4c566d805b4fa37a172b6afb8b90911df96f5b</microhd>
<fullhd>1d6e32b4a049d7daee9877bc2728c762ba8790c3</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/sVmTZxuXhSGYmNL8gbv2XcKTFUy.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/pbXgLEYh8rlG2Km5IGZPnhcnuSz.jpg</fanart>
<date>2018</date>
<genre>Drama.Musical</genre>
<extra>NA</extra>
<info>'Bohemian Rhapsody' es una celebración del grupo Queen, de su música y de su singular cantante Freddie Mercury, que desafió estereotipos para convertirse en uno de los showmans más queridos del mundo. El film plasma el meteórico ascenso de la banda al olimpo de la música a través de sus icónicas canciones y su revolucionario sonido, su crisis cuando el estilo de vida de Mercury estuvo fuera de control, y su triunfal reunión en la víspera del Live Aid, en la que Mercury, mientras sufría una enfermedad que amenazaba su vida, lidera a la banda en uno de los conciertos de rock más grandes de la historia. Refleja asimismo cómo se cimentó el legado de una banda que siempre se pareció más a una familia, y que continúa inspirando soñadores y amantes de la música hasta nuestros días.</info>
 
<page>3</page><title>BOLT</title>
<microhd>3ec8085be71c9e426c9a5993e16b7c598a10428b</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/23npziqYBfLaihW1QRIWbWpDJC2.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/iQBucIw8yqKMwqGccch4eMd7cj1.jpg</fanart>
<date>2008</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>La estrella canina de un programa de ciencia ficción, que cree que sus poderes son reales, se embarca en un viaje a través del país para salvar a su coprotagonista de una amenaza que cree que es igual de real.</info>
 
 
<page>3</page><title>BORAT, PELICULA FILM SECUELA</title>
<microhd>e721d86b18317d8eb9deefd2017fe435e9802cc0</microhd>
<fullhd>1635ea29facb85d4846c966d8392fb8d3c02ca91</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/i0TRERSqgiPujFJqKaz9cskmqcx.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/3Ysvp4ODDC6sucdQ9quHWkMiKED.jpg</fanart>
<date>2020</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Catorce años después de su primera aventura, el cuarto mejor periodista de Kazajistán vuelve a Estados Unidos, pero esta vez viaja con su hija</info>
 
<page>3</page><title>BOSQUE MALDITO</title>
<microhd>333dedbec5354aeb74fc08c1fddd5e64f06678af</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/jXHC3pcDQ3pGjAwPAIqF1nTVfPl.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/wTnFy6B5QCeRgjgCGBGlZaDESJ1.jpg</fanart>
<date>2019</date>
<genre>Terror</genre>
<extra>NA</extra>
<info>Una noche, el hijo más joven de Sarah (Seána Kerslake) desaparece entre la maleza que hay tras su casa rural. Aunque parece ser la misma persona cuando regresa a casa, su comportamiento comienza a ser cada vez más errático. Pronto, Sarah descubre que puede que el niño que ha vuelto no sea su hijo...</info>
 
<page>3</page><title>BOURNE: EL CASO BOURNE</title>
<microhd>f7640cbb76e48da962b2b0d1eb62f1e4e976c5d6</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/3r0UB8ooCKEMqOSoQmrTMWZT0C2.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/4vDGfUFhrznArh4eTYhK0AMr6id.jpg</fanart>
<date>2002</date>
<genre>Saga. Accion. Intriga. Thriller</genre>
<extra>NA</extra>
<info>Un hombre amnésico es rescatado por la tripulación de un barco pesquero italiano cuando flota a la deriva en el mar. No lleva nada consigo. Sólo las balas que lleva clavadas en la espalda y un número de cuenta de un banco suizo que lleva adherido a la cadera. Carece de identidad y de pasado, pero posee unas serie de talentos extraordinarios en artes lingüísticas, marciales y de autodefensa que sugieren una profesión de riesgo. Confuso y desorientado, emprende una frenética búsqueda para descubrir quién es y por qué su vida ha tomado un giro tan peligroso.</info>
 
<page>3</page><title>BOURNE: EL LEGADO DE BOURNE</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>1a5b5ced697b60971485fbb8ec4bc01b4630225a</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/pWVaLBdcVrTqSTBKIJLinmuxRso.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/g8ra9lMLYUgS36sfGJwfBkQUOyQ.jpg</fanart>
<date>2012</date>
<genre>Saga. Accion</genre>
<extra>NA</extra>
<info>El agente Aaron Cross (Jeremy Renner) es un producto del eficiente programa Outcome. Este programa diseña y entrena agentes cuya función consiste en actuar en solitario en misiones de alto riesgo. Sin embargo, en el momento en que la historia del agente Bourne está a punto de salir a la luz, los altos mandos de la agencia deciden tomar una solución drástica al respecto.</info>
 
<page>3</page><title>BOURNE: EL MITO DE BOURNE</title>
<microhd>b8f990d407d85ad7ba186440cd0b2ca27bb5b56b</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/kTFFwQHdaTUb5MJmB8KBGO7lh7N.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/fE7RqvVHbGHjBC9NjIjlLh2t4zK.jpg</fanart>
<date>2004</date>
<genre>Saga. Accion. Intriga. Thriller</genre>
<extra>NA</extra>
<info>Jason Bourne pensaba que había dejado atrás su pasado dos años antes. Durante ese tiempo, atormentado por un pasado que no consigue recordar, Bourne y Marie se trasladan de una ciudad a otra, viviendo de manera anónima y clandestina. Tratan de huir de una amenaza que creen percibir en la mirada de cualquier extraño, en cada llamada telefónica "equivocada". Cuando un agente aparece por la tranquila villa en la que se alojan, la pareja huye precipitadamente. Pero el juego del ratón y el gato ha vuelto a comenzar, obligando a entrar en acción a Bourne para enfrentarse a un grupo de implacables asesinos profesionales.</info>
 
<page>3</page><title>BOURNE: EL ULTIMATUM DE BOURNE</title>
<microhd>0bfcfdbc70cee4b6eaee3accc603febffe84ad6b</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/jdAciTzwshjaU3AWvSnAd48gZc7.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/r3DXmyyYWb44CeTkZg0JDV14WLn.jpg</fanart>
<date>2007</date>
<genre>Saga. Accion. Intriga. Thriller</genre>
<extra>NA</extra>
<info>Tercera entrega de las aventuras del agente Jason Bourne, que sigue investigando quién es y qué hay realmente detrás del programa secreto de la CIA llamado Treadstone. Un reportero británico de "The Guardian" le pone sobre una nueva pista facilitándole el nombre de Blackbriar. Bourne dará con él en Londres, en un intento de encajar las últimas piezas de ese pasado que él aún intenta recuperar.</info>
 
<page>3</page><title>BOURNE: JASON BOURNE</title>
<microhd>0bfcfdbc70cee4b6eaee3accc603febffe84ad6b</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ilaZC6JuYwGfOBexpo6U1redAgj.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/7mHeyU0a538bgguOeF57I8ZroSk.jpg</fanart>
<date>2016</date>
<genre>Saga. Accion. Thriller</genre>
<extra>NA</extra>
<info>Jason Bourne ha recuperado su memoria, pero eso no significa que el más letal agente de los cuerpos de élite de la CIA lo sepa todo. Han pasado 12 años desde la última vez que Bourne operara en las sombras. Pero todavía le quedan muchas preguntas por responder. En medio de un mundo convulso, azotado por la crisis económica y la guerra cibernética, Jason Bourne vuelve a surgir, de forma inesperada, cuando desde el pasado reaparece Nicky Parsons (Julia Stiles) con información sobre él de vital importancia. Desde un lugar oscuro y torturado, Bourne reanudará la búsqueda de respuestas sobre su pasado.</info>
 
<page>3</page><title>BRAVEHEART</title>
<microhd>1ed9c2020a804131f8518712dbbd4bc535394983</microhd>
<fullhd>e7648c94c4241ec2d27e1cd936abd80916c85d95</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/or1gBugydmjToAEq7OZY0owwFk.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/1m1c5gIWWkb1FEC9lzsoXHhhs7b.jpg</fanart>
<date>1995</date>
<genre>Aventuras. Drama</genre>
<extra>Mis</extra>
<info>En el siglo XIV, los escoceses viven oprimidos por los gravosos tributos y las injustas leyes impuestas por los ingleses. William Wallace es un joven escocés que regresa a su tierra despues de muchos años de ausencia. Siendo un niño, toda su familia fue asesinada por los ingleses, razón por la cual se fue a vivir lejos con un tío suyo.</info>
 
<page>4</page><title>BRIGHT</title>
<microhd>20c1ccf393376d0b24f1e7a0a92ca2adb537b678</microhd>
<fullhd>31be548fcbf7f5cd972704b1e5c33ff0f7b829cd</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/4xWEhLPlP9yg3DJbkKaBayBN98V.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/b09HLFIifMNljI7NI9CJgm2gN1Y.jpg</fanart>
<date>2017</date>
<genre>Fantastico. Accion</genre>
<extra>NA</extra>
<info>Ambientada en un presente alternativo donde los seres humanos, orcos, elfos y hadas han convivido desde el inicio de los tiempos, dos policías con perfiles muy distintos patrullan juntos. Uno es el humano Ward (Will Smith), y el otro el orco Jakoby (Joel Edgerton). Ambos inician una guardia nocturna que cambiará el futuro y el mundo que conocen. A pesar de sus diferencias personales, deberán trabajar juntos para proteger a una joven elfo y una misteriosa reliquia -una varita mágica- que, de caer en las manos equivocadas, podría destruirlo todo.</info>
 
<page>4</page><title>BUENAS NOCHES MAMA</title>
<microhd>e17a9756837612ab93242f28acea5fbed1ca7ed2</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/xgflUOfwrhlC6pfWm794okPPcsN.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/aCaqCvYn48b3lfGKGnUdVAE1yeB.jpg</fanart>
<date>2022</date>
<genre>Terror. Thriller. Drama </genre>
<extra>NA</extra>
<info>Dos hermanos gemelos llegan a la casa de su madre y comienzan a sospechar que algo no está bien. Remake de la película austriaca homónima de 2014.</info>
 
<page>4</page><title>BULLET TRAIN</title>
<microhd>NA</microhd>
<fullhd>cbc1de501060e4ca218fee3815a8a28984f02cda</fullhd>
<tresd>NA</tresd>
<cuatrok>23f13582106d85651f129b892af2dba07db2479c</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ybSIUt48PsM08F4UZwHdjL9ZVG2.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/pRrq1t1rBEELElYUA3B2eM3AXnP.jpg</fanart>
<date>2022</date>
<genre>Acción. Thriller. Comedia</genre>
<extra>NA</extra>
<info>Cinco asesinos a sueldo se encuentran a bordo de un tren bala que viaja de Tokio a Morioka con unas pocas paradas intermedias. Descubren que sus misiones no son ajenas entre sí. La pregunta es quién saldrá vivo del tren y qué les espera en la estación final.</info>
 
<page>4</page><title>BUSCANDO A DORY</title>
<microhd>eee1069dc5acaa4b505df5f22efd20fd4db7b2c6</microhd>
<fullhd>NA</fullhd>
<tresd>12cb3e508b78c50e2f718e11e1c70164ec1d53f8</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/wXBwo2DxTm6wZ8Cbu02t9kzzoK4.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/wijltj1toRcicHlCn8Cpsp4yPXe.jpg</fanart>
<date>2016</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Un año después de los acontecimientos narrados en "Buscando a Nemo", Dory vive apaciblemente con Marlin y su hijo Nemo. Pero durante un viaje para ver cómo las mantarrayas migran de vuelta a casa, los problemas de memoria de Dory parecen desaparecer durante un segundo: recuerda que tiene una familia e inmediatamente decide emprender viaje para reencontrarse con sus padres, a los que perdió hace años.</info>
 
<page>4</page><title>BUSCANDO A NEMO</title>
<microhd>16616892b94aa59a08da8e8c7c17da7492b43478</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/jPhak722pNGxQIXSEfeWIUqBrO5.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/oB7heykReUrityg2tu7Q3TmGFUA.jpg</fanart>
<date>2003</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>El pececillo Nemo, que es hijo único, es muy querido y protegido por su padre. Después de ser capturado en un arrecife australiano va a parar a la pecera de la oficina de un dentista de Sidney. Su tímido padre emprenderá una peligrosa aventura para rescatarlo. Pero Nemo y sus nuevos amigos tienen también un astuto plan para escapar de la pecera y volver al mar.</info>
 
<page>4</page><title>CABARET</title>
<microhd>NA</microhd>
<fullhd>55ce0508a678adc0ba6602da9158dc2f2cd15b21</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/1iSc8BDRPdBggMj9wNuFXlbOb5y.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/zB5YhLaW5r6xPaDzuJZTtCsG9TP.jpg</fanart>
<date>1972</date>
<genre>Drama. Musical</genre>
<extra>Mis</extra>
<info>Berlín, años 30. El partido naSi domina una ciudad donde el amor, el baile y la música se mezclan en la animada vida nocturna del Kit Kat Club. Un refugio mágico donde la joven Sally Bowles y un divertido maestro de ceremonias hacen olvidar las tristezas de la vida.</info>
 
<page>4</page><title>CADENA PERPETUA</title>
<microhd>818a06d899a73ce1694d2760673c3c8f9c9d7208</microhd>
<fullhd>37d7e9819ee82030d0e9a0ec475eaea88ab86dda</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/dc1fX265fZIIY5Hab8I7CdETyJy.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/9Xw0I5RV2ZqNLpul6lXKoviYg55.jpg</fanart>
<date>1994</date>
<genre>Drama</genre>
<extra>Culto</extra>
<info>Acusado del asesinato de su mujer, Andrew Dufresne (Tim Robbins), tras ser condenado a cadena perpetua, es enviado a la cárcel de Shawshank. Con el paso de los años conseguirá ganarse la confianza del director del centro y el respeto de sus compañeros de prisión, especialmente de Red (Morgan Freeman), el jefe de la mafia de los sobornos.</info>
 
<page>4</page><title>CAJAS OSCURAS</title>
<microhd>442933f8c25a0df231cfb2f424c21605d85175b8</microhd>
<fullhd>2eb779097b27bc2562fc9f8ea4461d709fa7bada</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/9Z1zkb4Z9hUlEuC0oMagVJaPUeS.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/1fOwb62XudDlevVyu1rqUlHUrES.jpg</fanart>
<date>2020</date>
<genre>Terror. Ciencia ficción</genre>
<extra>NA</extra>
<info>Un hombre lucha por recuperar la memoria después de sobrevivir a un trágico accidente automovilístico. Desesperado por volver a ser él mismo mientras intenta criar a su hija, recibe un tratamiento experimental que lo ayuda a indagar en un pasado que de repente se siente demasiado oscuro para ser el suyo.</info>
 
<page>4</page><title>CALL ME BY YOUR NAME</title>
<microhd>88b3a2fde83a2e05ae81238327d9abaea2fd3f0f</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/nyPcdV9gnzkiLlIxCG3xruvo9eo.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/8ndqvvwDVe9oDUQF1Jt6NCjAn7z.jpg</fanart>
<date>2017</date>
<genre>Romance. Drama</genre>
<extra>NA</extra>
<info>Elio Perlman (Timothée Chalamet), un joven de 17 años, pasa el cálido y soleado verano de 1983 en la casa de campo de sus padres en el norte de Italia. Se pasa el tiempo holgazaneando, escuchando música, leyendo libros y nadando hasta que un día el nuevo ayudante americano de su padre llega a la gran villa. Oliver (Armie Hammer) es encantador y, como Elio, tiene raíces judías; también es joven, seguro de sí mismo y atractivo. Al principio Elio se muestra algo frío y distante hacia el joven, pero pronto ambos empiezan a salir juntos de excursión y, conforme el verano avanza, la atrAccion mutua de la pareja se hace más intensa.</info>
 
<page>4</page><title>CAMERA CAFE: LA PELICULA</title>
<microhd>b8405c9bbc522c000fefcc734659a6ac737c207b</microhd>
<fullhd>c02573d008c0a6ffef25f0f0bdd6f21ae94fe7c0</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/wlHZV96gSY5UOmqPDlsFZ4ABWHy.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/fh5N9m1ZKiSiT1T1kPzV3rbeARs.jpg</fanart>
<date>2022</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Quesada, Julián, Marimar, Cañizares, Victoria y compañía se enfrentarán a una crisis que está a punto de hundir la empresa y, lo que es peor, deberá salvarla su nuevo director, que es nada más y nada menos que el rey del escaqueo, Quesada.</info>
 
<page>4</page><title>CAMPEONES</title>
<microhd>69aa5c69a6293a4a998756694e40497f8a9d827e</microhd>
<fullhd>2611528bcb30eeb71f13e0fe072b89a88e5683b7</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/lXLKmT6mtizYvweAgPXVHs0ApvR.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/5y2OhuVJFtWXAuTetMCEw9LUR2e.jpg</fanart>
<date>2018</date>
<genre>Comedia. Drama</genre>
<extra>NA</extra>
<info>Marco, un entrenador profesional de baloncesto, se encuentra un día, en medio de una crisis personal, entrenando a un equipo compuesto por personas con discapacidad intelectual. Lo que comienza como un problema se acaba convirtiendo en una lección de vida.</info>
 
<page>4</page><title>CANDYMAN</title>
<microhd>203ed6fd15dc22ef255bc35339ef986d5838d236</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>7bf6a5ac5b368db45ceb64f0db824c07b986f7fa</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/40FuQAiHjCvh9fYFigGAVWJ6Czu.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/2HOijUm4NUe3YHbRlTQlAR1A11T.jpg</fanart>
<date>2021</date>
<genre>Terror</genre>
<extra>NA</extra>
<info>Desde tiempos inmemoriales, los proyectos residenciales del barrio de Cabrini Green en Chicago se han visto amenazados por la historia de un supuesto asesino en serie con un gancho por mano al que se invoca fácilmente repitiendo su nombre cinco veces frente a un espejo. Hoy, una década después de que la última torre de Cabrini fuese derruída, el artista visual Anthony McCoy (Yahya Abdul-Mateen) y su novia Brianna Cartwright (Teyonah Parris), se mudan a un apartamento de lujo de un barrio ahora irreconocible, repleto de millennials y de personas que, por lo general, desconocen su oscuro pasado. </info>
 
<page>4</page><title>CANTA</title>
<microhd>NA</microhd>
<fullhd>467621ee9c75591d46985c495a64c0c29ae7ec79</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/5EyWAoHFGKfcIOT7tj0Cwe9Zx57.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/z9ft5HYHzWcasR6SGcgeluxTznB.jpg</fanart>
<date>2016</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Buster Moon es un elegante koala que regenta un teatro que conoció tiempos mejores. Es un optimista nato, lo que está muy bien si no fuera un poco caradura, pero ama a su teatro con pasión y es capaz de cualquier cosa para salvarlo. Sabe que el sueño de su vida está a punto de desaparecer, y sólo tiene una oportunidad para mantenerlo a flote: organizar un concurso de canto y conseguir que sea un gran éxito. Entre los muchos candidatos aparecerán una cerdita ama de casa y otro cerdo muy animoso, una puercoespín rockera, un gorila bondadoso, un ratón presumido y una elefante muy tímida.</info>
 
<page>4</page><title>CANTA 2</title>
<microhd>NA</microhd>
<fullhd>32c884a4bdf792d26963ee2198eb95714a8ff0bf</fullhd>
<tresd>NA</tresd>
<cuatrok>3e093382c0801ba59f34775d72b828bbf11e01ac</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ybjcJqJ4KGFYSgfDoODdRfqhGV9.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/tutaKitJJIaqZPyMz7rxrhb4Yxm.jpg</fanart>
<date>2021</date>
<genre>Infantil. Saga</genre>
<extra>NA</extra>
<info>Buster Moon y sus amigos deben persuadir a la estrella del rock Clay Calloway para que se una a ellos en el estreno de un nuevo espectáculo.</info>
 
<page>4</page><title>CAPONE</title>
<microhd>9447bbad018cd84d80fd0310a80775499b3341fc</microhd>
<fullhd>b5f17253cad32cef8fd075a095e9d194422569f7</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/75gDv38UgRtAukSxNXcjatyQmEa.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/8YpXQS3zo25ZexQVkQyhsLlzINB.jpg</fanart>
<date>2020</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Tras pasar 10 años en prisión, el gánster Al Capone, de 47 años, comienza a sufrir de demencia y su mente comienza a ser acosada por los recuerdos de su violento pasado.</info>
 
<page>4</page><title>CARS</title>
<microhd>e98d04d0e906e0edb605fc3bba124ceb435b3359</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/u4G8EkiIBZYx0wEg2xDlXZigTOZ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/8KeWhoMpqbzZRyHPkTtWSLWkL5L.jpg</fanart>
<date>2006</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>El aspirante a campeón de carreras Rayo McQueen parece que está a punto de conseguir el éxito, la fama y todo lo que había soñado, hasta que por error toma un desvío inesperado en la polvorienta y solitaria Ruta 66. Su actitud arrogante se desvanece cuando llega a una pequeña comunidad olvidada que le enseña las cosas importantes de la vida que había olvidado.</info>
 
<page>4</page><title>CARS 2</title>
<microhd>b285cdb11f7ed3675a94355f2190e7bcc9332e31</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/eQo1LQs5Vo9RHVHYUhNSfMZa3VB.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/robYRZk9LxJTwD7GlZCginji5k.jpg</fanart>
<date>2011</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Rayo McQueen y su amigo, la grúa Mate, viajan al extranjero para participar en el primer Campeonato Mundial en el que se decidirá cuál es el coche más rápido del planeta. La primera escala es Tokio (Japón), donde Mate tendrá que convertirse en un espía secreto al verse envuelto inesperadamente en un caso de espionaje internacional, que lo llevará a París y a Londres. También vivirán una aventura de proporciones épicas al viajar a Porto Corsa (Italia), para disputar el Campeonato. Sin embargo, los dos tendrán que renunciar a los placeres de la Riviera italiana -yates, playas y pasta al pesto- ya que Mate está muy ocupado con su labor de espía, mientras que McQueen compite contra los mejores coches del mundo.</info>
 
<page>4</page><title>CARS 3</title>
<microhd>470643e7057875195824f21e613ec125985fd2a9</microhd>
<fullhd>353b7ba413fe1fc155b3caeb3b2b5faf5e0b282f</fullhd>
<tresd>37baf00f48b089fe8ac831c50219e2a97b0f468a</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/4tp9W3QAbQcpanBtBceZtHD10UT.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/rND0GSv8X2Zzs7VhyFAs9BCEmYE.jpg</fanart>
<date>2017</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Sorprendido por una nueva generación de corredores ultrarrápidos, el legendario Rayo McQueen queda relegado repentinamente del deporte que tanto ama. Para retomar su carrera, va a necesitar la ayuda de una joven mecánica de carreras, Cruz Ramírez, que tiene su propio plan para ganar, además de la inspiración del fallecido Fabuloso Hudson Hornet y un par de giros inesperados. ¡Y para probar que el nº95 no está ni mucho menos acabado, el campeón tendrá que poner a prueba su valía en el gran circuito de la Copa Piston!</info>
 
<page>4</page><title>CARTAS A ROXANE</title>
<microhd>ea6b6fafa6e5d67db4b132a8761d70ae30fd0507</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/kc0hpag7wmSuhTok8o3xeavevGV.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/r9gdEpV1MoIORivtp00EfIFOM0u.jpg</fanart>
<date>2018</date>
<genre>Drama. Comedia</genre>
<extra>NA</extra>
<info>París, 1897, en plena Belle Époque. Edmond Rostand es un dramaturgo prometedor. Pero todo lo que ha escrito ha sido un fracaso y ahora sufre una parálisis creativa total. Gracias a su admiradora, la gran actriz Sarah Bernhardt, conoce al mejor actor del momento, Constant Coquelin, que insiste en interpretar su próxima obra. Y, además, quiere estrenarla dentro de tres semanas. El gran problema para Edmond es que todavía no la tiene escrita. Solo tiene el título: “Cyrano de Bergerac”.</info>
 
<page>4</page><title>CASA DE JACK</title>
<microhd>0e8f69636f1c0da418caaab5460f1871c04914e1</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/oeRnKXrWkiQBQm6rqMei2ZqQN7s.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ff6ujDT1Qt2H4lzWhLtK71W8Om6.jpg</fanart>
<date>2018</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Estados Unidos, década de 1970. Seguimos a Jack durante un período de 12 años, descubriendo los asesinatos que marcarán su evolución como asesino en serie. La historia se vive desde el punto de vista de Jack, quien considera que cada uno de sus asesinatos es una obra de arte en sí misma.</info>
 
<page>4</page><title>CASABLANCA</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>d75522e34f6239f3097cc3a14bd95cb2802b740c</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/8opAXe1bKRoYe7lKnIdO9kMDYYF.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/wOfzdzC0QZyhUIlyjeMuUYSb8Ax.jpg</fanart>
<date>1942</date>
<genre>Drama. Romance</genre>
<extra>Culto</extra>
<info>Años 40. A consecuencia de la Segunda Guerra Mundial, Casablanca era una ciudad a la que llegaban huyendo del nazismo gente de todas partes: llegar era fácil, pero salir era casi imposible, especialmente si el nombre del fugitivo figuraba en las listas de la Gestapo, que presionaba a la autoridades francesas al mando del corrupto inspector Renault. En este caso, el objetivo de la policía secreta alemana es el líder checo y héroe de la resistencia Victor Laszlo, cuya única esperanza es Rick Blaine, propietario del 'Rick’s Café' y antiguo amante de su mujer, Ilsa Lund. Rick e Ilsa se habían conocido en París, pero la entrada de las tropas alemanas en la capital francesa les separó. </info>
 
<page>4</page><title>CASI 40</title>
<microhd>d9320027a509309014bfba6071106af2ac8e6938</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/3hdYCs85IaRo7DoQzivk0rRgIvp.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/kGkJV0M6qjTiC2ztWSGJtyVg5bK.jpg</fanart>
<date>2018</date>
<genre>Drama. Comedia. Romance</genre>
<extra>NA</extra>
<info>Una modesta gira de conciertos vuelve a reunir a dos amigos de juventud. Ella, cantante de éxito, está ya retirada de la escena. Él, que sobrevive como vendedor de productos cosméticos, pretende relanzar la carrera Musical de quien fue su amor de adolescencia.</info>
 
<page>4</page><title>CASI IMPOSIBLE</title>
<microhd>c847af0762199d9f7e4a033f1cb03b59cc3df387</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/d5SjdpTAyn0zLzJYSAJ1AzosHh8.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/88r25ghJzVYKq0vaOApqEOZsQlD.jpg</fanart>
<date>2019</date>
<genre>Comedia. Romance</genre>
<extra>NA</extra>
<info>Cuando Fred Flarsky (Seth Rogen) se reencuentra inesperadamente con el primer amor de su vida, que ahora es una de las mujeres más influyentes del mundo, Charlotte Field (Charlize Theron), logra llamar su atención gracias a su peculiar sentido del humor y a su visión idealista del mundo y de la política. Mientras se prepara para aspirar a la presidencia del país, Charlotte contrata a Fred para que sea el encargado de escribir sus discursos. Sin embargo, Fred se va a encontrar como un pez fuera del agua en el equipo de élite de Charlotte. </info>
 
<page>4</page><title>CAUSEWAY</title>
<microhd>NA</microhd>
<fullhd>434c730f9e1c9b51b5a7119f428e915f21a76f2a</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/bUzKIqFIS05Ss31zRTfZfHJIgDP.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/trvBpyiWtAUXeys2Muevf8i1bhO.jpg</fanart>
<date>2022</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Una soldado estadounidense (Jennifer Lawrence) es herida en combate en Afganistán y sufre daño cerebral, tras lo cual vuelve a su casa de Nueva Orleans donde luchará por recuperarse.</info>
 
<page>4</page><title>CAVERNICOLA</title>
<microhd>37b23feb29f9be8a885d3f39c5663e6f64d9e2c5</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/sYGJUqRQeqOzw2n4MGcswXWNJC4.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/9m71RMP45mg599xtKPyjicyHn1a.jpg</fanart>
<date>2018</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Nueva producción de los creadores de "Wallace y Gromit", que narra la historia de Dug, que junto con su amigo Cerdog, unen a su tribu en contra del archienemigo Lord Nooth y su Ciudad de la Era del Bronce para salvar su hogar. De paso, enseña a su grupo de cavernícolas a jugar al fútbol.</info>
 
<page>4</page><title>CAZAFANTASMAS MAS ALLA</title>
<microhd>6ba3c996af50b5091a21f235ab5b772231ce99ee</microhd>
<fullhd>7234D8E47F575F5D2CE56D8892F296D81311374B</fullhd>
<tresd>NA</tresd>
<cuatrok>966f77af0954c8349a351ecd01d3f1cec9d214b3</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/9QRE6v3zuBSFK2b1pfw1PSEf3O7.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/qknLmBKDyjGMC40b7ZehqdfIi2A.jpg</fanart>
<date>2021</date>
<genre>Fantástico. Comedia</genre>
<extra>NA</extra>
<info>Una madre soltera y sus dos hijos llegan a una pequeña ciudad, descubriendo su conexión con los cazafantasmas originales y el legado secreto que dejó su abuelo tras de sí. Secuela directa de "Cazafantasmas 2".</info>
 
<page>4</page><title>CEGADO POR LA LUZ</title>
<microhd>70d54fcfb76576003cd00c5fe51fa0e380389bc6</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/nBW8D0w6c3FUnJmZt1SvYI21Mhn.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/5xVj9qDkZTALvIounsBdkqlLzH1.jpg</fanart>
<date>2019</date>
<genre>Drama. Comedia.Musical</genre>
<extra>NA</extra>
<info>Gurinder Chadha recupera la fórmula del éxito. (...) El protagonismo se lo queda el creyente, reafirmado en la fe de que la música de su dios (o Boss) puede dotar a su vida de auténtico sentido. (...) Es cine social a base de pinceladas que insinúan, pero que no acaban de incidir; es cine musical que se tararea, a falta de saberse toda la letra. Y ya suena bien así. </info>
 
<page>4</page><title>CELDA 211</title>
<microhd>edb45d9b49f22112ba5a7a038ae2376904019d4e</microhd>
<fullhd>c7a569df90aefb80863d335876f3a18869023d52</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/i2XCKfV81GcDbXbDNM80kK25Y0o.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/kujUJjGGKNN2d7G5NcCnplZPfGd.jpg</fanart>
<date>2009</date>
<genre>Thriller. Drama</genre>
<extra>Mis</extra>
<info>El día en que Juan (Alberto Ammann) empieza a trabajar en su nuevo destino como funcionario de prisiones, se ve atrapado en un motín carcelario. Decide entonces hacerse pasar por un preso más para salvar su vida y para poner fin a la revuelta, encabezada por el temible Malamadre (Luis Tosar). Lo que ignora es que el destino le ha preparado una encerrona. </info>
 
<page>4</page><title>CEMENTERIO DE ANIMALES</title>
<microhd>21d51583d78e899061e725634b24a06ce09c1c77</microhd>
<fullhd>d5c2c91a213daf66d1c74933c5609b0b6944a15a</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/vnw6g9c7qzNdzvpQhwWGRzBxwM0.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/dMWVeuVce8ZLKLI0xpVevihEwm8.jpg</fanart>
<date>2019</date>
<genre>Terror </genre>
<extra>NA</extra>
<info>El doctor Louis Creed (Clarke) se muda con su mujer Racher (Seimetz) y sus dos hijos pequeños de Boston a un pueblecito de Maine, cerca del nuevo hogar de la familia descubrirá un terreno misterioso escondido entre los árboles. Cuando la tragedia llega, Louis hablará con su nuevo vecino, Jud Crandall (Lithgow), desencadenando una peligrosa reacción en cadena que desatará un mal de horribles consecuencias.</info>
 
<page>4</page><title>CENICIENTA</title>
<microhd>027e1ca8e459e60f0cd503c89fe98e5fa6c03d11</microhd>
<fullhd>887af2766e30c250abe2ad36d48620b002515d70</fullhd>
<tresd>NA</tresd>
<cuatrok>ca2e33d39941a3b9146940da2a6fabf9866a58ba</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/q3EG1UUwcwMJEcuXSgfzzLhRUZZ.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/7ZiMrbitgbyio4eOzFKRruPKu5G.jpg</fanart>
<date>2021</date>
<genre>Musical. Fantástico. Comedia</genre>
<extra>NA</extra>
<info>Cenicienta (Cabello) es una joven ambiciosa cuyos sueños son más grandes de lo que el mundo le permite, pero que será capaz de perseverar y conseguir sus objetivos con la ayuda de su Fab G (Billy Porter).</info>
 
<page>4</page><title>CERDITA</title>
<microhd>NA</microhd>
<fullhd>f62691a230c1f1bdd6dde903476c40f43fcde226</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/5N4YFFm355wQwXTS4K3GRDNOnV8.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/2iGUavwv86Hubv3V5Iq4qEQXDfE.jpg</fanart>
<date>2022</date>
<genre>Thriller. Drama. Terror</genre>
<extra>NA</extra>
<info>Para Sara, el verano solo significa tener que soportar las continuas burlas de las otras chicas de su pequeño pueblo. Pero todo terminará cuando un desconocido llegue al pueblo y secuestre a sus acosadoras. Sara sabe más de lo que dice, y tendrá que decidir entre hablar y salvar a las chicas, o no decir nada para proteger al extraño hombre que la ha salvado...</info>
 
<page>4</page><title>CHAOS WALKING</title>
<microhd>8ed3b570cc44e07008d1547eb3e30045df5048a1</microhd>
<fullhd>4070260f406591b3739d9429f38792610c1b6cc9</fullhd>
<tresd>NA</tresd>
<cuatrok>f7620af275e1170f0b21b348d3a592f131513237</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/a0R1Ybj3m4M4ZeztizD7F6fmPCE.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/ovggmAOu1IbPGTQE8lg4lBasNC7.jpg</fanart>
<date>2021</date>
<genre>Ciencia ficción. Aventuras</genre>
<extra>NA</extra>
<info>Año 2267. En un planeta lejano, en la pequeña población de Prentisstown, el joven colono Todd ha crecido creyendo que una raza alienígena asesinó a todas las mujeres y contagió con el "ruido" al resto de los hombres. El ruido permite que entre ellos todos puedan escuchar lo que están pensando. Pero todo cambia cuando un día Todd se encuentra con Viola, la superviviente de una nave espacial que se ha estrellado contra el planeta. Todd se queda perplejo y confuso, pues nunca ha visto a una mujer... y ella no tiene "ruido".</info>
 
<page>4</page><title>CHERRY</title>
<microhd>d3c180f3629363ae51507f4282a8ffda80a25685</microhd>
<fullhd>20e839502e9c32daaa221a6fbeed35bb05b3f481</fullhd>
<tresd>NA</tresd>
<cuatrok>d6d46bc3d7622b1c760d59ffe4b0bd87fffbb77b</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/pwDvkDyaHEU9V7cApQhbcSJMG1w.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/uQtqiAu2bBlokqjlURVLEha6zoi.jpg</fanart>
<date>2021</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Cuenta la historia real de Nico Walker, que volvió de la guerra de Iraq con un trastorno de estrés postraumático no diagnosticado que le llevó primero a hacerse adicto al opio y posteriormente a robar bancos.</info>
 
<page>4</page><title>CHINATOWN</title>
<microhd>d170363a30c323e50f5410f8a285d5a238e61f63</microhd>
<fullhd>22ec6af9b170466ce8f7a11539746d0cae6bdde7</fullhd>
<tresd>NA</tresd>
<cuatrok>528bad1c3ce1de4f5b2573f8823a67b2bd78e348</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/sPXhA6PU5iKt4LMIZDuNOL1nW9s.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/eJ6kPtua2rEvjxHdCtnIobXJZSf.jpg</fanart>
<date>1974</date>
<genre>Intriga. Drama</genre>
<extra>Mis</extra>
<info>Los Ángeles, 1937. El detective Gittes, especializado en divorcios, recibe la visita de la esposa de Mulwray, el jefe del Servicio de Aguas de la ciudad, que sospecha que su marido la engaña. Al mismo tiempo, Gittes descubre que los agricultores acusan a Mulwray de corrupción por su negativa a construir un pantano que paliaría la sequía que sufren. Poco después, el escándalo salta a la prensa, pero la cosa se complica cuando una mujer se presenta en el despacho de Gittes con una sorprendente revelación</info>
 
<page>4</page><title>CHIP Y CHOP LOS GUARDIANES RESCATADORES</title>
<microhd>00af1e3909d6b972da3aef4b1257d531487af254</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/cVE6Mop3gCqIZfnQEkvGV2rG0IM.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/qK7Ssnrfvrt65F66A1thvehfQg2.jpg</fanart>
<date>2022</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Treinta años después, las estrellas de las series de animación de Disney de los años 1990, Chip y Chop, viven en Los Ángeles entre dibujos animados y humanos. Chip lleva una vida rutinaria y hogareña como vendedor de seguros, mientras que Chop se ha hecho la cirugía 3D y se dedica a explotar la nostalgia de convención en convención, desesperado por revivir sus días de gloria. Cuando un antiguo compañero de reparto desaparece misteriosamente, Chip y Chop se ven obligados a recuperar la amistad perdida y a hacer de detectives una vez más como Guardianes Rescatadores para salvar a su amigo.</info>
 
<page>4</page><title>CHRISTINE</title>
<microhd>NA</microhd>
<fullhd>3C11F631254A2EED5BC5EE506757A7C3A5C2EBA1</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/bWi3Cjg8gkf5h3xgU91CVtxkjUc.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/3ZDTjZ8bUwA8p9aDydBC8nkf3MO.jpg</fanart>
<date>1983</date>
<genre>Terror. Fantástico</genre>
<extra>Mis</extra>
<info>Christine es un Plymouth Fury de 1958 que salió de una cadena de montaje de automóviles de Detroit, pero no es un coche cualquiera. En el fondo de su chasis se aloja el mismísimo diablo, que alberga un deseo de venganza insaciable que hiela la sangre a cualquiera y destruye todo lo que encuentra en su camino.</info>
 
<page>4</page><title>CHRISTOPHER-ROBIN</title>
<microhd>fa1abbbce525dfe6f52c1f0344417f4993b7f994</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/yFTnOaszS1xnnnx2jOTPwIsQIqk.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/hnh4oqnGPlVFEMUuw41nYxKRYf0.jpg</fanart>
<date>2018</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>El niño que se embarcó en innumerables aventuras en el bosque de los Cien Acres con su banda de animales de peluche, ha crecido y… ha perdido el rumbo. Ahora les toca a sus amigos de la infancia aventurarse en nuestro mundo y ayudar a Christopher Robin a recordar al niño cariñoso y juguetón que aún tiene dentro.</info>
 
<page>4</page><title>CICLO CLINT EASTWOOD PARTE 1</title>
<microhd>NA</microhd>
<fullhd>06c42baf7ffdae6b3181fee3dc03d9208fe211a5</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://i.imgur.com/o50RsqX.png</thumbnail>
<fanart>https://i.imgur.com/Yro2TTg.jpg</fanart>
<date>VARIOS</date>
<genre>Accion. Belico. Drama. Thriller.</genre>
<extra>Misc</extra>
<info>«Clint» Eastwood Jr. (San Francisco, California; 31 de mayo de 1930) es un actor, director, productor, músico y compositor estadounidense. Su primer papel relevante fue como secundario en el reparto de la serie de televisión Rawhide (1959-1965). Alcanzó la fama interpretando al Hombre sin nombre en los spaghetti western conocidos como Trilogía del dólar que dirigió Sergio Leone en la década de 1960 —Por un puñado de dólares, La muerte tenía un precio y El bueno, el feo y el malo— y a Harry Callahan en la serie de películas de Harry el Sucio durante los años 1970 y 1980.</info>
 
<page>4</page><title>CICLO CLINT EASTWOOD PARTE 2</title>
<microhd>NA</microhd>
<fullhd>8a049611bede7d235523f9bc5827df495c27d67d</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://i.imgur.com/fSmtilU.png</thumbnail>
<fanart>https://i.imgur.com/Yro2TTg.jpg</fanart>
<date>VARIOS</date>
<genre>Accion. Belico.Drama.Thriller.</genre>
<extra>Mis</extra>
<info>«Clint» Eastwood Jr. (San Francisco, California; 31 de mayo de 1930) es un actor, director, productor, músico y compositor estadounidense. Su primer papel relevante fue como secundario en el reparto de la serie de televisión Rawhide (1959-1965). Alcanzó la fama interpretando al Hombre sin nombre en los spaghetti western conocidos como Trilogía del dólar que dirigió Sergio Leone en la década de 1960 —Por un puñado de dólares, La muerte tenía un precio y El bueno, el feo y el malo— y a Harry Callahan en la serie de películas de Harry el Sucio durante los años 1970 y 1980.</info>
 
<page>4</page><title>CICLO CLINT EASTWOOD PARTE 3</title>
<microhd>NA</microhd>
<fullhd>5316f541e147bed812a26a0d3b58737090249b15</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://i.imgur.com/mnb3gjC.png</thumbnail>
<fanart>https://i.imgur.com/Yro2TTg.jpg</fanart>
<date>VARIOS</date>
<genre>Accion. Belico. Drama. Thriller.</genre>
<extra>Mis</extra>
<info>«Clint» Eastwood Jr. (San Francisco, California; 31 de mayo de 1930) es un actor, director, productor, músico y compositor estadounidense. Su primer papel relevante fue como secundario en el reparto de la serie de televisión Rawhide (1959-1965). Alcanzó la fama interpretando al Hombre sin nombre en los spaghetti western conocidos como Trilogía del dólar que dirigió Sergio Leone en la década de 1960 —Por un puñado de dólares, La muerte tenía un precio y El bueno, el feo y el malo— y a Harry Callahan en la serie de películas de Harry el Sucio durante los años 1970 y 1980.</info>
 
<page>4</page><title>CICLO CLINT EASTWOOD PARTE 4</title>
<microhd>NA</microhd>
<fullhd>2de90e727fb55c3c3bf41098140c46efdd892e5c</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://i.imgur.com/fR1TKSD.png</thumbnail>
<fanart>https://i.imgur.com/Yro2TTg.jpg</fanart>
<date>VARIOS</date>
<genre>Accion. Belico. Drama. Thriller.</genre>
<extra>Mis</extra>
<info>«Clint» Eastwood Jr. (San Francisco, California; 31 de mayo de 1930) es un actor, director, productor, músico y compositor estadounidense. Su primer papel relevante fue como secundario en el reparto de la serie de televisión Rawhide (1959-1965). Alcanzó la fama interpretando al Hombre sin nombre en los spaghetti western conocidos como Trilogía del dólar que dirigió Sergio Leone en la década de 1960 —Por un puñado de dólares, La muerte tenía un precio y El bueno, el feo y el malo— y a Harry Callahan en la serie de películas de Harry el Sucio durante los años 1970 y 1980.</info>
 
<page>4</page><title>CIELO DE MEDIANOCHE</title>
<microhd>8ed5f8b0f86056023eac2b13ea36bfac5743279f</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/rttZeIKCI4VCIVsJGdVkYxSor8G.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/zDk9q66EpQoPQEVlcy3I4i41gFt.jpg</fanart>
<date>2020</date>
<genre>Ciencia ficción. Drama</genre>
<extra>NA</extra>
<info>Augustine (George Clooney) es un solitario científico que se encuentra en el Ártico, y que trata de contactar con una nave espacial que intenta regresar a la Tierra. Augustine quiere impedir que Sully (Felicity Jones) y sus compañeros astronautas regresen a su hogar, donde se ha producido una misteriosa catástrofe global.</info>
 
<page>4</page><title>CINCUENTA SOMBRAS DE GREY</title>
<microhd>fbcaaf1c573fdd27b4aebbcebdbad3c89c2fb373</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/mNZcZOIlTwDKd30xLnRR4p0ZELg.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/veSwOwoyojI6INSClpG19TkNjvN.jpg</fanart>
<date>2015</date>
<genre>Romance. Drama.Saga</genre>
<extra>NA</extra>
<info>Cuando Anastasia Steele, una estudiante de Literatura de la Universidad de Washington (Seattle), recibe el encargo de entrevistar al popular y joven empresario Christian Grey, un millonario de 27 años, queda impresionada ante su extraordinario atractivo. La inexperta e inocente Ana intenta olvidarlo, pero no lo consigue. Cuando la pareja, por fin, inicia una apasionada relación, a Ana le sorprenden las peculiares prácticas eróticas de Grey, al tiempo que descubre los límites de sus más oscuros deseos. </info>
 
<page>4</page><title>CINCUENTA SOMBRAS LIBERADAS</title>
<microhd>ce94860a4c2185326a12f43e247ffbfb791a1607</microhd>
<fullhd>81c3c6b0c167d61bb89c675cea2b0df16b87ac3d</fullhd>
<tresd>NA</tresd>
<cuatrok>84d624d51104564151856354023e161226cb94c5</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/sM8hwgWZlmZf0h4aOkNopb3HBIo.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/9ywA15OAiwjSTvg3cBs9B7kOCBF.jpg</fanart>
<date>2018</date>
<genre>Romance. Drama.Saga</genre>
<extra>NA</extra>
<info>Creyendo que han dejado atrás las sombras del pasado, los recién casados Christian y Anastasia disfrutan de su relación y de su vida llena de lujos. Pero justo cuando Ana empieza a relajarse, aparecen nuevas amenazas que ponen en riesgo su felicidad. Adaptación de la última novela de la saga "50 sombras de Grey".</info>
 
<page>4</page><title>CINCUENTA SOMBRAS MAS OSCURAS</title>
<microhd>e3023ef6462bf947cd53eb667e1a8c23fae8242d</microhd>
<fullhd>dfdc159f60b743f5201e17324a73f85c12f1b664</fullhd>
<tresd>NA</tresd>
<cuatrok>b2515a822da258b72f1a23b7b221727a1e879959</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/jvBAQOg2ObZKYXZGxYSz3Fkr7Qt.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/jrRDzBS8rGpixz0xjynbSgcxRHV.jpg</fanart>
<date>2017</date>
<genre>Romance. Drama.Saga</genre>
<extra>NA</extra>
<info>Cuando Christian Grey, que se siente herido, intenta convencer a Anastasia Steele de que vuelva a formar parte de su vida, ella le exige un nuevo acuerdo antes de aceptar. Pero cuando la pareja empieza a ser más confiada y a tener una cierta estabilidad, aparecen mujeres del pasado de Christian decididas a frenar en seco sus esperanzas de un futuro juntos... Secuela de "50 sombras de Grey", en la que Grey sigue explorando sus demonios interiores y la joven Anastasia confronta sus sentimientos y confusión sobre su relación con el atractivo millonario, un tipo misterioso lleno de secretos.</info>
 
<page>4</page><title>CINEMA PARADISO</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>931e038e27c3411be61420398338ad096fa75fcd</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/hHwsr3t5n7VVUbPyU8VZswn0jkL.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/LQbept4Ix8zlv1BtUhwSpOeImb.jpg</fanart>
<date>1988</date>
<genre>Drama. Comedia</genre>
<extra>Culto</extra>
<info>'Cinema Paradiso' es una historia de amor por el cine. Narra la historia de Salvatore, un niño de un pueblecito italiano en el que el único pasatiempo es ir al cine. Subyugado por las imágenes en movimiento, el chico cree ciegamente que el cine es magia; pero, un día, Alfredo, el operador, accede a enseñarle al pequeño los misterios y secretos que se ocultan detrás de una película. Salvatore va creciendo y llega el momento en el que debe abandonar el pueblo y buscarse la vida. Treinta años después recibe un mensaje, en el que le comunican que debe volver a casa.</info>
 
<page>4</page><title>CIUDAD DE DIOS</title>
<microhd>NA</microhd>
<fullhd>4EE3E392FF4F52C601427F8AA536EA937FE02C8C</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/xhX7gx9h7vbUt1XRMBRXLwfRU5m.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/6NWJLzckS3QR4g1mZacjQHZysVZ.jpg</fanart>
<date>2002</date>
<genre>Drama</genre>
<extra>Culto</extra>
<info>Basada en hechos reales, describe el mundo del crimen organizado en Cidade de Deus, un suburbio de Río de Janeiro, desde finales de los sesenta hasta principios de los ochenta, época durante la cual el tráfico de drogas y la violencia impusieron su ley en las favelas. A finales de los sesenta, Buscapé, un niño de 11 años tímido y sensible, observa a los niños duros de su barrio, sus robos, sus peleas, sus enfrentamientos diarios con la policía. Pero él sabe muy bien lo que quiere ser si consigue sobrevivir: fotógrafo. Dadinho, un niño de su edad que se traslada al barrio, sueña con ser el criminal más peligroso de Río de Janeiro y empieza su aprendizaje haciendo recados para los delincuentes locales. Admira a Cabeleira y su pandilla, que se dedican a atracar los camiones del gas. Un día Cabeleira le da a Dadinho la oportunidad de cometer su primer asesinato.</info>
 
<page>4</page><title>CIUDADANO KANE</title>
<microhd>65ab0fab0cd9ee13d49b1370b9220b546b3f78e8</microhd>
<fullhd>cd5c6dcd2658a63d8fe088bafeb243a9f6bed98e</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/f6rRdIkuR2giJFrKGVNJ8UajHHn.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/3Ne0EpyuhZkIbfSyDzzMaNkb7Z7.jpg</fanart>
<date>1941</date>
<genre>Drama</genre>
<extra>Culto</extra>
<info>Un importante magnate estadounidense, Charles Foster Kane, dueño de una importante cadena de periódicos, de una red de emisoras, de dos sindicatos y de una inimaginable colección de obras de arte, muere en Xanadú, su fabuloso castillo de estilo oriental. La última palabra que pronuncia antes de expirar, ”Rosebud”, cuyo significado es un enigma, despierta una enorme curiosidad tanto en la prensa como entre la población. Así, un grupo de periodistas emprende una investigación para desentrañar el misterio.</info>
 
<page>4</page><title>CLASICOS DE DISNEY 1 PARTE (SD)</title>
<microhd>a60b98fe0c394aec7af66d6f80a538411ba06178</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://i.imgur.com/bmdR8q2.png</thumbnail>
<fanart>https://i.imgur.com/Ps4ZxJc.jpg</fanart>
<date>VARIOS</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>CLASICOS DE DISNEY</info>
 
<page>4</page><title>CLASICOS DE DISNEY 2 PARTE  (SD)</title>
<microhd>6cf13a5f9612744a005536bc384aff77179c7c7d</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://i.imgur.com/bmdR8q2.png</thumbnail>
<fanart>https://i.imgur.com/Ps4ZxJc.jpg</fanart>
<date>VARIOS</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>CLASICOS DE DISNEY</info>
 
<page>5</page><title>CLASICOS DE DISNEY 3 PARTE (SD)</title>
<microhd>99c49a2d781f101c6cc3926496879a7144184518</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://i.imgur.com/bmdR8q2.png</thumbnail>
<fanart>https://i.imgur.com/Ps4ZxJc.jpg</fanart>
<date>VARIOS</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>CLASICOS DE DISNEY</info>
 
<page>5</page><title>CLEAN</title>
<microhd>c7ac5eb73a5aee490a5bf6062b1b37a55a1ef8d1</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/qhBN4nkuTEgxfzcxKk9KV1sojmL.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/xRz1gK72ioqwAhHFMQOWqE2WTDR.jpg</fanart>
<date>2021</date>
<genre>Drama. Thriller </genre>
<extra>NA</extra>
<info>Atormentado por su pasado, un basurero llamado Clean busca tener una vida tranquila de redención. Pero pronto se ve forzado a reconciliarse con la violencia de su pasado. </info>
 
<page>5</page><title>CLIFFORD EL GRAN PERRO ROJO</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>e831730c0020553227eebd53c1de3f18515b16be</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/6t05L4Ckj3OGX1jrHEbJhn2hm3Q.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/zBkHCpLmHjW2uVURs5uZkaVmgKR.jpg</fanart>
<date>2021</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Cuando Emily Elizabeth conoce a un rescatador mágico de animales que le regala un pequeño cachorro rojo, nunca se hubiera imaginado que al despertarse se encontraría un sabueso gigante de tres metros en su pequeño apartamento de Nueva York. Mientras su madre soltera se encuentra de viaje de negocios, Emily y su divertido pero impulsivo tío Casey se embarcan en una gran aventura.</info>
 
<page>5</page><title>COCO</title>
<microhd>NA</microhd>
<fullhd>657c3322830369ddb9bb1c13f4f01b1ed66251bd</fullhd>
<tresd>71a6910960a85e47bd4a0f699b84f9b48d816b88</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/oburjHqzOrTtDLYbLunKLwUHhk1.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/askg3SMvhqEl4OL52YuvdtY40Yb.jpg</fanart>
<date>2017</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Miguel es un joven con el sueño de convertirse en leyenda de la música a pesar de la prohibición de su familia. Su pasión le llevará a adentrarse en la "Tierra de los Muertos" para conocer su verdadero legado familiar</info>
 
<page>5</page><title>CODA: LOS SONIDOS DEL SILENCIO</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>3ab0dfcc83ba9baad9c60f7917fd938a5c633603</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/vfS10F7cJb1mMNbD6dbY7CYHCVl.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/4Tz8V8aRim8cFgKEWprSUjBy8tY.jpg</fanart>
<date>2022</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Ruby (Emilia Jones) es el único miembro oyente de una familia de sordos. Ella es una CODA -child of deaf adult-. A sus 17 años, trabaja por la mañana con sus padres y su hermano en Gloucester, Massachusetts, antes de ir a clase, tratando de mantener a flote el negocio pesquero familiar. Ávida de encontrar nuevas aficiones, Ruby decide probar suerte en el coro de su instituto, donde no sólo descubre una latente pasión por el canto, sino también una fuerte atracción física por el chico con el que debe realizar un dueto. Su entusiasta profesor (Eugenio Derbez) ve algo especial en ella y la anima a que piense en la posibilidad de entrar en la escuela de música, algo que la obligaría a tener que tomar una decisión de cara a su futuro: o sus estudios, o su familia. </info>
 
<page>5</page><title>CODIGO 8</title>
<microhd>b0882acb0fc7e34a82b0419b3a6318f017e607b0</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/a3NEbPDzxz14pvmzUvp0uYDqNG0.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/lNJ88bfIirpixIewMprT3RhWBfJ.jpg</fanart>
<date>2019</date>
<genre>Ciencia ficcion</genre>
<extra>NA</extra>
<info>Un hombre desesperado que posee poderes especiales se enfrenta a la policía tras cometer un delito menor. Adaptación al largometraje del corto homónimo dirigido por Jeff Chan en 2016. </info>
 
<page>5</page><title>CODIGO BANSHEE</title>
<microhd>NA</microhd>
<fullhd>4628baaf58179a171557aa97275808e666c8604d</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/h1r6A3fTe2urhf9ZTdgY0KAhOwc.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/ttkibtcAjoilW1PbTIFy9U9YOdB.jpg</fanart>
<date>2022</date>
<genre>Acción. Thriller</genre>
<extra>NA</extra>
<info>Banshee es una letal asesina que trata de localizar a Caleb (Antonio Banderas), que fue su mentor y actualmente vive oculto. La situación es de vida o muerte ya que Anthony Greene, el poderoso mercenario que mató al padre de Banshee y mejor amigo de Caleb, persigue a Caleb con un ejército de asesinos a sueldo, a los que Banshee trata de hacer frente con tal de protegerlo.</info>
 
<page>5</page><title>CODIGO EMPERADOR</title>
<microhd>40bbfe1b2f89e090f9e9a2e8d0352d1ad36c611d</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/qWPfb3LypRYBu2PFuzfWhZe8Yzk.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/wrxo3DkBvFh9Z0Pwhe7E14hBSVP.jpg</fanart>
<date>2022</date>
<genre>Thriller. Intriga</genre>
<extra>NA</extra>
<info>Juan trabaja para los servicios secretos; con el fin de tener acceso al chalet de una pareja implicada en el tráfico de armas se acerca a Wendy, la asistenta filipina que vive en la casa y establece con ella una relación que se irá volviendo cada vez más compleja. En paralelo, Juan realiza otros trabajos “no oficiales” para proteger los intereses de las élites más poderosas del país, que ahora han puesto sus ojos en Ángel González, un político aparentemente anodino cuyos trapos sucios deberá buscar o “inventar”.</info>
 
<page>5</page><title>COLD WAR</title>
<microhd>24c84b9a4628e1b79444657f866249781c5992c4</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/eAspFSsmOwI0NWsyaEVYJBuM8xz.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/rOcw5VX3is3TiZhmxLGdYqBcTcP.jpg</fanart>
<date>2018</date>
<genre>Romance. Drama. Musical</genre>
<extra>NA</extra>
<info>Con la Guerra Fría como telón de fondo, “Cold War” presenta una apasionada historia de amor entre dos personas de diferente origen y temperamento que son totalmente incompatibles, pero cuyo destino les condena a estar juntos.</info>
 
<page>5</page><title>COLETTE</title>
<microhd>6192686133344262ef38245b4120a00735da4e30</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/rpk4yuS1Pe4nPCzIcj5MD3w2obq.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/t2xCXhMjmjM3xT19aTBYppqSW1g.jpg</fanart>
<date>2018</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Cuenta la historia de Sidonie-Gabrielle Colette (Keira Knightley), autora de las polémicas novelas que causaron gran revuelo en el París de los años 20 "Claudine" y "Gigi", desde su infancia en el campo hasta su consagración en la sociedad parisina junto a su marido, el también autor Henry Gautheir-Villas “Willy” (Dominic West), que en un principio actúa como mentor de Colette.</info>
 
<page>5</page><title>COLOR OUT OF SPACE</title>
<microhd>4e7f96b257ab6d2c28da11a8fbdd9fb292d8a505</microhd>
<fullhd>924f3babc7d8e36aec80e28e592256f42409228e</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/2NI8LEElrEmgpVHt4G3GSIkRAkn.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/vj2gCvD5vZDJ865izTU0J0wJBVc.jpg</fanart>
<date>2019</date>
<genre>Terror. Ciencia ficción. Intriga</genre>
<extra>NA</extra>
<info>Un meteorito se estrella cerca de la granja de los Gardner, liberando un organismo extraterrestre que convierte la tranquila vida rural de la familia en una pesadilla colorista y alucinógena. Uno de los relatos más emblemáticos de Lovecraft llega al cine de la mano de Nicolas Cage y Richard Stanley.</info>
 
<page>5</page><title>COMBATE EN EL CIELO</title>
<microhd>43ea5f91477bb96521634f42f69023cca031fd90</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ve7xmUK167YZloN8LnSyhKmmNgE.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/fhYWAKXeEbhD20S3sp4RFaxiOG9.jpg</fanart>
<date>2018</date>
<genre>Belico. Accion. Drama</genre>
<extra>NA</extra>
<info>Un coronel norteamericano (Bruce Willis) entrena a pilotos chinos para que combatan contra el ejército japonés. El temperamento de algunos hace que su trabajo sea más complicado de lo que había previsto cuando le asignaron la misión, pero no por ello pierde la confianza en los jóvenes soldados que le han sido asignados. Mientras tanto, un pequeño grupo de espías y de refugiados transportan un descodificador a través de zonas rurales devastadas con la esperanza de que sea el artilugio definitivo para que la contienda llegue a su fin.</info>
 
<page>5</page><title>COMO ENTRENAR A TU DRAGON</title>
<microhd>3911a1ee333e32e4cc248b700d8ee154ea8f81ee</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/jjGyBXyhXs2aofZDVOf1zguTrvJ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/kxklJL1v8MYEU5xdU6W5VvmBwVz.jpg</fanart>
<date>2010</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Ambientada en el mítico mundo de los rudos vikingos y los dragones salvajes, y basada en el libro infantil de Cressida Cowell, esta comedia de acción narra la historia de Hipo, un vikingo adolescente que no encaja exactamente en la antiquísima reputación de su tribu como cazadores de dragones. El mundo de Hipo se trastoca al encontrar a un dragón que le desafía a él y a sus compañeros vikingos, a ver el mundo desde un punto de vista totalmente diferente.</info>
 
<page>5</page><title>COMO ENTRENAR A TU DRAGON 2</title>
<microhd>a39daeb7157e9ab8a0c1f3a7b38c12f5e662ffc6</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/bXcRw7FL8QzUZOvY2fZdwDv5ymQ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/9q0HTEzChHgUqVzNnVJGU5O7tlM.jpg</fanart>
<date>2014</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Han pasado cinco años desde que Hipo empezó a entrenar a su dragón, rompiendo la tradición vikinga de cazarlos. Astrid y el resto de la pandilla han conseguido difundir en la isla un nuevo deporte: las carreras de dragones. Mientras realizan una carrera, atraviesan los cielos llegando a territorios inhóspitos, donde nadie antes ha estado. Durante un viaje descubren una cueva cubierta de hielo que resulta ser el refugio de cientos de dragones salvajes, a los que cuida un misterioso guardián. Hipo y los suyos se unen al guardián para proteger a los dragones de las fuerzas malignas que quieren acabar con ellos.</info>
 
 
<page>5</page><title>COMO ENTRENAR A TU DRAGON 3</title>
<microhd>d59ab4cb781ff5b1ef7ace036a5a37b2318e26b6</microhd>
<fullhd>NA</fullhd>
<tresd>fe112fd12fea3105c234604aab6c88ff2fa88f7d</tresd>
<cuatrok>c25005cd40908452de35a765beb7d1280922df1d</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/oW75ilbEjKc5s72zxiV51cbogfi.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/h3KN24PrOheHVYs9ypuOIdFBEpX.jpg</fanart>
<date>2019</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Lo que comenzó como la inesperada amistad entre un joven vikingo y un temible dragón Furia Nocturna se ha convertido en una épica trilogía que ha recorrido sus vidas. En esta nueva entrega, Hipo y Desdentao descubrirán finalmente su verdadero destino: para uno, gobernar Isla Mema junto a Astrid; para el otro, ser el líder de su especie. Pero, por el camino, deberán poner a prueba los lazos que los unen, plantando cara a la mayor amenaza que jamás hayan afrontado... y a la aparición de una Furia Nocturna hembra.</info>
 
<page>5</page><title>COMO SOBREVIVIR EN UN MUNDO MATERIAL (KAJILLIONAIRE)</title>
<microhd>NA</microhd>
<fullhd>2c3ee894d600bdc8647cf49473fb6ce7088e634f</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/puTbQrgUjp6i0wXcDQFSxQhK4Ff.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/llMc0bO5G8jpkMqqcLNy7Y8LYBf.jpg</fanart>
<date>2020</date>
<genre>Drama. Comedia </genre>
<extra>NA</extra>
<info>Theresa (Debra Winger) y Robert (Richard Jenkins) son dos estafadores profesionales que llevan 26 años formando a su única hija, Old Dolio (Evan Rachel Wood), para timar, estafar y robar a cada oportunidad que se le presente. Durante un golpe mal planificado y a la desesperada, la familia convence a una desconocida (Gina Rodriguez) para que se una a su siguiente fraude, algo que sin duda acabará poniendo patas arriba todo su mundo.</info>
 
<page>5</page><title>CON FALDAS Y A LO LOCO</title>
<microhd>56cb89f54bd11794fc9021995113a6f05532eb73</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/vwTSGBUsexhpyVKyhtV4cHA6nMD.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/iSbhQqeDnkvFuP7QWwy0qeX0LYy.jpg</fanart>
<date>1959</date>
<genre>Comedia</genre>
<extra>Culto</extra>
<info>Época de la Ley Seca (1920-1933). Joe y Jerry son dos músicos del montón que se ven obligados a huir después de ser testigos de un ajuste de cuentas entre dos bandas rivales. Como no encuentran trabajo y la mafia los persigue, deciden vestirse de mujeres y tocar en una orquesta femenina. Joe (Curtis) para conquistar a Sugar Kane (Monroe), la cantante del grupo, finge ser un magnate impotente; mientras tanto, Jerry (Lemmon) es cortejado por un millonario que quiere casarse con él.</info>
 
<page>5</page><title>CON LA MUERTE EN LOS TALONES</title>
<microhd>e31b4f48a648f5635c7d5cc6364db1b102fb06f4</microhd>
<fullhd>e31b4f48a648f5635c7d5cc6364db1b102fb06f4</fullhd>
<tresd>NA</tresd>
<cuatrok>10bcf922afcbcc324b769d81e8631ec0ae5a7602</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/8r81l0fxwB5uxlAHzVXZ6IhO0o9.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ukJZb0By1JCfR436RAbAQ1O1mMK.jpg</fanart>
<date>1959</date>
<genre>Intriga</genre>
<extra>Culto</extra>
<info>Debido a un malentendido, a Roger O. Thornhill, un ejecutivo del mundo de la publicidad, unos espías lo confunden con un agente del gobierno llamado George Kaplan. Secuestrado por tres individuos y llevado a una mansión en la que es interrogado, consigue huir antes de que lo maten. Pero cuando al día siguiente regresa a la casa acompañado de la policía, le espera una sorpresa.</info>
 
<page>5</page><title>CON TODOS LOS HONORES</title>
<microhd>06a3d06acf9ac4fb3fe6913ab49a1bc751b023a2</microhd>
<fullhd>0668f17f90b5ba2cbb5e72b7748a3f6e29b6055d</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/LxLwlh4cP3HNj8dnHqlf5BUCpZ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/nou1GMynVfaiNbvjTxx2VdFHJJ.jpg</fanart>
<date>2019</date>
<genre>Bélico. Drama</genre>
<extra>NA</extra>
<info>Narra la historia del paramédico William H. Pitsenbarger, que salvó él solo a más de 60 personas en la guerra del Vietnam, y de la investigación de un miembro del Pentágono, Scott Huffman (Sebastian Stan), que trabajó contra viento y marea superando todos los obstáculos para que se honrara al médico con la medalla al valor.</info>
 
<page>5</page><title>CONTRATIEMPO</title>
<microhd>1adb21cd676d107cc8dd6665d6d3cadd3f55ff4f</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/2aDS5gmgvcw7P7BfoxqtnIktHDk.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/k0KEzkdLHtebX4Jhx7uXJqz34wQ.jpg</fanart>
<date>2016</date>
<genre>Thriller. Intriga</genre>
<extra>NA</extra>
<info>Adrián Doria, un joven y exitoso empresario, despierta en la habitación de un hotel junto al cadáver de su amante. Acusado de asesinato, decide contratar los servicios de Virginia Goodman, la mejor preparadora de testigos del país. En el transcurso de una noche, asesora y cliente trabajarán para encontrar una duda razonable que le libre de la cárcel.</info>
 
<page>5</page><title>COUNTDOWN. LA HORA DE TU MUERTE</title>
<microhd>60dd820d8ed57828f07216ac07f971c3479fbd43</microhd>
<fullhd>76c45e06e0661dcec21152612501c754161665cd</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/jYwCTKSmvAey3PDvrj7TCZuso5q.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/zETkzgle7c6wAeW11snnVUBp67S.jpg</fanart>
<date>2019</date>
<genre>Terror. Thriller</genre>
<extra>NA</extra>
<info>Quinn, una joven enfermera, descarga una aplicación para el móvil llamada 'Countdown', que puede predecir el momento exacto en el que una persona va a morir. En ese momento descubre que a ella sólo le quedan tres días de vida. Con el tiempo jugando en su contra y tras ser perseguida por una persona desconocida, tratará desesperadamente de burlar al destino antes de que se le agote el tiempo.</info>
 
<page>5</page><title>CREED 2</title>
<microhd>047241a768d01d5db0e94863091f0ac387737f2c</microhd>
<fullhd>61ceff04636e1393dc35def3456bae63788a7e22</fullhd>
<tresd>NA</tresd>
<cuatrok>7402d7205cda716cc5a980cb6620bdffa8e1b142</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/2t53WEu1Ck7qAMS3theMnQK2N2c.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/2t53WEu1Ck7qAMS3theMnQK2N2c.jpg</fanart>
<date>2018</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Adonis Creed se debate entre las obligaciones personales y el entrenamiento para su próxima gran pelea, con el desafío de su vida por delante. Enfrentarse a un oponente que tiene vínculos con el pasado de su familia solo intensifica su inminente batalla en el ring. Afortunadamente Rocky Balboa está a su lado a lo largo de todo el camino, y juntos se cuestionarán por lo que vale la pena luchar y descubrirán que nada es más importante que la familia... Secuela de "Creed", el spin-off de Rocky que en 2015 obtuvo muy buenas críticas y dio inicio a una nueva saga.</info>
 
<page>5</page><title>CREED III</title>
<microhd>NA</microhd>
<fullhd>7e3e06cfdb82de14296123d31811f929a44b4f70</fullhd>
<tresd>NA</tresd>
<cuatrok>76d878ece23180bf67fd956aa24c8a05020da03e</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/fcFMd3HdyX7r5gtFwVnn2qr5Yhq.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/5i6SjyDbDWqyun8klUuCxrlFbyw.jpg</fanart>
<date>2023</date>
<genre>Drama </genre>
<extra>NA</extra>
<info>Después de dominar el mundo del boxeo, Adonis Creed ha progresado tanto en su carrera como en su vida familiar. Cuando Damian (Jonathan Majors), un amigo de la infancia y antiguo prodigio del boxeo, reaparece después de cumplir una larga condena en prisión, Adonis Creed quiere demostrar que merece una oportunidad en el ring. El enfrentamiento entre estos antiguos amigos es algo más que una simple pelea. Para ajustar cuentas, Adonis debe arriesgar su futuro para enfrentarse a Damian, un boxeador que no tiene nada que perder. Tercera entrega de la revitalizada franquicia 'Rocky' para MGM, en la que Jordan retoma su papel de "Adonis Creed".</info>
 
<page>5</page><title>CRIMENES DEL FUTURO</title>
<microhd>NA</microhd>
<fullhd>42cb9be3e29847f5889acba1d4871e723cec07f8</fullhd>
<tresd>NA</tresd>
<cuatrok>1080348d8b591f831708cdc01857fff90b3e1136</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/6UA0pEWBy9KpbVghhHoWRex7Wk2.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/sqdsuvy8X6Maila4IAc7deMtPAA.jpg</fanart>
<date>2022</date>
<genre>Ciencia ficción. Fantástico. Terror</genre>
<extra>NA</extra>
<info>Cuando la especie humana se adapta a un entorno artificial, el cuerpo humano es objeto de nuevas transformaciones y mutaciones. Con la ayuda de su compañera Caprice (Léa Seydoux), Saul Tenser (Viggo Mortensen), célebre artista performativo, escenifica la metamorfosis de sus órganos en espectáculos de vanguardia. Timlin (Kristen Stewart), una investigadora de la Oficina del Registro Nacional de Órganos, sigue de cerca sus prácticas. Es entonces cuando un grupo misterioso aparece: desean aprovechar la fama de Saul para revelar al mundo la próxima etapa de la evolución humana…</info>
 
<page>5</page><title>CRISIS</title>
<microhd>40acabb80fff7c297c68a80b3bec1f3dd54c4fee</microhd>
<fullhd>cab16419619236e178abde9de4bb64fdafdba059</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ao4JajLcPITivfstlzENge3MPkq.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/gFDOQMQwo5UWegXE5kNEchn1eKx.jpg</fanart>
<date>2021</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Tres historias sobre el mundo de los opiáceos chocan entre sí: un traficante de drogas planea una operación que implica a varios cárteles entre Canadá y los Estados Unidos; una arquitecta que se recupera de una fuerte adicción al OxyContin descubre la verdad que esconde su hijo de su relación con la droga; un profesor de universidad trata de averiguar qué relación une al gobierno con una compañía farmacéutica que está diseñando un nuevo analgésico que aseguran no "provoca ninguna adicción".</info>
 
 
<page>5</page><title>CRONICAS DE NAVIDAD</title>
<microhd>690e0ff3d86e8f6ccc1f91898ddf830400be00b0</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/qz2dTKSQ9VSAswb1O9jWS15jqL1.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/vUbWBVHngojXOu1kBgH6xFkZZTA.jpg</fanart>
<date>2018</date>
<genre>Comedia.Infantil Familiar</genre>
<extra>NA</extra>
<info>Kate Pierce (Darby Camp) y su hermano Teddy (Judah Lewis) pretenden grabar a Papá Noel (Kurt Russell) en Nochebuena. Después de que se monten a escondidas en su trineo, provocan un accidente que podría arruinar toda la Navidad. A partir de ese momento los hermanos vivirán junto a Papá Noel y sus fieles elfos toda una serie de aventuras para salvar la Navidad antes de que sea demasiado tarde.</info>
 
 
<page>5</page><title>CRUELLA</title>
<microhd>4c893416eee51a60302c5688962c807741873497</microhd>
<fullhd>5f1512feb8fba9ea1914c1a1eae1ee2a7e946894</fullhd>
<tresd>NA</tresd>
<cuatrok>599f760ce492a3577a73f3d44ca74f5bb5f67143</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/qRHphMIGRVQmdCVKnEdjwFyuk9V.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/6MKr3KgOLmzOP6MSuZERO41Lpkt.jpg</fanart>
<date>2021</date>
<genre>Comedia </genre>
<extra>NA</extra>
<info>Explora los rebeldes comienzos de una de las villanas más conocidas del cine y famosa por su elegancia: Cruella de Vil. Ambientada en el Londres de los años 70, en plena revolución del punk rock, la película muestra a una joven estafadora llamada Estella, así como la serie de acontecimientos que la llevan a asumir su lado malvado y a convertirse en la estridente y vengativa Cruella.</info>
 
 
<page>5</page><title>CRY MACHO</title>
<microhd>5ba13f95d6f9e8614710f1139874036a7972a630</microhd>
<fullhd>4a6085067898bdb0e48f7f8f17f4d44939cad92f</fullhd>
<tresd>NA</tresd>
<cuatrok>08736fb636a9433f9e463c3091fccead4fd2861f</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/cXoLeeBdNRwmWpLgHkekhdehlaa.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/g6wufgtycJCP508tlC3crSYFCgC.jpg</fanart>
<date>2021</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Texas, 1978. Una ex estrella de rodeo y criador de caballos retirado (Eastwood) acepta un encargo de un antiguo jefe: traer a su hijo pequeño desde México de vuelta a casa para alejarlo de su madre alcohólica. En el viaje, ambos se embarcarán en una inesperada aventura.</info>
 
 
<page>5</page><title>CRYPTO</title>
<microhd>d113379a6417762e0275086d9119c87cdef1c9fc</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/eSeYalxGSsB69rJj4EVy6W4HI7g.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/1s9fMh2TALuBqGSc0wET8HwUEf8.jpg</fanart>
<date>2019</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Martin es uno de los agentes del FBI más prometedores de todo Wall Street. Sin embargo, tras una disputa con sus jefes, el joven debe trasladarse a su ciudad natal y trabajar desde allí. Al poco tiempo, Martin comprende que su relocalización ha sido de todo menos una coincidencia. </info>
 
 
<page>5</page><title>CUANDO ERAMOS SOLDADOS</title>
<microhd>NA</microhd>
<fullhd>387cdbf142a046f225268e0733dd4f45a3972366</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/lo284scuFbx1Zz4Tv5yJ3ctNSIJ.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/ev3D1Y6SSvKnHZLV1RjTRJ9u2np.jpg</fanart>
<date>2002</date>
<genre>Bélico. Acción</genre>
<extra>Mis</extra>
<info>En plena guerra del Vietnam, el coronel Hal Moore (Mel Gibson) y sus soldados (unos 400 hombres) aterrizaron en noviembre de 1965 en una región conocida como "El valle de la muerte". Allí fueron recibidos por más de 2.000 soldados del Vietcong, desencadenándose una de las batallas más feroces de la guerra. </info>
 
 
<page>5</page><title>CUANDO TERMINES DE SALVAR EL MUNDO</title>
<microhd>NA</microhd>
<fullhd>506537ac2653d89496a0820b5d1e7ae87e10dc20</fullhd>
<tresd>NA</tresd>
<cuatrok>275f1766552b29e1b4b13adbc344acf2024e2402</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/6piyB7y6glrXSgd2o5nB7RdpIxZ.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/a0xHxLTsyoCiejCZFZEoH42FuEx.jpg</fanart>
<date>2022</date>
<genre>Comedia. Drama</genre>
<extra>NA</extra>
<info>Evelyn y su inconsciente hijo Ziggy buscan sustitutos el uno para el otro, ya que Evelyn trata desesperadamente de criar a un adolescente sin pretensiones en su refugio, mientras que Ziggy se desvive por una joven brillante en la escuela.</info>
 
 
<page>5</page><title>CUBE</title>
<microhd>47a53c4f363d177a25089a39a273f25fce710842</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/eUgfqHKTiiHDf9uYBADLLvoIec6.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/hO9nTzou529IUI8N6fjC5SfvYbm.jpg</fanart>
<date>1997</date>
<genre>Ciencia ficción. Terror. Thriller. Intriga. Saga</genre>
<extra>Culto</extra>
<info>Seis personas aparecen encerradas en un complejo laberinto de habitaciones cúbicas que esconde trampas mortales. No saben cómo llegaron allí, pero pronto descubren que deberán resolver ciertos enigmas y sortear con habilidad todas las trampas si quieren sobrevivir.</info>
 
 
<page>5</page><title>CUBE ZERO</title>
<microhd>83a22ef5b7841a8201a93417ec0c110c8e091176</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/vWm244ZX9OeDBw5f0MAc2roeATb.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/8xuCer3BVGsMS1pMFMlE0irdMmJ.jpg</fanart>
<date>2004</date>
<genre>Ciencia ficción. Intriga. Terror. Saga</genre>
<extra>NA</extra>
<info>Precuela de "Cube". Rains se despierta en un laberinto de habitaciones perfectamente cúbicas. Lleva un extraño uniforme gris y un código de barras tatuado en su mano. No recuerda nada, ni su nombre. Pronto encontrará otros prisioneros del laberinto que están en su misma situación. A medida que se van desplazando por los cubos, irán muriendo uno a uno en las trampas que irán encontrando. Pero Rains tiene un admirador secreto, Wynn, que forma parte del grupo de observadores cuyo trabajo es controlar a los prisioneros. Se siente atraído por ella e intentará ayudarla. En el universo del cubo nadie está a salvo. Los observadores son observados y los prisioneros huyen en busca de una salida que no saben dónde les llevará.</info>
 
 
<page>5</page><title>CUBE²: HYPERCUBE</title>
<microhd>e1809e4847cfd2241e6985461f32fcdc802cd089</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/bEqqwtwUP7lm56VyeVONhv9JtYu.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/dFH8vmJhOC7i7jzswlM5UFR4zmv.jpg</fanart>
<date>2002</date>
<genre>Terror. Thriller. Ciencia ficción. Saga</genre>
<extra>NA</extra>
<info>Secuela de la exitosa película de culto "Cube" (1997), de Vincenzo Natali. Ocho extraños se despiertan en una habitación con forma de cubo, sin tener ni idea de cómo ni cuando llegaron allí. Pronto descubren que están en una especie de cuarta dimension donde no rigen las leyes de la física, y tendrán que descubrir los misterios del "hypercubo" si quieren sobrevivir... </info>
 
 
<page>5</page><title>CUENTA PENDIENTE</title>
<microhd>25fa20a89da313fb418f080055154b3abd880b0c</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/pQiXbt7Rm12BidjWi655TLcDr6k.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/arYmRZmKN1D9j6NPclq021J5aYP.jpg</fanart>
<date>2018</date>
<genre>Accion</genre>
<extra>NA</extra>
<info>El único superviviente de un grupo de atracadores que trató de robar un camión blindado es extraído de su celda para ser llevado a un centro experimental en el que se convierte en un conejillo de indias para probar una nueva y potente droga.</info>
 
 
<page>5</page><title>CUENTOS AL CAER LA NOCHE</title>
<microhd>7c3a09c616cc55bd8510f1bcf5ff8e637917a69a</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/2R9nfnwJ0N4wLCoLznI2nNUidZI.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/piz0BFUJ4Bro4BFzxxkNpsGCmOd.jpg</fanart>
<date>2021</date>
<genre>Terror. Comedia. Fantástico</genre>
<extra>NA</extra>
<info>Cuando Alex (Winslow Fegley), un chico obsesionado con las historias de miedo, es encerrado por una joven bruja malvada (Krysten Ritter) en su moderno apartamento de Nueva York, conoce a Yasmin (Lydia Jewett), que también está atrapada y se da cuenta de que debe contar una historia de miedo nueva cada noche para sobrevivir.</info>
 
 
<page>5</page><title>CUESTION DE JUSTICIA</title>
<microhd>aa02b804c1ce878d59a5d1f948931ca4df83ea22</microhd>
<fullhd>fc74503559c739a9f6cb04be07a86b53af7e883e</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/5vz4biYDRFWlWn7aAdOzpQzmVkA.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/zNqsGEWHlXwFw8wod0HDkgrvsl8.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Cuenta la historia real del joven abogado Bryan Stevenson (Jordan) y de su histórica batalla por la justicia. Después de licenciarse en Harvard, Bryan recibe ofertas de trabajo muy lucrativas. Pero él prefiere poner rumbo a Alabama para defender a personas que han sido condenadas erróneamente o que carecían de recursos para tener una representación legal adecuada, y lo hace con el apoyo de la activista local Eva Ansley (Larson). Uno de sus primeros y más turbulentos casos es el de Walter McMillian (Foxx), que en 1987 fue sentenciado a la pena muerte por el asesinato de una chica de 18 años, a pesar de que las pruebas demostraban ostensiblemente su inocencia. En los años posteriores, Bryan se ve envuelto en un laberinto de maniobras legales y políticas y de un racismo abierto y descarado mientras lucha por Walter y otros como él, a pesar de tenerlo todo en su contra, incluido el sistema legal.</info>
 
 
<page>5</page><title>CUESTION DE SANGRE</title>
<microhd>0AE3086FB9939F7739943CE64B99539B40E2249F</microhd>
<fullhd>ce13fde18685e721c5318ee661ae04c4c6048687</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/6UecisHYDLsRGc9XWx057accTfR.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/ckCrunvxJ1SqL1w4goJGGGZ5PGP.jpg</fanart>
<date>2021</date>
<genre>Drama. Thriller</genre>
<extra>NA</extra>
<info>Bill Baker (Matt Damon), un rudo operario de una plataforma petrolífera estadounidense, viaja a Marsella para visitar a su hija, que está en prisión por un asesinato que afirma no haber cometido. Lejos de casa, las cosas no serán nada fáciles para un padre dispuesto a todo para demostrar la inocencia de su hija.</info>
 
 
<page>5</page><title>CULPA MIA</title>
<microhd>NA</microhd>
<fullhd>tbp3j3t642dgoyshrzpjmjxwapatit3z</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/e2XUzlj6SzD4jL8ldfn3lkMsFch.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/lntyt4OVDbcxA1l7LtwITbrD3FI.jpg</fanart>
<date>2023</date>
<genre>Romance. Drama </genre>
<extra>NA</extra>
<info>Noah debe dejar su ciudad, novio y amigos para mudarse a la mansión del nuevo marido de su madre. Allí conoce a su nuevo hermanastro Nick y sus personalidades chocan desde el primer momento. Pero la atracción que sienten les llevará a vivir una relación prohibida, donde el carácter rebelde y atormentado de cada uno de ellos pondrá del revés sus mundos haciendo que acaben perdidamente enamorados. Adaptación del primer libro de la trilogía "Culpables" de Mercedes Ron.</info>
 
 
<page>5</page><title>CYRANO</title>
<microhd>NA</microhd>
<fullhd>e04d646252a9c141724bdf47c935ea644e7173b7</fullhd>
<tresd>NA</tresd>
<cuatrok>7630316314a357f58ebc8ae5ac0c544b1fe517fe</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/32nYknMmMN3tvVYSy98uth7SbKY.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/wbF4Vgv5Ab80YosIziAvun6Fdgq.jpg</fanart>
<date>2022</date>
<genre>Musical. Drama. Romance</genre>
<extra>NA</extra>
<info>Película que narra la vida de Cyrano de Bergerac, novelista y dramaturgo francés del siglo XVII.</info>
 
 
<page>5</page><title>DA 5 BLOODS: HERMANOS DE ARMAS</title>
<microhd>11c9962c9e080c4c8b847e9bf623cf04b2bffb14</microhd>
<fullhd>dd8efc6fe525294fdac92eb96d5f53fb7c10a4af</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/6Peu8K4UUlmpTqvIQKLktKidhf4.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/Aq5Zhj9iaTF6BEKNk05dlUxeHKa.jpg</fanart>
<date>2020</date>
<genre>Bélico. Drama</genre>
<extra>NA</extra>
<info>Cuatro excombatientes afroamericanos -Paul (Delroy Lindo), Otis (Clarke Peters), Eddie (Norm Lewis) y Melvin (Isiah Whitlock, Jr.)- regresan a Vietnam en busca de los restos del jefe de su escuadrón, caído en combate (Chadwick Boseman), y la promesa de un tesoro escondido. Junto al hijo de Paul (Jonathan Majors), se enfrentarán a obstáculos naturales y humanos, y constatarán los estragos causados por la inmoralidad de la guerra de Vietnam.</info>
 
 
<page>5</page><title>DC LIGA DE SUPERMASCOTAS</title>
<microhd>NA</microhd>
<fullhd>f7efd55f1906a59c723e3cce0f0fe5201d96ecbf</fullhd>
<tresd>NA</tresd>
<cuatrok>7be8a64cc4ab367dc0a54eda8a8b3c7e0c7865ba</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/hnvEEIanHDzPDV6xSUkGXv9YI9l.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/oRHGLlCHb9qeU8hpK07cLL7PRAm.jpg</fanart>
<date>2022</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Krypto el Superperro y Superman son amigos inseparables que comparten los mismos superpoderes y luchan juntos contra el crimen en Metrópolis. Cuando Superman y el resto de la Liga de la Justicia son secuestrados, Krypto debe convencer a un variopinto grupo de un albergue –Ace el sabueso, PB la cerdita barrigona, Merton la tortuga y Chip la ardilla– de dominar sus nuevos poderes y ayudarlo a rescatar a los superhéroes.</info>
 
 
<page>5</page><title>DE AMOR Y MONSTRUOS</title>
<microhd>bb88493bae7b2aaec9abaac5d3b2e4877ae01154</microhd>
<fullhd>4c7d3ec9b692af1cd957db0939436b5e67265bbb</fullhd>
<tresd>NA</tresd>
<cuatrok>4284be2a87b97988533ce47b5c093db402a5e8c4</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/jtMNOMkCkj0hkm3ubWtd5rt1N5s.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/3Y7lAFEdpk3a25LFE1xzqTaGIXi.jpg</fanart>
<date>2020</date>
<genre>Aventura</genre>
<extra>NA</extra>
<info>Un joven adolescente aprende a sobrevivir en un mundo postapocalíptico repleto de monstruos con la ayuda de un experto cazador.</info>
 
 
<page>5</page><title>DE CAPERUCITA A LOBA</title>
<microhd>NA</microhd>
<fullhd>6eb3a279b5de2edc097fcc9a2e9024159fb7e9af</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/3XmCuwIwriQMMaHmkfh7T756Htd.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/hwV7KQXbia0mNpcGHX3b3SnLsl8.jpg</fanart>
<date>2023</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Marta consigue pasar de Caperucita a loba cuando decide usar cada situación patética en que la pone el amor para aprender a reírse de sí misma más fuerte que nadie. Internándose en el bosque del drama, llegará a convertirse en la protagonista de su propia comedia al descubrir que del drama brota la risa y de la risa, ¡el poder!</info>
 
 
<page>5</page><title>DEADWOOD</title>
<microhd>fe6fd868511423fef75c78052b649f686744933f</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/fCQ13cqd02dop7Chkov0fPl4r3t.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/h5pWclqlDhRmkfKHCD101YmS8u1.jpg</fanart>
<date>2019</date>
<genre>Western. Accion</genre>
<extra>NA</extra>
<info>Ubicada argumentalmente 10 años después del final de la tercera temporada de la serie "Deadwood", que finalizó su emisión en 2006, las rivalidades pasadas se reinician, las alianzas se ponen a prueba y las viejas heridas se reabren, ya que todos se quedan para navegar por los cambios inevitables que la modernidad y el tiempo han provocado.</info>
 
 
<page>5</page><title>DEEP BLUE SEA 3</title>
<microhd>76116683ba506781c7a8454d72fc6847003d93f7</microhd>
<fullhd>0de5e404a4bba184927f56b21a59dc7a1481acdd</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/bKthjUmxjHjvJK8FktFfQdmwP12.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/hIHtyIYgBqHybOgUdoAmveipuiO.jpg</fanart>
<date>2020</date>
<genre>Acción</genre>
<extra>NA</extra>
<info>La doctora Emma Collins y su equipo están pasando su tercer verano en la isla Little Happy estudiando el efecto del cambio climático en el gran tiburón blanco. Junto a los dos últimos habitantes del antiguo pueblo pesquero, su vida se ve alterada cuando un equipo "científico" liderado por su exnovio y marino biológico Richard aparece buscando tres tiburones toro que son algo más de lo que aparentan en un principio.</info>
 
 
<page>5</page><title>DEJAME SALIR</title>
<microhd>ed288ddb2ad6031d9488b5ae83094cba2f3eee24</microhd>
<fullhd>9345f3b00ad6ac3411b01f44338d1a884a7aeea7</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/qU1qFOPRyhlOR9aLuNeBMSolZO3.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/5OlAmzEUaO0A12cM7g5g420w4d7.jpg</fanart>
<date>2017</date>
<genre>Intriga. Terror. Thriller</genre>
<extra>NA</extra>
<info>Un joven afroamericano visita a la familia de su novia blanca, un matrimonio adinerado. Para Chris (Daniel Kaluuya) y su novia Rose (Allison Williams) ha llegado el momento de conocer a los futuros suegros, por lo que ella le invita a pasar un fin de semana en el campo con sus padres, Missy (Catherine Keener) y Dean (Bradley Whitford). Al principio, Chris piensa que el comportamiento "demasiado" complaciente de los padres se debe a su nerviosismo por la relación interracial de su hija, pero a medida que pasan las horas, una serie de descubrimientos cada vez más inquietantes le llevan a descubrir una verdad inimaginable.</info>
 
 
<page>5</page><title>DEL REVES (INSIDE OUT)</title>
<microhd>ed9152211d082fb8458b52b400222f8f725b5931</microhd>
<fullhd>NA</fullhd>
<tresd>33d988452086ad0daa9c51227c01e443d408d874</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/sG3bHZWCMOZwhUq71WbPG9Vrrwc.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/9cGSFGZVVLsrnbs0AFPDh6X2ynl.jpg</fanart>
<date>2015</date>
<genre>Infantil</genre>
<extra>Mis</extra>
<info>Riley es una chica que disfruta o padece toda clase de sentimientos. Aunque su vida ha estado marcada por la Alegría, también se ve afectada por otro tipo de emociones. Lo que Riley no entiende muy bien es por qué motivo tiene que existir la Tristeza en su vida. Una serie de acontecimientos hacen que Alegría y Tristeza se mezclen en una peligrosa aventura que dará un vuelco al mundo de Riley.</info>
 
 
<page>5</page><title>DELICIOSO</title>
<microhd>c742e73d30887c01e3b46bb7072ee059ac1388a4</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/dV7Duh1RhNKpxlAJjao2RCbcBeR.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/y50zJUQOP5NmdUj8vsyeSSar14B.jpg</fanart>
<date>2021</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Francia, 1789, justo antes de la Revolución Francesa. Con la ayuda de una mujer inesperada, un chef que ha sido despedido por su jefe encuentra el valor para abrir un restaurante por sí solo. </info>
 
 
<page>5</page><title>DEPRAVED</title>
<microhd>e9e93613fbc02bc64f6c38b8257b4b328d7206e6</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/nRUVpJUcYTbH8xIeLNpMhkJy1Xv.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/4Jow4UWkpITDlhYWdEKvFJOPDS5.jpg</fanart>
<date>2019</date>
<genre>Terror. Thriller</genre>
<extra>NA</extra>
<info>Un cirujano de campo desilusionado que sufre estrés postraumático crea un cuerpo a través de distintas partes de cadáveres humanos para tratar de "crear una vida" en un apartamento de Brooklyn.</info>
 
<page>6</page><title>DESAFIO TOTAL</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>3efe722c2ac42a78bf26f566b8420bcc9f630aba</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/eWNa6zObciJBTEAMDohkKH2Na2J.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/fbyEq9pzdpNSh97vSSVhCkO6urW.jpg</fanart>
<date>1990</date>
<genre>Ciencia ficción. Acción</genre>
<extra>Mis</extra>
<info>La Tierra, año 2084. Doug Quaid, un hombre que lleva una vida aparentemente tranquila, vive atormentado por una pesadilla que todas las noches lo transporta a Marte. Decide entonces recurrir al laboratorio de Recall, una empresa de vacaciones virtuales que le ofrece la oportunidad de materializar su sueño gracias a un fuerte alucinógeno, pero la droga hace aflorar a su memoria una estancia verdadera en Marte cuando era el más temido agente del cruel Coohagen.</info>
 
 
<page>6</page><title>DESAPARECIDA SIN RASTRO</title>
<microhd>NA</microhd>
<fullhd>f354358df8120497d3b606d77e29b0119a7970dd</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/2r5ZISrHQUQLBMdAJF3CDDAxp54.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/3EaCqZdbY2uAMhhw9rQwmHjYC0Y.jpg</fanart>
<date>2022</date>
<genre>Thriller. Intriga</genre>
<extra>NA</extra>
<info>A punto de divorciarse, Will Spann (Butler) conduce a su todavía esposa Lisa (Alexander) a casa de sus padres cuando ella desaparece misteriosamente sin dejar rastro durante una parada en una gasolinera.</info>
 
 
<page>6</page><title>DESENCANTADA VUELVE GISELL</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>ecc44a7852f7636e868c59f1a686594a8637011c</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/6NveXOU3wlkbBrk4lR4noumF1fx.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/kpUre8wWSXn3D5RhrMttBZa6w1v.jpg</fanart>
<date>2022</date>
<genre>Romance. Comedia. Aventuras. Fantástico. Musical.Infantil</genre>
<extra>NA</extra>
<info>Diez años después de su felices para siempre, Giselle cuestiona su felicidad, sin darse cuenta, cambiando la vida de los del mundo real y de los habitantes de Andalasia.</info>
 
 
<page>6</page><title>DESESPERADA</title>
<microhd>db4cf1b4292561d887ff6d005633459bc81d2bc4</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/i1idkdgMXbchkls9JfnKnUTTywb.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/qp8qKiP7Q7zK4z3LItwWMHfV9kJ.jpg</fanart>
<date>2021</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>En medio del bosque que rodea su hogar, a kilómetros de la ciudad y abrumada por el pánico, Amy Carr (Naomi Watts) recibe una llamada terrible: las autoridades están buscando al responsable de un tiroteo que ha tenido lugar en la escuela de su hijo adolescente, Noah. Amy se niega a sucumbir a la desesperación. Con la única ayuda de su móvil, buscará todos los recursos posibles para tratar de lograr la salvación de su hijo.</info>
 
 
<page>6</page><title>DESPIERTA LA FURIA</title>
<microhd>02a1be7ae3bd80bae37c02f6f5334e518ad9fb5f</microhd>
<fullhd>caf022801eade4ac2cb58fa6fa87f4d8b404a96e</fullhd>
<tresd>NA</tresd>
<cuatrok>31037b759704e26613a7301b8a739f5e82c60552</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/fvqIv59EgwY7lJ5QfqSWpCxOFxK.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/70AV2Xx5FQYj20labp0EGdbjI6E.jpg</fanart>
<date>2021</date>
<genre>Thriller. Acción</genre>
<extra>NA</extra>
<info>H (Jason Statham) es el misterioso tipo que acaba de incorporarse como guardia de seguridad en una compañía de furgones blindados. Durante un intento de atraco a su camión, sorprende a sus compañeros mostrando habilidades propias de un soldado profesional, dejando al resto del equipo preguntándose quién es realmente y de dónde viene.</info>
 
 
<page>6</page><title>DESTINO FATAL II</title>
<microhd>NA</microhd>
<fullhd>53acc9e356e8b1c2778ea3b9882b30ee0212ec39</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/6m4TTnatasZcMi6qE4Hbe9LdJgz.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/x9EWK3SbrDGpcpJbkg8VGXr9nHv.jpg</fanart>
<date>2003</date>
<genre>Terror. Saga</genre>
<extra>NA</extra>
<info>Ha pasado un año desde el accidente del vuelo 180. Ahora, un grupo de jóvenes estudiantes que se dirigen a Florida en las vacaciones de primavera, evitan un mortal accidente de tráfico gracias a las premoniciones de uno de los chicos.</info>
 
 
<page>6</page><title>DESTINO FINAL</title>
<microhd>NA</microhd>
<fullhd>8d2de17c26ef61348e974be88b070d811bb2167b</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/omO4gpAlImEQOVVEHRTAIHfmAGK.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/7LU0ryVKd0AkmENyEzCYiGullu5.jpg</fanart>
<date>2000</date>
<genre>Terror. Saga</genre>
<extra>NA</extra>
<info>Un joven estudiante tiene una premonición al subirse al avión en el que se dispone a ir a París con sus compañeros. Presintiendo que algo grave va a pasar, él y sus amigos desembarcan antes del despegue. Efectivamente, el avión sufre un trágico accidente, y los jóvenes piensan que se han librado de una muerte segura gracias a la premonición de su amigo. Pero el destino no ha sido vencido todavía...</info>
 
 
<page>6</page><title>DESTINO FINAL III</title>
<microhd>NA</microhd>
<fullhd>a65d4050a718bb6b4a4851bac4bbd55e847bfd3b</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/icB8NTqRMsTm9UKl6PlDMKJOEZq.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/iiKTyL7jTw5FzVBlhLCu9I7M86w.jpg</fanart>
<date>2006</date>
<genre>Terror. Saga</genre>
<extra>NA</extra>
<info>Una estudiante del instituto (Winstead) tiene una premonición sobre una tragedia en un parque de atracciones local, por lo que decide no montarse en una montaña rusa que presiente va a descarrilar... (</info>
 
 
<page>6</page><title>DESTINO FINAL IV</title>
<microhd>NA</microhd>
<fullhd>ceeb42cc55270ae15a8a36800ba03a080de466ad</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/rvvKHROhxc6pMOfeYqWbUqUZYvS.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/iVJL9RamSBUcW7BzzvU8eHeIsA8.jpg</fanart>
<date>2009</date>
<genre>Terror. Thriller . Saga</genre>
<extra>NA</extra>
<info>Nick O’Bannon y unos amigos acuden a un circuito de carreras para presenciar una prueba del Nascar. Durante ésta tiene lugar un terrible accidente que conlleva desastrosas consecuencias para el estadio. Pero Nick descubre que se trata de sólo una visión de algo que está a punto de suceder, y junto con otras doce personas consigue salir del recinto y escapar de una tragedia segura. Pensando que han burlado a la muerte, el grupo continúa con su vida, pero desafortunadamente para Nick, Lori y sus amigos sólo es el principio, ya que la muerte no deja cabos sueltos, y regresará para llevarse a los supervivientes de una manera brutal.</info>
 
 
<page>6</page><title>DESTINO FINAL V</title>
<microhd>NA</microhd>
<fullhd>ecd73e3bfc1464a02ec13070748349dd5a111672</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/wRzqLektUs0R6i84sjvF2dS69BW.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/g3jvFCHwMxslAuHdgsC1S0wgLrC.jpg</fanart>
<date>2011</date>
<genre>Terror. Thriller. Saga</genre>
<extra>NA</extra>
<info>Sam Lawton (Nicholas D'Agosto) tiene una premonición sobre la destrucción de un puente colgante que causaría su muerte y la de otras personas. La visión se hace realidad, pero Sam se las arregla para salvarse a sí mismo y a algunos otros de la catastrófica tragedia. Sin embargo, Sam y su novia Molly (Emma Bell) descubren que no están realmente a salvo: la muerte los persigue a ellos y a los que sobrevivieron al horrible accidente... Quinta entrega de la popular serie de terror "Destino final". </info>
 
 
<page>6</page><title>DETROIT</title>
<microhd>b73068b76b5b9a7d1f983babb3f9c37070171e7f</microhd>
<fullhd>f95010692ffb9dc92bbb5e4242c06526ad473217</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/iubd2Qh1A5WRHUOOwXC8m1PmzQ5.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/6YgSDUHRA4memDCjyKiwBJS9cNv.jpg</fanart>
<date>2017</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>En julio de 1967, graves disturbios raciales sacudieron la ciudad de Detroit, en el estado de Michigan. Todo comenzó con una redada de la policía en un bar nocturno sin licencia, que acabó convirtiéndose en una de las revueltas civiles más violentas de los Estados Unidos. Los incidentes más graves ocurrieron en el motel Algiers, cuando miembros de la policía y la Guardia Nacional acudieron ante unos disparos de un arma de fogueo.</info>
 
 
<page>6</page><title>DIA DE LLUVIA EN NUEVA YORK</title>
<microhd>85d15e8036d95c545be264217ecaedbc6b397006</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/vKazTdswTtPCGFWLJkoVHX5cxlX.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/vX9VJBdpXWvDTH920AY37xXzNjz.jpg</fanart>
<date>2019</date>
<genre>Romance. Comedia </genre>
<extra>NA</extra>
<info>Gatsby Welles (Timothée Chalamet) y Ashleigh (Elle Fanning) son una joven pareja enamorada de universitarios que se dispone a pasar un fin de semana en la ciudad de Nueva York. Ella va a entrevistar al reconocido cineasta Roland Pollard (Liev Schreiber), que pasa por un momento de crisis creativa, y durante su azarosa aventura conocerá al cautivador actor Francisco Vega (Diego Luna). Por su parte, Gatsby también conocerá a una joven, Chan (Selena Gómez), que le ayudará a poner en orden sus sentimientos. El lluvioso fin de semana estará plagado de encuentros, desencuentros y equívocos.</info>
 
 
<page>6</page><title>DIAMANTES EN BRUTO</title>
<microhd>baa64d562d75c788c920cfe84b37c4d4b3b5850c</microhd>
<fullhd>505c0bdf9c989e884cc20c2de145af3e09a3948d</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/osnO3hkqjyYIfVVwstS2sdfbn1b.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/eGljNfNCrPhFYG2RXXmmE0OKu5.jpg</fanart>
<date>2019</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Howard Ratner es el propietario de una joyería ubicada en el barrio de los diamantes de la ciudad de Nueva York que vende en exclusiva a ricos y famosos. Un día se produce un importante robo que le obliga a tener que afrontar una deuda económica que no está preparado para pagar.</info>
 
 
<page>6</page><title>DIVERGENTE</title>
<microhd>7c26526ae9dd8580a7ad12ca9e9f88108179a781</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>1b936eef2c129b4e67064a35652d0fde108b2eca</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/t36b9yHx1JbrkcpaWPatcDLtEYu.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ipikQozG6cAtLqhaFqRneA0LI4o.jpg</fanart>
<date>2014</date>
<genre>Saga.Ciencia ficcion. Accion. Aventuras. Romance</genre>
<extra>NA</extra>
<info>En un mundo distópico en el que la sociedad se divide en cinco categorías (Verdad, Abnegación, Osadía, Cordialidad y Erudición), los jóvenes deben elegir, atendiendo a sus virtudes personales más destacadas, a qué facción pertenecer. Beatrice sorprende a los suyos con su decisión, pero ella no es como los demás: guarda un secreto del que podría depender el orden social e incluso su propia vida.</info>
 
 
<page>6</page><title>DIVERGENTE: INSURGENTE</title>
<microhd>fe8bd5b681aa45e10da3ed805566e4b8fb2aa5e5</microhd>
<fullhd>eb1237e757c46349cb5a8b9aa511761bd2b99e44</fullhd>
<tresd>NA</tresd>
<cuatrok>acb6155d268eca1d6fef202f0860b3f3f6ec9386</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/jY5sTxd5dROtnINsOUGQvY9CzFn.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/8YNNyQiPZlF9qv5EGOUK20mnXVk.jpg</fanart>
<date>2015</date>
<genre>Saga.Ciencia ficcion. Accion. Aventuras. Romance</genre>
<extra>NA</extra>
<info>Tris (Shailene Woodley) y Cuatro (Theo James) son fugitivos que tratan de eludir la persecución de Jeanine (Kate Winslet), la líder de los eruditos. Tris pretende averiguar qué era lo que su familia defendió con tanto sacrificio. Atormentada por las decisiones de su pasado, pero decidida a proteger a los suyos, Tris, con la ayuda de Cuatro, se enfrentará a un reto tras otro para desbloquear la verdad de su pasado y el futuro de su mundo. Secuela de "Divergente"</info>
 
 
<page>6</page><title>DIVERGENTE: LEAL</title>
<microhd>bd06b02af58e74d3113552c34226cc555d76a556</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>c05273b4d6cb00ad59c1a42295650d4b792c1926</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/vuuTnHvqW860vsCFRt6OC6lvcDI.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/qSP072apfe2EEcd5Qg9vGYy2OLw.jpg</fanart>
<date>2016</date>
<genre>Saga.Ciencia ficcion. Accion. Aventuras. Romance</genre>
<extra>NA</extra>
<info>Tras las revelaciones trascendentales de 'Insurgente', Tris debe escapar con Cuatro e ir más allá del muro que rodea Chicago. Por primera vez dejarán la única ciudad y familia que conocen. Una vez fuera, todo aquello que presuponían como cierto, pierde cualquier sentido tras la revelación de nuevas verdades. Tris y Cuatro deben decidir rápidamente en quién confiar mientras se inicia una guerra despiadada que amenaza a toda la humanidad. Para sobrevivir, Tris se verá forzada a tomar decisiones imposibles sobre el coraje, la lealtad, el sacrificio y el amor. Secuela de "Insurgente" (2015) y punto final a la saga iniciada por "Divergente" (2014). Dividida en dos partes, una estrenada en 2016 y otra en 2017.</info>
 
 
<page>6</page><title>DOCE HOMBRES SIN PIEDAD</title>
<microhd>b965c23b006de3f265b60f37b7ad7cfebc2bc147</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/oYr0UtQ9dR2NaInzC4K7gkwDon8.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/2ExdVu0AITPediwjpxGEl9e2NuF.jpg</fanart>
<date>1957</date>
<genre>Drama. Intriga </genre>
<extra>Mis</extra>
<info>Los doce miembros de un jurado deben juzgar a un adolescente acusado de haber matado a su padre. Todos menos uno están convencidos de la culpabilidad del acusado. El que disiente intenta con sus razonamientos introducir en el debate una duda razonable que haga recapacitar a sus compañeros para que cambien el sentido de su voto.</info>
 
 
<page>6</page><title>DOCTOR STRANGE EN EL MULTIVERSO DE LA LOCURA</title>
<microhd>688eea73acc37f01c5d6d5e5de1d29689797d881</microhd>
<fullhd>81c48f7736f6c50e806ce61769ce4ad423d741c9</fullhd>
<tresd>NA</tresd>
<cuatrok>0d7c26a2a420dc4629b27716dcb8f542f59ede97</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/vThe85YlGE5r7fqEVFePETqnWzk.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/AdyJH8kDm8xT8IKTlgpEC15ny4u.jpg</fanart>
<date>2022</date>
<genre>Fantástico. Acción. Terror</genre>
<extra>NA</extra>
<info>Viaja a lo desconocido con el Doctor Strange, quien, con la ayuda de tanto antiguos como nuevos aliados místicos, recorre las complejas y peligrosas realidades alternativas del multiverso para enfrentarse a un nuevo y misterioso adversario.</info>
 
 
<page>6</page><title>DOCTOR SUEÑO</title>
<microhd>aed7de3124119b15f4818223c9e30a4752eb5c62</microhd>
<fullhd>7d8fc46a6bf609763545323e808aad10a14f198a</fullhd>
<tresd>NA</tresd>
<cuatrok>d5cf16a69f9e4a45d8a1d3c68659addc5c95ce11</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/oYx5zclp0WHZ0gVQ9nyDIwDcgxd.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/sy6JFRYHTUGYAG9mIhJkZttosFV.jpg</fanart>
<date>2019</date>
<genre>Terror. Thriller</genre>
<extra>NA</extra>
<info>Secuela del film de culto "El resplandor" (1980) dirigido por Stanley Kubrick y también basado en una famosa novela de Stephen King. La historia transcurre algunos años después de los acontecimientos de "The Shining", y sigue a Danny Torrance (Ewan McGregor), traumatizado y con problemas de ira y alcoholismo que hacen eco de los problemas de su padre Jack, que cuando sus habilidades psíquicas resurgen, se contacta con una niña de nombre Abra Stone, a quien debe rescatar de un grupo de viajeros que se alimentan de los niños que poseen el don de "el resplandor".</info>
 
 
<page>6</page><title>DOG:  UN VIAJE SALVAJE</title>
<microhd>d322bb3e95b891bab3d12d8a9ecf9170e46c0d88</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/yNpSqsRuLyyrRltXvtrbXhZinqO.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/5sEjFKFapCIlOTusgCNbdzHdh8R.jpg</fanart>
<date>2022</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Briggs, acompañado de su perro Lulu, un malinois belga, viaja por la costa oeste para ir al funeral de un amigo.</info>
 
 
<page>6</page><title>DOGMAN</title>
<microhd>315ce2584f377e805e5ade72047499126c040953</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/acXsVpAw7suTVDjKx3TJ4GMxE9B.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/zquMDqlhGYXdXiteRguQSlVGznx.jpg</fanart>
<date>2018</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>El dueño de una peluquería canina a las afueras de Roma se deja influenciar por un delincuente local hasta que su vida personal se complica y decide tomar las riendas de la situación.</info>
 
 
<page>6</page><title>DOLOR Y GLORIA</title>
<microhd>44e82ae5d3e1e2745f9c1a9aa531b0e8020879bc</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/nk5ngrHSQ7ZpxCG2t4YCyt7qjqx.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/1N55RWiZfxD5A67SW4oip9KqhwY.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Narra una serie de reencuentros en la vida de Salvador Mallo, un director de cine en su ocaso. Algunos de ellos físicos, y otros recordados, como su infancia en los años 60, cuando emigró con sus padres a Paterna, un pueblo de Valencia, en busca de prosperidad, así como el primer deseo, su primer amor adulto ya en el Madrid de los 80, el dolor de la ruptura de este amor cuando todavía estaba vivo y palpitante, la escritura como única terapia para olvidar lo inolvidable, el temprano descubrimiento del cine, y el vacío, el inconmensurable vacío ante la imposibilidad de seguir rodando. "Dolor y Gloria" habla de la creación, de la dificultad de separarla de la propia vida y de las pasiones que le dan sentido y esperanza. En la recuperación de su pasado, Salvador encuentra la necesidad urgente de volver a escribir.</info>
 
 
<page>6</page><title>DONDE CABEN DOS</title>
<microhd>4696409a5e2193c65e7ee58ef1740aa8d3f96f00</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/rpp4L7UIdXDAxn5UQBs2FkpTgWA.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/4v64DRdMtx8fQGUZ7hE1R2iChCr.jpg</fanart>
<date>2021</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Una pareja atrapada en la rutina, un joven desengañado de su última relación amorosa, una novia desesperada, dos primos separados desde su último verano en el pueblo, un grupo de amigos con ganas de experimentar… Durante una noche, todos ellos vivirán situaciones descabelladas en las que jamás hubieran pensado encontrarse, para terminar, a la mañana siguiente, de la mejor manera posible: muy juntos y muy revueltos.</info>
 
 
<page>6</page><title>DOS HOMBRES Y UN DESTINO</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>763a63d2a38c0001de945059d68a3c1db501dba3</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/2w6Ik8P1E20KOH5ykQlfTjawAIf.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/C6jcUqld8M6hZQmzBxgwHuQ4qT.jpg</fanart>
<date>1969</date>
<genre>Accion</genre>
<extra>Culto</extra>
<info>Un grupo de jóvenes pistoleros se dedica a asaltar los bancos del estado de Wyoming y el tren-correo de la Union Pacific. El jefe de la banda es el carismático Butch Cassidy (Newman), y Sundance Kid (Redford) es su inseparable compañero. Un día, después de un atraco, el grupo se disuelve. Será entonces cuando Butch, Sundance y una joven maestra de Denver (Ross) formen un trío de románticos forajidos que, huyendo de la ley, llegan hasta Bolivia.</info>
 
 
<page>6</page><title>DOWNTON ABBEY</title>
<microhd>2aebd5e6b34b777aa91e7d907caaaa996149fb48</microhd>
<fullhd>74d320c7be080f6f10bc1ceac5d454a75806e73c</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/cSZ3S6mCN4gsTQoJztBdHEbDSuD.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/mFcfdnAYaQQhXhKcoITiDYtuO5b.jpg</fanart>
<date>2019</date>
<genre>Drama </genre>
<extra>NA</extra>
<info>1927. La familia Crawley y su carismática servidumbre se preparan para el momento más crucial de sus vidas. Una visita del rey y la reina de Inglaterra desatará una situación de escándalo, romance e intriga que pondrá en peligro el futuro de Downton. Continuación de la aclamada serie televisiva, 'Downton Abbey'.</info>
 
 
<page>6</page><title>DOWNTON ABBEY: UNA NUEVA ERA</title>
<microhd>NA</microhd>
<fullhd>d157de65ac4a172d1d9c5ae578a654cd0780ef38</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/yRng7Bgz8nW714NQpmQYx6XkLWH.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/nBHKT3pm5DmidZd5VS4bD5Hc2S3.jpg</fanart>
<date>2022</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Violet, la condesa viuda de Grantham, ha heredado una villa en el sur de Francia de un viejo amigo. Mientras tanto, un cineasta obtiene el permiso de Mary para rodar una película en Downton Abbey. Robert piensa que tener actores y actrices dando vueltas, maquillados, es una idea terrible, pero otros miembros de la familia, y los sirvientes, están encantados con la idea. Para perderse la "película espantosa" de Mary, como la llama Robert, algunos miembros de la familia viajan al sur de Francia para resolver el misterio de por qué el amigo de Violet le dejó una villa. Secuela de la adaptación cinematográfica de la popular serie. </info>
 
 
<page>6</page><title>DRACULA DE BRAM STOKER</title>
<microhd>8754b5d9ab80306844b4dd52644effeaf41dd1ea</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/8xIZr8VedktMvaqQbfUXMWA7f1H.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/mpaFRJcH6PbDZcOT06KGHoLFbYj.jpg</fanart>
<date>1992</date>
<genre>Terror. Romance. Fantástico</genre>
<extra>Culto</extra>
<info>En el año 1890, el joven abogado Jonathan Harker viaja a un castillo perdido de Transilvania, donde conoce al conde Drácula, que en 1462 perdió a su amor Elisabeta. El Conde, fascinado por una fotografía de Mina Murray, la novia de Harker, que le recuerda a su Elisabeta, viaja hasta Londres "cruzando océanos de tiempo" para conocerla. Ya en Inglaterra, intentará conquistar y seducir a Lucy, la mejor amiga de Mina. </info>
 
 
<page>6</page><title>DRAGON BALL: SUPER SUPER HERO</title>
<microhd>NA</microhd>
<fullhd>cb632c92ba748154a96d5e90df8bd8668266e6ea</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/pi0iZOEHeA3ih4p1IwAG4x2DZNH.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/pvXv36QP109F2c2yUcS2hCPkrUG.jpg</fanart>
<date>2022</date>
<genre>Acción. Fantástico. Aventuras</genre>
<extra>NA</extra>
<info>Son Goku destruyó en su momento a la Patrulla Roja. Ahora, ciertos individuos han decidido continuar con su legado y han creado a los androides definitivos: Gamma 1 y Gamma 2. Estos dos androides se autoproclaman "superhéroes" y deciden atacar a Piccolo y a Gohan. ¿Cuál es el objetivo de la nueva Patrulla Roja? Ante un peligro inminente, ¡llega el momento del despertar del Superhéroe! </info>
 
 
<page>6</page><title>DUMBO</title>
<microhd>9d7add37a59117f98d47f16353887368df246072</microhd>
<fullhd>462ad0cd3cf12d7afdc13def88ab2a8cd5f580c2</fullhd>
<tresd>d7419fb9c7615b228b7588f3cc1cc5f3d8f483ea</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/kpR8mgdTvP7ewJfJZhAr80iraTe.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/5tFt6iuGnKapHl5tw0X0cKcnuVo.jpg</fanart>
<date>2019</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Holt Farrier (Colin Farrell) cuenta con la ayuda de sus hijos Milly (Nico Parker) y Joe (Finley Hobbins) para cuidar a un elefante recién nacido cuyas orejas gigantes le hacer ser el hazmerreír en un Circo que no pasa por su mejor momento. La familia circense de Holt incluye además a la señorita Atlantis (Sharon Rooney), Rongo (DeObia Oparei), Pramesh Singh (Roshan Seth) y su sobrino (Ragevan Vasan), La grandiosa Catherine (Zenaida Alcalde) y el magnífico Iván (Miguel Muñoz). Max Medici (Danny DeVito) dueño del circo, se decepciona al saber sobre las enormes orejas del pequeño paquidermo hasta que descubre que es capaz de volar, llevando al circo de regreso a la prosperidad</info>
 
 
<page>6</page><title>DUNE</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>6af77d282c8b4d238bd157270cf0906d4d1615b3</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/y5CTmxiBcyOfxsVttnNci7QJ96K.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/xBUggE2z6dpKYO0ba00DZC11iOE.jpg</fanart>
<date>1984</date>
<genre>Ciencia ficción. Aventuras. Drama. Intriga. Fantástico </genre>
<extra>Culto</extra>
<info>Arrakis, el planeta del desierto, feudo de la familia Harkonnen desde hace generaciones, queda en manos de la Casa de los Atreides después de que el emperador ceda a la casa la explotación de las reservas de especia, una de las materias primas más valiosas de la galaxia y también una droga capaz de amplificar la conciencia y extender la vida. El duque Leto (Oscar Isaac), la dama Jessica (Rebecca Ferguson) y su hijo Paul Atreides (Timothée Chalamet) llegan a Dune con la esperanza de recuperar el renombre de su casa, pero pronto se verán envueltos en una trama de traiciones y engaños que los llevarán a cuestionar su confianza entre sus más allegados y a valorar a los lugareños de Dune, los Fremen, una estirpe de habitantes del desierto con una estrecha relación con la especia.</info>
 
 
<page>6</page><title>DUNE</title>
<microhd>a675fd9b36e92e4eef563d787c90507fe8c2d5ea</microhd>
<fullhd>87a5058d7a1c6d065f3f4e7d59c1c191a751d9dc</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/5F6GBIdWpRk6f52FX5VipK57vuv.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/eeijXm3553xvuFbkPFkDG6CLCbQ.jpg</fanart>
<date>2021</date>
<genre>Ciencia ficción. Aventuras. Drama</genre>
<extra>NA</extra>
<info>Arrakis, el planeta del desierto, feudo de la familia Harkonnen desde hace generaciones, queda en manos de la Casa de los Atreides después de que el emperador ceda a ésta la explotación de las reservas de especia, una de las materias primas más valiosas de la galaxia y también una droga capaz de amplificar la conciencia y extender la vida. El duque Leto (Oscar Isaac), la dama Jessica (Rebecca Ferguson) y el hijo de ambos, Paul Atreides (Timothée Chalamet), llegan al planeta con la esperanza de recuperar el renombre de su casa, pero pronto se verán envueltos en una trama de traiciones y engaños que les llevarán a cuestionar su confianza entre sus más allegados y a valorar a los lugareños, los Fremen, una estirpe de habitantes del desierto con una estrecha relación con la especia.</info>
 
 
<page>6</page><title>DUNGEONS Y DRAGONS: HONOR ENTRE LADRONES </title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>78b1d2badc2d22ee10c6386cb18ed55d62e625cd</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/77XBzUUFX5aHI3Jz3ipJTtsDWzk.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/zRwehDUrCqLTBXqOGxWxr925NwE.jpg</fanart>
<date>2023</date>
<genre>Fantástico. Aventuras</genre>
<extra>NA</extra>
<info>Adaptación cinematográfica del primer juego de rol de la historia, publicado por primera vez en 1974. Un ladrón encantador y una banda de aventureros increíbles emprenden un atraco épico para recuperar una reliquia perdida, pero las cosas salen rematadamente mal cuando se topan con las personas equivocadas.</info>
 
 
<page>6</page><title>DUNKERQUE</title>
<microhd>b9dcc916e7d66660e27f6ef2a707e907073d5931</microhd>
<fullhd>07d1e6db22ad063bdee0ceb6dc5f0da2f2488338</fullhd>
<tresd>NA</tresd>
<cuatrok>78943d5f415160a1aa56f175a9d79d6d3de59a6c</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/iWzrUYxUfnpmLbm0rwyGKuFwpTO.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/4yjJNAgXBmzxpS6sogj4ftwd270.jpg</fanart>
<date>2017</date>
<genre>Belico. Drama</genre>
<extra>NA</extra>
<info>Año 1940, en plena 2ª Guerra Mundial. En las playas de Dunkerque, cientos de miles de soldados de las tropas británicas y francesas se encuentran rodeados por el avance del ejército alemán, que ha invadido Francia. Atrapados en la playa, con el mar cortándoles el paso, las tropas se enfrentan a una situación angustiosa que empeora a medida que el enemigo se acerca.</info>
 
 
<page>6</page><title>EDUARDO MANOSTIJERAS</title>
<microhd>31089e96c199fc6cb76cc42a7fcee579f159da8b</microhd>
<fullhd>46fa573017c79c864e005cba8425420d165d3c18</fullhd>
<tresd>NA</tresd>
<cuatrok>3d983de75ac7869274c6e03b3eaf2dde68fd32f2</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/vd8tCekihFx82bzmENaASvbhNmx.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/qOL963em2govahmx5TzxDrYPxbY.jpg</fanart>
<date>1990</date>
<genre>Fantástico. Drama. Romance</genre>
<extra>Culto</extra>
<info>Durante una noche de Navidad, una anciana le cuenta a su nieta la historia de Eduardo Manostijeras (Johnny Depp), un muchacho creado por un extravagante inventor (Vincent Price) que no pudo acabar su obra, dejando al joven con cuchillas en lugar de dedos. </info>
 
 
<page>6</page><title>EFECTOS COLATERALES DEL AMOR</title>
<microhd>23ae5a774f5eaa983dc20e5c3ff39397b8454858</microhd>
<fullhd>75d61ebe112472e75e06deae816dfa5bd6c26356</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/1lGihI0QLz3at9WtxMRbGLgeb0P.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/89TNHx1b2gC8mefEqSMFaBSEuvs.jpg</fanart>
<date>2020</date>
<genre>Romance. Drama</genre>
<extra>NA</extra>
<info>Henry Page es un estudiante adolescente que se considera un romántico empedernido, pero nunca se ha enamorado. El joven aspira a ser editor del periódico del instituto y vive feliz centrado en sus estudios para entrar en una buena universidad... hasta que la joven Grace Town entra en su clase. Su nueva compañera no es precisamente la chica de sus sueños, pero poco a poco se enamora de ella cuando los dos son elegidos para editar el periódico del instituto.</info>
 
 
<page>6</page><title>EJERCITO DE LOS MUERTOS</title>
<microhd>0ef45bf9615cec79b80883ceff93eb3498875e98</microhd>
<fullhd>3423a0adefb6031b7ef75aa74f6127e046c9ad98</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/4h1Uc13QPjHTTUX0bikICWUxTSV.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/zfrJMwj7lJcciE4ZxhShfex5VZA.jpg</fanart>
<date>2021</date>
<genre>Acción. Terror</genre>
<extra>NA</extra>
<info>Un grupo de mercenarios decide llevar a cabo el mayor atraco que jamás se haya realizado en la ciudad de Las Vegas justo después de que se produzca una epidemia de muertos vivientes. Para ello tendrán que adentrarse en una zona de cuarentena, con los riesgos que ello conlleva.</info>
 
 
<page>6</page><title>EL AGENTE INVISIBLE</title>
<microhd>a86b186edc83329f4d6bd757c871ce416a141f09</microhd>
<fullhd>37d4a72b60df643842b76b388073d82cafb96ac8</fullhd>
<tresd>NA</tresd>
<cuatrok>84d270680a138cc9e70e187c2788c7af47d017fa</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/6RU2Nj5zbVuPdCLGWiMZes0ZISO.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/lxZnN5r6nFy9s0cc8oel8XSjUNs.jpg</fanart>
<date>2022</date>
<genre>Thriller. Acción</genre>
<extra>NA</extra>
<info>El agente de la CIA Court Gentry (Ryan Gosling), alias Sierra Seis, es sacado de una cárcel federal reclutado por su supervisor, Donald Fitzroy (Billy Bob Thornton), Gentry fue en su día un mercader de la muerte altamente cualificado, autorizado por la Agencia. Pero ahora las tornas han cambiado y Seis es el objetivo, perseguido por todo el mundo por Lloyd Hansen (Chris Evans), un antiguo compañero de la CIA que no se detendrá ante nada para acabar con él. La agente Dani Miranda (Ana de Armas) le cubre las espaldas. La necesitará.</info>
 
 
<page>6</page><title>EL ALMA DE LA FIESTA</title>
<microhd>f641c5ea2fc68cfbd2ceab594e1fb34109b5c847</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/l8zn4nvkaq4EztuWrwM3nUZqD97.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/5uayF96i4JWtdNH3uJy5IpImDBT.jpg</fanart>
<date>2018</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Después de que su marido le deje, una mujer ya adulta decide ingresar en la universidad y acaba en la misma clase que su hija. La experiencia universitaria cambiará su vida.</info>
 
 
<page>6</page><title>EL ARBOL DE LA SANGRE</title>
<microhd>2e95887590f14219fc393fd65cc22e5f2fc16745</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/hiS06OYHEokGx1cw8ZNRsrhoUw.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/zSv1YmyAoneNzcfvE6B22txvmxx.jpg</fanart>
<date>2018</date>
<genre>Drama. Intriga</genre>
<extra>NA</extra>
<info>Marc (Álvaro Cervantes) y Rebeca (Úrsula Corberó) son una joven pareja que viaja hasta un antiguo caserío vasco que perteneció a su familia. Allí escribirán la historia común de sus raíces familiares, creando así un gran árbol genealógico donde se cobijan relaciones de amor, desamor, sexo, locura, celos e infidelidades, y bajo el que también yace una historia repleta de secretos y tragedias.</info>
 
 
<page>6</page><title>EL ARBOL DE LOS DESEOS</title>
<microhd>47788b9c53ed8338ae16efaa306c320b9000bdad</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/dRWmmAF6Gz5JslicHeWuo0g9tjm.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/8ki3UBFOn0ghCa8oUDHaKhmAMO3.jpg</fanart>
<date>2020</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Una joven zarigüeya se equivoca a la hora de pedir un deseo y congela toda su ciudad natal de Sanctuary City, amenazando así la vida de todos sus habitantes.</info>
 
 
<page>6</page><title>EL ARMA DEL ENGAÑO (OPERATION MINCEMEAT)</title>
<microhd>NA</microhd>
<fullhd>a597664fa90231296b701882f018240c8606529f</fullhd>
<tresd>NA</tresd>
<cuatrok>fe872b153403fb4af815e46a9ac7236d3deb653e</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/lmIibRnjWDg56Rsx0KY1Y1U0wok.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/jCSpD2Sj67qOVnOu4b3JsXl8NO4.jpg</fanart>
<date>2021</date>
<genre>Bélico. Drama</genre>
<extra>NA</extra>
<info>Año 1943, en plena II Guerra Mundial. Las fuerzas aliadas están decididas a lanzar un asalto definitivo en Europa. Pero se enfrentan un desafío importante: proteger durante la invasión a sus tropas de la potencia de fuego alemana, y así evitar una posible masacre. Dos brillantes oficiales de inteligencia, Ewen Montagu (Firth) y Charles Cholmondeley, son los encargados de establecer la estrategia de desinformación más inspirada e improbable de la guerra... Una historia inspirada en hechos reales basada en un 'bestseller' de Ben Macintyre.</info>
 
 
<page>6</page><title>EL ASESINO</title>
<microhd>NA</microhd>
<fullhd>4923e13cafdb977747cb00758c05f3589551406e</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/wXbAPrZTqJzlqmmRaUh95DJ5Lv1.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/gb7yF7jMB79IDAd7mf7i9Civt1O.jpg</fanart>
<date>2023</date>
<genre>Thriller. Intriga</genre>
<extra>Estreno</extra>
<info>Después de un fatídico error, un asesino se enfrenta a sus jefes y a sí mismo en una persecución internacional que, según él, no es personal.</info>
 
 
<page>6</page><title>EL ASESINO DE LOS CAPRICHOS</title>
<microhd>3ba85e00eaf368e71036cabca01c41c003d27f17</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/pouCFm3zKwwoBY3FwZIqGXCGhXQ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/lKucAholNGL48EKhtWfaMUR02n9.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Dos policías van tras la pista de un misterioso asesino en serie que escoge a sus víctimas entre la clase pudiente de Madrid y reproduce con sus cadáveres las escenas de los Caprichos de Goya.</info>
 
 
<page>6</page><title>EL BANQUERO</title>
<microhd>8b88f11422710976eda7085d788fa766137a0d6f</microhd>
<fullhd>3b29da97a2c4d293817313f2a53f4c171e166f3a</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/sxV1aV1ca2gKLnEoNdzaeySmgU.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/jqz8FwISQfyum47PUqgPTGtmiMk.jpg</fanart>
<date>2020</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Historia de Joe Morris (Jackson) y Bernard Garrett (Mackie), que en los años 50 se convirtieron en los dos primeros banqueros afroamericanos en los Estados Unidos.</info>
 
 
<page>6</page><title>EL BAR</title>
<microhd>86a2906dca5e454b37b00214258ea1c854335f6f</microhd>
<fullhd>3b29da97a2c4d293817313f2a53f4c171e166f3a</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/fTQrMo36JzbxNdXztHIvxoheuZp.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/oMzJmraMLpRX2xFJLaEtlj2rsJK.jpg</fanart>
<date>2017</date>
<genre>Thriller. Intriga. Comedia</genre>
<extra>NA</extra>
<info>9:00 horas. Un grupo de personas absolutamente heterogéneo desayuna en un bar en el centro de Madrid. Uno de ellos tiene prisa; al salir por la puerta recibe un disparo en la cabeza. Nadie se atreve a socorrerle. Están atrapados.</info>
 
 
<page>6</page><title>EL BEBE JEFAZO</title>
<microhd>c5e50705ee9cffaf1dc07f1eb5cd9d11940b9602</microhd>
<fullhd>78944caea2f60c2a9fa2f0874ae5cc54f64bfcfb</fullhd>
<tresd>3e00abd01c11f9927bec2010db6f4be8f9333892</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/dPiXM1aFbJ9XJGPyf5ZULmEjzkR.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/bTFeSwh07oX99ofpDI4O2WkiFJ.jpg</fanart>
<date>2017</date>
<genre>Infantil. Saga</genre>
<extra>NA</extra>
<info>La llegada de un hermanito trastoca por completo la idílica vida del pequeño Tim, hasta entonces hijo único de 7 años y el ojito derecho de sus padres. Su nuevo hermano es un peculiar bebé, que viste traje y corbata y lleva maletín. Tim comienza a sospechar de él, hasta que descubre que puede hablar.</info>
 
 
<page>6</page><title>EL BEBE JEFAZO: NEGOCIOS DE FAMILIA</title>
<microhd>NA</microhd>
<fullhd>F51190824BBE4C4D3EDAD4095A8DFA899476048A</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/x39EhBRJJddkmO7k1sadKz8VdPq.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/sJHdy9tX51VT5Z4SMtCOWfPHN2M.jpg</fanart>
<date>2021</date>
<genre>Infantil. Saga</genre>
<extra>NA</extra>
<info>Los hermanos Templeton se han convertido en adultos y se han alejado el uno del otro, pero un nuevo jefe bebé con un enfoque de vanguardia está a punto de unirlos nuevamente e inspirar un nuevo negocio familiar.</info>
 
 
<page>6</page><title>EL BUEN PATRON</title>
<microhd>8C8C34DED98A3467272375FF394234A1F605FDD6</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/p5n51b1Bp74kzo6vD7gTGMdJYtW.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/psjZg80xI2KP7xLZ1qKWwYpFzHU.jpg</fanart>
<date>2021</date>
<genre>Comedia. Drama</genre>
<extra>NA</extra>
<info>Julio Blanco, el carismático propietario de una empresa que fabrica balanzas industriales en una ciudad española de provincias, espera la inminente visita de una comisión que decidirá la obtención de un premio local a la excelencia empresarial. Todo tiene que estar perfecto para la visita. Sin embargo, todo parece conspirar contra él. Trabajando a contrarreloj, Blanco intenta resolver los problemas de sus empleados, cruzando para ello todas las líneas imaginables, y dando lugar a una inesperada y explosiva sucesión de acontecimientos de imprevisibles consecuencias.</info>
 
 
<page>6</page><title>EL BUENO, EL FEO Y EL MALO</title>
<microhd>35a5783cfd0b5ed0e685cfc5be6678c06b18b2c8</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>6ea255370637c346022a2614d3f33da9fa24438c</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/fSWnIXsxA47smMLwRELSWkxbVJ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/x4biAVdPVCghBlsVIzB6NmbghIz.jpg</fanart>
<date>1966</date>
<genre>Western</genre>
<extra>Culto</extra>
<info>Durante la guerra civil norteamericana (1861-1865), tres cazadores de recompensas buscan un tesoro que ninguno de ellos puede encontrar sin la ayuda de los otros dos. Así que colaboran entre sí para conseguir el botín.</info>
 
 
<page>6</page><title>EL BUSCAVIDAS</title>
<microhd>NA</microhd>
<fullhd>14acdd853dba1643ec36221a45052705a5568d18</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/eXCqruAof7cBVxWAdVvDyh2xsp6.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/1aspiiXDHVizaCYTNaWwRQSmXCh.jpg</fanart>
<date>1961</date>
<genre>Drama</genre>
<extra>Mis</extra>
<info>Eddie Felson (Newman) es un joven arrogante y amoral que frecuenta con éxito las salas de billar. Decidido a ser proclamado el mejor, busca al Gordo de Minnesota (Gleason), un legendario campeón de billar. Cuando, por fin, consigue enfrentarse con él, su falta de seguridad le hace fracasar. El amor de una solitaria mujer (Laurie) podría ayudarlo a abandonar esa clase de vida, pero Eddie no descansará hasta vencer al campeón sin importarle el precio que tenga que pagar por ello.</info>
 
 
<page>7</page><title>EL CABALLERO VERDE</title>
<microhd>b36b033ab9f3a1684cc3cb9a5f5a4a2f5e61b682</microhd>
<fullhd>dc9e852623737037c118072761d29d503d50554e</fullhd>
<tresd>NA</tresd>
<cuatrok>8859ebcb3bc339e162ce95ba47c601ba680c9d57</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/7EH5LmyWWepSqPB9FLMbiz0xpJr.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/eP07auW0baxhbflms6obNfRB0NG.jpg</fanart>
<date>2021</date>
<genre>Fantástico. Drama. Thriller</genre>
<extra>NA</extra>
<info>La historia medieval de Sir Gawain y el caballero verde contada desde el punto de vista del género de la fantasía.</info>
 
 
<page>7</page><title>EL CAIRO CONFIDENCIAL</title>
<microhd>58d18bd718e50c3e484b4fdf61daaa9c07b6e602</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/9zcpnBRAgOqaVHsichPSIUy8m2O.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/mF1JKUX513VAvA6MeCvw9sPMDrP.jpg</fanart>
<date>2017</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>Noureddine, un detective corrupto con un futuro brillante en el cuerpo de policía, y cuyo principal propósito no es exactamente hacer el bien sino hacerse rico, es enviado al hotel Nile Hilton, donde acaban de descubrir el cadáver de una hermosa mujer. La identidad de ésta, sus conexiones con las élites de El Cairo y otros incidentes más personales acabarán llevando a Noureddine a tomar decisiones trascendentales y a descubrirse a sí mismo.</info>
 
 
<page>7</page><title>EL CALENDARIO DE ADVIENTO</title>
<microhd>NA</microhd>
<fullhd>a0ea869f77210e08d41b15169ef0c90b905bf9db</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/xzaRekUnw8SNodDFEWzVlbFkBxU.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/7iuwjpIfV0xzA6wh3YHCwr5OUbL.jpg</fanart>
<date>2021</date>
<genre>Terror. Thriller</genre>
<extra>NA</extra>
<info>Eva, una antigua bailarina, vive postrada en una silla de ruedas sin poder caminar. Su monótona vida cambia cuando su amiga Sophie le regala un antiguo calendario de adviento para recibir la Navidad, un objeto de madera que tiene poderes mágicos: cada ventanita que abre contiene una sorpresa que provoca repercusiones en la vida real, algunas buenas, pero la mayoría malas.</info>
 
 
<page>7</page><title>EL CALLEJON DE LAS ALMAS PERDIDAS</title>
<microhd>84ec57f8e1c82f3881074006d7adb0c5f1d71c18</microhd>
<fullhd>84ec57f8e1c82f3881074006d7adb0c5f1d71c18</fullhd>
<tresd>NA</tresd>
<cuatrok>83817efa0e931c739b063e81d5f89de4eae0bd87</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/vH5rh852MByFzEwEIQ7Us7dJhy1.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/rxZYmy3wV6egFkRE3bQpqpmzLsR.jpg</fanart>
<date>2021</date>
<genre>Intriga. Drama</genre>
<extra>NA</extra>
<info>Un buscavidas (Bradley Cooper) se compincha con una psiquiatra para estafar a millonarios... Remake del film "El callejón de las almas perdidas" (Nightmare Alley), de 1947.</info>
 
 
<page>7</page><title>EL CAMINO: UNA PELICULA DE BREAKING BAD</title>
<microhd>6fd20523e40f3411f0cdd3beb5b318e330e30ed5</microhd>
<fullhd>a4fc9e28ca0d6383e5f7f8fb8b1f12559ae54b5c</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/hoWADuvXs3Ua4AXBAiZYnppTupO.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/uLXK1LQM28XovWHPao3ViTeggXA.jpg</fanart>
<date>2019</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Tiempo después de los eventos sucedidos tras el último episodio de la serie "Breaking Bad", el fugitivo Jesse Pinkman (Aaron Paul) huye de sus perseguidores, de la ley y de su pasado.</info>
 
 
<page>7</page><title>EL CLAN DE LOS IRLANDESES</title>
<microhd>NA</microhd>
<fullhd>b7fdb072ebec5c6a7d7fb6a4c13006fab8af48b2</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/o2tUFp2DRbVVE4skoRbrfoO260v.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/t5hmPeAYB0PJIaSInStylrcpV4b.jpg</fanart>
<date>1990</date>
<genre>Thriller. Acción </genre>
<extra>Mis</extra>
<info>Tras diez años de ausencia, Terry Noonan (Sean Penn) regresa al conflictivo barrio de Hell's Kitchen, en Nueva York, e ingresa en un grupo mafioso irlandés. El jefe de la banda es Frankie Flannery (Ed Harris); para él trabajan algunos amigos de la infancia de Terry y también Jackie (Gary Oldman), el exaltado hermano de Frankie. Al entrar en la banda, renacen los sentimientos de Noonan por Kathleen (Robin Wright), la hermana de los Flannery.</info>
 
 
<page>7</page><title>EL CLUB DE LA LUCHA</title>
<microhd>NA</microhd>
<fullhd>b37c6eb4f8501f2c820726bc957c3cec552e68ac</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/sgTAWJFaB2kBvdQxRGabYFiQqEK.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/52AfXWuXCHn3UjD17rBruA9f5qb.jpg</fanart>
<date>1999</date>
<genre>Drama. Thriller</genre>
<extra>Culto</extra>
<info>Un joven hastiado de su gris y monótona vida lucha contra el insomnio. En un viaje en avión conoce a un carismático vendedor de jabón que sostiene una teoría muy particular: el perfeccionismo es cosa de gentes débiles; sólo la autodestrucción hace que la vida merezca la pena. Ambos deciden entonces fundar un club secreto de lucha, donde poder descargar sus frustaciones y su ira, que tendrá un éxito arrollador.</info>
 
 
<page>7</page><title>EL CLUB SECRETO DE LOS NO HEREDEROS AL TRONO</title>
<microhd>NA</microhd>
<fullhd>ee4f53223bb767a81d4b5ea8c8ccaedff6475248</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/gKLvBS6eGIAzaFNjW9JRszwEs40.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/oSSEcPDfwgZSv2i01Oqxdb9t8fI.jpg</fanart>
<date>2020</date>
<genre>Fantástico. Acción. Aventuras.Infantil</genre>
<extra>NA</extra>
<info>Conocemos a Sam, una princesa nacida en segundo lugar. No es la típica princesa y no encaja en ese mundo. Cuando su madre, la reina, la envía a clases de verano en un internado, Sam averigua que es una tapadera: ella y los demás alumnos descubren que tienen superpoderes y que están ahí para entrenar y entrar a formar parte del club secreto de los no herederos al trono. ¿Serán capaces de aprender a usar sus poderes para salvar sus reinos?</info>
 
 
<page>7</page><title>EL CODIGO DA VINCI</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>eba2ca83a35630d52d68d14c18a4f48866dc36af</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/n5N1pdoAqSkO6ZmiTdryBS4jmCO.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/rbeXab3IISrxL7jVPjb7jZ4TdeE.jpg</fanart>
<date>2006</date>
<genre>Intriga. Thriller</genre>
<extra>Mis</extra>
<info>El catedrático y afamado simbologista Robert Langdon (Tom Hanks) se ve obligado a acudir una noche al Museo del Louvre, cuando el asesinato de un restaurador deja tras de sí un misterioso rastro de símbolos y pistas. Con la ayuda de la criptógrafa de la policía Sophie Neveu (Audrey Tautou) y poniendo en juego su propia vida, Langdon descubre que la obra de Leonardo Da Vinci esconde una serie de misterios que apuntan a una sociedad secreta encargada de custodiar un antiguo secreto que ha permanecido oculto durante dos mil años... </info>
 
 
<page>7</page><title>EL COLECCIONISTA DE HUESOS</title>
<microhd>b0dba9325fd3d7cdd3445ad5d09bf6766151cec7</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/kmFgwA6bho6CqmAuH5lc49qD2nc.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/hMclNPye6G3ouN5QdusrlTK0SaE.jpg</fanart>
<date>1999</date>
<genre>Intriga. Thriller</genre>
<extra>Mis</extra>
<info>El detective Lincoln Rhyme (Denzel Washington) sufrió un terrible accidente durante una investigación que le dejo tetrapléjico. Sin embargo sigue siendo uno de los mejores policías de Nueva York, capaz de seguir el rastro de sus pesquisas desde la cama en la que está postrado. Sólo necesita un poco de ayuda, la de una agente que sea capaz de conectar con su intuición y llevar a la acción sus conclusiones. Junto a la oficial novata Amelia (Angelina Jolie), Rhyme hará todo lo posible por encontrar al brutal asesino que está llenando de cadáveres la ciudad: un psicópata que además está jugando al gato y al ratón con la policía, dejando pistas que Lincoln intenta descifrar.</info>
 
 
<page>7</page><title>EL COLOR DEL DINERO</title>
<microhd>NA</microhd>
<fullhd>795f6ee8fa724af5822eadd3cc8e3d8379a00754</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/eGtURb1zBDPtaoo3umzM5ZH8dvH.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/hhCp6ZLDjS5wUHkGg7k3VGvH3Xm.jpg</fanart>
<date>1986</date>
<genre>Drama</genre>
<extra>Mis</extra>
<info>Eddie Felson, antiguo campeón de billar retirado, vive de su negocio de licores. Un día, en una sala de juego, conoce a Vincent, un joven jugador de billar, que aún no ha encontrado un oponente de su talla, y que siempre va acompañado de su novia, que es la que se encarga de las apuestas que se hacen a favor de Vincent.</info>
 
 
<page>7</page><title>EL CONTADOR DE CARTAS</title>
<microhd>NA</microhd>
<fullhd>39d938ebbc3c5295fb503f129f69d15a13158d2a</fullhd>
<tresd>NA</tresd>
<cuatrok>b8b8674f07b6a6638e54dd3f1c50448c492989b7</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/xlgavRWhvFNpi7JxHPNYyMrJ76y.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/llSJSiPiqrHih6TTAqz662ji0A4.jpg</fanart>
<date>2021</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>William Tell (Oscar Isaac) es un exmilitar y jugador profesional de póker. Su meticulosa vida se trastoca cuando se le acerca Cirk, un joven que busca ayuda en Tell para ejecutar su plan de venganza contra un coronel militar.</info>
 
 
<page>7</page><title>EL CONVENTO</title>
<microhd>e3d489cda057124d73b7cd42d080209a4a5cea4c</microhd>
<fullhd>51054f03ea972ebcbaffb94671d0de287d3f756e</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/6iTPFr7MknkLORF8GqRpovNyE6l.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/qgy7GXZFHONsd2R9fazJha8n94z.jpg</fanart>
<date>2018</date>
<genre>Terror</genre>
<extra>NA</extra>
<info>Georgia, 1957. Mary se ha quedado embarazada, y su entorno se cae a pedazos: su padre es un alcohólico y su novio no puede ayudarle ni a ella ni al bebé. El único refugio es el convento del pueblo. Pronto descubriremos el terror que se esconde detrás de los hábitos, en un lugar regido por estrictas normas, dispuestas para explotar las vulnerabilidades de las jóvenes monjas.</info>
 
 
<page>7</page><title>EL CORREDOR DEL LABERINTO</title>
<microhd>a38982b76e4920dfb6ef35350a35fdc7a28bed2a</microhd>
<fullhd>136fff3a300460bb8a8933e73b692ce2d5f566a1</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/egKi4jZKKVZzR8NaaNjEwMpEgHs.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/6u6KIyDKojx7pGhCn0skRHdmqX3.jpg</fanart>
<date>2014</date>
<genre>Ciencia ficcion. Accion. Aventuras.Saga</genre>
<extra>NA</extra>
<info>Año 2024. Cuando el joven Thomas despierta, se encuentra en un ascensor y no recuerda quién es. De pronto, el ascensor se detiene, las puertas se abren y una multitud de chicos lo recibe. Ha llegado al Claro, un lugar rodeado de altísimos muros con dos portones que todas las mañanas se abren y dan paso a un inmenso laberinto. De noche, las puertas se cierran y por el laberinto circulan unas aterradoras criaturas llamadas laceradores. Todo lo que ocurre en el Claro sigue unas pautas: al abrirse las puertas, algunos chicos salen a correr al laberinto para buscar una salida. Una vez al mes, el ascensor sube con un nuevo chico, nunca una chica... hasta ahora. Tras la llegada de Thomas, suena una alarma y el ascensor trae a otra persona. Es una chica, y en la nota que la acompaña pone: "Ella es la última. No llegarán más". Las cosas en el Claro empiezan a cambiar, y lo único en lo que Thomas puede pensar es en lo mucho que desea ser un corredor.</info>
 
 
<page>7</page><title>EL CORREDOR DEL LABERINTO: LA CURA MORTAL</title>
<microhd>71735e78011680d70f6b51ef8d56423fea741b1f</microhd>
<fullhd>6d7c57c342a5546f47f855a4edb6ca194920d0d8</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/kuZOnvzplxPVh71t827F19zY5Ab.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/kuZOnvzplxPVh71t827F19zY5Ab.jpg</fanart>
<date>2018</date>
<genre>Saga.Ciencia ficcion. Accion</genre>
<extra>NA</extra>
<info>Thomas no puede confiar en CRUEL. La organización le borró sus recuerdos y lo encerró en el Laberinto. Luego lo dejó al borde de la muerte, en el Desierto. Y lo separaron de los Habitantes, sus únicos amigo. Ahora CRUEL asegura que el tiempo de las mentiras ha terminado. Con toda la información que reunió gracias a las Pruebas, está en condiciones de avanzar en la cura de la Llamarada. Pero Thomas debe pasar por la Prueba Final. ¿Logrará sobrevivir al procedimiento? ¿Será cierto que se terminaron las mentiras? Quizá la verdad sea más terrible aún... una solución letal, sin retorno.</info>
 
 
<page>7</page><title>EL CORREDOR DEL LABERINTO: LAS PRUEBAS</title>
<microhd>f3f5046cd137277f685ae18b397fa1ee405bcdfa</microhd>
<fullhd>fb64e56546d81380b3c1b0509736e493599cb11a</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/5064TPKlsU7xXWQY99kMmM82o8J.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/4mcOCiR06dqQ5eoEJcG3zvonjOa.jpg</fanart>
<date>2015</date>
<genre>Saga.Accion. Ciencia ficcion</genre>
<extra>NA</extra>
<info>Thomas (Dylan O'Brien) y los demás clarianos tendrán que enfrentarse al mayor desafío de sus vidas: buscar pistas sobre la misteriosa y poderosa organización "CRUEL". Esta aventura los llevará a "La Quemadura", un apocalíptico lugar lleno de inimaginables obstáculos. Aliados con miembros de la Resistencia, tendrán que descubrir quiénes son los dirigentes de la secta y cuáles son sus planes.</info>
 
 
<page>7</page><title>EL CUADERNO DE SARA</title>
<microhd>b1d637ca6cbedd97a31b245f81b3fcd103e20aec</microhd>
<fullhd>80ce0856022363959957525e63d4795b9cf5162d</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/27iv6n8hz3W9o46jzxtP23Ccuwz.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/nT0HGpCfTwtVSGxEpqhj3dKtTSo.jpg</fanart>
<date>2018</date>
<genre>Aventuras. Intriga. Drama</genre>
<extra>NA</extra>
<info>Laura busca desde hace años a su hermana Sara, desaparecida en medio de la selva del Congo. Ni la ONG para la que trabajaba ni la Embajada tenían noticias de su paradero... hasta que aparece una foto de un poblado minero del este del Congo con la imagen borrosa de Sara. Sin dudarlo un momento, Laura viaja hasta Kinshasa dispuesta a adentrarse en el territorio de los "Señores de la Guerra", dejando atrás las diferencias que la separaron de su hermana, y sin sospechar que esa peligrosa aventura le llevará a poner en peligro su propia vida.</info>
 
 
<page>7</page><title>EL CUERVO</title>
<microhd>7f218ac38f727b1adf12741495375ce9d11ed614</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/926j4wnskRBMtWJuR4bCrlKb1ZF.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/3OdZX8PPJBHySOwWk0M47zq4BPx.jpg</fanart>
<date>1994</date>
<genre>Fantástico. Acción</genre>
<extra>Culto</extra>
<info>Eric Draven y Shelly Webster están a punto de casarse, pero la noche antes del enlace son asesinados brutalmente. Un año después, el alma de Eric vuelve a la Tierra en forma de cuervo para vengarse.</info>
 
 
<page>7</page><title>EL CURIOSO CASO DE BENJAMIN BUTTON</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>ed68fb85485b6cc9b5e0cf2cb1b0ed49c3fd52c9</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/tq8yOj5fu4aLVdMUbibm9GEkmuN.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/fbSWjlLM6d4QdisljSnpTkKuZ4Z.jpg</fanart>
<date>2008</date>
<genre>Fantástico. Romance. Drama</genre>
<extra>Mis</extra>
<info>Un hombre (Brad Pitt) nace con ochenta años y va rejuveneciendo a medida que pasa el tiempo; es decir, en lugar de cumplir años los descumple. Esta es la historia de un hombre extraordinario, de la gente que va conociendo, de sus amores y amistades, pero sobre todo de su relación con Daisy (Cate Blanchett), la mujer de su vida.</info>
 
 
<page>7</page><title>EL DEMONIO VESTIDO DE AZUL</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>30c22080ea339b017320245ac8308c18ec2a8449</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/fxzA7U1eQK7fr5z7rF6df4vn1ZR.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/116bqpMui0uJ2DIZnKczsG9LSQf.jpg</fanart>
<date>1995</date>
<genre>Intriga. Thriller</genre>
<extra>Mis</extra>
<info>Los Ángeles, 1948. Ezekiel "Easy" Rawlins busca un trabajo honrado para poder sufragar los gastos de su casa. Acuciado por la falta de dinero, se ve obligado a aceptar el encargo de encontrar a una mujer blanca desaparecida hace pocos días. Sólo cuenta con una pista: la mujer en cuestión es muy aficionada a frecuentar los clubes nocturnos de jazz. Lo que en principio parece un trabajo fácil se convierte en una pesadilla al verse involucrado en dos violentos asesinatos que apuntan a una misteriosa mujer vestida de azul.</info>
 
 
<page>7</page><title>EL DIA DE LA BESTIA</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>19e59043a4d879e74545ac49de5075c08ef70f72</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/o9KchOiUG4x6sctpHJrabsl1Ltj.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/i8Erc9MnI9BxIqRYagls8rsNZO6.jpg</fanart>
<date>1995</date>
<genre>Comedia. Acción. Fantástico. Terror</genre>
<extra>Culto</extra>
<info>Un sacerdote cree haber descifrado el mensaje secreto del Apocalipsis según San Juan: el Anticristo nacerá el 25 de diciembre de 1995 en Madrid. Para impedir el nacimiento del hijo de Satanás, el cura se alía con José María, un joven aficionado al death metal. Ambos intentan averiguar en qué parte de Madrid tendrá lugar el apocalíptico acontecimiento. Con la ayuda del profesor Cavan, presentador de un programa de televisión de carácter esotérico y sobrenatural, el cura y José Mari invocan al diablo en una extraña ceremonia. </info>
 
 
<page>7</page><title>EL DIA DEL SI</title>
<microhd>5e7245caa2ba0df0d4660d9e48cc53a54bc9a430</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/rejrD9ovTHJbfmpLM0mbEliEPV6.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/cJwkku3SPprS0Splfgh8VFRd0xn.jpg</fanart>
<date>2021</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Sintiendo como si siempre dijeran NO a sus hijos y compañeros de trabajo, Allison y Carlos deciden dar a sus tres hijos un YES DAY, en el que durante 24 horas los niños mandarán. No se imaginaban que se embarcarían en una aventura vertiginosa en torno a Los Ángeles, que unirá a su familia más que nunca.</info>
 
 
<page>7</page><title>EL DIABLO A TODAS HORAS</title>
<microhd>26b999c0514b502996c4510fa62cbf3aa22366bd</microhd>
<fullhd>f8f9759ae9b47eafa7e089ff82d176fd1ca9af05</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/hqTavMAi69tdWuuFjfcIzs1y2cf.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/vP21EA7RiPJJhLTLLn7SPHIJg1r.jpg</fanart>
<date>2020</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Desesperado por salvar a su mujer, Willard Russell convierte sus oraciones en un sacrificio. Las acciones de Russel llevan a su hijo Arvin a pasar de ser un niño que sufre abusos en el instituto a convertirse en un hombre que sabe cuándo y cómo ha de pasar a la acción. Los acontecimientos que se dan lugar en Knockemstiff (Ohio) desatan una tormenta de fe, violencia y redención que se desarrolla a lo largo de dos décadas.</info>
 
 
<page>7</page><title>EL DOBLE MAS QUINCE</title>
<microhd>5e0402f0d616047d742eb6d587b44a502af68ccf</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/dP6gJr9c2bHVhcwZi4DDrAnwKUn.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/htky1xCC1sypJj8Rh2HOOqWaeP8.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Cuando pasas de los 45 años, puede parecer que ya has hecho todo en la vida: un marido, dos hijos, un perro y una casa bonita con jardín. Pero... ¿eso es todo? ¿Así? ¿Y qué hay de ti en esa ecuación sin nombre? ¿Dónde estás tú y tus deseos? Cuando eres un adolescente, tienes toda la vida por delante. Tiempo para casarte, tener un par de hijos, un gato, y una casa bonita con jardín. Pero... ¿y si no tienes ni idea de qué hacer? ¿no sabes qué camino escoger? ¿Y nadie te ayuda a encontrar el camino? Ana y Eric. Eric y Ana. Los dos están perdidos. No tienen ni idea de qué hacer con el resto de sus vidas. Un día se conocen en un chat de sexo.</info>
 
 
<page>7</page><title>EL DUQUE</title>
<microhd>NA</microhd>
<fullhd>714205c2af43e1d074678d83c1f30f1d5488678c</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/hjYNzmpgU1bRV3xUHdNfcI6wrhE.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/dq6M58VE5mQhLJ2rFEZLNAmrWxz.jpg</fanart>
<date>2021</date>
<genre>Comedia. Drama</genre>
<extra>NA</extra>
<info>En 1961, Kempton Bunton, un taxista de 60 años, robó el retrato del duque de Wellington, de Francisco de Goya, de la National Gallery de Londres. Fue el primer (y sigue siendo el único) robo de la historia de la galería. Kempton envió notas de rescate diciendo que devolvería la pintura a condición de que el gobierno invirtiera más en el cuidado de los ancianos. Lo que sucedió a continuación se convirtió en algo legendario.</info>
 
 
<page>7</page><title>EL ESCANDALO</title>
<microhd>c7b579f1560deb58856032ec95eb58d5c1759d9a</microhd>
<fullhd>5738918008138fb36a6a127666801d6e20a3effc</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/h7F3E54nG9p2CUBFB1LWM1TaHz6.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/wPnruCu5SxF5kdIyLteJTTUzve2.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Deconstrucción de la caída de uno de los imperios mediáticos más poderosos y controvertidos de las últimas décadas, Fox News, y de cómo un grupo de explosivas mujeres logró acabar con el hombre responsable de él: Roger Ailes.</info>
 
 
<page>7</page><title>EL ESCUADRON SUICIDA</title>
<microhd>d124b7d63d4276f3af19d15d2562dc4d007235c3</microhd>
<fullhd>1075aed15cf956630504eb93c01d1105006d4934</fullhd>
<tresd>NA</tresd>
<cuatrok>41c8d10e51b9cc0ceaed4a2ac4eec95859179b86</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/9G24WDiVEQZqzFk0lQ5fGWCCbEv.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/jKgulFr4CErJH94aMazXJTYxk7o.jpg</fanart>
<date>2021</date>
<genre>Acción. Fantástico. Thriller</genre>
<extra>NA</extra>
<info>Un grupo de super villanos se encuentran encerrados en Belle Reve, una prisión de alta seguridad con la tasa de mortalidad más alta de Estados Unidos. Para salir de allí harán cualquier cosa, incluso unirse al grupo Task Force X, dedicado a llevar a cabo misiones suicidas bajo las órdenes de Amanda Waller. Fuertemente armados son enviados a la isla Corto Maltese, una jungla repleta de enemigos.</info>
 
 
<page>7</page><title>EL ESPIA INGLES</title>
<microhd>3763ADF258459678EB51E0E7EEE5DA241C020D17</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/2vgvbxnN72CjTvmvXforxGFLjBV.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/3pIqd1hgZ2xqzWEyiYp4blqE9Fi.jpg</fanart>
<date>2020</date>
<genre>Intriga. Drama</genre>
<extra>NA</extra>
<info>Durante la Guerra Fría entre Estados Unidos y Rusia, el ingeniero Greville Wynne (Benedict Cumberbatch) se infiltra como espía en el MI6, servicio de inteligencia británico. Cuando la crisis de los misiles cubanos promete inclinar la balanza a favor del país soviético, Wynne comenzará a trabajar con la CIA para filtrar información sobre el plan que tienen en marcha los rusos y así evitar una catástrofe.</info>
 
 
<page>7</page><title>EL ESPIA QUE ME PLANTO</title>
<microhd>0d99740ab292b931f6a058a58f7871c4debaf668</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/1hlGrXXby3mxdlmFKksDRvfOtAe.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/uN6v3Hz4qI2CIqT1Ro4vPgAbub3.jpg</fanart>
<date>2018</date>
<genre>Comedia. Accion</genre>
<extra>NA</extra>
<info>Audrey y Morgan son dos amigas que se ven involucradas en una conspiración internacional cuando una de ellas descubre que su ex-novio era en realidad un espía.</info>
 
 
<page>7</page><title>EL ESPINAZO DEL DIABLO</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>94c46cf46b097b8cb64795d08bffdfa94fb7b4d4</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/jdvZ3aAopxGJ7aIRpU9SgTukgzh.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/vfgUpzmWAqRn01cAmnA9OW25nLl.jpg</fanart>
<date>2001</date>
<genre>Terror. Thriller</genre>
<extra>Mis</extra>
<info>Trascurre el año 1939, recién finalizada la guerra civil. Carlos, un niño de diez años, llega a un orfanato que acoge a huérfanos de víctimas republicanas. Su presencia alterará la rutina diaria de un colegio dirigido por Carmen y cuyo profesor, el señor Casares, simpatiza con la perdida causa republicana. Además le acechará el fantasma de uno de los antiguos ocupantes del orfanato. </info>
 
 
<page>7</page><title>EL ESPIRITU DE LA NAVIDAD</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>d2e93d77b0ba31710df00a6a96b1775c167763a3</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/7jq5Yi5HVqJw04AN4RAXcscuZ0M.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/fHDvGGPFry65ou79WLi6JsjCZrM.jpg</fanart>
<date>2022</date>
<genre>Musical. Comedia</genre>
<extra>NA</extra>
<info>Una versión musical de la historia clásica de Charles Dickens, 'A Christmas Carol', de un misántropo avaro que es llevado a un viaje mágico. Cada víspera de Navidad, el fantasma del espíritu de la Navidad selecciona un alma oscura para ser reformada por una visita de tres espíritus. Pero esta temporada, ha elegido al Scrooge equivocado... Por primera vez, el clásico clásico "A Christmas Carol" se cuenta desde la perspectiva de los fantasmas en este musical sobre el cuento de Dickens.</info>
 
 
<page>7</page><title>EL ESTRANGULADOR DE BOSTON</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>9c95ec5a864330374724646fca4065f22b6f7edd</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/Af1vSPTbraVVqc6kB3Avm55gxcq.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/5av3yFurgZWs0hjYqz3S3Hpa1kR.jpg</fanart>
<date>2023</date>
<genre>Drama. Intriga</genre>
<extra>NA</extra>
<info>Loretta McLaughlin fue la reportera que conectó por primera vez los asesinatos y dio a conocer la historia del estrangulador de Boston. Ella y Jean Cole desafiaron el sexismo de principios de la década de 1960 para informar sobre el asesino en serie más notorio de la ciudad.</info>
 
 
<page>7</page><title>EL EXORCISMO DE DIOS</title>
<microhd>NA</microhd>
<fullhd>659e15d6db42221e5fe1712c47de9a1ae0c31b22</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/aSfvsaIdtF5eXf8IsvIYdzhI3XL.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/wTmZDJb8FSmFuzOvyKXHgtDOwRG.jpg</fanart>
<date>2022</date>
<genre>Terror. Drama</genre>
<extra>NA</extra>
<info>Cuando los niños de un pequeño pueblo mexicano comienzan a morir de posesión demoníaca, los habitantes buscan la ayuda de Peter Williams, un sacerdote norteamericano marcado por un exorcismo que salió mal. Williams es considerado un santo entre sus feligreses, pero su primer encuentro con el demonio le dejó tocado para siempre. Ahora, deberá superar sus miedos para enfrentarse de nuevo contra el mal.</info>
 
 
<page>7</page><title>EL EXORCISTA</title>
<microhd>9bccb08b887c7051dc2db8758b394aa9a5e4fe89</microhd>
<fullhd>b920b1d7210c010af0d991fc7a65eea57fc0c167</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/lNlWuDxf3EvnbpNCMc7zVtpEQAC.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/rQU3sOBms4WA7iFAqBCtbMXgaB8.jpg</fanart>
<date>1973</date>
<genre>Terror. Saga</genre>
<extra>Culto</extra>
<info>Adaptación de la novela de William Peter Blatty que se inspiró en un exorcismo real ocurrido en Washington en 1949. Regan, una niña de doce años, es víctima de fenómenos paranormales como la levitación o la manifestación de una fuerza sobrehumana. Su madre, aterrorizada, tras someter a su hija a múltiples análisis médicos que no ofrecen ningún resultado, acude a un sacerdote con estudios de psiquiatría. Éste, convencido de que el mal no es físico sino espiritual, es decir que se trata de una posesión diabólica, decide practicar un exorcismo. Seguramente la película de terror más popular de todos los tiempos.</info>
 
 
<page>7</page><title>EL EXORCISTA DEL PAPA</title>
<microhd>c862f6e2c04144c5b9fc4deb201d067fb62f2d6c</microhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/zBjUhxhSZKLVTrrCwNJDaGtv5a2.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/hiHGRbyTcbZoLsYYkO4QiCLYe34.jpg</fanart>
<date>2023</date>
<genre>Terror</genre>
<extra>NA</extra>
<info>Película sobre Gabriele Amorth, un sacerdote que ejerció como exorcista principal del Vaticano, realizando más de cien mil exorcismos a lo largo de su vida. Amorth escribió dos libros de memorias donde detalló sus experiencias luchando contra Satanás.</info>
 
 
<page>7</page><title>EL EXORCISTA III</title>
<microhd>NA</microhd>
<fullhd>d4516be9a27c04e1a0d8d0ab474cc3e3e6068902</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/4JNaaRHzvfopeig4BqSlfufFrVK.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/k6CDxvHzLZMNI6KvTNyLLessbJi.jpg</fanart>
<date>1990</date>
<genre>Terror. Saga</genre>
<info>Una mujer que se arrastra por el techo. Un criminal muerto hace tiempo que asesina víctima tras víctima. Las llamas estallan, las serpientes se deslizan, la tierra se abre revelando el tormentoso infierno. El demonio ha vuelto... pero el sacerdote Damien Karras vuelve a notar la presencia del maligno, y pedirá la ayuda del escéptico detective Kinderman para derrotarle.</info>
 
 
<page>7</page><title>EL EXORCISTA: EL COMIENZO</title>
<microhd>NA</microhd>
<fullhd>d4516be9a27c04e1a0d8d0ab474cc3e3e6068902</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/aPYqU3oKjZoWtvk0GQLI78hfkSI.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/A2kXYkGGdeLU20YHPM9qs6GX9nN.jpg</fanart>
<date>2004</date>
<genre>Terror. Saga</genre>
<info>Precuela de "El exorcista" (The Exorcist, 1973), de William Friedkin, que relata el primer encuentro del padre Lankester Marin (Stellan Skarsard) con el diablo, en África, durante la Segunda Guerra Mundial.</info>
 
 
<page>7</page><title>EL EXORCISTA: EL COMIENZO. LA VERSION PROHIBIDA</title>
<microhd>NA</microhd>
<fullhd>d4516be9a27c04e1a0d8d0ab474cc3e3e6068902</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/hvkDGQpcRMJILtrOfHVyGqtlvnM.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/vc31DWCeiEJn2eOe40CEVQiGXFY.jpg</fanart>
<date>2005</date>
<genre>Terror. Saga</genre>
<info>El Cairo, 1949. Lankester Merrin ha abandonado el sacerdocio y malvive como puede gracias a su reputación como arqueólogo. Un coleccionista privado lo contrata para que robe una imagen sagrada de un templo cristiano recién descubierto en África Oriental. Cuando Merrin acude al lugar, descubre que los hechos inexplicables que rodean el hallazgo podrían tener un origen sobrenatural.</info>
 
 
<page>7</page><title>EL EXOSRCISTA II: EL HEREJE</title>
<microhd>NA</microhd>
<fullhd>d4516be9a27c04e1a0d8d0ab474cc3e3e6068902</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/4CRJen2ZgHylKf8uOdbyCNlmfsb.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/sHm9pSBmB08Rhxj4yPAiefjf2ox.jpg</fanart>
<date>1977</date>
<genre>Terror. Saga</genre>
<extra>NA</extra>
<info>El padre Lamont es el encargado de investigar la muerte del padre Merrin y la causa de la posesión diabólica de la joven Regan. Después de viajar a África para investigar otro caso de posesión demoníaca, regresa a Nueva York y se encuentra con que Regan está nuevamente poseida pero, en esta ocasión, por la máquina hipnótica del doctor Gene Tuskin.</info>
 
 
<page>7</page><title>EL EXTRAÑO</title>
<microhd>9aa530e2faf1cdc7a6ec81a0985d60d51f9a7980</microhd>
<fullhd>bb421284494f0fa901a01174fab3c6f4587b9bcf</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/vYhPCfSDlLObeV5bUIkmeUyR4gl.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/u6WzMRpTkGzIlfsPNtDfIBfEy9z.jpg</fanart>
<date>2022</date>
<genre>Thriller. Intriga</genre>
<extra>NA</extra>
<info>Un policía infiltrado creará una intensa e íntima relación con un sospechoso de asesinato para ganarse su confianza e intentar sacarle una confesión.</info>
 
 
<page>7</page><title>EL FARO</title>
<microhd>8573e467ab3111c311e144ae10822f6dbbfdc764</microhd>
<fullhd>2661a31a56ecbd0b736a440216ace55c09b4413f</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/9daFbip4WTIW3llj07VQ019qNEW.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/5BmcysaAASA00FM0gRjD0ClMUY9.jpg</fanart>
<date>2019</date>
<genre>Drama. Fantastico. Terror</genre>
<extra>NA</extra>
<info>Una remota y misteriosa isla de Nueva Inglaterra en la década de 1890. El veterano farero Thomas Wake (Willem Dafoe) y su joven ayudante Ephraim Winslow (Robert Pattinson) deberán convivir durante cuatro semanas. Su objetivo será mantener el faro en buenas condiciones hasta que llegue el relevo que les permita volver a tierra. Pero las cosas se complicarán cuando surjan conflictos por jerarquías de poder entre ambos. </info>
 
 
<page>7</page><title>EL FOTOGRAFO DE MAUTHAUSEN</title>
<microhd>NA</microhd>
<fullhd>d371b09ff036864a8959500b250d26a8ff4c5059</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/jMYW5HdduWQ3QX3IB0P7A8KeJzm.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/r11O1YcuGGhlGrNLDcWjGQGDMhg.jpg</fanart>
<date>2018</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>Con la ayuda de un grupo de prisioneros españoles que lideran la organización clandestina del campo de concentración de Mauthausen, Francesc Boix (Mario Casas), un preso que trabaja en el laboratorio fotográfico, arriesga su vida al planear la evasión de unos negativos que demostrarán al mundo las atrocidades cometidas por los nazis. Miles de imágenes que muestran desde dentro toda la crueldad de un sistema perverso. Las fotografías que lograron salvar Boix y sus compañeros fueron determinantes para condenar a altos cargos nazis en los juicios de Núremberg en 1946. Boix fue el único español que asistió como testigo.</info>
 
<page>7</page><title>EL FOTOGRAFO DE MINAMATA</title>
<microhd>7fe20f43da0e5ad6d823b133ddc19030904a7d97</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/osmpFmDbFUkkBeFWb5ZahAmur3P.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/vJDaoMXNy8e3r4XbAmQq52AMaCw.jpg</fanart>
<date>2020</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Nueva York, 1971. Tras sus celebrados días como uno de los fotoperiodistas más venerados de la II Guerra Mundial, W. Eugene Smith se siente desconectado de la sociedad y de su carrera. La revista Life lo envía a la ciudad costera japonesa de Minamata, cuya población ha sido devastada por el envenenamiento por mercurio, resultado de décadas de negligencia industrial. Smith se sumerge en la comunidad y sus imágenes le dan al desastre una dimensión humana desgarradora.</info>
 
 
<page>7</page><title>EL GANGSTER,EL POLICIA Y EL DIABLO</title>
<microhd>a83f4de36133824f95fabe64a8a04b6449fa74a5</microhd>
<fullhd>4c72b25653a21f644ee96f0d7de3a3794fcacaab</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/o9rByfNAkGJuXOSQLQ6UpUfGTOQ.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/tEBmjclGd6oVxin8axu4ELf10bl.jpg</fanart>
<date>2019</date>
<genre>Thriller. Acción. Drama. Cine negro</genre>
<extra>NA</extra>
<info>Un jefe de la mafia (Ma Dong-Seok) y un oficial de policía responsable de un sanguinario comando (Kim Mu-Yeol) tendrán que dejar a un lado sus diferencias para unirse en la persecución de un asesino de nombre K (Kim Sung-Kyun).</info>
 
<page>7</page><title>EL GOLPE</title>
<microhd>3b60383e1d4dd96db3eb46cbea207c2b200752c9</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>f0619d0fb0c9fc2a4ab4b4176d39aa1ea506e754</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/u7AE0gjhOKZ0hdhaaSAnCmZWVdJ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/1r9lH3DaSWKGyvZaoo6BF5STOk5.jpg</fanart>
<date>1973</date>
<genre>Intriga. Comedia. Drama</genre>
<extra>Culto</extra>
<info>En Illinois, en 1936, dos ladrones (Redford y Newman) dan un golpe a un hombre de confianza de un gángster y se embolsan un montón de dinero. El gángster decide vengarse y mata a uno de los que dieron el golpe, mientras que el otro logra escapar y entra en contacto con un ex compinche. Los dos deciden intentar un gran timo que hará temblar las finanzas del jefe de la mafia.</info>
 
<page>7</page><title>EL GORDO Y EL FLACO</title>
<microhd>51b2c2c4724ce038b88bcb5d569d492311913b97</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/cLLf98TjCgV496zfhJejGwn2ecy.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/6oyC0KcZPx9aqA1WkXtkfA1txXU.jpg</fanart>
<date>2018</date>
<genre>Drama. Comedia</genre>
<extra>NA</extra>
<info>Stan (Steve Coogan) y Ollie (John C. Reilly), conocidos en castellano como "El gordo y el flaco", se embarcan en su gira de despedida ahora que su época dorada parece haber quedado anclada en el pasado. Con la ayuda de sus respectivas mujeres, Lucille (Shirley Henderson) e Ida (Nina Arianda), ambos logran conquistar al público de las salas de Reino Unido gracias a su peculiar manera de interpretar y de entender el mundo. Este biopic ahonda en la personalidad de ambos personajes y en las circunstancias personales que les convirtieron en figuras cómicas de leyenda.</info>
 
 
<page>7</page><title>EL GRAN BAÑO</title>
<microhd>c7c3707ff7f69ab7b562138440518cde20b18199</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/iX7TqHrdTdgPkgDUZYmUWjzTVas.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/tuDrwrreZLfV2cRBuJgvZ9IRHMU.jpg</fanart>
<date>2018</date>
<genre>Comedia. Drama</genre>
<extra>NA</extra>
<info>En plena crisis de los cuarenta, un peculiar grupo de hombres decide formar el primer equipo nacional de natación sincronizada masculino. Desafiando estereotipos e ignorando la incomprensión de los que les rodean, se sumergen en una insólita aventura que les llevará a hacer frente a las dificultades y a sacar lo mejor de si mismos gracias a la ilusión y el trabajo en equipo.</info>
 
 
<page>7</page><title>EL GRAN LEBOWSKI</title>
<microhd>NA</microhd>
<fullhd>7c507f3a7990d4eb8549dd3fb69dc961e201890e</fullhd>
<tresd>NA</tresd>
<cuatrok>b173e484a20ace97297591169dd15d69bd05de70</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/EJFkJD9BH400jfzKz3W5xLYHQa.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/rYIySejPHVlRKhcXCL75Omhwkag.jpg</fanart>
<date>1998</date>
<genre>Comedia</genre>
<extra>Culto</extra>
<info>El Nota (Jeff Bridges), un vago que vive en Los Angeles, un día es confundido por un par de matones con el millonario Jeff Lebowski, con quien sólo comparte apellido. Después de que orinen en su alfombra, el Nota inicia la búsqueda de El Gran Lebowski. De su encuentro surgirá un trato: el Nota recibirá una recompensa si consigue encontrar a la mujer del magnate.</info>
 
 
<page>7</page><title>EL GRAN SHOWMAN</title>
<microhd>ba26860ba4c9d7e818073c0731c3a18fb5faee80</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/x0STU4rSFiXqxAdBTxwb0cWRsAj.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/mm2bP2C2KjleM817t35wDjrnhlC.jpg</fanart>
<date>2017</date>
<genre>Musical. Drama</genre>
<extra>NA</extra>
<info>Biopic sobre Phineas Taylor Barnum (1810-1891), un empresario circense estadounidense que fundó el "Ringling Bros. and Barnum and Bailey Circus", conocido como "el mayor espectáculo en la tierra".</info>
 
 
<page>7</page><title>EL GRINCH</title>
<microhd>9cf5e6915b4730d96f52089bf40c8959331a4b2e</microhd>
<fullhd>NA</fullhd>
<tresd>e003121b07d2cfced948abb8bfb222d0e21c21aa</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/eCHNCoat9L0Q3p4XjBQqV2Zn7Ux.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/rWW4fIPeAXJwObE9rHaUI2Wisj5.jpg</fanart>
<date>2018</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Cada año, en Navidad, los lugareños perturban su pacífica soledad con celebraciones cada vez más desmesuradas, luminosas y ruidosas. Cuando los Quién declaran que ese año van a preparar una Navidad el triple de grande, el Grinch se da cuenta de que solo hay un modo de recuperar algo de paz y silencio: robar la Navidad. Para ello, decide hacerse pasar por Santa Claus en Nochebuena, haciéndose con un reno muy peculiar para tirar de su trineo. Mientras tanto, en Villa Quién, una dulce niña llamada Cindy-Lou, desbordante de espíritu navideño, planea con sus amigos atrapar a Santa Claus durante su visita en Nochebuena para darle las gracias por ayudar a su trabajadora madre. Sin embargo, a medida que se acerca la noche mágica, sus buenas intenciones amenazan con chocar con las del Grinch, mucho más perversas.</info>
 
<page>8</page><title>EL HALLOWEEN DE HUBIE</title>
<microhd>09028ccde53789158b7c21a9679396c26d0abeff</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/rY38O6EdAdkRAffxtghztnQGMn7.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/zvw6tGsV96tARnbgFAjDEHh682O.jpg</fanart>
<date>2020</date>
<genre>Comedia. Terror</genre>
<extra>NA</extra>
<info>Pese a su devoción por el pueblo de Salem, Hubie Dubois (Sandler) reconoce que la noche de Halloween no es precisamente la más atractiva para los jóvenes desde hace unos años. En esta ocasión, no obstante, algo distinto está a punto de suceder, y sólo él podrá salvar esta mágica noche</info>
 
 
<page>8</page><title>EL HECHIZO</title>
<microhd>d9c6d0d5c1601131e546854e12335b7aa273d6f4</microhd>
<fullhd>f6a1df59890b975698d665f1277d19fabddb3e1a</fullhd>
<tresd>NA</tresd>
<cuatrok>092b7f964efa07c99495623ff8936e570c47ddca</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/9clw9UBQXmi3EFgjgIPGJSluQr7.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/5gllGAa3c9UqeRI8r6GXiQJIEtp.jpg</fanart>
<date>2020</date>
<genre>Terror. Thriller</genre>
<extra>NA</extra>
<info>Un padre sobrevive a un accidente de avión en los Apalaches, pero sospecha de la pareja de ancianos que le cuidan con remedios tradicionales.</info>
 
 
<page>8</page><title>EL HIJO</title>
<microhd>3eace26c3987c8bc7dd17ec3b891f890ccf6b10b</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/vcytE1v9e4RGjdhDYobbkFUNJxB.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/90kZ8H7qScKHOzWTqWqYUGQfmxR.jpg</fanart>
<date>2019</date>
<genre>Terror. Ciencia ficcion</genre>
<extra>NA</extra>
<info>¿Qué pasaría si un niño de otro mundo aterrizara de emergencia en la Tierra, pero en lugar de convertirse en un héroe para la humanidad fuera algo mucho más siniestro? </info>
 
 
<page>8</page><title>EL HILO INVISIBLE</title>
<microhd>64aadfa7304bb9dd02805167780ae2dfd845bb43</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/qVjtYgbhErZcF6l8fGxlgYH1KPl.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/alXluTpJv6amasXz6zlhZ0fX7WP.jpg</fanart>
<date>2017</date>
<genre>Drama. Romance</genre>
<extra>NA</extra>
<info>En el Londres de la posguerra, en 1950, el famoso modisto Reynolds Woodcock (Daniel Day-Lewis) y su hermana Cyril (Lesley Manville) están a la cabeza de la moda británica, vistiendo a la realeza y a toda mujer elegante de la época. Un día, el soltero Reynolds conoce a Alma (Vicky Krieps), una dulce joven que pronto se convierte en su musa y amante. Y su vida, hasta entonces cuidadosamente controlada y planificada, se ve alterada por la irrupción del amor.</info>
 
 
<page>8</page><title>EL HOMBRE DE ACERO</title>
<microhd>c98272d3c0fccdd9771ed3f7c33cb2e50c30f3a3</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>cfb4798ca9bf0bfb66050ac85f85aba3c2d39336</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/5JW44QGgpEWx4aWXM0uVSi2xtrI.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/6lsvafjUX32pVbqvKcO12851TTt.jpg</fanart>
<date>2013</date>
<genre>Ciencia ficción. Fantástico. Acción </genre>
<extra>Mis</extra>
<info>Desde Krypton, un lejano planeta muy avanzado tecnológicamente, un bebé es enviado en una cápsula a través del espacio a la Tierra para que viva entre los humanos. Educado en una granja en Kansas en los valores de sus padres adoptivos, Martha (Diane Lane) y Jonathan Kent (Kevin Costner), el joven Clark Kent (Henry Cavill) comienza desde niño a desarrollar poderes sobrehumanos, y al llegar a la edad adulta llega a la conclusión de que esos poderes le exigen grandes responsabilidades, para proteger no sólo a los que quiere, sino también para representar una esperanza para el mundo.</info>
 
 
<page>8</page><title>EL HOMBRE DE MIMBRE</title>
<microhd>NA</microhd>
<fullhd>9b3437e8d8734675c8d3ccc84825bda57f557975</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/qeNitlsX0CMxj4of9UM9cVuvUIN.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/kXpej2jwz7mUk5IcMkxdAfwoo3D.jpg</fanart>
<date>1973</date>
<genre>Intriga. Terror</genre>
<extra>Culto</extra>
<info>Una carta que hace sospechar que una joven desaparecida ha sido asesinada lleva al sargento Howie de Scotland Yard hasta Summerisle, una isla en la costa de Inglaterra. Allí el inspector se entera de que hay una especie de culto pagano, y conoce a Lord Summerisle, el líder religioso de la isla...</info>
 
 
<page>8</page><title>EL HOMBRE DE TORONTO</title>
<microhd>9f397807a288506f840df9069ebf1b7727b190d9</microhd>
<fullhd>f98f5068c827733e30add87e2d2d59587a7267c7</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ogLJ4HZUsRZlydxldhNGJVcBm5c.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/v1qxK4kSMAdE2HICDh637vGETVD.jpg</fanart>
<date>2022</date>
<genre>Comedia. Acción</genre>
<extra>NA</extra>
<info>El asesino más mortal del mundo es conocido como "El hombre de Toronto", y Teddy es el mayor desastre de Nueva York. Ambos se encontrarán en un Airbnb y el destino les obliga a formar equipo para salvarse. ¿Conseguirán aguantarse el uno al otro?</info>
 
 
<page>8</page><title>EL HOMBRE DEL AGUA</title>
<microhd>45f6e640e705710e1677ad955fd54330a18636d4</microhd>
<fullhd>90700bf26c30a54f2de74e4122ac6d274ecfd8b5</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/1lXwd3eZOE8KoLsmIw7evmiDFTF.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/eXs3lQsZE8O9rUrwEryRsDmL9I6.jpg</fanart>
<date>2020</date>
<genre>Aventuras. Drama</genre>
<extra>NA</extra>
<info>Un niño emprende la misión para intentar salvar a su madre enferma buscando una figura mítica que se dice que tiene poderes curativos mágicos.</info>
 
 
<page>8</page><title>EL HOMBRE DEL NORTE</title>
<microhd>NA</microhd>
<fullhd>ff53eb0ae6b0a53e328848e7836bc52072b6f315</fullhd>
<tresd>NA</tresd>
<cuatrok>cb7d46cce12134c072e48c87b3f5c1c35dda1593</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/rdx0bIkwxW3EHvWn5kxZBFUT1Am.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/wlLYqhnaNKy7S9UGL2Q4bNAIrKF.jpg</fanart>
<date>2022</date>
<genre>Aventuras. Acción. Drama</genre>
<extra>NA</extra>
<info>El príncipe Amleth está a punto de convertirse en hombre pero, en ese momento, su tío asesina brutalmente a su padre y secuestra a la madre del niño. Dos décadas después, Amleth es un vikingo que tiene la misión de salvar a su madre.</info>
 
 
<page>8</page><title>EL HOMBRE DEL SOTANO</title>
<microhd>efba48609ffa53503839b3e36fcc2d76467cd05b</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/rFC9x1q4gZhATpYa8dKquhe1v2J.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/iF6NBWUtjixuf6TdrxXpvfvnKM6.jpg</fanart>
<date>2021</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>En París, una pareja de orígenes judíos decide vender un sótano insalubre en el edificio donde viven. Un hombre normal y corriente, el señor Fonzic (François Cluzet), aparece para comprarlo. Hasta aquí nada inusual, pero el hombre, que resulta ser un negacionista del holocausto, se muda al sótano y lo convierte en su residencia permanente. La pareja, intenta desesperadamente cancelar la venta, sin éxito. Poco a poco, su presencia cambiará la vida de la pareja.</info>
 
 
<page>8</page><title>EL HOMBRE INVISIBLE</title>
<microhd>c393c6250346f7cdb5bbbde450c646d34d3e4c6c</microhd>
<fullhd>12a759df0b7703acb525f425824041e84d91a68b</fullhd>
<tresd>NA</tresd>
<cuatrok>2f1c0ddc4a4fb0f6b5741dedf3e3f6adba4e5806</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/xk4RpcvVQ7JmnrkQtZ479EwWieT.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/uZMZyvarQuXLRqf3xdpdMqzdtjb.jpg</fanart>
<date>2020</date>
<genre>Ciencia ficción. Intriga. Terror </genre>
<extra>NA</extra>
<info>Cecilia (Elisabeth Moss) rehace su vida tras recibir la noticia de que su exnovio, un maltratador empedernido, ha fallecido. Sin embargo, su cordura comienza a tambalearse cuando empieza tener la certeza de que en realidad sigue vivo.</info>
 
 
<page>8</page><title>EL HOYO</title>
<microhd>8a5c4ac56cda44b800be26ac3fc3b18d4d85fcb7</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/qv30R1uDDKTLWDlNc3qi6MRdRf0.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/uNiTCaBIpw4vrRGEFKFm3VSt8Sm.jpg</fanart>
<date>2019</date>
<genre>Ciencia ficcion. Thriller</genre>
<extra>NA</extra>
<info>El futuro, en una distopía. Dos personas por nivel. Un número desconocido de niveles. Una plataforma con comida para todos ellos. ¿Eres de los que piensan demasiado cuando están arriba? ¿O de los que no tienen agallas cuando están abajo? Si lo descubres demasiado tarde, no saldrás vivo del hoyo.</info>
 
 
<page>8</page><title>EL IMAGINARIO DEL DOCTOR PARNASSUS</title>
<microhd>NA</microhd>
<fullhd>43fb4217612a0f168db71fa7ec3b41c6b39cfec9</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/lzceuiHbqiOGRkDjzysVQ15DVK7.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/cxqsMFPdZpU2Lhjg0mcSyz4k6j8.jpg</fanart>
<date>2009</date>
<genre>Fantástico. Aventuras</genre>
<extra>Mis</extra>
<info>Con una extraordinaria habilidad para guiar la imaginación de los demás, el Doctor Parnassus oculta un oscuro secreto. Siglos atrás hizo un trato con el Diablo para ganar inmortalidad, a cambio de entregarle a cualquier hijo que tuviese al cumplir los 16 años. Ahora, deambulando con su humilde teatro ambulante y con una hija a punto de llegar a esa edad, Parnassus está decidido a cambiar el trato.</info>
 
 
<page>8</page><title>EL INSOPORTABLE PESO DE UN TALENTO DESCOMUNAL</title>
<microhd>NA</microhd>
<fullhd>d904c6bdf211d25b1fd9f20ba838de4031575a49</fullhd>
<tresd>NA</tresd>
<cuatrok>a39d242dc5dc091ee9f2ae6b69770f562d93efc0</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/nLXJRnVQlajzNYd8xDHC553Yi8H.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/M7Kiquud2bjrhQvZXeIrvW0J4a.jpg</fanart>
<date>2022</date>
<genre>Acción. Comedia</genre>
<extra>NA</extra>
<info>Nicolas Cage se interpreta a sí mismo en esta loca comedia de acción. La versión ficticia de Cage es un actor sin blanca que se ve obligado a aceptar una oferta de un millón de dólares para asistir al cumpleaños de un superfan excéntrico multimillonario (Pedro Pascal). Pero la situación da un giro inesperado cuando Cage es reclutado por una agente de la CIA (Tiffany Haddish) y obligado a estar a la altura de su propia leyenda para salvarse a sí mismo y a sus seres queridos. Con una carrera construida para este preciso momento, el actor ganador de premios debe asumir el papel de su vida: Nick Cage.</info>
 
 
<page>8</page><title>EL INSTANTE MAS OSCURO</title>
<microhd>61be3049e3de8b21a37d249bd7239f21dcce7e49</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/bvfIACbN7YtScwvEt0UhGsheWae.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/zXwFJMwvQcJFitP9GcHZvHAHGe8.jpg</fanart>
<date>2017</date>
<genre>Drama. Belico</genre>
<extra>NA</extra>
<info>Mayo de 1940. Winston Churchill (Gary Oldman) se convierte en primer ministro británico en un momento realmente crucial de la Segunda Guerra Mundial, pues los nazis avanzan imparables conquistando prácticamente la totalidad del continente europeo y amenazando con una invasión a Inglaterra. Churchill deberá entonces explorar la posibilidad de un tratado de paz con Alemania, o ser fiel a sus ideales y luchar por la liberación de Europa.</info>
 
 
<page>8</page><title>EL IRLANDES</title>
<microhd>33938c384213d067b1dc2b8cc5a59da070a5870d</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/sNilmIAwBbH0DmBOWBS6gWvNDex.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ww5aGS5H2tUtckxFFNOJE2790S7.jpg</fanart>
<date>2019</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Frank Sheeran fue un veterano de la Segunda Guerra Mundial, estafador y sicario que trabajó con algunas de las figuras más destacadas del siglo XX. 'El irlandés' es la crónica de uno de los grandes misterios sin resolver del país: la desaparición del legendario sindicalista Jimmy Hoffa. Un gran viaje por los turbios entresijos del crimen organizado; sus mecanismos internos, sus rivalidades y su conexión con la política. Adaptación del libro "I Heard You Paint Houses", de Charles Brandt, a cargo del guionista Steven Zaillian (La lista de Schindler, American Gangster).</info>
 
 
<page>8</page><title>EL JARDIN SECRETO</title>
<microhd>03b10ed931b03278e38dd7362f09d6dfd38b1aad</microhd>
<fullhd>6d3d9da62385b7b5bcd655cdb9d49d763d7ddaeb</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/figneljeYCOBLAyyi1odZxz8h5g.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/8PK4X8U3C79ilzIjNTkTgjmc4js.jpg</fanart>
<date>2020</date>
<genre>Drama. Fantástico. Aventuras.Infantil</genre>
<extra>NA</extra>
<info>Mary Lennox (Dixie Egerickx) es un niña nacida en la India de padres británicos. Cuando ambos fallecen de repente, Mary es enviada a Inglaterra con su tío, Archibald Craven (Colin Firth), que vive en un remoto paraje en el páramo de Yorkshire. Allí comienza a descubrir secretos familiares, sobre todo después de conocer a su enfermizo primo (Edan Hayhurst), que durante años ha estado encerrado en una de las alas de la mansión. Juntos, los dos niños exploran un jardín descubierto por Mary, que les ayuda a cerrar las heridas que han sufrido en su corta existencia.</info>
 
 
<page>8</page><title>EL JINETE DEL DRAGON</title>
<microhd>431d7eca13a50ac7cc7bdcf77e37e8f2456e2e62</microhd>
<fullhd>f1f164abe3f3304faae645f99cb33f4d9e4e5193</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/g8TTuBZFSFzQLTyP92XE1I9WVVC.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/lMVNROwayzVQTe3h7alrjq7mobM.jpg</fanart>
<date>2020</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Un joven dragon plateado se une a un espíritu de la montaña y a un niño huérfano en una aventura en el Himalaya para encontrar la entrada a los cielos.</info>
 
 
<page>8</page><title>EL JINETE PALIDO</title>
<microhd>NA</microhd>
<fullhd>cb979b7da3e25d967c5928a2a6f3e28e825f94d6</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/3O8N9atcWqH1xmKMPHyjiGXSDdg.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/lSH1j2uChyuxKmslXRf1AGyi43p.jpg</fanart>
<date>1985</date>
<genre>Accion</genre>
<extra>Mis</extra>
<info>Un grupo de colonos buscadores de oro se establece en un lugar de California, pero sufren el acoso de los hombres de Lahood, el propietario del resto de las explotaciones mineras. Pero un día al poblado llega un misterioso y frío predicador (Clint Eastwood) que se pone de parte de los colonos, y comienza a enfrentarse al temido cacique local.</info>
 
 
<page>8</page><title>EL JOVENCITO FRANKENSTEIN</title>
<microhd>AFF1BD61D80540853D7C182C5DD73618ED7945FF</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ai5w3uAAwqeKdJhypxo9zZVCizC.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/wKSJLUgJtCMSZOIU1By8g5FSaHk.jpg</fanart>
<date>1974</date>
<genre>Comedia. Terror</genre>
<extra>Culto</extra>
<info>El joven doctor Frederick Frankenstein, un neurocirujano norteamericano, trata de escapar del estigma legado por su abuelo, quien creó años atrás una horrible criatura. Pero, cuando hereda el castillo de Frankenstein y descubre un extraño manual científico en el que se explica paso a paso cómo devolverle la vida a un cadáver, comienza a crear su propio monstruo.</info>
 
 
<page>8</page><title>EL JUEGO DE LAS LLAVES</title>
<microhd>NA</microhd>
<fullhd>83b52fb41fba8a886c0b4b5331720129b1011811</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/149Ah5I0qnI4Fe4qegeEHk3i4BK.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/1fwW0N8tZktMMIGm1hV8wWycW8P.jpg</fanart>
<date>2022</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Laura lleva toda la vida con Antonio y, justo cuando se empieza a plantear si realmente esa es la vida que quiere, Sergio y Siena se cruzan en sus vidas. Sergio es un excompañero de instituto y Siena, su novia millenial que llega para revolucionar las vidas de todos. Laura, Raquel y Cris, íntimas amigas desde hace años, convencen a sus respectivos maridos para jugar a un juego que les propone Siena: el juego de las llaves. El juego consiste en que todos ponen sus llaves en un cuenco. Al azar, cada uno escoge unas llaves y este debe ir a pasar la noche con el dueño o la dueña de las llaves. Ese juego revolucionará al grupo de amigos y sus vidas. Les hará descubrir quiénes son y qué quieren realmente. Una comedia sobre la monogamia a largo plazo, la autorrealización y el placer. Una historia sobre cuatro parejas que son amigos y deciden ser swingers entre ellos. Remake en versión para cine de la serie mexicana del mismo nombre.</info>
 
 
<page>8</page><title>EL JUICIO DE LOS 7 DE CHICAGO</title>
<microhd>a8c1fafd7ceb098a5b31056a93d3681a91826abb</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/3pbPyNkwl8DOGUgO9OqHdHiIrAi.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/tUEBmT7KqteCrK15kYRgvrE34jf.jpg</fanart>
<date>2020</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>En 1969 se celebró uno de los juicios más populares de la Historia de Estados Unidos, en el que siete individuos fueron juzgados tras ser acusados de conspirar en contra de la seguridad nacional. Este hecho traería una serie de conflictos sociales (manifestaciones, movimientos ciudadanos) que pasarían a la posteridad en una época de grandes cambios en todos los niveles del pueblo norteamericano.</info>
 
 
<page>8</page><title>EL LIBRO DE LA SELVA</title>
<microhd>NA</microhd>
<fullhd>8a2bb59d24f335b938b68e79e1f63abfb354f086</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/pQGB08Kxyt3TLN4L01E175BRcvu.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/tB2w4m0rW62MTufTjRj0gFLMVBP.jpg</fanart>
<date>2016</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Mowgli (Neel Sethi), un niño criado en la selva por una manada de lobos, emprende un fascinante viaje de autodescubrimiento cuando se ve obligado a abandonar el único hogar que ha conocido en toda su vida. Nueva adaptación de la novela de Rudyard Kipling.</info>
 
 
<page>8</page><title>EL LOBO DE SNOW HOLLOW</title>
<microhd>a0b37351339e5ca9a358e82b557cb3453d84d9f8</microhd>
<fullhd>0aa68f3c24d99ec7fdfa47a563e93ad43de9d149</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/iVxiT6JxvrYelR1uDHbZQ0DYkYb.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/hLtv6gYV9hIPWPFDniLSCodhmq.jpg</fanart>
<date>2020</date>
<genre>Terror. Comedia. Thriller</genre>
<extra>NA</extra>
<info>El terror se adueña de un pequeño pueblo al pie de las montañas cuando una serie de cadáveres son descubiertos tras la luna llena. Tras perder a su hija adolescente, incapaz de dormir y al cuidado de su padre enfermo, el oficial Marshall lucha para recordar cada día que no existe tal cosa como eso que llaman "hombres-lobo".</info>
 
 
<page>8</page><title>EL MAESTRO JARDINERO</title>
<microhd>NA</microhd>
<fullhd>49782379bf02ca08fe454732422763f509a93ecf</fullhd>
<tresd>NA</tresd>
<cuatrok>502e9d0111aee0db5c06a79ff26279d8533a69b5</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/kYNUoG7OjbHSbowh0agN9EeZRU.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/kmsC7wvrKbla7kCg6YTWHtxmaGI.jpg</fanart>
<date>2023</date>
<genre>Thriller. Drama</genre>
<extra>Estreno</extra>
<info>Narvel Roth (Joel Edgerton) es el meticuloso horticultor de Gracewood Gardens. Está tan dedicado a cuidar los jardines de esta maravillosa e histórica finca como a complacer a su jefa, la rica viuda Sra. Havernhill (Sigourney Weaver). Pero el caos se apodera de la ordenada existencia de Narvel cuando la Sra. Haverhill le exige que tome como aprendiz a su rebelde y problemática sobrina nieta Maya (Quintessa Swindell). Esta nueva situación va a sacar a la luz oscuros secretos de un pasado violento que también es una amenaza para todos.</info>
 
 
<page>8</page><title>EL MAGNIFICO IVAN</title>
<microhd>ef6c3130f151966c781b0566c5bd2f1ec91d8515</microhd>
<fullhd>b802609c62bacb156eebdcfc32db3f2b021d904b</fullhd>
<tresd>NA</tresd>
<cuatrok>a6ab3cb6983247853f8f4186de4227ee5ea97e25</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/a8DMtkQBCA44tDNxapqsF99M6K0.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/fFdOJxmG2U7IYYlkFKtDk1nGPhF.jpg</fanart>
<date>2020</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Un gorila llamado Ivan trata de unir las piezas que conforman el puzle de su pasado con la ayuda de Stella, un elefante con el que urde un plan para huir de la cautividad</info>
 
 
<page>8</page><title>EL MEDIADOR (BLACKLIGHT)</title>
<microhd>NA</microhd>
<fullhd>748aef04c12a7e379477d8c3fd33f6e5f4f5485e</fullhd>
<tresd>NA</tresd>
<cuatrok>300edd5a1d68f35680e32fcff62e18f148674315</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/idi3NPEXNXYjgk8RMmmqHYPGXdP.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/iDeWAGnmloZ5Oz3bocDp4rSbUXd.jpg</fanart>
<date>2022</date>
<genre>Acción. Thriller</genre>
<extra>NA</extra>
<info>Travis Block, un agente del gobierno en la sombra que se especializa en eliminar operativos cuyas coberturas han sido expuestas, descubre una conspiración mortal dentro de sus propias filas que alcanza los niveles más altos del poder.</info>
 
 
<page>8</page><title>EL MENU</title>
<microhd>NA</microhd>
<fullhd>c217eef8bf6e576f99cc42ff26c3d21b9cf2e2e6</fullhd>
<tresd>NA</tresd>
<cuatrok>06160c0d37950066a40af89362c4f8c34ad4835d</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/eNcfCScUUyHQyjP84P25a8ColWp.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/mSyQoValhBsJdq3JNGXJww2Q5yL.jpg</fanart>
<date>2022</date>
<genre>Comedia. Thriller. Terror</genre>
<extra>NA</extra>
<info>Una joven pareja viaja a uno de los destinos más exclusivos del mundo para cenar en un restaurante que ofrece una experiencia culinaria única. Sin embargo, el chef (Fiennes) ha preparado un ingrediente secreto que tendrá un resultado sorprendente en los dos enamorados.</info>
 
 
<page>8</page><title>EL METODO WILLIANS</title>
<microhd>794177A3DAD4D165A54781B63C6B262A4F7D4FB4</microhd>
<fullhd>2789dbff28f3980f25cd4ee2590f1a59051500c3</fullhd>
<tresd>NA</tresd>
<cuatrok>c63ff8ea03588e426a056091a0424d30812a45a9</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/vKwpFmvvhPiZmuou2D6AOsFjReF.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/z2B6hwZZDHvwt9DxljNejHx6dVh.jpg</fanart>
<date>2021</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Biopic sobre Richard Williams, un padre inasequible al desaliento que ayudó a formar a dos de las deportistas más extraordinarias de todos los tiempos, dos atletas que acabarían marcando época en el deporte del tenis. Richard tenía una visión muy clara del futuro de sus hijas, y sirviéndose de métodos arriesgados y poco convencionales, elaboró un plan que llevaría a Venus y Serena Williams de las calles de Compton, California, al olimpo del deporte, convirtiéndolas en iconos del tenis.</info>
 
 
<page>8</page><title>EL MILAGRO DEL PADRE STU</title>
<microhd>c0f37628e351b1fdaae9bc993ce8d1e3c6cb3207</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/hQ7rObRPHpLwyfz0a2wasTAXfSK.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/rmhMB6KBxdSYatfiFoas60uF6Fc.jpg</fanart>
<date>2022</date>
<genre>Drama </genre>
<extra>NA</extra>
<info>La historia real de un boxeador convertido en sacerdote. Cuando una lesión pone fin a su carrera como boxeador amateur, Stuart Long se muda a Los Ángeles en busca de dinero y fama. Mientras se las arregla como empleado de un supermercado, conoce a Carmen, una maestra de escuela dominical que parece inmune a su encanto de chico malo. Decidido a ganársela, el agnóstico de toda la vida comienza a ir a la iglesia para impresionarla. Sin embargo, un accidente de motocicleta lo deja preguntándose si puede usar su segunda oportunidad para ayudar a otros, lo que lo lleva a darse cuenta de que está destinado a ser un sacerdote católico.</info>
 
 
<page>8</page><title>EL MISTERIO DEL DRAGON</title>
<microhd>90d124db1f88c25c6a6ae27fb75da6f8cfa28a67</microhd>
<fullhd>5637c59325a0da0490b21f410615522f3f27bdb5</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/hM0FtHli2m2R9qqxPnIkdG2FZi9.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/x4UvBrMe8KLfAs5fK7QhLsNiems.jpg</fanart>
<date>2019</date>
<genre>Fantástico. Aventuras. Acción</genre>
<extra>NA</extra>
<info>El cartógrafo inglés Jonathan Green recibe la orden de crear un mapa de la lejana Rusia. En su largo viaje, Jonathan vivirá todo tipo de aventuras, desde enfrentamientos con extrañas criaturas y batallas con maestros en artes marciales hasta brujas milenarias ocultas en los rincones más recónditos de la legendaria China. Pero todas las pruebas parecerán un juego de niños cuando tenga que enfrentarse al mayor de los enemigos jamás creado por la magia negra: el gran Rey de los Dragones.</info>
 
 
<page>8</page><title>EL MUNDO ES VUESTRO</title>
<microhd>NA</microhd>
<fullhd>a560505d9e9947f2cb7118a48e488d7d870bd00a</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/htzdiCe0WBFxCCcRk45aD65SuRn.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/rN4SV1I4o3OY76BgTwO0AXY44kW.jpg</fanart>
<date>2022</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Rafi (Alfonso Sánchez), completamente arruinado, se cuela en la montería organizada por la Marquesa que reúne a toda la alta sociedad española para venderles su negocio y dar el pelotazo por fin. Dentro está Fali (Alberto López), que ha sido reprogramado, y ya no es un compadre. Juntos descubrirán que la montería no es lo que parece, y que en la finca se está decidiendo el futuro de España.</info>
 
 
<page>8</page><title>EL NIÑO DETRAS DE LA PUERTA</title>
<microhd>NA</microhd>
<fullhd>d9e3eb6175189e10d0910a23fc99faf08c122548</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/kj06cDpZKYKvXstU1EU9NDL7jAe.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/wsiCAh4YKqMxpZ0pd0Ke6tZS9Gz.jpg</fanart>
<date>2022</date>
<genre>Thriller. Intriga. Terror</genre>
<extra>NA</extra>
<info>Cuando Bobby y su mejor amigo, Kevin, son secuestrados y encerrados en una extraña casa en mitad de ninguna parte, el primero logra escapar. Pero a medida que se aleja escucha los gritos desesperados de su compañero, por lo que decide dar media vuelta y tratar de ayudarle.</info>
 
 
<page>8</page><title>EL NIÑO QUE DOMO EL VIENTO</title>
<microhd>311dafd57766cb5f364366898579748bcb9f23ab</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/kjdYOYNpPTgL9Ll8fVGlTyfbc08.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/hpGkXl6iNdtQLXHS7xiv57N217B.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Un chico de la República de Malaui decide ayudar a las personas de su pueblo construyendo una turbina después de leer un libro en el que se explican los pasos para su creación. Inspirada en una historia real.</info>
 
 
<page>8</page><title>EL NOMBRE DE LA ROSA</title>
<microhd>NA</microhd>
<fullhd>4226708bdf0709542d3c21eb8af0f78d56c82d0d</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/f8vGTeZnZEV1Y5NEp1bjTkq5NzU.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/2JB2nJQOBZ7Q9vAtzbha2YsqJbH.jpg</fanart>
<date>1986</date>
<genre>Intriga. Drama</genre>
<extra>Mis</extra>
<info>Siglo XIV. Fray Guillermo de Baskerville (Sean Connery), monje franciscano y antiguo inquisidor, y su inseparable discípulo, el novicio Adso de Melk (Christian Slater), visitan una abadía benedictina, situada en el norte de Italia, para esclarecer la muerte del joven miniaturista Adelmo de Otranto. Durante su estancia, desaparecen misteriosamente otros monjes que después aparecen muertos.</info>
 
 
<page>8</page><title>EL OFICIAL Y EL ESPIA</title>
<microhd>af52999f0e55a78fbf090835bb040d1810051182</microhd>
<fullhd>8b843a4da3e7a1ed974f1473ab64e03876a2f570</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/uQCZY0JGa5c5Plku7rtcyIcXip7.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/jArl1j9sL2mYjRW23EaZKWAr2E8.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>En 1894, el capitán francés Alfred Dreyfus, un joven oficial judío, es acusado de traición por espiar para Alemania y condenado a cadena perpetua en la Isla del Diablo, en la Guayana Francesa. Entre los testigos que hicieron posible esta humillación se encuentra el coronel Georges Picquart, encargado de liderar la unidad de contrainteligencia que descubrió al espía. Pero cuando Picquart se entera de que se siguen pasando secretos militares a los alemanes, se adentrará en un peligroso laberinto de mentiras y corrupción, poniendo en peligro su honor y su vida.</info>
 
 
<page>8</page><title>EL OTRO GUARDAESPALDAS</title>
<microhd>1b146aeb0e45d687ff878ce03be80bdaab0d9df1</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/1nZslwtHa8vEF1EmghFY49zgCx8.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/7KsqfXDECZMryX1Rv4RKsT7SIjQ.jpg</fanart>
<date>2017</date>
<genre>Accion. Comedia</genre>
<extra>NA</extra>
<info>El prestigioso guardaespaldas Michael Bryce (Ryan Reynolds) recibe un nuevo cliente: un asesino a sueldo, Darius Kincaid (Samuel L. Jackson), que debe testificar en un juicio en La Haya contra un cruel dictador (Gary Oldman).</info>
 
 
<page>8</page><title>EL OTRO GUARDAESPALDAS 2</title>
<microhd>NA</microhd>
<fullhd>b87559b4ddeb17176926f595b463f3de5914c173</fullhd>
<tresd>NA</tresd>
<cuatrok>d60cca04ae6d0e14140e1109073b844b6d457ba0</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/mpfIHi2nT7hRGphllLz0Nz8fIFV.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/bjIPzixuWnOzxDG25WaXKuy9lYZ.jpg</fanart>
<date>2021</date>
<genre>Acción. Comedia</genre>
<extra>NA</extra>
<info>Secuela de 'El otro guardaespaldas' (2017). El guardaespaldas Michael Bryce (Ryan Reynolds) y el asesino a sueldo Darius Kincaid (Samuel L. Jackson) vuelven a la carga en una nueva misión para garantizar la paz y estabilidad en Europa. Bryce, aún bajo investigación y sin licencia, se encuentra disfrutando de su año sabático, cuando Sonia Kincaid (Salma Hayek), la impulsiva y peligrosa esposa de Darius, reaparece para que la ayude a liberar a su marido y luchar contra un complot mundial en el que están implicados un malvado griego (Antonio Banderas) y un célebre exagente (Morgan Freeman).</info>
 
 
<page>8</page><title>EL PACTO</title>
<microhd>28dbcc4cfde22d2716ca0402bc5343e863551310</microhd>
<fullhd>eea0f0178f55d4ca8890be784645a0d4402a4e7a</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/gpcIEW8fcmuPzJQfFr4X3PtWVWs.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/zcTinIK3viPag7sn4XYjO4DzDaN.jpg</fanart>
<date>2018</date>
<genre>Terror. Thriller</genre>
<extra>NA</extra>
<info>Mónica ve cómo, de repente, Clara, su hija, entra en un coma profundo e inexplicable. Cuando los médicos la dan por muerta, un desconocido le propone un pacto: él salvará a Clara pero, a cambio, Mónica deberá entregarle una vida. Mónica acepta y, contra todo pronóstico, su hija se salva. Ahora ella deberá entregarle otra vida a cambio.</info>
 
 
<page>8</page><title>EL PADRE</title>
<microhd>NA</microhd>
<fullhd>6d8a0e7192a2bd0feb4e274820608e88c462fe5e</fullhd>
<tresd>NA</tresd>
<cuatrok>2419be6fd35d2c2789a66d680e74726b43b81cb8</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/yjY9ElmM48sZaW1xktVyM2aa5QB.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/xaARARWguVSXtMlr4z6xMiePe2i.jpg</fanart>
<date>2020</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Anthony (Anthony Hopkins), un hombre de 80 años mordaz, algo travieso y que tercamente ha decidido vivir solo, rechaza todos y cada uno de las cuidadoras que su hija Anne (Olivia Colman) intenta contratar para que le ayuden en casa. Está desesperada porque ya no puede visitarle a diario y siente que la mente de su padre empieza a fallar y se desconecta cada vez más de la realidad. Anne sufre la paulatina pérdida de su padre a medida que la mente de éste se deteriora, pero también se aferra al derecho a vivir su propia vida.</info>
 
 
<page>8</page><title>EL PADRE DE LA NOVIA</title>
<microhd>NA</microhd>
<fullhd>e5eba98959c08511237034912501ab8f51a7a7e8</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/wuhRy3B36plwnAxAUKD0AgzZC7a.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/oQMqMLIJXZAVMplga9RhDyZInAW.jpg</fanart>
<date>2022</date>
<genre>Comedia. Drama. Romance</genre>
<extra>NA</extra>
<info>Billy e Ingrid llevan mucho tiempo casados y les sorprende que su hija mayor, Sofía, vuelva a Miami de visita. Pero las sorpresas continúan cuando ella les presenta a su nuevo novio, Adan, y les dice que están pensando mudarse a México. Nueva versión de 'El padre de la novia' (1991) ahora protagonizada por una familia latina que vive en Estados Unidos.</info>
 
 
<page>8</page><title>EL PADRINO</title>
<microhd>161acc9453d65abe0ad2e2d8aa22348951bd43ad</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/so55lmelL3JbcK2GiVH98I6Iy8u.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/rSPw7tgCH9c6NqICZef4kZjFOQ5.jpg</fanart>
<date>1972</date>
<genre>Drama. Saga</genre>
<extra>Culto</extra>
<info>América, años 40. Don Vito Corleone (Marlon Brando) es el respetado y temido jefe de una de las cinco familias de la mafia de Nueva York. Tiene cuatro hijos: Connie (Talia Shire), el impulsivo Sonny (James Caan), el pusilánime Fredo (John Cazale) y Michael (Al Pacino), que no quiere saber nada de los negocios de su padre. Cuando Corleone, en contra de los consejos de 'Il consigliere' Tom Hagen (Robert Duvall), se niega a participar en el negocio de las drogas, el jefe de otra banda ordena su asesinato. Empieza entonces una violenta y cruenta guerra entre las familias mafiosas. </info>
 
 
<page>8</page><title>EL PADRINO. PARTE II</title>
<microhd>979b5647ed32b6ef417d40808594cd82857ac81c</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/iJqEycKV96JCBWqx2Ij38Ufa8EH.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/poec6RqOKY9iSiIUmfyfPfiLtvB.jpg</fanart>
<date>1974</date>
<genre>Drama. Saga</genre>
<extra>Culto</extra>
<info>Continuación de la historia de los Corleone por medio de dos historias paralelas: la elección de Michael como jefe de los negocios familiares y los orígenes del patriarca, Don Vito Corleone, primero en su Sicilia natal y posteriormente en Estados Unidos, donde, empezando desde abajo, llegó a ser un poderosísimo jefe de la mafia de Nueva York.</info>
 
 
<page>8</page><title>EL PADRINO. PARTE III</title>
<microhd>6165595f735ac557ef210f32abb40dd361a588fa</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/39T8QSSfczilXlMExRX4NDJrm8C.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/gh6KNAyGTJMQ2yXTwKEHE09RoYp.jpg</fanart>
<date>1990</date>
<genre>Drama. Saga</genre>
<extra>Culto</extra>
<info>Michael Corleone, heredero del imperio de don Vito Corleone, intenta rehabilitarse socialmente y legitimar todas las posesiones de la familia negociando con el Vaticano. Después de luchar toda su vida se encuentra cansado y centra todas sus esperanzas en encontrar un sucesor que se haga cargo de los negocios. Vincent, el hijo ilegítimo de su hermano Sonny, parece ser el elegido.</info>
 
 
<page>8</page><title>EL PAIS DE LOS SUEÑOS</title>
<microhd>NA</microhd>
<fullhd>cb4d9e05cd569d4489b2d1aab6839a5fdd977850</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/xvsftW0ai916VTtH2gPjSFC7mGN.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/2moTpZqkQPDOrYn2CfVXdFxVZQk.jpg</fanart>
<date>2022</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Una niña busca a su padre, desaparecido en un místico país de los sueños, con la ayuda de una gran criatura mitad hombre y mitad monstruo.</info>
 
 
<page>8</page><title>EL PARAMO</title>
<microhd>e5139a880aa1d7bb059174eeb2843b9570bd371c</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/hdi08iM9ClmtCAcnZP04QLAV2zz.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/mhIPm0FhVX60n9sSlcWL436TFae.jpg</fanart>
<date>2021</date>
<genre>Terror. Intriga</genre>
<extra>NA</extra>
<info>Narra la historia de una familia que vive aislada del resto de la sociedad, cuya tranquilidad se verá perturbada por una presencia aterradora que pondrá a prueba los lazos que les unen.</info>
 
 
<page>8</page><title>EL PARQUE MAGICO</title>
<microhd>8eb21728ecd64ef7e49ba3109853859b39ef7e85</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/eW6JtZ5cyzfVe5s9DFDKaJay94b.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/8HFKkF1KubemUngzL1VGuOBKuov.jpg</fanart>
<date>2019</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>June (Brianna Denski) es una niña optimista y alegre que un día descubre un parque de atracciones llamado "Wonderland" escondido en el bosque. Allí conoce a una serie de divertidos animales que le explican que el parque, su hogar durante muchos años, está prácticamente en ruinas. Sólo June, con la ayuda de su imaginación y de sus nuevos amigos, podrá arreglarlo para salvar así a los animales que en él habitan, llevando de vuelta la magia a un lugar de ensueño. </info>
 
 
<page>8</page><title>EL PEOR VECINO DEL MUNDO</title>
<microhd>NA</microhd>
<fullhd>8d7bcf934bced8f51539db9a586529d5ee05d48e</fullhd>
<tresd>NA</tresd>
<cuatrok>1b89bffda5653886ac814e9880647fea41cb8975</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/8bNLrZt9lrmnt6LsaGG8GfDfgYR.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/wUvnilQQ6LWNvSHrYZb5x4ALjL1.jpg</fanart>
<date>2022</date>
<genre>Drama. Comedia </genre>
<extra>NA</extra>
<info>Otto Anderson (Tom Hanks) es un viudo cascarrabias y muy obstinado. Cuando una alegre joven familia se muda a la casa de al lado, Otto encuentra la horma de su zapato en la espabilada, y muy embarazada, Marisol, con la que entablará una muy improbable amistad que pondrá su mundo patas arriba... Remake de la película sueca 'A Man Called Ove' de 2015. </info>
 
 
<page>8</page><title>EL PIANISTA</title>
<microhd>ae5edcb188c2b43509763e52a3bb640cda03b1e8</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/mxfLOWnHnSlbdraKfzRn5mqoqk7.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/fuFcyX9pXcLeKfsEpArtnqV3gyX.jpg</fanart>
<date>2002</date>
<genre>Drama</genre>
<extra>Mis</extra>
<info>Wladyslaw Szpilman, un brillante pianista polaco de origen judío, vive con su familia en el ghetto de Varsovia. Cuando, en 1939, los alemanes invaden Polonia, consigue evitar la deportación gracias a la ayuda de algunos amigos. Pero tendrá que vivir escondido y completamente aislado durante mucho tiempo, y para sobrevivir tendrá que afrontar constantes peligros.</info>
 
 
<page>8</page><title>EL PLAN</title>
<microhd>a14a33e7cc84fa5b84e294376678e0503851e509</microhd>
<fullhd>c490f59bef8143fb12d7a9e979e1ac88b45cd887</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/7WU3ACz95EOi6QUph0zKy3dbvAs.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/tnCVmCrWbhK1ZnRrPNhWkLvOmmU.jpg</fanart>
<date>2020</date>
<genre>Drama. Comedia</genre>
<extra>NA</extra>
<info>Son las nueve de la mañana de un caluroso día de verano en el barrio madrileño de Usera. Paco, Ramón y Andrade, tres amigos que llevan en paro desde que cerró la empresa en la que trabajaban, han quedado para ejecutar un plan. Cuando por fin se reúnen, un contratiempo les impide salir de casa: el coche que necesitaban para trasladarse está averiado. Mientras buscan otra manera de llegar a su destino, se ven envueltos en una serie de incómodas discusiones que poco a poco derribarán sus muros y arrancarán sus máscaras.</info>
 
<page>9</page><title>EL PLANETA DE LOS SIMIOS:</title>
<microhd>NA</microhd>
<fullhd>5b6fd996c9b7b66e77945c50eee1010275a60037</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/1kDUbM10YBFOJxMm6uZEYnCbrvQ.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/ftwsAkyzGEqqiDh1E2tdeOSKS4l.jpg</fanart>
<date>1973</date>
<genre>Ciencia ficción. Fantástico. Aventuras . Saga</genre>
<extra>Mis</extra>
<info>Caesar, el simio que capitaneó la revuelta contra la raza humana, es un benévolo gobernante que fomenta la convivencia con los hombres, a pesar de la oposición del general Aldo (Akins), jefe militar de los simios. No obstante, los hombres que han sobrevivido a un holocausto nuclear son considerados ciudadanos de segunda clase. Caesar decide realizar un viaje a la "Ciudad Prohibida" con MacDonald (Austin Stoker) y el sabio Virgil (Paul Williams) en busca de unas cintas en las que estaría grabada la muerte de sus padres. Durante el viaje, es observado por un grupo de hombres mutantes que viven en la devastada zona de guerra. Este es el motivo de que el General Aldo desafíe la autoridad de Caesar y, además, provoque un enfrentamiento entre simios y hombres para dirimir quién gobernará el planeta.</info>
 
 
<page>9</page><title>EL PLANETA DE LOS SIMIOS: EL AMANECER DEL PLANETA DE LOS SIMIOS</title>
<microhd>f725acf5c9ce97f4e22a3b566ae202844ee598ec</microhd>
<fullhd>65126d56790886a4dc6de74edd1ee7345426c91c</fullhd>
<tresd>NA</tresd>
<cuatrok>fc4803d97f849fcd5443135993192900d9461eea</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/qNu2N4a9nuGWgkeg3NzdRTDOz5N.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/3SozaNPOYUadcmTPgndDibMyDNC.jpg</fanart>
<date>2014</date>
<genre>Ciencia ficción. Acción.Saga</genre>
<extra>Mis</extra>
<info>Tras la aparición del devastador "virus de los simios" desarrollado en un laboratorio y que casi acabó con los humanos, un grupo de simios muy evolucionados, capitaneados por César, se han convertido en la raza dominante del planeta. La única amenaza para su desarrollo la representa un grupo de seres humanos que han sobrevivido al virus, al que ahora son inmunes. Los hombres necesitan la energía de una presa cerca del asentamiento de lo simios, así que César, siempre razonable y buscando el bien de los suyos, firma una tregua de paz que evite estallar una guerra que determinará cuál será la raza dominante... Secuela de la película "El origen del planeta de los simios".</info>
 
 
<page>9</page><title>EL PLANETA DE LOS SIMIOS: EL ORIGEN DEL PLANETA DE LOS SIMIOS</title>
<microhd>NA</microhd>
<fullhd>90472ef4182c1912f59bee93efbf6c07cddcdc89</fullhd>
<tresd>NA</tresd>
<cuatrok>8bd68496a4dbff67d703d23cf1ae43eba824afba</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/kyCXO8lTKs62V5A4IsdNLjvskWM.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/kZuR5hZ1dzXE2EEhOlWBLhcLelj.jpg</fanart>
<date>2011</date>
<genre>Ciencia ficción. Aventuras. Acción. Saga.</genre>
<extra>Mis</extra>
<info>Precuela del ya mítico largometraje "El planeta de los simios". Will Rodman (James Franco) es un joven científico que está investigando con monos para obtener un tratamiento contra el alzheimer, una enfermedad que afecta a su padre (John Lithgow). Uno de esos primates, César, un chimpancé recién nacido al que Will se llevó a casa para protegerlo, experimenta una evolución en su inteligencia verdaderamente sorprendente. En el estudio del simio le ayudará una bella primatóloga llamada Caroline (Freida Pinto).</info>
 
 
<page>9</page><title>EL PLANETA DE LOS SIMIOS: EL PLANETA DE LOS SIMIOS</title>
<microhd>NA</microhd>
<fullhd>fe4b5231bfcb9ac962d3a43b3b0a34449c5e947f</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/rRvUkYCByrlVMy4GMME3iDAe6er.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/rF0Y0fYVsNFEvGve8Nq0Ij9EDCM.jpg</fanart>
<date>2001</date>
<genre>Ciencia ficción. Fantástico. Aventuras. Acción. Saga </genre>
<extra>Mis</extra>
<info>Año 2029. En una misión rutinaria, el astronauta Leo Davidson (Mark Wahlberg) pierde el control de su nave y aterriza en un extraño planeta, que está gobernado por una raza de simios cuya inteligencia es similar a la de los seres humanos, a los que, sin embargo, tratan como si fueran animales. Con la ayuda de una chimpancé (Helena Bonham-Carter) y de un pequeño grupo de rebeldes, Leo encabeza una rebelión contra el poderoso ejército dirigido por el general Thade (Tim Roth). La clave de la victoria consiste en llegar a un templo sagrado, situado en la zona prohibida del planeta, que contiene extraordinarios secretos del pasado.</info>
 
 
<page>9</page><title>EL PLANETA DE LOS SIMIOS: EL PLANETA DE LOS SIMIOS</title>
<microhd>9ed9c4b725ee911d1595d37752430beb6f326bb6</microhd>
<fullhd>edd983d9d634899b56d4637b0f52a8c267a8fcb9</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/6tZTnv7dGQrTdgZrTgvlVDy9WmN.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/zRo7v3syLG19ZaLxZRFRLN1L9iX.jpg</fanart>
<date>1968</date>
<genre>Ciencia ficción. Aventuras.Saga</genre>
<extra>Culto</extra>
<info>George Taylor es un astronauta que forma parte de la tripulación de una nave espacial -en una misión de larga duración- que se estrella en un planeta desconocido en el que, a primera vista, no hay vida inteligente. Sin embargo, muy pronto se dará cuenta de que está gobernado por una raza de simios mentalmente muy desarrollados que esclavizan a unos seres humanos que carecen de la facultad de hablar. Cuando su líder, el doctor Zaius, descubre horrorizado que Taylor posee el don de la palabra, decide que hay que eliminarlo.</info>
 
 
<page>9</page><title>EL PLANETA DE LOS SIMIOS: HUIDA DEL PLANETA DE LOS SIMIOS</title>
<microhd>NA</microhd>
<fullhd>1727958574cecb4c990b44702a8f275614fbc52b</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/2CcWnH9P1472GTRrOcCimhG1mNY.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/5Y2V0yEI8w9xr8Jge6fkFKDILrI.jpg</fanart>
<date>1971</date>
<genre>Ciencia ficción. Fantástico. Aventuras. Saga</genre>
<extra>Mis</extra>
<info>Tres simios, la doctora Zira, Cornelius y el doctor Milo, han conseguido sobrevivir al desastre nuclear que ha destruido su planeta y, tras reparar la nave de los seres humanos, viajan a través del tiempo hasta aterrizar en la Tierra a finales del siglo XX. Mientras son objeto de estudio en un centro de investigación para determinar su nivel de inteligencia, se niegan a revelar sus conocimientos sobre la civilización humana.</info>
 
 
<page>9</page><title>EL PLANETA DE LOS SIMIOS: LA GUERRA DEL PLANETA DE LOS SIMIOS</title>
<microhd>eb3ae78f0e7e01f992fd2830fb5f64281a914683</microhd>
<fullhd>594b47aec6c46c724a00828d8b773cf56276b81a</fullhd>
<tresd>9a6d99510909f7140cf4431a49b78311431528c8</tresd>
<cuatrok>231075c9194266bc41e3d1c8dd22dcb8f66032f1</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/lTwp3vdxcewkaLw41w0mVGRsHYw.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ulMscezy9YX0bhknvJbZoUgQxO5.jpg</fanart>
<date>2017</date>
<genre>Ciencia ficcion. Aventuras. Belico. Saga</genre>
<extra>NA</extra>
<info>César y sus monos son forzados a encarar un conflicto mortal contra un ejército de humanos liderado por un brutal coronel. Después de sufrir pérdidas enormes, César lucha con sus instintos más oscuros en una búsqueda por vengar a su especie. Cuando finalmente se encuentren, Cesar y el Coronel protagonizarán una batalla que pondrá en juego el futuro de ambas especies y el del mismo planeta. Tercera película de la nueva saga de 'El Planeta de los Simios'.</info>
 
 
<page>9</page><title>EL PLANETA DE LOS SIMIOS: LA REBELIOS DE LOS SIMIOS</title>
<microhd>NA</microhd>
<fullhd>37aa09a54574205c350af3f7f43bfc8f7a0cf641</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/jvgClcSOyFrODmnHkTfdKHz7nSv.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/sSoFyfKCcNlKJj1AkKLkHQG3VDD.jpg</fanart>
<date>1972</date>
<genre>Ciencia ficción. Fantástico. Aventuras . Saga</genre>
<extra>Mis</extra>
<info>Estados Unidos, 1991. Tras una epidemia que mató a todos los gatos y perros de la Tierra, los hombres adoptaron como mascotas a los monos, que acabaron convertidos en sirvientes. Se creó entonces un centro para domesticar a los ejemplares salvajes por medio de la violencia y del electroshock. Armando ha criado en secreto a César, el hijo de dos simios parlantes: Zira y Cornelius. Cuando César comprueba lo que los hombres están haciendo con los de su especie, manifiesta públicamente su desacuerdo y, desde entonces, se ve obligado a ocultarse. Cuando descubre que puede comunicarse con los suyos telepáticamente, empieza a organizar una insurrección: incita a los suyos a la desobediencia general, los arma y comineza a vislumbrar que la inteligencia es poder.</info>
 
 
<page>9</page><title>EL PLANETA DE LOS SIMIOS: REGRESO AL PLANETA DE LOS SIMIOS</title>
<microhd>NA</microhd>
<fullhd>55d0aa02a843612101fb6d2597a9af4800e4a187</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/2fxR8ufBjvG6yPRpdCw0I8q8j2M.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/5vhuNmBt3oxbmsu7L4exJjJixC4.jpg</fanart>
<date>1970</date>
<genre>Ciencia ficción. Acción. Saga</genre>
<extra>Mis</extra>
<info>Una nueva nave procedente del espacio y del tiempo aterriza al mando del astronauta Brent, que acude en busca del desaparecido George Taylor. Vuelve a encontrarse con los simios que dominan el mundo, relegando a los humanos a la esclavitud. Sin embargo, con la ayuda de la doctora Zira seguirá las huellas de Taylor hasta la Zona Prohibida, en cuyas catacumbas, lo que son las ruinas del metro de Nueva York, encuentra a una raza humana mutante con poderes telepáticos.</info>
 
 
<page>9</page><title>EL PODER DEL PERRO</title>
<microhd>dc46b3b548809066587545a936e66aebcbcebcfa</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ya0seaFy9E0Q0x3xSvXi12Mi06g.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/gAsHuCQMN7mv4uFIvM4ACQ09hPr.jpg</fanart>
<date>2021</date>
<genre>Drama </genre>
<extra>NA</extra>
<info>Montana, 1925. Los acaudalados hermanos Phil (Cumberbatch) y George Burbank (Plemons) son las dos caras de la misma moneda. Phil es impetuoso y cruel, mientras George es impasible y amable. Juntos son copropietarios de un enorme rancho donde tienen ganado. Cuando George se casa con una viuda del pueblo, Rose (Dunst), Phil comienza a despreciar a su nueva cuñada, que se instala en el rancho junto a su hijo, el sensible Peter (Smit-McPhee).</info>
 
 
<page>9</page><title>EL PRACTICANTE</title>
<microhd>e655d7c532c983310039ccdbb5c0ef31c2bf27fb</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/snyVRvYz8P06rY1FbcsxTT3uZVp.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/lOQGb1IqtBw6l7BR6ODBikYbQhZ.jpg</fanart>
<date>2020</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>Ángel (Mario Casas) trabaja como técnico en emergencias sanitarias a bordo de una ambulancia. Tras sufrir un grave accidente, su vida junto a Vane (Déborah François) empieza a desmoronarse. Obsesionado con la idea de que ella le es infiel, convertirá su vida en un infierno del que será difícil escapar.</info>
 
 
<page>9</page><title>EL PRECIO DE LA VENGANZA</title>
<microhd>NA</microhd>
<fullhd>0b564b6954c319a3ab38ec784f691618121f9d09</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/3asWgnY5FQ2JnRYPlRvz05IykXk.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/znvdYDz61mKEQRlcJSaGDBB0KeS.jpg</fanart>
<date>2022</date>
<genre>Acción. Thriller</genre>
<extra>NA</extra>
<info>Cuando su hija es brutalmente asesinada y la justicia legal parece poco probable, William Duncan decide tomarse la justicia por su mano.</info>
 
 
<page>9</page><title>EL PRECIO DEL PODER  (SCARFACE)</title>
<microhd>f1ef6b4b82372d2aeb7fe9739d68f1ec792168ad</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>bb82a66ca13b45edb882b4f7740c0e9a0f5fa24c</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/32pLDObtIt2MJcdPG9mQKuybImL.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ulrM7v3rmcOE0WYWF48vResVemc.jpg</fanart>
<date>1983</date>
<genre>Drama</genre>
<extra>Culto</extra>
<info>Tony Montana es un emigrante cubano frío e implacable que se instala en Miami con el propósito de convertirse en un gángster importante, y poder así ganar dinero y posición. Con la colaboración de su amigo Manny Rivera inicia una fulgurante carrera delictiva, como traficante de cocaína, con el objetivo de acceder a la cúpula de una organización de narcos</info>
 
 
<page>9</page><title>EL PROFESIONAL (LEON)</title>
<microhd>479fd3fb952ed00133a9a8650bf3e580ce1a05c5</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/d64S30YeHZlCJzscBXJqC16TtiU.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/jRJrQ72VLyEnVsvwfep8Xjlvu8c.jpg</fanart>
<date>1994</date>
<genre>Accion. Thriller. Drama </genre>
<extra>Mis</extra>
<info>Mathilda es una niña de doce años que no se lleva bien con su familia, excepto con su hermano pequeño. Su padre es un narcotraficante que hace negocios con Stan, un corrupto agente de la D.E.A. (Departamento gubernamental contra las drogas). Un día, mientras Mathilda está en un supermercado, Stan mata a su familia. Se refugia entonces en casa de Léon, un solitario y misterioso vecino que resulta ser un asesino a sueldo, pero, como no le queda otra alternativa, hará un pacto con él: ella se encargará de las tareas domésticas y le enseñará a leer a Léon; éste, a cambio, le enseñará a disparar para poder vengarse de quienes mataron a su hermano.</info>
 
 
<page>9</page><title>EL PROFESOR DE PERSA</title>
<microhd>af485cf0c4750cce6c806403363730cb746ccb40</microhd>
<fullhd>4a267b95c4f2e6c1f7ba9cd63604c8f908800d07</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/kkGVeHuC7Os9yRmP44MLCcMu0N1.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/kgP4YOUWKhJZWL0Z8LJ6G6LBMEM.jpg</fanart>
<date>2020</date>
<genre>Drama </genre>
<extra>NA</extra>
<info>Francia, 1942. Gilles es arrestado por soldados de las SS junto con otros judíos y enviado a un campo de concentración en Alemania. Allí consigue evitar la ejecución al jurar a los guardias que no es judío, sino persa. Gracias a esta artimaña, Gilles consigue mantenerse con vida, pero tendrá que enseñar un idioma que no conoce a uno de los oficiales del campo, interesado en aprenderlo. Al tiempo que la relación entre ellos aumenta, las sospechas de los soldados van en incremento.</info>
 
 
<page>9</page><title>EL PROYECTO ADAM</title>
<microhd>1D37CA5B98CC00BFF23A2BB741291D702D9F301D</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>da0a9bfebb9a4b7b768313d92f222c322e6e65e4</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/k5waciRFMN5bwudDrgFLffoAorm.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/y1zfDhKOMAtWhTrpaCVnjj3R4hX.jpg</fanart>
<date>2022</date>
<genre>Ciencia ficción. Fantástico. Acción. Comedia. Aventuras</genre>
<extra>NA</extra>
<info>Adam Reed (Reynolds) es un viajero del tiempo del año 2050 que se ha aventurado en una misión de rescate para buscar a Laura (Zoe Saldana), la mujer que ama, que se perdió en el continuo espacio-tiempo en circunstancias misteriosas. Cuando la nave de Adam se estropea, es enviado en espiral al año 2022, y al único lugar que conoce de esta época de su vida: su casa, donde vive su yo cuando tenía 13 años.</info>
 
 
<page>9</page><title>EL PUENTE DE LOS ESPIAS</title>
<microhd>3A6B7B919B71071A4AC09AE3E2506FA092C88370</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/kZ0zW5ueiBoxNoKKIbKTz1alRv4.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/12M3SvYO4WMdlXCajWklz1zUAly.jpg</fanart>
<date>2015</date>
<genre>Thriller. Drama</genre>
<extra>Mis</extra>
<info>ames Donovan (Tom Hanks), un abogado de Brooklyn (Nueva York) se ve inesperadamente involucrado en la Guerra Fría entre su país y la URSS cuando se encarga de defender a Rudolf Abel, detenido en los Estados Unidos y acusado de espiar para los rusos. Convencido de que Abel debe tener la mejor defensa posible, Donovan incluso rechazará cooperar con la CIA cuando la Agencia intenta que viole la confidencialidad de comunicaciones entre abogado y su cliente. </info>
 
 
<page>9</page><title>EL QUINTO ELEMENTO</title>
<microhd>a7f48f3b58aa3bec7191cfb748fdc12c8119d707</microhd>
<fullhd>8bb17a4285c1916787239f1c12453be77d1f8520</fullhd>
<tresd>NA</tresd>
<cuatrok>055c834b3613c2a8572b50c633e4e2fbb4d72df6</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/wnps5IL8YpeJpZvciDRXt7yZF8a.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/7wI5lpdE2hbxJb0P0T3OhIIbDWR.jpg</fanart>
<date>1997</date>
<genre>Ciencia ficción. Acción</genre>
<extra>Culto</extra>
<info>Cada 5.000 años se abre una puerta entre dos dimensiones. En una dimensión existe el Universo y la vida. En la otra dimensión existe un elemento que no está hecho ni de tierra, ni de fuego, ni de aire, ni de agua, sino que es una anti-energía, la anti-vida: es el quinto elemento.</info>
 
 
<page>9</page><title>EL RASCACIELOS</title>
<microhd>b6a4051db1927a7637898801f8e0e83859934ed9</microhd>
<fullhd>e626e682ac724934910b8abbfe37fb79318e8a6f</fullhd>
<tresd>a7c51d7613996737c700099c51fb8d677b93b7d8</tresd>
<cuatrok>4e00fe85ad484ec025f99ca489c5f291fb41d4e9</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/iZewx4w2Fh8lBuKoOlJqfPC5uWs.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/oMKFQmoVgB69fyXfSMu0lGlHJP2.jpg</fanart>
<date>2018</date>
<genre>Accion. Thriller</genre>
<extra>NA</extra>
<info>Will Sawyer, antiguo líder del Equipo de Rescate de Rehenes del FBI y veterano de guerra del ejército de Estados Unidos, ahora se encarga de evaluar la seguridad de los rascacielos. Durante un viaje de trabajo en China, se ve incriminado en el incendio del edificio más alto y seguro del mundo. Perseguido y a la fuga, Will deberá encontrar a los que le han tendido la trampa, limpiar su nombre y rescatar a su familia, atrapada en el interior del rascacielos… sin sucumbir a las llamas.</info>
 
 
<page>9</page><title>EL REGRESO DE BEN</title>
<microhd>3c3fb43131e1bdc0d1c4b64baa3a529835bdc01f</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/rkbna75rRIO9MEJaemohZeRpRIc.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/7hY7uGllNl1aTyFtG4fBqlCjbQ5.jpg</fanart>
<date>2018</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Ben Burns (Hedges) regresa a casa en la víspera de Navidad. Su madre, Holly (Roberts), le da la bienvenida contenta, pero rápidamente se da cuenta de que está sufriendo. Durante las siguientes 24 horas, Holly hará todo lo posible para evitar que su familia se derrumbe.</info>
 
 
<page>9</page><title>EL REGRESO DE MARY POPPINS</title>
<microhd>d6e919aa3fda5ff59cf633c8a5ba90320b630e8c</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>d9a300c4d1155444a88fc4239d4146066077c1f8</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/d9bLYgkgppdsyGaI4uSrZedBKpA.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/d9bLYgkgppdsyGaI4uSrZedBKpA.jpg</fanart>
<date>2018</date>
<genre>Infantil.Musical</genre>
<extra>NA</extra>
<info>Mary Poppins (Emily Blunt) es la niñera casi perfecta, con unas extraordinarias habilidades mágicas para convertir una tarea rutinaria en una aventura inolvidable y fantástica. Esta nueva secuela, vuelve para ayudar a la siguiente generación de la familia Banks a encontrar la alegría y la magia que faltan en sus vidas después de una trágica pérdida personal. La niñera viene acompañada de su amigo Jack (Lin-Manuel Miranda), un optimista farolero que ayuda a llevar la luz -y la vida- a las calles de Londres.</info>
 
 
<page>9</page><title>EL REHEN</title>
<microhd>dd73a480d2db16f1298bfff6d5de46a17cc99b5e</microhd>
<fullhd>a7caf584781cb246249d183f05e0da3f4b84c123</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/xOxkZGKeiasZtczCh4Yj1U7WZSh.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/2nLOJ8gtattGzYzwlYM8PrcgknH.jpg</fanart>
<date>2018</date>
<genre>Thriller. Accion</genre>
<extra>NA</extra>
<info>Mason Skiles (Jon Hamm) es uno de los diplomáticos más importantes de Estados Unidos que abandonó el Líbano en la década de los 70 tras un trágico incidente. Diez años más tarde, la CIA lo llama de vuelta a un Beirut devastado por la guerra con una misión que sólo él puede cumplir. Mientras tanto, una agente de la CIA (Rosamund Pike) que trabaja encubierta en la embajada estadounidense tiene la tarea de mantener a Skiles con vida y asegurar que la misión sea un éxito. Sin saber quién está de su lado y con vidas en juego, Skiles debe superar todos los obstáculos para exponer la verdad.</info>
 
 
<page>9</page><title>EL REINO</title>
<microhd>bf7b97b9bac00ea03dd8b9f9dfd3d2b6d87fc5ce</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/hPYRQYycflQbUcb6H0XRGwZkGLz.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/aH2LtMpL4T0gIO41D8Z5eXN2G2a.jpg</fanart>
<date>2018</date>
<genre>Thriller. Intriga. Drama</genre>
<extra>NA</extra>
<info>Manuel (Antonio de la Torre), un influyente vicesecretario autonómico que lo tiene todo a favor para dar el salto a la política nacional, observa cómo su perfecta vida se desmorona a partir de unas filtraciones que le implican en una trama de corrupción junto a Paco, uno de sus mejores amigos. Mientras los medios de comunicación empiezan a hacerse eco de las dimensiones del escándalo, el partido cierra filas y únicamente Paco sale indemne. Manuel es expulsado, señalado por la opinión pública y traicionado por los que hasta hace unas horas eran sus amigos. Aunque el partido pretende que cargue con toda la responsabilidad, Manuel no se resigna a caer solo. Con el único apoyo de su mujer y de su hija, y atrapado en una espiral de supervivencia, Manuel se verá obligado a luchar contra una maquinaria de corrupción que lleva años engrasada, y contra un sistema de partidos en el que los reyes caen, pero los reinos continúan.</info>
 
 
<page>9</page><title>EL REINO DE LOS CIELOS ( EL MONTAJE DEL DIRECTOR)</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>d26646bcf7f316a4a5632a08192ef2fd7e53465f</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/zXlzitOjgbz0H5w0GNXWFW4uskW.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/kP8rK9dGS1pr0HrnmXfIi2heWjo.jpg</fanart>
<date>2005</date>
<genre>Aventuras. Romance </genre>
<extra>Mis</extra>
<info>Siglo XII, Europa entera está inmersa en las Cruzadas para recuperar la Tierra Santa. Godofredo de Ibelin (Liam Neeson), caballero respetado por el rey de Jerusalén y comprometido con el mantenimiento de la paz en la región, emprende la búsqueda de su hijo ilegítimo Balian (Orlando Bloom), joven herrero francés que llora la pérdida de su mujer y su hijo. Godofredo convence a su hijo para que lo acompañe en su misión. Tras la muerte de su padre, Balian hereda sus tierras y su título de caballero en Jerusalén, ciudad en la que cristianos, musulmanes y judíos han conseguido convivir pacíficamente durante la tregua entre la Segunda y la Tercera Cruzada. Con una fidelidad inquebrantable, Balian sirve a su rey y, además, se enamora de la princesa Sibylla (Eva Green), la enigmática hermana del monarca.</info>
 
 
<page>9</page><title>EL RENACIDO ( THE REVENANT )</title>
<microhd>4e572c1e29936f1bd4c3146b1e2d52a7d40d72b7</microhd>
<fullhd>7b19eb31f2c66bc9f89a08fc8441520b37f8159d</fullhd>
<tresd>NA</tresd>
<cuatrok>f53943096350fab1e49fbd5d8d734629c59b40cb</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/k3Jh7J5GGMx1lmr7qmynRxOvOK5.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/t81KcskqIsHnNvRfQAVw2nS9Wv7.jpg</fanart>
<date>2015</date>
<genre>Aventuras. Accion</genre>
<extra>Mis</extra>
<info>Año 1823. En las profundidades de la América salvaje, el explorador Hugh Glass (Leonardo DiCaprio) participa junto a su hijo mestizo Hawk en una expedición de tramperos que recolecta pieles. Glass resulta gravemente herido por el ataque de un oso y es abandonado a su suerte por un traicionero miembro de su equipo, John Fitzgerald (Tom Hardy). Con la fuerza de voluntad como su única arma, Glass deberá enfrentarse a un territorio hostil, a un invierno brutal y a la guerra constante entre las tribus de nativos americanos, en una búsqueda implacable para conseguir vengarse.</info>
 
 
<page>9</page><title>EL RESPLANDOR</title>
<microhd>5a52bdebcf7e1ffc70e01f962bd21bb4561171e7</microhd>
<fullhd>4610241be9bac43bcb78b9cdf266d6a05f636e72</fullhd>
<tresd>NA</tresd>
<cuatrok>e414b83e19398d45a528fb1e3303b901fb91905c</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/mm003Mj2e9kJRsrxiVdPn2BSBPh.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/AdKA2F1SzYPhSZdEbjH1Zh75UVQ.jpg</fanart>
<date>1980</date>
<genre>Terror</genre>
<extra>Culto</extra>
<info>Jack Torrance se traslada con su mujer y su hijo de siete años al impresionante hotel Overlook, en Colorado, para encargarse del mantenimiento de las instalaciones durante la temporada invernal, época en la que permanece cerrado y aislado por la nieve. Su objetivo es encontrar paz y sosiego para escribir una novela. Sin embargo, poco después de su llegada al hotel, al mismo tiempo que Jack empieza a padecer inquietantes trastornos de personalidad, se suceden extraños y espeluznantes fenómenos paranormales.</info>
 
 
<page>9</page><title>EL RETORNO DE LAS BRUJAS 2</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>e95b04ccb0be9218e4d2efd79ad8d87fe3b09b72</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/i2uxePzWHeliW3q8ck4W5zfv43w.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/zDv99D1WkpEDxpsz3ZcCUY0dvU3.jpg</fanart>
<date>2022</date>
<genre>Comedia. Fantástico. Terror</genre>
<extra>NA</extra>
<info>Han pasado 29 años desde que alguien encendió la Vela de la Llama Negra y resucitó a las hermanas Sanderson del siglo XVII, y ahora buscan venganza. De tres estudiantes de secundaria depende impedir que las voraces brujas causen un nuevo tipo de estrago en Salem antes del amanecer el día de Halloween.</info>
 
 
<page>9</page><title>EL REVERENDO</title>
<microhd>64aadfa7304bb9dd02805167780ae2dfd845bb43</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/6nUHwmTIQQSWf39K728IYF41PMp.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/g8G91vbvyUqnyFSlVruwiaZsUrn.jpg</fanart>
<date>2018</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>El encuentro con un activista medioambiental y su esposa embarazada (Amanda Seyfried) radicaliza poco a poco la ideología de un pastor evangélico (Ethan Hawke), un antiguo capellán del ejército, todavía marcado por la muerte de su hijo en Irak, que dirige una pequeña iglesia en el norte del estado de Nueva York.</info>
 
 
<page>9</page><title>EL REY DE ZAMUNDA</title>
<microhd>fe0fedbe8f51026a6d41b325575658ddf3581f9d</microhd>
<fullhd>69902466ac7f65cc3158bb4e8dcf8864b7016376</fullhd>
<tresd>NA</tresd>
<cuatrok>b92d8d6690c526c85faea4221e51e61fecc43007</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/d0GwE6T6rHf4qPD8U8tihwVJKUR.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/vKzbIoHhk1z9DWYi8kyFe9Gg0HF.jpg</fanart>
<date>2021</date>
<genre>Comedia </genre>
<extra>NA</extra>
<info>Akeem vuelve a América cuando descubre que tiene un hijo que llevaba buscando mucho tiempo, para así lograr que se convierta en el rey de Zamunda.</info>
 
 
<page>9</page><title>EL REY LEON</title>
<microhd>ca5911ae02a2c22084e10503e482d0676ad26a59</microhd>
<fullhd>67935285a5eab0b8290e4eebeec5f0230f2703db</fullhd>
<tresd>c09cf79aa4b1541b29a88c5dab635d877c0c6c52</tresd>
<cuatrok>87ca5f5b9bf1e942b045541bc25abf9fbd689a82</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/vEuDJgzrnkhmyBdtlO7hYCZ5Z9P.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/4G7SzRAaXYZ5hYfS05wbTzjv2Tn.jpg</fanart>
<date>2019</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Tras el asesinato de su padre, un joven león abandona su reino para descubrir el auténtico significado de la responsabilidad y de la valentía. Remake de "El Rey León", dirigido y producido por Jon Favreau, responsable de la puesta al día, con el mismo formato, de "El libro de la selva"</info>
 
 
<page>9</page><title>EL REY LEON ( 1994 )</title>
<microhd>8055b8f89a8d96889286b0b0b78e14c51edec080</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/b0MxU37dNmMwKtoPVYPKOZSIrIn.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/oNCJd3Norc27NlYMWqvV3uVs857.jpg</fanart>
<date>1994</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>La sabana africana es el escenario en el que tienen lugar las aventuras de Simba, un pequeño león que es el heredero del trono. Sin embargo, al ser injustamente acusado por el malvado Scar de la muerte de su padre, se ve obligado a exiliarse. Durante su destierro, hará buenas amistades e intentará regresar para recuperar lo que legítimamente le corresponde</info>
 
 
<page>9</page><title>EL REY LEON 2: EL TESORO DE SIMBA  ( sd)</title>
<microhd>e0838e36c81a4604fcb817b3682094a03bcce471</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/zdD5VcvTD43p048ueswYdgLJZh.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/mlLM0fERcKZXWFH8vUPSdh4o749.jpg</fanart>
<date>1998</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Kiara, la primogénita de Simba y heredera de sus dominios, escapa de sus protectores y, buscando aventuras, se adentra en sombríos territorios que están fuera de los límites del reino de su padre. En su camino conoce a Kovu, un joven cachorro: alcanzar la paz entre los habitantes de las tierras oscuras y los del territorio de Simba dependerá de ellos dos</info>
 
 
<page>9</page><title>EL REY LEON 3 : HAKUNA MATATA ( sd )</title>
<microhd>8d5304a83b9e9c0849740fd4dd4b9db1481f14f9</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/x2SAEOXURbKDgKfdHdZ49VhPGrB.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/hAOfzKLvxQcewp2OrEVajLrNhR0.jpg</fanart>
<date>2004</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>La diversión reina en esta nueva película en al que Timón y su simpático amigo Pumba son los grandes héroes de la Sabana. El Rey León 3 nos traslada al pasado, antes de que comenzase la historia de Simba, e incluso mucho más atrás. En esta nueva comedia de aventuras, Timón y Pumba te descubrirán sus orígenes, cuándo se conocieron y cómo ayudaron a Simba a salvar el Seregeti. Su historia incluye algunos de los grandes momentos de El Rey León, que ahora resultarán un poco diferentes desde su punto de vista. Por ejemplo, os sorprenderá conocer la verdad sobre cómo se desarrolló el trascendental momento en el que Simba fue presentado al reino animal.</info>
 
 
<page>9</page><title>EL REY PROSCRITO</title>
<microhd>a452687e51d452f9780264b43851ab9d966e4949</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/tP05gdE8mrHW7hf6h5lcUIzas6U.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/yuusjIfudAR1IJJlyTeaFqELwjm.jpg</fanart>
<date>2018</date>
<genre>Aventuras. Drama</genre>
<extra>NA</extra>
<info>Escocia, año 1304. Tras la ejecución de William Wallace, obligado a entrar en batalla para salvar a su familia, su gente y su país de la tiránica ocupación inglesa, Robert the Bruce se apodera de la corona de la Escocia medieval y lidera a un grupo de hombres insurgentes para enfrentar la ira del ejército más fuerte del mundo conducido por el despiadado rey Eduardo I y su débil hijo, el príncipe de Gales.</info>
 
 
<page>9</page><title>EL SACRIFICIO DE UN CIERVO SAGRADO</title>
<microhd>9cdbda6e811730697130f35d9ec8e86a7018cd33</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/tqxogphzYLJJkXeJJho2utdWzFl.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/854uDv6rzbwF82jOtFe931SMrRs.jpg</fanart>
<date>2017</date>
<genre>Thriller. Drama </genre>
<extra>NA</extra>
<info>Steven es un eminente cirujano casado con Anna, una respetada oftalmóloga. Viven felices junto a sus dos hijos, Kim y Bob. Cuando Steven entabla amistad con Martin, un chico de dieciséis años huérfano de padre, a quien decide proteger, los acontecimientos dan un giro siniestro. Steven tendrá que escoger entre cometer un impactante sacrificio o arriesgarse a perderlo todo. </info>
 
 
<page>9</page><title>EL SARGENTO DE HIERRO</title>
<microhd>NA</microhd>
<fullhd>f703f6b71d05646498d9aa62d2bd389fbacba1f8</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/kqahkdYsUJyp2XAw20qupebw5tS.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/sU8BrL7SVAzeKXpMNL6spYNrJ9s.jpg</fanart>
<date>1986</date>
<genre>Bélico. Drama. Acción</genre>
<extra>Mis</extra>
<info>El sargento Tom Highway, veterano de las guerras de Vietnam y Corea, vuelve a los Estados Unidos para llevar a cabo una misión nada fácil: instruir a un grupo de novatos desmotivados e indisciplinados para convertirlos en auténticos marines.</info>
 
 
<page>9</page><title>EL SASTRE DE LA MAFIA</title>
<microhd>NA</microhd>
<fullhd>1ec347b1ab9ad9edc5a88932d0e379faad15507f</fullhd>
<tresd>NA</tresd>
<cuatrok>f3b0bdc7420c9767c5af4f96bdf7f5a7f491e346</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/x8mEZ0eYaiUhluyAHdev0dP0Pw5.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/AcGvSRSwQcThKysnD4ohR18NoAt.jpg</fanart>
<date>2022</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Chicago. 1956. Leonard (Rylance), es un sastre inglés que confeccionaba trajes en la mundialmente famosa Savile Row de Londres. Después de una tragedia personal termina en Chicago, trabajando en una pequeña sastrería en una zona difícil de la ciudad donde hace ropa elegante para las únicas personas a su alrededor que pueden pagarla: una familia de gángsters. Esta familia de mafiosos intentará aprovecharse de la naturaleza gentil y complaciente de Leonard, que junto a su asistenta Mable (Zoey Deutch) se verá implicado con la mafia de una manera cada vez más grave.</info>
 
 
<page>9</page><title>EL SECRETO DE MARROWBONE</title>
<microhd>e5e39165fadc925130c59cffd6f6de425395288e</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>ecac04fe649f6f1565a85322e651a29806ffb780</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/sQYr9634GY0ZRX68pyEh1XNXhCR.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/mc7w3y83wQU9YAIAmP1hoxqQped.jpg</fanart>
<date>2017</date>
<genre>Terror. Thriller. Drama</genre>
<extra>NA</extra>
<info>Cuatro hermanos, temiendo que les separen tras la muerte de su madre, se esconden del mundo en su abandonada granja, un lugar cuyas viejas paredes esconden un terrible secreto...</info>
 
 
<page>9</page><title>EL SENTIDO DE LA VIDA</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>f1318b9c185a8945b0d18a941215e863fa019cfe</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/tlM66Ms9pCo89W3TzEPk6mKloAj.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/m1qQxJ7vp3QE5QyqeP7qSOF3SUx.jpg</fanart>
<date>1983</date>
<genre>Comedia. Musical</genre>
<extra>Culto</extra>
<info>Conjunto de episodios que muestran de forma disparatada los momentos más importantes del ciclo de la vida. Desde el nacimiento a la muerte, pasando por asuntos como la filosofía, la historia o la medicina, todo tratado con el inconfundible humor de los populares cómicos ingleses. El prólogo es un cortometraje independiente rodado por Terry Gilliam: "Seguros permanentes Crimson".</info>
 
 
<page>9</page><title>EL SEÑOR DE LOS ANILLOS: EL HOBBIT LA BATALLA DE LOS CINCO EJERCITOS E. EXTENDIDA</title>
<microhd>68236f19a0bf62eed614ab754d180d2d84b155f1</microhd>
<fullhd>efd748432dae54c6cc1a65e6592f1b3c4912b59f</fullhd>
<tresd>32dadb2532ea364bfa486b0cf95da5c56bc05637</tresd>
<cuatrok>f9999440264ded22fbe79e4ec50679568b35a2ff</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/6OiNSLcRKJsBLXwb6DEi6IQ0JFk.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/bVmSXNgH1gpHYTDyF9Q826YwJT5.jpg</fanart>
<date>2014</date>
<genre>Fantastico. Aventuras. Accion.Sagas</genre>
<extra>NA</extra>
<info>Después de haber recuperado el reino del Dragón Smaug en la montaña, la Compañía ha desencadenado, sin querer, una potencia maligna. Un Smaug enfurecido vuela hacia la Ciudad del Lago para acabar con cualquier resto de vida. Obsesionado con las enormes riquezas en su poder, el rey enano Thorin se vuelve codicioso, mientras Bilbo intenta hacerle entrar en razón haciendo algo desesperado y peligroso. Pero hay aún mayores peligros por delante. Sin la ayuda del mago Gandalf, su gran enemigo Sauron ha enviado legiones de orcos hacia la Montaña Solitaria en un ataque épico. Cuando la oscuridad se cierna sobre ellos, las razas de los Enanos, Elfos y Hombres deberán decidir si unirse o ser destruidos. Bilbo se encontrará así en la batalla épica de los Cinco Ejércitos, donde el futuro de la Tierra Media está en juego.</info>
 
 
<page>9</page><title>EL SEÑOR DE LOS ANILLOS: EL HOBBIT LA DESOLACIÓN DE SMAUG E.EXTENDIDA</title>
<microhd>NA</microhd>
<fullhd>5510a39ddfb37bb5a0bce1f4eb8f18512508c90c</fullhd>
<tresd>NA</tresd>
<cuatrok>32a14cdd02914a1334d31933e5f8c673717db4f1</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/1VXJNn7EYMc7UOhXoyInxZbgKNN.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/npCPnwDyWfQltGfIZKN6WqeUXGI.jpg</fanart>
<date>2013</date>
<genre>Fantastico. Aventuras. Accion.Sagas</genre>
<extra>NA</extra>
<info>"El Hobbit: La Desolación de Smaug" continua la aventura de Bilbo Bolsón en su viaje con el mago Gandalf y trece enanos liderados por Thorin Escudo de Roble en una búsqueda épica para reclamar el reino enano de Erebor. En su camino toparán con multitud de peligros y harán frente al temible dragón Smaug. </info>
 
 
<page>9</page><title>EL SEÑOR DE LOS ANILLOS: EL HOBBIT UN VIAJE INESPERADO E.EXTENDIDA</title>
<microhd>f69f855d7503182f28c0e4c806e4af6c07f3bad1</microhd>
<fullhd>530aa52fd1e66703f7760064b9880c8ecd54525b</fullhd>
<tresd>NA</tresd>
<cuatrok>463cce11ae3428b612a8ecc84cd1e6f3d9158bcd</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/2Also6fDCjczi6xEFYFOqiau3f7.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/gIh8GMJI0uzwP3CrMDRc1X21Xw7.jpg</fanart>
<date>2012</date>
<genre>Fantastico. Aventuras. Accion.Sagas</genre>
<extra>NA</extra>
<info>Precuela de la trilogía "El Señor de los Anillos", obra de J.R.R. Tolkien. En compañía del mago Gandalf y de trece enanos, el hobbit Bilbo Bolsón emprende un viaje a través del país de los elfos y los bosques de los trolls, desde las mazmorras de los orcos hasta la Montaña Solitaria, donde el dragón Smaug esconde el tesoro de los Enanos. Finalmente, en las profundidades de la Tierra, encuentra el Anillo Único, hipnótico objeto que será posteriormente causa de tantas sangrientas batallas en la Tierra Media.</info>
 
 
<page>9</page><title>EL SEÑOR DE LOS ANILLOS: EL RETORNO DEL REY 1 PARTE E.EXTENDIDA</title>
<microhd>NA</microhd>
<fullhd>d6d90e00a48d47a29f37459c278ebb734572d49a</fullhd>
<tresd>NA</tresd>
<cuatrok>8007d0ab07a90ba8c510ef1b72ad58139d03789a</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/fYA3YXw5WzM3wahYhxlcEqmolFj.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/lXhgCODAbBXL5buk9yEmTpOoOgR.jpg</fanart>
<date>2003</date>
<genre>Fantastico. Aventuras. Accion.Sagas</genre>
<extra>Culto</extra>
<info>Las fuerzas de Saruman han sido destruidas, y su fortaleza sitiada. Ha llegado el momento de decidir el destino de la Tierra Media, y, por primera vez, parece que hay una pequeña esperanza. El interés del señor oscuro Sauron se centra ahora en Gondor, el último reducto de los hombres, cuyo trono será reclamado por Aragorn. Sauron se dispone a lanzar un ataque decisivo contra Gondor. Mientras tanto, Frodo y Sam continuan su camino hacia Mordor, con la esperanza de llegar al Monte del Destino.</info>
 
 
<page>9</page><title>EL SEÑOR DE LOS ANILLOS: EL RETORNO DEL REY 2 PARTE E.EXTENDIDA</title>
<microhd>NA</microhd>
<fullhd>2e3079e80720de80358702ce612df63c90979185</fullhd>
<tresd>NA</tresd>
<cuatrok>8007d0ab07a90ba8c510ef1b72ad58139d03789a</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/mWuFbQrXyLk2kMBKF9TUPtDwuPx.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/9DeGfFIqjph5CBFVQrD6wv9S7rR.jpg</fanart>
<date>2003</date>
<genre>Fantastico. Aventuras. Accion.Sagas</genre>
<extra>Culto</extra>
<info>Las fuerzas de Saruman han sido destruidas, y su fortaleza sitiada. Ha llegado el momento de decidir el destino de la Tierra Media, y, por primera vez, parece que hay una pequeña esperanza. El interés del señor oscuro Sauron se centra ahora en Gondor, el último reducto de los hombres, cuyo trono será reclamado por Aragorn. Sauron se dispone a lanzar un ataque decisivo contra Gondor. Mientras tanto, Frodo y Sam continuan su camino hacia Mordor, con la esperanza de llegar al Monte del Destino.</info>
 
 
<page>9</page><title>EL SEÑOR DE LOS ANILLOS: LA COMUNIDAD DEL ANILLO 1 PARTE E EXTENDIDA</title>
<microhd>NA</microhd>
<fullhd>a71407bc282e0e373c460cce82ed68bb559f903b</fullhd>
<tresd>NA</tresd>
<cuatrok>7ff2c7176d46e054bea200ab15df1331dcbb2404</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/9xtH1RmAzQ0rrMBNUMXstb2s3er.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/q9nym8NbrISGGHLpQbhxfevKCZ2.jpg</fanart>
<date>2001</date>
<genre>Fantastico. Aventuras. Accion.Sagas</genre>
<extra>NA</extra>
<info>En la Tierra Media, el Señor Oscuro Saurón ordenó a los Elfos que forjaran los Grandes Anillos de Poder. Tres para los reyes Elfos, siete para los Señores Enanos, y nueve para los Hombres Mortales. Pero Saurón también forjó, en secreto, el Anillo Único, que tiene el poder de esclavizar toda la Tierra Media. Con la ayuda de sus amigos y de valientes aliados, el joven hobbit Frodo emprende un peligroso viaje con la misión de destruir el Anillo Único. Pero el malvado Sauron ordena la persecución del grupo, compuesto por Frodo y sus leales amigos hobbits, un mago, un hombre, un elfo y un enano. La misión es casi suicida pero necesaria, pues si Sauron con su ejército de orcos lograra recuperar el Anillo, sería el final de la Tierra Media.</info>
 
 
<page>9</page><title>EL SEÑOR DE LOS ANILLOS: LA COMUNIDAD DEL ANILLO 2 PARTE E XTENDIDA</title>
<microhd>NA</microhd>
<fullhd>44b6009983ec5f12956fd901882f4e6340fdb57d</fullhd>
<tresd>NA</tresd>
<cuatrok>7ff2c7176d46e054bea200ab15df1331dcbb2404</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/p6QjtBmO6U105l44U0qMDSLxT0L.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/vRQnzOn4HjIMX4LBq9nHhFXbsSu.jpg</fanart>
<date>2001</date>
<genre>Fantastico. Aventuras. Accion.Sagas</genre>
<extra>NA</extra>
<info>En la Tierra Media, el Señor Oscuro Saurón ordenó a los Elfos que forjaran los Grandes Anillos de Poder. Tres para los reyes Elfos, siete para los Señores Enanos, y nueve para los Hombres Mortales. Pero Saurón también forjó, en secreto, el Anillo Único, que tiene el poder de esclavizar toda la Tierra Media. Con la ayuda de sus amigos y de valientes aliados, el joven hobbit Frodo emprende un peligroso viaje con la misión de destruir el Anillo Único. Pero el malvado Sauron ordena la persecución del grupo, compuesto por Frodo y sus leales amigos hobbits, un mago, un hombre, un elfo y un enano. La misión es casi suicida pero necesaria, pues si Sauron con su ejército de orcos lograra recuperar el Anillo, sería el final de la Tierra Media.</info>
 
 
<page>9</page><title>EL SEÑOR DE LOS ANILLOS: LAS DOS TORRES PARTE 1 E.EXTENDIDA</title>
<microhd>NA</microhd>
<fullhd>88b6cbf6699a1b020e7ab8c67899cf6e03802215</fullhd>
<tresd>NA</tresd>
<cuatrok>6f348517b387ebbc7e8ed91603b3a6c1d9175c80</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/qHEU9067PL8iIZULg7q58NvoWlS.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/kWYfW2Re0rUDE6IHhy4CRuKWeFr.jpg</fanart>
<date>2002</date>
<genre>Fantastico. Aventuras. Accion.Sagas</genre>
<extra>NA</extra>
<info>Tras la disolución de la Compañía del Anillo, Frodo y su fiel amigo Sam se dirigen hacia Mordor para destruir el Anillo Único y acabar con el poder de Sauron, pero les sigue un siniestro personaje llamado Gollum. Mientras, y tras la dura batalla contra los orcos donde cayó Boromir, el hombre Aragorn, el elfo Legolas y el enano Gimli intentan rescatar a los medianos Merry y Pipin, secuestrados por los orcos de Mordor. Por su parte, Saurón y el traidor Sarumán continúan con sus planes en Mordor, a la espera de la guerra contra las razas libres de la Tierra Media.</info>
 
 
<page>9</page><title>EL SEÑOR DE LOS ANILLOS: LAS DOS TORRES PARTE 2 E.EXTENDIDA</title>
<microhd>NA</microhd>
<fullhd>61c52cfd90c2e6e8515e08b1607e1d73cad8267e</fullhd>
<tresd>NA</tresd>
<cuatrok>6f348517b387ebbc7e8ed91603b3a6c1d9175c80</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/up6gIHZlfEQZkHIfQwcOOaGOzOt.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/3wWhdg4DP6emDJC3XZyr8M1wKm4.jpg</fanart>
<date>2002</date>
<genre>Fantastico. Aventuras. Accion.Sagas</genre>
<extra>NA</extra>
<info>Tras la disolución de la Compañía del Anillo, Frodo y su fiel amigo Sam se dirigen hacia Mordor para destruir el Anillo Único y acabar con el poder de Sauron, pero les sigue un siniestro personaje llamado Gollum. Mientras, y tras la dura batalla contra los orcos donde cayó Boromir, el hombre Aragorn, el elfo Legolas y el enano Gimli intentan rescatar a los medianos Merry y Pipin, secuestrados por los orcos de Mordor. Por su parte, Saurón y el traidor Sarumán continúan con sus planes en Mordor, a la espera de la guerra contra las razas libres de la Tierra Media.</info>
 
 
<page>9</page><title>EL SEXTO SENTIDO</title>
<microhd>eccc2748b508c2cb1bb74a1ee581e8e777cd5dbe</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/pvjph7FouToPBY3PhTvTyOYoJDX.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/4l4tYHgABoc1xby86uJP36HdUiM.jpg</fanart>
<date>1999</date>
<genre>Terror. Intriga. Drama</genre>
<extra>Mis</extra>
<info>El doctor Malcom Crowe es un conocido psicólogo infantil de Philadelphia que vive obsesionado por el doloroso recuerdo de un joven paciente desequilibrado al que fue incapaz de ayudar. Cuando conoce a Cole Sear, un aterrorizado y confuso niño de ocho años que necesita tratamiento, ve que se le presenta la oportunidad de redimirse haciendo todo lo posible por ayudarlo. Sin embargo, el doctor Crowe no está preparado para conocer la terrible verdad acerca del don sobrenatural de su paciente: recibe visitas no deseadas de espíritus atormentados.</info>
 
 
<page>9</page><title>EL SHOW DE TRUMAN</title>
<microhd>f635df364405f76011db228de418d864fdacd980</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/4BGVZSwv5n0hMIAWwiSjMFDQ63K.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/bxY7ve1LP8atCIuvr4jeeJMmU4w.jpg</fanart>
<date>1998</date>
<genre>Drama. Comedia</genre>
<extra>Culto</extra>
<info>Truman Burbank es un hombre corriente y algo ingenuo que ha vivido toda su vida en uno de esos pueblos donde nunca pasa nada. Sin embargo, de repente, unos extraños sucesos le hacen sospechar que algo anormal está ocurriendo. Todos sus amigos son actores, toda su ciudad es un plató, toda su vida está siendo filmada y emitida como el reality más ambicioso de la historia.</info>
 
<page>10</page><title>EL SILENCIO DE LA CIUDAD BLANCA</title>
<microhd>f8de9f2dc86e444e3071affe70c70c4797fc1043</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/7Qp6SLVtuuH9GS7vvrOqwIQtlda.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/tr0QbJHWjI3pWhY75oTNl3o9lcC.jpg</fanart>
<date>2019</date>
<genre>Thriller. Intriga</genre>
<extra>NA</extra>
<info>Vitoria, 2016. Los cadáveres de un chico y una chica de veinte años aparecen desnudos en la cripta de la Catedral Vieja. Unai López de Ayala, un inspector experto en perfiles criminales, debe cazar al asesino ritual que lleva aterrorizando a la ciudad desde hace dos décadas. La sucesión imparable de crímenes y una investigación policial contaminada por las redes sociales llevarán al límite a Unai, enfrentándolo a un asesino camaleónico y despiadado que podría estar más cerca de lo que creía.</info>
 
 
<page>10</page><title>EL SILENCIO DE LOS CORDEROS</title>
<microhd>NA</microhd>
<fullhd>9f1c27c4fe837f0df2e306d16aaf0f0a06ac5f47</fullhd>
<tresd>NA</tresd>
<cuatrok>d8c99473b8aaafe3d7a2e9567eb5b8f1a59313f4</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/8FdQQ3cUCs9goEOr1qUFaHackoJ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/lDJx0ZKbfYbGoe8mwWmVKSQr0ub.jpg</fanart>
<date>1991</date>
<genre>Thriller. Intriga</genre>
<extra>Culto</extra>
<info>El FBI busca a "Buffalo Bill", un asesino en serie que mata a sus víctimas, todas adolescentes, después de prepararlas minuciosamente y arrancarles la piel. Para poder atraparlo recurren a Clarice Starling, una brillante licenciada universitaria, experta en conductas psicópatas, que aspira a formar parte del FBI. Siguiendo las instrucciones de su jefe, Jack Crawford, Clarice visita la cárcel de alta seguridad donde el gobierno mantiene encerrado al Dr. Hannibal Lecter, antiguo psicoanalista y asesino, dotado de una inteligencia superior a la normal. Su misión será intentar sacarle información sobre los patrones de conducta del asesino que están buscando.</info>
 
 
<page>10</page><title>EL SILENCIO DEL PANTANO</title>
<microhd>37b39e8096a2be7333f7b68ceccb5f943ee3dbd6</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/vf31x1jjfNKJlMLjxEyJoMBULgN.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/af4IcSStHxULkSta540zxdplL2.jpg</fanart>
<date>2019</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>"Q" es un periodista reconvertido en escritor de novela negra. En dos de sus libros publicados, ambientados siempre en su Valencia natal, "Q" narra sangrientos asesinatos utilizando como telón de fondo la corrupción política y la podredumbre del alma humana. El único problema es que los crímenes que tienen lugar en sus novelas no son tan ficticios como parece.</info>
 
 
<page>10</page><title>EL SISTEMA</title>
<microhd>NA</microhd>
<fullhd>0e3bc166f42f3649527bdc9c3299016d0830733f</fullhd>
<tresd>NA</tresd>
<cuatrok>fe86ab4e564db91cf3c6fb56a34b829c5d841afb</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/6BZAYG6oooTLv72w9ePKvbpqDlj.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/tVKxO4wg0MCIC2I3Ltuzeua11tp.jpg</fanart>
<date>2022</date>
<genre>Acción. Thriller</genre>
<extra>NA</extra>
<info>Cuando un joven soldado, recién regresado de la guerra, queda atrapado en una redada de drogas, las autoridades lo reclutan para que se infiltre en una prisión notoriamente peligrosa para descubrir qué está pasando realmente.</info>
 
 
<page>10</page><title>EL SONIDO DEL METAL</title>
<microhd>0092fa8f7d87b710b234468915ea1e79a14fa06a</microhd>
<fullhd>e57dd47022be42308c6c1348b98265ab9c89aa5b</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/y89kFMNYXNKMdlZjR2yg7nQtcQH.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/xks5xGl0o3ykclowqO8nPLbw70b.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Narra la historia de un joven batería de una banda de música que comienza a perder la audición.</info>
 
 
<page>10</page><title>EL SOTANO DE MA</title>
<microhd>c8d42e91660fb12525c16a091e9cfafeb1d99bce</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/gUPZu9stlOUHqT9FCRYwiKkFtMo.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/pso2IsKlksUwx1simcDT7fKumAP.jpg</fanart>
<date>2019</date>
<genre>Thriller. Terror</genre>
<extra>NA</extra>
<info>Una mujer solitaria (Octavia Spencer) entabla amistad con un grupo de adolescentes y decide invitarles a una fiesta en su casa. Justo cuando los jóvenes piensan que su suerte no puede ir a mejor, una serie de extraños acontecimientos comienzan a suceder, poniendo en tela de juicio las intenciones de su nueva y misteriosa amiga.</info>
 
 
<page>10</page><title>EL SUPERVIVIENTE</title>
<microhd>be93a77f99294ef343b805b8b81adc2ffa96f18b</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/x7XhPGGtT8eIJnBuWoiB2TxjE9D.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/2P7pGVJ5dGixxdmUyUvQBdkJNKR.jpg</fanart>
<date>2021</date>
<genre>Acción. Thriller</genre>
<extra>NA</extra>
<info>Año y medio después de la caída de la civilización debido a un brote viral, un ex agente del FBI se ve obligado a proteger a una joven inmune a la enfermedad de un peligroso líder de pandillas que la persigue. </info>
 
 
<page>10</page><title>EL TEST</title>
<microhd>NA</microhd>
<fullhd>4a3ee93129bc6d9bfe75fbe14685c14bebd567cc</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/my6xtC0KV52D17j7bmlcp8eZ276.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/2TNK2ZFcZVNr5xebws73ynqFsq7.jpg</fanart>
<date>2022</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>¿Qué escogerías, cien mil euros ahora o un millón dentro de diez años? La premisa parece sencilla. Un matrimonio, Héctor de 38 años y Paula de 37, se enfrenta a serios problemas económicos que se pondrán a prueba por su rico amigo Toni, pero primero deben ponerse de acuerdo sobre su elección. Héctor quiere 100.000€ para intentar renovar su bar ruinoso y poco rentable y convertirlo en negocio de éxito, pero Paula prefiere esperar al gran premio. Adaptación de la obra de Jordi Vallejo.</info>
 
 
<page>10</page><title>EL TRAIDOR</title>
<microhd>1fe555e75a6b8683e1e58157afde47509bfc94a2</microhd>
<fullhd>7458301471a82cdf4eceba92940bbe3cf6c6215d</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/cycbNjy4cGcDeuGonUfPZuYJeHN.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/dI8UCV6SlYWmxp50WGbq94stBVA.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>A principios de los años 80 se produjo una guerra entre los jefes de la mafia siciliana. Tommaso Buscetta, un hombre hecho a sí mismo, decide huir para esconderse en Brasil. Sus hijos son asesinados mientras él se ve incapaz de hacer nada para impedirlo. Cuando es extraditado por la justicia brasileña, Buscetta toma una decisión totalmente inesperada tanto para él como para todos los que le conocían: decide reunirse con el juez Giovanni Falcone, traicionando así el juramento que realizó con la Cosa Nostra. </info>
 
 
<page>10</page><title>EL ULTIMO BAILE DE MAGIC MIKE</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>d348ff328c437926b561919497438631cf4793ad</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/p98DxMJbXjI6ITI9aFh3U0u5oF7.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/baO7ZhMtqgv9z5mqFqq9uEAx8Gs.jpg</fanart>
<date>2023</date>
<genre>Comedia. Drama </genre>
<extra>NA</extra>
<info>“Magic” Mike Lane (Tatum) vuelve a subir al escenario después de un largo paréntesis, luego de un negocio que fracasó, dejándolo en la ruina y tomando trabajos de barman en Florida. Para lo que espera sea un último baile, Mike se dirige a Londres con una rica dama de la alta sociedad (Hayek) que lo atrae con una oferta que no puede rechazar. Con todo en juego, una vez que Mike descubra lo que ella realmente tiene en mente, ¿podrán él (y sus nuevos bailarines) lograrlo? Tercera y última entrega de la saga "Magic Mike"</info>
 
 
<page>10</page><title>EL ULTIMO DUELO</title>
<microhd>0a324f854f411239f14026b18e2d0310ec7a1f43</microhd>
<fullhd>81b4eaa23f66d923581cedd8fdba3fdd2d61cb8d</fullhd>
<tresd>NA</tresd>
<cuatrok>bfc8f21a9e7ccd27881d22dd85aa729ce41dd187</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/pA2zkZnfNR5v8hc07ekQGxgJg1r.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/mFbS5TwN95BcSEfiztdchLgTQ0v.jpg</fanart>
<date>2021</date>
<genre>Drama. Intriga</genre>
<extra>NA</extra>
<info>Francia, 1386. Cuenta el enfrentamiento entre el caballero Jean de Carrouges (Matt Damon) y el escudero Jacques LeGris (Adam Driver), al acusar el primero al segundo de abusar de su esposa, Marguerite de Carrouges (Jodie Comer). El Rey Carlos VI decide que la mejor forma de solucionar el conflicto es un duelo a muerte. El que venza será el ganador, sin embargo, si lo hace el escudero, la esposa del caballero será quemada como castigo por falsas acusaciones.</info>
 
 
<page>10</page><title>EL ULTIMO MERCENARIO</title>
<microhd>e8e48922d55331f69c9c1b5cb586b5f47574e5ab</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ttpKJ7XQxDZV252KNEHXtykYT41.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/tXcJZtsEIYkZpjonbym3GKs7pJN.jpg</fanart>
<date>2021</date>
<genre>Acción. Comedia</genre>
<extra>NA</extra>
<info>Cuando descubre que su hijo ha sido falsamente acusado de tráfico de drogas y armas, un misterioso ex-agente secreto (Van Damme) decide regresar a casa para ayudarle, enfrentándose a un burócrata (Alban Ivanov) y desmantelando una operación de la mafia.</info>
 
 
<page>10</page><title>EL ULTIMO MOHICANO</title>
<microhd>78db2210dbbee808d8bf7d60e3ac79e18d11a51e</microhd>
<fullhd>45605416e00a43342501acd075ca5d89e64f0466</fullhd>
<tresd>NA</tresd>
<cuatrok>60b9da1a71bba0cf4cec22e37d25ac58bcb9fbc9</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/dM5SCW7niVczfDBxWey2pksUci0.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/fUYxUbxnDH5r6vLc6eqkHhPltYv.jpg</fanart>
<date>1992</date>
<genre>Aventuras. Acción. Romance</genre>
<extra>Mis</extra>
<info>Año 1757, hace tres años que franceses e ingleses luchan en tierras norteamericanas. Mientras que los franceses cuentan con el apoyo de los nativos, los ingleses reclutan a los colonos blancos. Hawkeye -Ojo de halcón- (Daniel Day-Lewis) es un hombre blanco que fue adoptado por los indios mohicanos. Tras salvar de una emboscada de los hurones a Cora Munro (Madeleine Stowe) y a su hermana pequeña Alice (Jodhi May), hijas de un oficial británico, las acompaña hasta el fuerte inglés William Henry, que está sufriendo el asedio de los franceses y los hurones.</info>
 
 
<page>10</page><title>EL ULTIMO SAMURAI</title>
<microhd>71a319a9a58f02629510a703185d6a8b93d8d7e4</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>91cedafdc4e676a4841f2cf525851d0c76da1a47</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/DfwutbQodAsYxdqJu7Cme5jifC.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/z354BaTVzKj7E60WLzDoSmUuO4u.jpg</fanart>
<date>2003</date>
<genre>Aventuras. Acción. Drama</genre>
<extra>Mis</extra>
<info>Año 1876. El capitán Nathan Algren es un hombre que sobrevive atormentado por los recuerdos de la Guerra Civil (1861-1865) y de las campañas contra los indios, en las que participó arriesgando su vida. Desde entonces, el mundo ha cambiado radicalmente: el pragmatismo ha reemplazado al valor, el interés personal ha ocupado el lugar del sacrificio, y el sentido del honor ha desaparecido. En un país muy lejano, otro soldado ve también cómo su modo de vida está a punto de desintegrarse. Es Katsumoto, el último líder de un antiguo linaje de guerreros, los venerados samuráis, que dedicaron sus vidas a servir al emperador y a preservar el espíritu de la milenaria cultura japonesa.</info>
 
 
<page>10</page><title>EL VERANO QUE VIVIMOS</title>
<microhd>NA</microhd>
<fullhd>70d79bca2d39f3b27c23a0b57b97057dbbef803f</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/oxbnkrC2GWEVbkUPzwFi1QX1qbP.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/9uAAieZy0B6bkfy6IL8KIrPEnGL.jpg</fanart>
<date>2020</date>
<genre>Romance. Drama</genre>
<extra>NA</extra>
<info>Año 1998. Isabel, estudiante de periodismo, se ve obligada a realizar sus prácticas en el diario de un pequeño pueblo costero gallego para terminar la carrera. Al llegar, quiere empezar cuanto antes a investigar, a demostrar todo lo que ha aprendido para convertirse en una auténtica periodista. Pero el puesto que le asignan es el último que ella esperaba: la escritura y gestión de las esquelas que llegan a la redacción. Pero esto, que podría parecer en principio algo aburrido, se convierte en la puerta a una investigación que la llevará por diferentes puntos de la geografía española en busca de una historia de amor imposible.</info>
 
 
<page>10</page><title>EL VEREDICTO, LA LEY DEL MENOR</title>
<microhd>7a8bd7cc6c9d49d139d8c2e11494568cae1e104a</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/jXH0mIeGBRKpNuaBlcNqZLiLN7Z.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/6wSMtXHhvjnCu3Pj4AtYhjgtj86.jpg</fanart>
<date>2017</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Fiona Maye (Emma Thompson) es una prestigiosa jueza del Tribunal Superior de Londres especializada en derechos familiares que atraviesa por una grave crisis matrimonial. Cuando llega a sus manos el caso de Adan (Fionn Whitehead), un adolescente con leucemia que se niega a hacerse una transfusión de sangre al ser Testigo de Jehová, Fiona descubrirá sentimientos ocultos que desconocía, y luchará para que Adan entre en razón y sobreviva.</info>
 
 
<page>10</page><title>EL VIAJE A PARIS DE LA SEÑORA HARRIS</title>
<microhd>NA</microhd>
<fullhd>74bd89e6c136756c7dd176a4443dfe8e7ee06f25</fullhd>
<tresd>NA</tresd>
<cuatrok>b2276363830e4a6580563b4be32071a342d53cf6</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/hZhYBbfjtEadc7d3BUwaZIiCuQ3.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/z2QcdCqCDFmLruwEXBA4vRLgLM.jpg</fanart>
<date>2022</date>
<genre>Comedia. Drama</genre>
<extra>NA</extra>
<info>Nueva adaptación de la novela de Paul Gallico. Narra la historia de una señora de la limpieza que enviuda (Manville) en los años 50 en Londres, y se enamora perdidamente de un vestido de Dior. Decide que ella debe hacerse con uno.</info>
 
 
<page>10</page><title>EL VIRTUOSO</title>
<microhd>11e93fef72f250b79681a8b28b05485cf2620a06</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>29b50bcb635ecbb06bb9bf92f648839869a70a0b</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/mZ1V79NONaYz1Cxd3FtTig4NBd1.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/3Ef8PWUiP1ehO1ESEroxb736srR.jpg</fanart>
<date>2021</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>Un asesino profesional (Mount) debe localizar y matar a su último objetivo para satisfacer una deuda pendiente con su mentor. Pero la única información que se le ha dado es la hora y el lugar donde encontrar a su presa: las 5 de la tarde en un restaurante rústico en la ciudad decadente. Sin nombre, sin descripción, nada. Cuando llega al lugar, hay varios objetivos posibles, incluido el sheriff del condado. Poniendo en peligro su vida, el asesino se embarca en una cacería humana para encontrar al objetivo y cumplir su misión. Pero el peligro aumenta cuando los encuentros eróticos con una mujer lugareña amenazan con descarrilar su tarea.</info>
 
 
<page>10</page><title>ELEMENTAL</title>
<microhd>NA</microhd>
<fullhd>4a1b07ccc8747aa5ffd42959b4b1302291528917</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/d79DeKDCgFOM23O8Dr6MELZVooY.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/cSYLX73WskxCgvpN3MtRkYUSj1T.jpg</fanart>
<date>2023</date>
<genre>Infantil</genre>
<extra>Estreno</extra>
<info>Ambientada en Ciudad Elemento, donde conviven habitantes de fuego, agua, tierra y aire. La protagonista de la historia es Candela, una joven fuerte, ingeniosa y con carácter, cuya amistad con un chico sensible, afable y tranquilo, llamado Nilo, cambia su perspectiva sobre el mundo en el que viven.</info>
 
 
<page>10</page><title>ELVIS</title>
<microhd>NA</microhd>
<fullhd>d0f8cadc2f5627531cf6b39ac302645468a80fda</fullhd>
<tresd>NA</tresd>
<cuatrok>a193237a373e9fabd36309c1b5b98c53c71fa67f</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/tzL9Aju1nfoirQJQBGxSEOgt1hQ.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/cdmPPmuTEX8ZFwFAFm3gfx34XLe.jpg</fanart>
<date>2022</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>La película explora la vida y la música de Elvis Presley (Butler) a través del prisma de su relación con el coronel Tom Parker (Hanks), su enigmático manager. La historia profundiza en la compleja dinámica que existía entre Presley y Parker que abarca más de 20 años, desde el ascenso de Presley a la fama hasta su estrellato sin precedentes, en el contexto de la revolución cultural y la pérdida de la inocencia en Estados Unidos. Y en el centro de ese periplo está Priscilla Presley (Olivia DeJonge), una de las personas más importantes e influyentes en la vida de Elvis.</info>
 
 
<page>10</page><title>EMBAUCADORES</title>
<microhd>NA</microhd>
<fullhd>ab514802987bfc7fccf19cb344d155603ea6386f</fullhd>
<tresd>NA</tresd>
<cuatrok>ca55f1d011f9ba4c872b0cd1cf639a2bd7234a1d</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/eGcpjDqXlMxUTL4OBougtUEEJP6.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/cG5QZHyIRJXqo53YA41gbNMlpIM.jpg</fanart>
<date>2023</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>Esta película explora los secretos de la ciudad de Nueva York, desde los áticos de la Quinta Avenida hasta los rincones sombríos de Queens. Los motivos son sospechosos y las expectativas cambian cuando nada es lo que parece.</info>
 
 
<page>10</page><title>EMBOSCADA FINAL</title>
<microhd>dcf8662867208707ca9cdd43656c3f21b066691f</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/rTtsOpF1iVwrR0p7Uvvd0nMA4X.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/pZ78ksjPlXf3q2EeONN8WdHE03Y.jpg</fanart>
<date>2019</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>Año 1934. Frank Hamer (Kevin Costner) y su sufrido compañero Manny Gault (Harrelson) son dos Ranger de Texas que sufrieron el reinado de robos de Bonnie &amp; Clyde. Ambos fueron asignados como investigadores especiales de un caso que trajo de cabeza a banqueros y policías por igual, y que se llevó por delante la vida hasta de trece agentes de la ley ante la incredulidad del pueblo.</info>
 
 
<page>10</page><title>EMILY LA ESTAFADORA</title>
<microhd>NA</microhd>
<fullhd>cd98e5fca14ec9b0d37bd516da21ad7d10c231c7</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/omgF37fXTGoWl4Jg4gQNqOgBCrU.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/7bck0VVQW3YEMitkacJcxJKveQK.jpg</fanart>
<date>2022</date>
<genre>Drama. Thriller</genre>
<extra>NA</extra>
<info>Emily se ve involucrada en una estafa con tarjetas de crédito tras cargar con una deuda, lo que la arrastra al submundo criminal y mortal de Los Ángeles.</info>
 
 
<page>10</page><title>EMMA</title>
<microhd>828d2a3e7b99f160e474808f2eaa919b03978013</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/hd5YQjsIPuLmZuvWfdv36apz8tE.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/5GbkL9DDRzq3A21nR7Gkv6cFGjq.jpg</fanart>
<date>2020</date>
<genre>Comedia. Romance</genre>
<extra>NA</extra>
<info>Guapa, inteligente y rica, la joven Emma Woodhouse es una reina sin rival en su pequeño pueblo. Nueva adaptación de la novela de Jane Austen publicada en 1815 sobre la vida de la joven Emma. El guion corre a cargo de la novelista ganadora del Premio Booker Eleanor Catton, que en el año 2013 se convirtió en la más joven en recibir el prestigioso galardón.</info>
 
 
<page>10</page><title>EN EL NOMBRE DEL PADRE</title>
<microhd>5a1a031843f887249060e55f08639b5e9d83fd31</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/1znQm625BPrSiKQ7pw64l0IQBxK.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/jgD5rT4pijE7Hm596A6rp5jalXE.jpg</fanart>
<date>1993</date>
<genre>Drama</genre>
<extra>Mis</extra>
<info>Belfast, años 70. Gerry (Day-Lewis) es un gamberro que no hace nada de provecho, para disgusto de su padre Giuseppe (Postlethwaite), un hombre tranquilo y educado. Cuando Gerry se enfrenta al IRA, su padre lo manda a Inglaterra. Una vez allí, por caprichos del azar, es acusado de participar en un atentado terrorista y condenado a cadena perpetua con "los cuatro de Guildford". También su padre es arrestado y encarcelado. En prisión Gerry descubre que la aparente fragilidad de su padre esconde en realidad una gran fuerza interior. Con la ayuda de una abogada entregada a la causa (Thompson), Gerry se propone demostrar su inocencia, limpiar el nombre de su padre y hacer pública la verdad sobre uno de los más lamentables errores legales de la historia reciente de Irlanda.</info>
 
 
<page>10</page><title>EN GUERRA CON  MI ABUELO</title>
<microhd>1977e0eca47754dabdecaea3d5baa52551919575</microhd>
<fullhd>66b096bdf96ddbfa099ed532afdc938a0fe517d7</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/9UQxxaJMZODEIh5zh7sm0ag75EY.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/a9jZrU7LJk6mAUjmkbEmTiC52l0.jpg</fanart>
<date>2020</date>
<genre>Comedia . Infantil</genre>
<extra>NA</extra>
<info>El joven Peter se ve obligado a abandonar su habitación cuando su abuelo Ed (Robert De Niro), recientemente enviudado, se muda a su casa. Con la ayuda de sus amigos, Peter declara la guerra a su abuelo para hacerle abandonar la habitación, pero Ed es un testarudo y no va a ponérselo nada fácil.</info>
 
<page>10</page><title>EN LOS MARGENES</title>
<microhd>NA</microhd>
<fullhd>52c7f0e9c2da22f851aac834abfce1a1bf3f4a51</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/d5LEM0kbhLfiKX9001vjb8tYlcD.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/gqoxrdRl86uOX24YBKWPPYvYN1p.jpg</fanart>
<date>2022</date>
<genre>Drama. Thriller</genre>
<extra>NA</extra>
<info>La cuenta atrás de tres personajes, con senda historias entrelazadas, que tratan de mantenerse a flote y sobrevivir a 24 horas claves que pueden cambiar el curso de sus vidas. El film explora el efecto que una situación de estrés económico tiene sobre las relaciones personales, y cómo el afecto y la solidaridad pueden ser un motor para salir adelante.</info>
 
<page>10</page><title>EN REALIDAD NUNCA ESTUVISTE AQUI</title>
<microhd>641f3de06409b4f50a105d964a11d98f40935082</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/505AFHvK3BoHC2zwWXBWkevRTYF.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/h4Lk8Cgfvlzz0hcnSJBTAOwNDia.jpg</fanart>
<date>2017</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Joe (Joaquin Phoenix), ex marine y antiguo veterano de guerra, es un tipo solitario que dedica su tiempo a intentar salvar a mujeres que son explotadas sexualmente. No se permite ni amigos ni amantes y se gana la vida rescatando jóvenes de las garras de los tratantes de blancas. Un día recibe la llamada de un político porque su hija ha sido secuestrada.</info>
 
<page>10</page><title>EN TRANSITO</title>
<microhd>f095a9109a288c08887801c9374bfc44e64e22e7</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/gb5skjiP2Zu3YZu3GXjkOx3f9Uh.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/opQyWvdFkhjY1pYBiO4neOPXKWS.jpg</fanart>
<date>2018</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Segunda Guerra Mundial. En Marsella, refugiados de toda Europa embarcan rumbo a América, huyendo de la ocupación NaSi. Entre ellos, el joven alemán Georg, que suplanta la identidad de un escritor muerto para utilizar su visado que le garantiza refugio en México. En Marsella Georg se enamora de Marie, una joven que busca desesperadamente al hombre a quien ama, sin el que no está dispuesta a irse.</info>
 
 
<page>10</page><title>EN UN BARRIO DE NUEVA YORK</title>
<microhd>6d784740726601aa0ca2508f15e3ccb963711ba1</microhd>
<fullhd>aaec19bc87746a5e2e44ad37913b053eb2926f04</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/nMeMxDRxQN9JlvAadduPDdkpxjh.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/uEJuqp08dH6IQwZJGASlPZOXqKu.jpg</fanart>
<date>2021</date>
<genre>Musical. Romance. Drama. Comedia</genre>
<extra>NA</extra>
<info>Basado en el musical de Broadway, sigue a un grupo de vecinos del barrio Washington Heights, en Nueva York. El principal es Usnavi (Anthony Ramos), el simpático dueño de una bodega, criado por su abuela, que sueña con volver algún día a su República Dominicana de origen; la abuela Claudia, que desempeña el rol de abuela para muchos de los vecinos del barrio; Vanessa, de quien Usnavi está perdidamente enamorado; y Nina, una vieja amiga de Usnavi que regresa al barrio después de mucho tiempo, llevándole noticias inesperadas a sus padres, quienes han estado ahorrando toda la vida para darle una mejor educación académica de la que ellos tuvieron.</info>
 
 
<page>10</page><title>ENCANTO</title>
<microhd>5d1e4bfb25ee97ba54e8ff5fd2d3a859d833baea</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>25c48bc909a4212cffe18a7a34b121c4230d7bd6</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/3wdx3pfyYEwuvYRGBr9TkxM5vEQ.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/9xL8gUBarG4Bd7x48BjUpSJvvlw.jpg</fanart>
<date>2021</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Cuenta la historia de una familia extraordinaria, los Madrigal, que viven escondidos en las montañas de Colombia, en una casa mágica situada en un enclave maravilloso llamado Encanto. La magia de Encanto ha dotado a todos los niños de la familia un don único, desde la súperfuerza hasta el poder de curar... Pero se olvidó de un miembro de la familia: Mirabel. Cuando ésta descubre que la magia que rodea Encanto está en peligro, decide que ella, la única Madrigal normal, podría ser la última esperanza de su extraordinaria familia.</info>
 
 
<page>10</page><title>ENEMIGOS PUBLICOS</title>
<microhd>NA</microhd>
<fullhd>1ef96f7b8ddcdb007756242767279226859c4b02</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/lzOsbkW43WSKQKnkRa6kBbjmd1m.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/44gHT9j3DSey4Gdew0FSQWeNZZb.jpg</fanart>
<date>2009</date>
<genre>Thriller. Drama</genre>
<extra>Mis</extra>
<info>Basada en la obra de Brian Burrough "Public Enemies: America's Greatest Crime Wave and the Birth of the FBI, 1933-43". Narra la historia de Melvin Purvis (Christian Bale), el agente del FBI que en los años treinta dirigió la búsqueda del legendario atracador de bancos John Dillinger (Johnny Depp) y su banda. </info>
 
 
<page>10</page><title>ENOLA HOLMES</title>
<microhd>2289a432e167ae4e5bbf1e1b7e88e9d120e42901</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>adfab58092a9e898ff0a14e9dc518cb1d7010eae</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/8STWO9pUTqubPV04iXZ2R6SClME.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/HScQCcEd3yucY3XYRCnkRzl7Gp.jpg</fanart>
<date>2020</date>
<genre>Intriga. Comedia</genre>
<extra>NA</extra>
<info>Cuando Enola, la hermana adolescente de Sherlock Holmes, descubre que su madre ha desaparecido, no duda en emprender su búsqueda. Tendrá que emplear todas sus dotes detectivescas para que su famoso hermano no dé con ella. Y para desentrañar la conspiración en torno a un misterioso y joven lord.</info>
 
 
<page>10</page><title>ENOLA HOLMES 2</title>
<microhd>NA</microhd>
<fullhd>7683d9cbd5cd78a40079d8ff157e417f1f7417f2</fullhd>
<tresd>NA</tresd>
<cuatrok>16dbc8180a1d7aaab44d752bfeecedb7d5407618</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/tegBpjM5ODoYoM1NjaiHVLEA0QM.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/3UJWmxfZB71Kynf26G8HjstOeJH.jpg</fanart>
<date>2022</date>
<genre>Intriga. Comedia. Acción</genre>
<extra>NA</extra>
<info>Después del éxito de su primer caso, Enola Holmes (Millie Bobby Brown) sigue los pasos de su popular hermano, Sherlock (Henry Cavill), abriendo su propia agencia, descubriendo que la vida como mujer detective a sueldo no es tan fácil como parece. Resignada a aceptar la realidad como adulta. A punto de cerrar el negocio cuando una joven cerillera sin dinero le ofrece a Enola su primer trabajo oficial: encontrar a su hermana desaparecida Sin embargo, este caso resulta ser mucho más desconcertante de lo esperado, que hace que Enola se vea inmersa en un nuevo y peligroso mundo: desde las siniestras fábricas de Londres y los coloridos salones de música, hasta los más altos niveles de la sociedad y el propio 221B de Baker Street. A medida que las chispas de una conspiración mortal se encienden, Enola debe recurrir a la ayuda de sus amigos -y del propio Sherlock- para desentrañar su misterio. ¡El juego comienza de nuevo!</info>
 
 
<page>10</page><title>ENREDADOS</title>
<microhd>6514d163ba4b3ae0c3b7c3a89117081548edd7de</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/z5kvXWek4smCyeWBDJQkT5sLc9T.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/xbMhTsaSaVSBp1KXL7MRmzQ5ddN.jpg</fanart>
<date>2010</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Flynn Rider, el más buscado -y encantador- bandido del reino, se esconde en una misteriosa torre y allí se encuentra con Rapunzel, una bella y avispada adolescente con una cabellera dorada de 21 metros de largo, que vive encerrada allí desde hace años. Ambos sellan un pacto y a partir de ese momento la pareja vivirá emocionantes aventuras en compañía de un caballo superpolicía, un camaleón sobreprotector y una ruda pandilla de matones.</info>
 
 
<page>10</page><title>ENTERRADOS</title>
<microhd>0a713ee7201b63851dfa6d3afd5c9cf165a37111</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/e8Hb4g3TEPFySwq5iao2IuUbEdb.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/e8Hb4g3TEPFySwq5iao2IuUbEdb.jpg</fanart>
<date>2018</date>
<genre>Drama </genre>
<extra>NA</extra>
<info>Narra la historia de cinco mineros cautivos en una galería a 600 metros de profundidad tras producirse un gran derrumbe en una mina de Asturias.</info>
 
 
<page>10</page><title>ENTRE LA RAZON Y LA LOCURA</title>
<microhd>97d975d8626182a585ae43b38cab4803a526fb4d</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/iujhDhSHxMUrWnEY7WDIqTsxfOg.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/zhNfM1CSQqJtIqelwlK9iKtSd9P.jpg</fanart>
<date>2019</date>
<genre>Drama. Thriller</genre>
<extra>NA</extra>
<info>A mediados del siglo XIX, el profesor James Murray comienza a compilar palabras para la primera edición del Oxford English Dictionary, un diccionario que tiene la ambiciosa tarea de recopilar todas las palabras de la lengua inglesa. Para sorpresa de Murray, un doctor de un asilo psiquiátrico será una de sus mayores ayudas...</info>
 
 
<page>10</page><title>ENTRE LA VIDA Y LA MUERTE</title>
<microhd>NA</microhd>
<fullhd>ed7b6845f99ba761946f733263fa283d9c023097</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/aEZYj3QgjxAHqc28VGmYitpe91p.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/4dLFIAQ9IM2fR6IgeHuTaoF8Qp2.jpg</fanart>
<date>2022</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Leo Castañeda, español afincado en Bruselas, trabaja como conductor de metro. Una noche presencia el suicidio de Hugo, su hijo, del que llevaba más de dos años sin saber nada. Tras lo sucedido, Leo empieza a indagar en las causas de su muerte y descubre que estuvo implicado en un atraco. La búsqueda de respuestas lo conducirá a una peligrosa investigación y a enfrentarse a su propio pasado.</info>
 
 
<page>10</page><title>ERASE UNA VEZ DEADPOOL</title>
<microhd>3af393a9542b6935c5d9e44b10d4911ac41916e9</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/faNkvVJw2EbIZ1uT9DP0ctAjjxD.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/7RqpTZq0mPpTcEwZ6qqwRZAFoLe.jpg</fanart>
<date>2018</date>
<genre>Comedia. Accion. Fantastico</genre>
<extra>NA</extra>
<info>Deadpool (Ryan Reynolds) le cuenta al actor Fred Savage (el niño de "La Princesa Prometida") algunas de sus aventuras, en un homenaje-parodia a dicha película de 1987 en la que el abuelo le contaba historias a Fred. 'Once Upon a Deadpool' es una revisión estrenada en cines de "Deadpool 2" adaptada a la calificación por edades PG-13, es decir: un reestreno navideño sin violencia, palabras malsonantes y otras características del film original de David Leitch, añadiéndose además nuevas escenas a la película.</info>
 
 
<page>10</page><title>ERASE UNA VEZ EN AMERICA</title>
<microhd>b98b0898117af8578ea44323d7582c1361cb21c0</microhd>
<fullhd>60976d1a0d87dcb06c2433390cc538b98d27a5ce</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/8zxvJVzKztKAp1AYeGoVpiZM2qG.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/z1s7ASlypXYGeMLoJ2Np4CpWdzd.jpg</fanart>
<date>1984</date>
<genre>Drama</genre>
<extra>Mis</extra>
<info>Principios del siglo XX. David Aaronson, un pobre chaval judío, conoce en los suburbios de Manhattan a Max, otro joven de origen hebreo dispuesto a llegar lejos por cualquier método. Entre ellos nace una gran amistad y, con otros colegas, forman una banda que prospera rápidamente, llegando a convertirse, en los tiempos de la Ley Seca (1920-1933), en unos importantes mafiosos.</info>
 
 
<page>10</page><title>ERASE UNA VEZ EN VENEZUELA</title>
<microhd>4e37a4e31e5fcb08f5a75edd9ad314c99c4cccf4</microhd>
<fullhd>eef2fb23b083d52f9acf2bfce323682edb817e90</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/qqYRkF6T31BWJjFapIda8jHizGy.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/jzAIt02m3sU7Y0noBxkxMB2awSo.jpg</fanart>
<date>2020</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Hace no mucho tiempo, la vida en Congo Mirador, un pueblo flotante a poca distancia del lago Maracaibo, era atractiva y bohemia. Hoy se encuentra afectada por las corruptelas y el cambio climático que provoca que sus habitantes abandonen la población poco a poco, pudriéndose entre la polución y la negligencia: una pequeña pero profética reflexión sobre el estado actual de Venezuela. Allá siguen intentando dar vida al lugar la señora Tamara, representante del partido chavista en el pueblo; y Natalie, la profesora, contraria a todo lo que significa el poder hoy en día; ambas mujeres reflejo de un país dividido.</info>
 
 
<page>10</page><title>ERASE UNA VEZ EN... HOLLYWOOD</title>
<microhd>e897d7e9b84f3f4c96da0d6352c3880cad754849</microhd>
<fullhd>cbbd6dfeff463a5d13a6b3b62e21a99583407f6b</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/vKSyaptSA7zZ9H8mSfaDnvyQl9k.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/yB2hTgz9CTVYjlMWPSl3LPx5nWj.jpg</fanart>
<date>2019</date>
<genre>Thriller. Drama. Comedia</genre>
<extra>NA</extra>
<info>Hollywood, años 60. La estrella de un western televisivo, Rick Dalton (DiCaprio), intenta amoldarse a los cambios del medio al mismo tiempo que su doble (Pitt). La vida de Dalton está ligada completamente a Hollywood, y es vecino de la joven y prometedora actriz y modelo Sharon Tate (Robbie) que acaba de casarse con el prestigioso director Roman Polanski. </info>
 
 
<page>10</page><title>ES POR TU BIEN</title>
<microhd>ee2abff5c0089acfff5c736af2fb5e3a84203f89</microhd>
<fullhd>3c861e0e26a6bbdf0fbb258d4debb0bda1d66778</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/6G59UxqW2iDgvwnOcRGDJptNnk6.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/833Pdpn6BLm1bsUWo1bZB5PpImm.jpg</fanart>
<date>2017</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>La peor pesadilla que un padre puede tener con una hija es que ésta crezca y llegue el día en que le presente a su novio, y que éste sea un tipo despreciable que busca una sola cosa de su preciada y amada niñita. Y ese día es precisamente el que les llega a tres padres: a Arturo, a Poli y a Chus. Sus dulces hijas han decidido tirar su futuro a la basura echándose tres novios a su juicio abominables. Así que la única solución es aunar fuerzas para librarse de ellos como sea.</info>
 
 
<page>10</page><title>ESCAPE ROOM</title>
<microhd>9fbe3cb74691b5dce89ecbeac93544934e136642</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/aUtWgKLWOkfGa6NZRQlYgCtjjOq.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/9IJ1ci1PoZH8XEWrZJkUUKnXwTV.jpg</fanart>
<date>2019</date>
<genre>Terror. Thriller. Intriga</genre>
<extra>NA</extra>
<info>Seis desconocidos se encuentran en una habitación mortal en la que deberán usar su ingenio para sobrevivir. </info>
 
 
<page>10</page><title>ESCAPE ROOM 2: MUERES POR SALIR</title>
<microhd>72c77b0476f4e9e1fabb627dd09e15a67d4ed88e</microhd>
<fullhd>40cebad75e12c4a310dbe408ea007c6b134d54d8</fullhd>
<tresd>NA</tresd>
<cuatrok>dfbd86245833183d7a57e24fc66351caf30f1a54</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/hEmpLIyeRGR683s31Q9oCwkSoCi.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/fQB7APBjtBKpOakeDi9sUXbBeCR.jpg</fanart>
<date>2021</date>
<genre>Acción. Thriller. Terror</genre>
<extra>NA</extra>
<info>Seis personas se encuentran encerradas de manera inesperada en una nueva serie de escape rooms, revelando paulatinamente aquello que tienen en común para sobrevivir… y descubriendo que todos ya habían jugado el juego con anterioridad. Secuela de "Escape Room" (2019).</info>
 
 
<page>10</page><title>ESCAPE ROOM: LA PEL-LICULA</title>
<microhd>e619f6f1fd04b288344a19ef14c7b486ebb21472</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/bsT3usCK8nTFa6MMiysx2A6gTZX.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/7WcwKFjhHjMobnlsfFUIpj3b4F7.jpg</fanart>
<date>2022</date>
<genre>Comedia. Thriller. Intriga</genre>
<extra>NA</extra>
<info>Cuatro amigos. Una habitación. Muchos secretos. Y una única vía de salida: la verdad. Una comedia que pone patas arriba los planes de dos parejas dispuestas a pasar una gran noche en un escape room. Si quieren salir de este juego tan imprevisible como alocado, tendrán que poner a prueba su amistad. Para ellos ha llegado la hora de la verdad.</info>
 
 
<page>10</page><title>ESCUADRON DE LA MUERTE</title>
<microhd>71c27c1c2cc3f28554a77293fb6650cc711abd4e</microhd>
<fullhd>a3d5a72fc21841472e95850ea963c7aea6ee7c38</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/2Eab3ET5fVKpk4qEZfwfOCv1lj1.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/wWmLkSZbG6TPGGlA7jFWR794TMl.jpg</fanart>
<date>2019</date>
<genre>Belico</genre>
<extra>NA</extra>
<info>El soldado Andrew Briggman (Nat Wolff) es un ambicioso miembro de infantería que acaba de llegar a Afganistán. Lucha a diario por destacar dentro de un escuadrón repleto de soldados salvajes y un poco alocados. Cuando el jefe del grupo es relevado por un nuevo líder (Alexander Skarsgård), Briggman se siente inspirado por su determinación y trata de imitar su conducta temeraria, alcanzando rápidamente el puesto como segundo de su unidad. Sin embargo, cuando Briggman es testigo de que el pelotón comienza a matar civiles inocentes, se verá obligado a decidir entre denunciarlo (poniendo su vida en peligro prácticamente de inmediato) o callarse para participar en lo que cree que son crímenes contra los derechos humanos</info>
 
 
<page>10</page><title>ESO VA A DOLER</title>
<microhd>NA</microhd>
<fullhd>49de233b4de7f70b4706c3fc0ba7925a603249f0</fullhd>
<tresd>NA</tresd>
<cuatrok>8b3ecf90e6f2a678a41816e38034999c230a4cb9</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/95Llwyv1uEH37naeRavP61eRmRc.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/8uWzHUO0u78jbFofH2Kjt3QsVwh.jpg</fanart>
<date>2023</date>
<genre>Romance. Ciencia ficción</genre>
<extra>Estreno</extra>
<info>Anna (Buckley) sospecha cada vez más que su relación con su pareja de toda la vida, Ryan, puede no estar funcionando de verdad. En un intento por mejorar las cosas, se embarca en secreto en una nueva misión trabajando en un misterioso instituto diseñado para incitar y probar la presencia del amor romántico en parejas cada vez más desesperadas.</info>
 
 
<page>10</page><title>ESPECIE OCULTA</title>
<microhd>737449b140fa8eb354868d24d0a4204f25e0c439</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ouqDca02a6TvRXuaShcaXG3qNrG.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/qLL6H9SGEwRRW6PJHixyUtbBjBE.jpg</fanart>
<date>2020</date>
<genre>Thriller. Intriga</genre>
<extra>NA</extra>
<info>Liberty, una joven de 16 años, regresa a casa después de dos meses en un campamento y conoce al prometido de su madre, John Smith, cuyas cualidades parecen demasiado perfectas para ser humanas.</info>
 
 
<page>10</page><title>ESPEJO ESPEJO</title>
<microhd>dc7cb03b936a31f55dbdef39df3c922cc475b66b</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/3FUfdBpl7VjC0ZWAbhLMcave9Ar.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/92ko5XCqziH4NVZia5RX29UXX2d.jpg</fanart>
<date>2022</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Cuatro empleados de una oficina se preparan para la celebración de los 50 años de la empresa mientras intentan resolver ciertos problemas. Álvaro (Santi Millán), Cristina (Malena Alterio), Paula (Natalia de Molina) y Alberto (Carlos Areces) son cuatro personas en crisis que trabajan para la misma empresa de cosmética. Luchan por lo que quieren mientras se enfrentan a sus propios reflejos ante el espejo. Ambición, miedo, amor y traición se mezclan en una historia sobre la identidad. A veces tu peor enemigo eres tú mismo.</info>
 
<page>11</page><title>ESPIAS CON DISFRAZ</title>
<microhd>b17958b1a3080275c7ab5f2794c7c25c40cba2e2</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>6d0889d096e5d41b768ba5e1f6bc0b0af957e243</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/j2xcicPNXzNex47EYOfw45Dl1o0.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/qlYxtqVfu2LOdvYMMDPCSGX0Oz0.jpg</fanart>
<date>2019</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>El superespía Lance Sterling y el científico Walter Beckett son casi polos opuestos. Lance es tranquilo, afable y caballeroso. Walter no. Pero lo que le falta a Walter de habilidades sociales lo compensa con ingenio e inventiva, con los que crea increíbles artilugios que Lance usa en sus épicas misiones. Pero cuando los eventos dan un giro inesperado, Walter y Lance de repente tienen que confiar el uno en el otro de una manera completamente nueva. Y si esta extraña pareja no puede aprender a trabajar en equipo, todo el mundo estará en peligro. Adaptación al largometraje del corto de animación homónimo, dirigido y escrito por Lucas Martell en 2009. </info>
 
<page>11</page><title>ESTAFADORAS DE WALL STREET</title>
<microhd>0f9b2a803a1033a60e6a25af09da375de096e597</microhd>
<fullhd>c7861465204182330e21038e914e255fc4f32b2b</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/di8JeI8otBrQMy6SZr4Hc3Ve3oj.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/5GynP6w2OQWSbKnCLHrBIriF4Cw.jpg</fanart>
<date>2019</date>
<genre>Drama. Comedia</genre>
<extra>NA</extra>
<info>Basada en un artículo en New York Magazine, sigue a un grupo de exestripers que forma equipo para dar la vuelta a la tortilla en sus relaciones con clientes de Wall Street.</info>
 
<page>11</page><title>ESTE CUERPO ME SIENTA DE MUERTE</title>
<microhd>09d8a30425d0649d6e0b3641142b574f41302093</microhd>
<fullhd>2b8735d7b5568ab5311cc2bc6e326fe6c47b46c9</fullhd>
<tresd>NA</tresd>
<cuatrok>72970fadeed565ff88ae159098f071461f335b81</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/tFZfNYvZMmicI574rm5FvJC313k.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/ngpC8wikrZDnhjkFQMEjWSh7HAV.jpg</fanart>
<date>2020</date>
<genre>Terror. Thriller. Fantástico</genre>
<extra>NA</extra>
<info>Tras intercambiar su cuerpo con un asesino en serie (Vince Vaughn), una joven estudiante (Kathryn Newton) descubre que tiene menos de 24 horas antes de que el cambio sea permanente. Del director de "Feliz día de tu muerte".</info>
 
 
<page>11</page><title>ETERNALS</title>
<microhd>d6d7befd1db679a74a34d8ec0ff2f19b750434e0</microhd>
<fullhd>4b0fbcb7129f9afcb17b9503d46fcb913c29b5b4</fullhd>
<tresd>NA</tresd>
<cuatrok>30e156cec1f58db80f4aa96190a61e4acc80c864</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/v2RltE8xIdK5zrQgNxxXh4OAvHO.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/3G6wET9eLvYn3aoIj8NfQFhpYEB.jpg</fanart>
<date>2021</date>
<genre>Fantástico. Acción. Ciencia ficción. Drama</genre>
<extra>NA</extra>
<info>Hace millones de años, los seres cósmicos conocidos como los Celestiales comenzaron a experimentar genéticamente con los humanos. Su intención era crear individuos superpoderosos que hicieran únicamente el bien, pero algo salió mal y aparecieron los Desviantes, destruyendo y creando el caos a su paso. Ambas razas se han enfrentado en una eterna lucha de poder a lo largo de la historia. En medio de esta guerra, Ikaris y Sersi tratarán de vivir su propia historia de amor.</info>
 
 
<page>11</page><title>ETERNAMENTE ENAMORADOS</title>
<microhd>3e71853b07b28e16ffa6271dd53e2a1ea3022964</microhd>
<fullhd>0d9ec633d26626fab62071cf330ec2af3e41a762</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/AmcoCfKMLgPYtCXrmzz1wgtSYO9.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/xVr2AyekvqO0DwJg8VwZNaQi5Ji.jpg</fanart>
<date>2019</date>
<genre>Romance. Drama</genre>
<extra>NA</extra>
<info>Drama ambientado en irlanda sobre Joan y Tom, un veterano matrimonio que se enfrenta a un gran impacto emocional cuando a ella la diagnostican cáncer de mama.</info>
 
 
<page>11</page><title>EXPEDIENTE WARREN: ANABELLE CREATION</title>
<microhd>33918adf98c2bafed6fec651a708bf15b891ee18</microhd>
<fullhd>eb913adf94e7ce2f12fb2f3b5677c4a40dd75de1</fullhd>
<tresd>NA</tresd>
<cuatrok>ef5b9c21e009d78336a32b5e13ad3ca2f695941e</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/8CrGaT0MdqZu7ql9JaQ63ZsKpd5.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/o8u0NyEigCEaZHBdCYTRfXR8U4i.jpg</fanart>
<date>2017</date>
<genre>Saga. Terror</genre>
<extra>NA</extra>
<info>Varios años después del trágico fallecimiento de su hija, un juguetero que crea muñecas y su mujer, acogen en su casa a una monja enfermera y a un grupo de niñas, tratando de convertir su casa en un acogedor orfanato. Sin embargo, las nuevos inquilinos se convertirán en el objetivo de Annabelle, una muñeca poseída por un ser demoníaco. Secuela de "Annabelle"</info>
 
 
<page>11</page><title>EXPEDIENTE WARREN: ANABELLE VUELVE A CASA</title>
<microhd>629db489daf408b27a1a0d377a537e5d423cbb8b</microhd>
<fullhd>3e9e4a1d2b4a716faa07a584722b60a2d15a6682</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/zxwPksbRKGaAi06rapPfFTVsMHv.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/jB98SrdXAYSbiprjIwc7WfVCuCV.jpg</fanart>
<date>2019</date>
<genre>Saga. Terror </genre>
<extra>NA</extra>
<info>Los demonólogos Ed y Lorraine Warren están decididos a evitar que Annabelle cause más estragos, así que llevan a la muñeca poseída a la sala de objetos bajo llave que tienen en su casa. La colocan "a salvo" en una vitrina sagrada bendecida por un sacerdote. Pero una terrorífica noche nada santa, Annabelle despierta a los espíritus malignos de la habitación, que se fijan en nuevos objetivos: la hija de diez años de los Warren, Judy, su niñera y una amiga de esta. </info>
 
 
<page>11</page><title>EXPEDIENTE WARREN: ANNABELLE</title>
<microhd>0a274d7cf893fe43df7b4cfda8ff8208fef37886</microhd>
<fullhd>a30e078172b0079a5eb981e8b7c53c2c94899158</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/sCPwduyyqCrIpSk1kA0p8lwjveB.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/2gKX53lskaCYk9ycXmaDJKqWiy.jpg</fanart>
<date>2014</date>
<genre>Terror. Saga</genre>
<extra>NA</extra>
<info>John Form encuentra el regalo perfecto para su mujer embarazada, Mia: una preciosa e inusual muñeca vintage que lleva un vestido de novia blanco inmaculado. Sin embargo, la alegría de Mia al recibir a Annabelle no dura mucho. Durante una espantosa noche la pareja ve como miembros de una secta satánica invaden su hogar y los atacan brutalmente. No sólo dejan sangre derramada y terror tras su visita…los miembros de la secta conjuran a un ente de tal maldad que nada de lo que han hecho se compara al siniestro camino a la maldición que ahora es… Annabelle.</info>
 
 
<page>11</page><title>EXPEDIENTE WARREN: EL CASO ENFIELD</title>
<microhd>06fe06b5068bdb79cffb01d87012e2c696667abd</microhd>
<fullhd>d24babcdbf8ab402936a7a77f92c85df16b68e55</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/AtwMXjMs7zDrTAM8L6SnaF5Qqzl.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/8yd5GRYbSARUeqvhZYoagYUKp6Z.jpg</fanart>
<date>2016</date>
<genre>Terror.Saga</genre>
<extra>NA</extra>
<info>Secuela de la exitosa "Expediente Warren" (2013) que presenta un caso real de los renombrados demonólogos Ed y Lorraine Warren. Para resolverlo viajan al norte de Londres para ayudar a una madre soltera que vive con sus cuatro hijos en una casa plagada de espíritus malignos.</info>
 
 
<page>11</page><title>EXPEDIENTE WARREN: LA LLORONA</title>
<microhd>b54210b4406c5f175f3fc29aecf7e51d6b25bab8</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/9xXoFIbtO9ehzWKXMnkXv5EIZmN.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/u2vGggeMPAkhEtD7bYGfeThsQiM.jpg</fanart>
<date>2019</date>
<genre>Saga. Terror. Thriller</genre>
<extra>NA</extra>
<info>La Llorona es una aparición tenebrosa, atrapada entre el cielo y el infierno, con un destino terrible sellado por su propia mano. La mera mención de esta terrorífica leyenda mexicana ha causado terror durante generaciones. En vida, ahogó a sus hijos llena de rabia, por celos, arrojándose en el río tras ver l que había hecho. Ahora sus lágrimas son eternas y letales, y aquellos que escuchan su llamada de muerte en la noche están condenados. Se arrastra en las sombras y ataca a los niños, desesperada por reemplazar a los suyos. A medida que los siglos han pasado, su deseo se ha vuelto más voraz y sus métodos más terroríficos. </info>
 
 
<page>11</page><title>EXPEDIENTE WARREN: LA MONJA</title>
<microhd>c5f4c0c913f410ecd78777bc30272e8ee88e0c9b</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/q2JFJ8x0IWligHyuLJbBjqNsySf.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/gcLGhjQTDOYYeCCx4EzVfSRWG43.jpg</fanart>
<date>2018</date>
<genre>Terror.Saga</genre>
<extra>NA</extra>
<info>Cuando una joven monja se suicida en una abadía de clausura en Rumanía, un sacerdote experto en posesiones demoniacas y una novicia a punto de tomar sus votos, son enviados por el Vaticano para investigar. Juntos descubren el profano secreto de la orden. Arriesgando no solo sus propias vidas sino su fe y hasta sus almas, se enfrentan a una fuerza maléfica en forma de monja demoníaca, en una abadía que se convierte en un campo de batalla de horror entre los vivos y los condenados.... Spin-off de la película de terror de 2016 'The Conjuring 2'. Producida por Atomic Monster, productora del director especializado en el género de terror, James Wan. </info>
 
<page>11</page><title>EXPEDIENTE WARREN: OBLIGADO POR EL DEMONIO</title>
<microhd>d5e8c31a58b53d3d3f626d7d30b6d74d2fc831a8</microhd>
<fullhd>00eeb880af131573b2044fd29199d84d0439a6b9</fullhd>
<tresd>NA</tresd>
<cuatrok>23c75fb759e60061c1bf777e5778a4158639e02a</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ghMQALCyytc6W0wlOlMIKiMSRKV.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/qi6Edc1OPcyENecGtz8TF0DUr9e.jpg</fanart>
<date>2021</date>
<genre>Terror. Thriller. Intriga. Saga</genre>
<extra>NA</extra>
<info>Ambientada en los años 80. Ed y Lorraine Warren deberán afrontar un nuevo caso que se presenta con un hombre, Arne Cheyne Johnson, que es acusado de asesinato tras haber sido poseído por un demonio.</info>
 
 
<page>11</page><title>EXPEDIENTE WARREN: THE CONJURING</title>
<microhd>f0987e7751f1445bb972bf6341dbe91ba1f8ee52</microhd>
<fullhd>ee93826d7dfc334b6356345a4e638a476346171c</fullhd>
<tresd>NA</tresd>
<cuatrok>e8b8c8a1185f2f8a397e2290e53aab6847dcb227</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/sKuHxkCogdk6YWzTyXYPoo9qd9n.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/9J7SSlev5FtCARttcM9cNwwQRpZ.jpg</fanart>
<date>2013</date>
<genre>Terror.Saga</genre>
<extra>NA</extra>
<info>Basada en una historia real documentada por los reputados demonólogos Ed y Lorraine Warren. Narra los encuentros sobrenaturales que vivió la familia Perron en su casa de Rhode Island a principios de los 70. El matrimonio Warren, investigadores de renombre en el mundo de los fenómenos paranormales, acudieron a la llamada de esta familia aterrorizada por la presencia en su granja de un ser maligno.</info>
 
 
<page>11</page><title>EXTREMADAMENTE CRUEL, MALVADO Y PERVERSO</title>
<microhd>900bd116239a2ec3e72fcb0d253af914687575e7</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/jzo6I6SKTFPLXK8BqZhq7sTVCSV.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/uRoA67sM5vvCI4cplxhl67BTPly.jpg</fanart>
<date>2019</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Ted Bundy fue uno de los asesinos en serie más peligroso de los años 70 pero, además de asesino fue un secuestrador, violador, ladrón, necrófilo... Su novia, Elizabeth Kloepfer, se convirtió en una de sus más fieles defensoras, negándose a creer la verdad sobre él durante años. La historia de sus numerosos y terribles crimenes contada a través de los ojos de Elizabeth.</info>
 
 
<page>11</page><title>FALL (VERTIGO)</title>
<microhd>9894c839b3e2804a6dd4ff8d498407ba701a4c6b</microhd>
<fullhd>e35ec755d39b7d0d79a9fa60fa3a6f4723efb88a</fullhd>
<tresd>NA</tresd>
<cuatrok>1d69a4859c4100d047909c6348e029503a45c209</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/aOidBN8Ch4LD6M9HyvDjTqM9fLF.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/hT3OqvzMqCQuJsUjZnQwA5NuxgK.jpg</fanart>
<date>2022</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Para Becky y Hunter, la vida trata de superar tus miedos y empujar tus límites. Sin embargo, después de subir hasta la cima de una torre de comunicaciones abandonada, se encuentran atrapadas y sin forma de bajar. A 600 metros del suelo y totalmente alejadas de la civilización, las chicas pondrán a prueba sus habilidades de escaladoras expertas y lucharán deses</info>
 
 
<page>11</page><title>FAMILIA AL INSTANTE</title>
<microhd>d3a49a074763b90b01d0fba488b58e454db7a40d</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/pVxmxH7IYL7hnioTBOAPIw2vbr1.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/lwICpzZudw8BZ0bODaHgRWCdioB.jpg</fanart>
<date>2018</date>
<genre>Comedia . Infantil</genre>
<extra>NA</extra>
<info>La emoción y alegría de convertirse en padres llega de pronto para Pete y Ellie, una joven pareja que decide compartir su felicidad y sumar a su familia a Juan, Lita y Lizzy, tres niños que son hermanos entre sí. Pero con ello también comienzan muchos berrinches, portazos, cenas accidentadas, responsabilidades triples y... sobre todo, una gran familia.</info>
 
 
<page>11</page><title>FAMILY MAN</title>
<microhd>NA</microhd>
<fullhd>1fa555566d9489e84e88ae14b98324cb17374fb2</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/9iyQxVjLgFW0ztYKRxnEFtpVTSw.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/dfHFW0DqaRQ7bWAURFcKLMdDyMG.jpg</fanart>
<date>2000</date>
<genre>Drama. Comedia</genre>
<extra>Mis</extra>
<info>Jack Campbell (Nicolas Cage) es un egocéntrico broker de Wall Street cuya única obsesión es el trabajo y una vida llena de lujo. Un día, tras un incidente en una tienda el día de Nochebuena, se despierta viviendo otra vida: es un humilde vendedor de neumáticos de Nueva Jersey, casado con su antigua novia Kate (Téa Leoni), a la que había abandonado hacía años para que no obstaculizara su carrera en el mundo de las finanzas.</info>
 
 
<page>11</page><title>FANTASIA</title>
<microhd>a49c5de36dc886491e32dcc30ccada1f5e30f1c6</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/qCaPIUrbKsUKKFPykmKxc6bPbo0.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/nnL7vunWHE24iU7WGGP84q7FXO9.jpg</fanart>
<date>1940</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Todo un clásico de la Disney, una colección de interpretaciones animadas de grandes obras de música clásica. En "El Aprendiz de Brujo" (P. Dukas), Mickey Mouse, discípulo de un mago, se mete en un gran embrollo, pues sus conocimientos de magia son muy limitados. "La Consagración de la Primavera" (Stravinsky) cuenta la historia de la evolución, desde los seres unicelulares hasta la extinción de los dinosaurios. "La Danza de las Horas" (Ponchielli) es un fragmento de un ballet cómico interpretado por elefantes, hipopótamos, cocodrilos y avestruces. "Una Noche en el Monte Pelado" de Mussorgsky y el "Ave Maria" de Schubert describen el enfrentamiento entre la oscuridad y la luz.</info>
 
 
<page>11</page><title>FANTASIA 2000</title>
<microhd>864d657e64347d6ce571f1f3d605d48b344b435b</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/tidPU8Wq3p3DP7ShyfOkkQnT85J.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/t7vmK5JFBvRK0gEWPjFwem8WxfY.jpg</fanart>
<date>1999</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Al igual que su precedente, ésta película se divide en varios fragmentos -en este caso ocho- cada uno acompañado de distintas piezas musicales. Entre ellos se incluye El aprendiz de brujo, protagonizado por Mickey Mouse, que ya aparecía en la primera Fantasía. </info>
 
 
<page>11</page><title>FANTASY ISLAND</title>
<microhd>baf2fbca35ccdfd123b79b84b8818220fa7ea72d</microhd>
<fullhd>004b6f9236d97140b8578ae5b45847d3cd8fb030</fullhd>
<tresd>NA</tresd>
<cuatrok>84575a6554567c479925361478324346b1db161a</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/y0ZX1ymtnZ4ATsQJjXTPaG9RdWD.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/529hBs3xzwg2Cgu0xoVjOe3b5BN.jpg</fanart>
<date>2020</date>
<genre>Aventuras. Fantástico. Intriga. Terror</genre>
<extra>NA</extra>
<info>El enigmático Sr. Roarke hace realidad los sueños de los afortunados huéspedes que acuden a su lujoso y remoto resort tropical. Sin embargo, sus fantasías se convertirán en pesadillas, y los invitados deberán resolver el misterio que oculta la isla para poder escapar con vida. Adaptación del famoso programa de televisión de los años 70, ambientado en un resort ubicado en una isla mágica.</info>
 
 
<page>11</page><title>FARGO</title>
<microhd>275fb1f179803ad86ad2697013aad795ddd28681</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/pkuNNMtJ40h3l0eQIMM8CYHXJlQ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/747dgDfL5d8esobk7h4odaOFhUq.jpg</fanart>
<date>1996</date>
<genre>Thriller. Intriga </genre>
<extra>Mis</extra>
<info>Un hombre apocado y tímido, casado con la hija de un millonario que le impide disfrutar de su fortuna, decide contratar a dos delincuentes para que secuestren a su mujer con el fin de montar un negocio propio con el dinero del rescate. Pero, por una serie de azarosas circunstancias, al secuestro se suman tres brutales asesinatos, lo que obliga a la policía a intervenir.</info>
 
 
<page>11</page><title>FAST AND FURIOUS 1</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>7d3c5183c4224bd8997c2cef22c03def84501d29</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/aBOit0kuZYaXkZMlBthMX4axlvq.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/jY9ef5nqY4xIIMu3yzW3qamUCoi.jpg</fanart>
<date>2001</date>
<genre>Saga. Accion</genre>
<extra>NA</extra>
<info>Una misteriosa banda de delincuentes se dedica a robar camiones en marcha desde vehículos deportivos. La policía decide infiltrar un hombre en el mundo de las carreras ilegales para descubrir posibles sospechosos. El joven y apuesto Brian entra en el mundo del tunning donde conoce a Dominic, rey indiscutible de este mundo y sospechoso número uno, pero todo se complicará cuando se enamore de su hermana.</info>
 
 
<page>11</page><title>FAST AND FURIOUS 2</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>6078fd5c63b69ecbcf55d9bb19630640149b6ae3</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/3w1eUOd0Qhx41UVu7kcQQ06YP62.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/nw8OEhMZg2VnEwjinhQRBMAhSPE.jpg</fanart>
<date>2003</date>
<genre>Saga. Accion</genre>
<extra>NA</extra>
<info>El agente del FBI Brian O'Connor (Paul Walker) cayó en desgracia al ver su lealtad puesta a prueba en su anterior trabajo, el mundo de las carreras ilegales de Los Angeles. La decisión de O’Connor le permitió conservar su honor, pero le hizo perder su insignia y toda posibilidad de rehabilitarse. Ahora ha pasado el tiempo, O’Connor está en otra ciudad y tiene una última oportunidad. A los federales de Miami les está costando mucho detener a Carter Vellone (Cole Hauser), un empresario que utiliza su negocio como tapadera para un cartel internacional de blanqueo de dinero. Aduanas lleva un año sometiendo a Verone a una estricta vigilancia, y lo único que han podido establecer es su relación con las carreras callejeras ilegales. El tiempo se acaba, y los agentes deciden llamar a O’Connor para que haga lo que sabe hacer mejor que nadie: volver a infiltrarse. Pero antes pone algunas condiciones para aceptar la misión que puede permitirle recuperar su insignia. La primera: trabajar con su amigo de infancia y antiguo delincuente Roman Pearce (Tyrese), otro enfermo de la velocidad.</info>
 
<page>11</page><title>FAST AND FURIOUS 3 TOKYO RACE</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>f2dc6a8d51fe2d56d01894d2c4d948b377913cf8</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/wpSsR2wZ4XAkq5Mld6sG1yVdPGP.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/z9yXYuN0dHNTyWIPYMzvOiIdTj2.jpg</fanart>
<date>2006</date>
<genre>Saga. Accion</genre>
<extra>NA</extra>
<info>Shaun Boswell es un chico que no acaba de encajar en ningún grupo. En el instituto es un solitario, su única conexión con el mundo de indiferencia que le rodea es a través de las carreras ilegales, lo que no le ha convertido en el chico favorito de la policía. Cuando amenazan con encarcelarle, le mandan fuera del país a pasar una temporada con su padre, un militar destinado en Japón, que vive en un diminuto piso en un barrio barato de Tokio. En el país donde nacieron la mayoría de los coches modificados, las simples carreras en la calle principal han sido sustituidas por el último reto automovilístico que desafía la gravedad, las carreras de "drift" (arrastre), una peligrosa mezcla de velocidad en pistas con curvas muy cerradas y en zigzag. En su primera incursión en el salvaje mundo de las carreras de "drift", Shaun acepta ingenuamente conducir un D.K, el Rey del Drift, que pertenece a los Yakuza, la mafia japonesa. Para pagar su deuda, no tiene más remedio que codearse con el hampa de Tokio y jugarse la vida.</info>
 
 
<page>11</page><title>FAST AND FURIOUS 4 AUN MAS RAPIDO</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>269157ce4d9d955af24575fd9f6dfb1296bab074</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/vvewollXh3k2G8ruTYhhBTqZH19.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/aZGJeO9va2S7EFelJRQM4srtxpW.jpg</fanart>
<date>2007</date>
<genre>Saga. Accion</genre>
<extra>NA</extra>
<info>El fugitivo Dom Toretto (Vin Diesel) y el detective Brian O’Conner (Paul Walker) vuelven a encontrarse en Los Ángeles, pero sus relaciones no mejoran. Sin embargo, obligados a enfrentarse a un enemigo común, no les queda más remedio que unir sus fuerzas si quieren vencerlo. Después del atraco a un convoy, los dos protagonistas averiguan que si quieren vengarse, deberán llegar al límite de sus posibilidades al volante.</info>
 
 
<page>11</page><title>FAST AND FURIOUS 5</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>8ce3333d76d7659d9e1d94d0b9f4db18bf833d19</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/d7OwxVxXdkfUOwh5VvW6JpY0Ot9.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/AlkvibC9ROCSYq2hC8n2JTg9ByV.jpg</fanart>
<date>2011</date>
<genre>Saga. Accion</genre>
<extra>NA</extra>
<info>Desde que Brian O'Conner (Paul Walker) y Mia Toretto (Jordana Brewster) sacaron a Dom Toretto (Vin Diesel) de la cárcel, se han visto obligados a huir y cruzar muchas fronteras para evitar a la policía. Atrapados en Río de Janeiro, una vez más tienen que darse a la fuga; pero los tres se dan cuenta de que la única forma de poner fin a su huida permanente es enfrentarse de una vez por todas al empresario corrupto que quiere verlos muertos. Pero no es éste el único que les sigue la pista</info>
 
 
<page>11</page><title>FAST AND FURIOUS 6</title>
<microhd>11055481f9c029c7b1879a60cfb19b6a7f84453f</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>0fa540e6fa7077e4009a6ba4235885514ad5c5a8</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/n31VRDodbaZxkrZmmzyYSFNVpW5.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/rF8HkxQ1RlrcPg41gmL5AM56Qxd.jpg</fanart>
<date>2013</date>
<genre>Saga. Accion</genre>
<extra>NA</extra>
<info>Desde que Dom (Vin Diesel) y Brian (Paul Walker) le robaron a un mafioso 100 millones de dólares, se encuentran en paradero desconocido; no pueden regresar a casa porque la ley los persigue. Entretanto, Hobbs (Dwayne Johnson) ha seguido la pista en varios países a una banda de letales conductores mercenarios, cuyo cerebro (Luke Evans) cuenta con la inestimable ayuda de la sexy Letty (Michelle Rodriguez), una vieja novia de Dom, a la que éste daba por muerta. La única forma de detenerlos es enfrentarse a ellos en las calles; así que Hobbs le pide a Dom que reúna a su equipo en Londres.</info>
 
<page>11</page><title>FAST AND FURIOUS 7</title>
<microhd>NA</microhd>
<fullhd>dca08863facf22cf345eb3d6b3fd13512623487b</fullhd>
<tresd>NA</tresd>
<cuatrok>74f679340ce1d549a6079a05e4bd8aed5120313d</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/jPFsjmFlTFmpIY41BP3C4sVYOKw.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/7X6zOOEzXTQJkzDgBHVly1BgyYu.jpg</fanart>
<date>2015</date>
<genre>Saga.Accion</genre>
<extra>NA</extra>
<info>Ha pasado un año desde que el equipo de Dominic Torreto y Brian pudiera regresar finalmente a Estados Unidos, tras ser indultados. Desean adaptarse a una vida en la legalidad, pero el entorno ya no es el mismo. Dom intenta acercarse a Letty, y Brian lucha para acostumbrarse a la vida en una urbanización con Mia y su hijo. Ninguno de ellos imagina que un frío asesino británico, entrenado para realizar operaciones secretas, se cruzará en sus vidas para convertirse en su mayor enemigo.</info>
 
<page>11</page><title>FAST AND FURIOUS 8</title>
<microhd>aee434705073d02042475e79a8dd9fd46b158075</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>a3904999491f27a3b67d37b3ce01c8ddceb6525c</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/5b1zDwWxoDG8p8aHoRYfzOd7Lvy.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/jzdnhRhG0dsuYorwvSqPqqnM1cV.jpg</fanart>
<date>2017</date>
<genre>Saga. Accion. Thriller</genre>
<extra>NA</extra>
<info>Con Dom y Letty de luna de miel, Brian y Mia fuera del juego y el resto de la pandilla exonerada de todo cargo, el equipo está instalado en una vida aparentemente normal. Pero cuando una misteriosa mujer (Theron) seduze a Dom (Diesel) para regresar nuevamente al mundo del crimen, se ve incapaz de rechazar la oportunidad, traicionando así a todo el mundo cercano a él. A partir de ese momento todos se enfrentarán a pruebas como nunca antes habían tenido. Desde las costas de Cuba y las calles de Nueva York hasta las llanuras del mar de Barents en el océano Ártico, nuestra fuerza de élite recorrerá el globo para impedir que un anarquista desencadene el caos en el mundo... y por supuesto para traer de vuelta a casa al hombre que les hizo una familia.</info>
 
<page>11</page><title>FAST AND FURIOUS 9</title>
<microhd>cc4eff183741b3b54d516fed32daca4edc30aaf8</microhd>
<fullhd>f21d00056129a01fc1fe123ed56976ae73df8995</fullhd>
<tresd>NA</tresd>
<cuatrok>fc18f04d355ff04f0f786ef6753b9bcc227db62a</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/vdRgVlkkw1eHOdgYKXbzj0qSLB0.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/gGTCDNEvwG848u34Op1nZNALLUr.jpg</fanart>
<date>2021</date>
<genre>Acción. Thriller. Ciencia ficción. Saga</genre>
<extra>NA</extra>
<info>Dom Toretto (Vin Diesel) lleva una vida tranquila con Letty y su hijo, el pequeño Brian, pero saben que el peligro siempre acecha. Esta vez, esa amenaza obligará a Dom a enfrentarse a los pecados de su pasado si quiere salvar a quienes más quiere. El equipo se vuelve a reunir para impedir un complot a escala mundial, liderado por uno de los asesinos más peligrosos y mejor conductor a los que se han enfrentado; un hombre que además es el hermano desaparecido de Dom, Jakob (John Cena). Novena entrega de la famosa franquicia. </info>
 
 
<page>11</page><title>FAST AND FURIOUS HOBBS AND SHAW</title>
<microhd>90d525f099daa6e02e1274542dde8d5490413292</microhd>
<fullhd>0f30aa236319a6ecd62049adbf31a5fe2f115842</fullhd>
<tresd>NA</tresd>
<cuatrok>61615126be99ca043e87a0ca5d26b8cc497e5680</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/nU8PLLkOpYzfOyAE3yjrX8DviaM.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/qAhedRxRYWZAgZ8O8pHIl6QHdD7.jpg</fanart>
<date>2019</date>
<genre>Accion. Ciencia ficcion .Saga</genre>
<extra>NA</extra>
<info>Desde que se cruzaron los caminos del agente Hobbs (Johnson), un leal miembro de los servicios de Seguridad del Cuerpo Diplomático estadounidense, y del solitario mercenario Shaw (Statham), ex miembro de un cuerpo de élite del ejército británico, los insultos, golpes y burlas no han cesado entre ellos para ver cuál de los dos cae antes. Pero cuando un anarquista mejorado ciber-genéticamente llamado Brixton (Elba) se hace con el control de una peligrosa arma biológica, el mundo se enfrenta a una de sus mayores amenazas. Cuando Shaw se entera de que además Brixton ha derrotado a su hermana, una brillante e intrépida agente secreta del M16 (Kirby), él y Hobbs no tendrán más remedio que dejar su mortal enemistad a un lado para salvar el mundo y derrotar al único hombre capaz de acabar con ellos.</info>
 
 
<page>11</page><title>FAST AND FURIOUS X</title>
<microhd>NA</microhd>
<fullhd>c84494df0f0c037b02430a86bb3efd71199d7235</fullhd>
<tresd>NA</tresd>
<cuatrok>7f138ac083a1f3ef665f74b945cfca8a4697a8c4</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/fiVW06jE7z9YnO4trhaMEdclSiC.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/fSd5MjJNcauyEbHuzUdCE1TKttK.jpg</fanart>
<date>2023</date>
<genre>Acción. Thriller. Saga</genre>
<extra>Estreno</extra>
<info>Durante numerosas misiones más que imposibles, Dom Toretto y su familia han sido capaces de ser más listos, de tener más valor y de ir más rápido que cualquier enemigo que se cruzara con ellos. Pero ahora tendrán que enfrentarse al oponente más letal que jamás hayan conocido: Un terrible peligro que resurge del pasado, que se mueve por una sangrienta sed de venganza y que está dispuesto a destrozar a la familia y destruir para siempre todo lo que a Dom le importa. </info>
 
 
<page>11</page><title>FATALE</title>
<microhd>392d4c04db2c22aac5a15d5963727fda591f8f38</microhd>
<fullhd>19d7d80ba43cb12386bfa94424a0a346cb0c998c</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/9v43jAAfCYcQEgeMJ1H0rghN0of.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/ggSL2sBEQW7GvjPog62xUZmaamv.jpg</fanart>
<date>2020</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>Darren (Michael Ealy), un exitoso agente deportivo y hombre casado, se adentra en un mundo de pesadilla cuando comienza a ser manipulado por una mujer detective (Hilary Swank) con la que ha tenido una noche de pasión. La pregunta es: ¿hasta dónde estará dispuesto a llegar para salvar su matrimonio del error que ha cometido?</info>
 
 
<page>11</page><title>FEEDBACK</title>
<microhd>499a8b0b86488ae53ad97ab465542a84cf9ec854</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/xfOVhNP93QSiHn9hEWemTMiBK19.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/dLNJNymNyfO1CldLQKFYZhpKdjQ.jpg</fanart>
<date>2019</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>Jarvis Dolan es el periodista estrella del exitoso late night radiofónico londinense “The Grim Reality” (“La cruda realidad”). Al comienzo de su programa, unos macabros encapuchados irrumpen en el estudio, tomando el control a punta de pistola. Están allí para sacar a la luz un escándalo que podría suponer el final de Jarvis. Obligado a continuar con el programa siguiendo el guion de los asaltantes, la noche se convertirá en una pesadilla</info>
 
 
<page>11</page><title>FELIZ DIA DE TU MUERTE</title>
<microhd>d70e9c60b60b582b0c718ce5477cdbd984adada1</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>529afb3ba3685801e17a120b35db371805713b2b</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/uWmwvOvsbshDzOz5Zjb73AuAtN1.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/eGx5OfOdvM0gkHdmkLe3hcJuEIT.jpg</fanart>
<date>2017</date>
<genre>Terror. Intriga. Thriller.Saga</genre>
<extra>NA</extra>
<info>Una joven estudiante universitaria (Jessica Rothe) reconstruye el día de su asesinato reviviendo tanto los detalles cotidianos como su aterrador final, hasta intentar descubrir la identidad de su asesino.</info>
 
 
<page>11</page><title>FELIZ DIA DE TU MUERTE 2</title>
<microhd>3da821314e32cbc0160238a60858475e05c6e29a</microhd>
<fullhd>6c8f02c55a11e4ad13d53517f55e46905619befd</fullhd>
<tresd>NA</tresd>
<cuatrok>d82b94996bd7e95b5f7415df7e550b071f1b3e52</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/q6PSTBNlI9WtfuMlngz82yaudUg.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/v99NrmuSYn4HGwwpHWEkMVSeAL3.jpg</fanart>
<date>2019</date>
<genre>Saga. Terror. Intriga. Ciencia ficcion. Thriller. Comedia</genre>
<extra>NA</extra>
<info>Dos años después de los eventos acaecidos en la primera película, Tree Gelbman (Jessica Rothe) vuelve a entrar en el bucle temporal para descubrir el motivo por el cual accedió a él en primer lugar. También debe hacer frente a Lori, que tras resucitar a causa del bucle ha vuelto sedienta de venganza. Secuela de "Happy Death Day" (2017), uno de los éxitos de Blumhouse Productions.</info>
 
 
<page>11</page><title>FERDINAND</title>
<microhd>f08956e94eeb83c5e7187ae77301cdded28d94e0</microhd>
<fullhd>3c43b910c23185ab13be417ca776d12b9f6e05b3</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/bCo0AubAUaFQLowF1IwPjVv7hFt.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/rLPktUau4qv6DtPRqXSbefjqW0D.jpg</fanart>
<date>2017</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Ferdinand es un novillo muy tranquilo que prefiere sentarse bajo un árbol a oler las flores que saltar, resoplar y embestirse con otros toros. A medida que va creciendo y haciéndose fuerte, su temperamento no cambia y sigue siendo un toro manso. Un día, unos hombres vienen buscando al toro más grande, rápido y bravo... y Ferdinand es elegido equivocadamente para las corridas de toros de Madrid.</info>
 
 
<page>11</page><title>FESTIVAL DE LA CANCI0N DE EUROVISIÓN: LA HISTORIA DE FIRE SAGA</title>
<microhd>5487d6d79a927d5e611c1b544c25d0459d307f87</microhd>
<fullhd>b6684794e9abf442b87e0dccb0ac9775c6473ef0</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/7Wi7rpl9Ge1fxwArw9dP2BgkUi7.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/jMO1icztaUUEUApdAQx0cZOt7b8.jpg</fanart>
<date>2020</date>
<genre>Comedia. Musical</genre>
<extra>NA</extra>
<info>Lars (Will Ferrell) y Sigrit (Rachel McAdams) son dos músicos islandeses desconocidos que tienen delante la oportunidad de su vida: representar a su país en el concurso musical más importante del mundo. Ahora podrán demostrar que hay sueños por los que vale la pena luchar.</info>
 
 
<page>11</page><title>FIEBRE DEL SABADO NOCHE</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>b947af7adb2397d115b44599f456170e20a83b24</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/mUVyRzseR0ljtsyuhBviuy9cY4T.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/hsExnzxtgMOyew6uI1NwOD7Pu04.jpg</fanart>
<date>1977</date>
<genre>Musical. Drama</genre>
<extra>Mis</extra>
<info>En Nueva York, después de trabajar toda la semana en una tienda de pinturas de Brooklyn, Tony Manero (John Travolta) se prepara esmeradamente para disfrutar de la noche del sábado en la disco: se empapa en colonia Brut, se pone una camisa de flores ajustada, pantalones de tela de gabardina y zapatos de plataforma. En la discoteca de moda, Tony deslumbrará a todos con lo que mejor sabe hacer: bailar.</info>
 
 
<page>11</page><title>FIGURAS OCULTAS</title>
<microhd>426bdd4533bfa5295c77b7cc4da2e49726d43280</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/yiufjE3kfM9e0qIlEdGEmkUcXky.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/rfkIeCaIHhN3K5wjJJqKmfUjYp8.jpg</fanart>
<date>2016</date>
<genre>Drama</genre>
<extra>Mis</extra>
<info>Narra la historia nunca contada de tres brillantes mujeres científicas afroamericanas que trabajaron en la NASA a comienzos de los años sesenta (en plena carrera espacial, y asimismo en mitad de la lucha por los derechos civiles de los negros estadounidenses) en el ambicioso proyecto de poner en órbita al astronauta John Glenn.</info>
 
 
<page>11</page><title>FINCH</title>
<microhd>347826b85f005114421fac0f8aec0ee3c59938fe</microhd>
<fullhd>33e9fc4840565386dc703a0517c30f15cf4fa413</fullhd>
<tresd>NA</tresd>
<cuatrok>aaa42fe01b4c297bd6cb54466b0dc7e56f7e9a4d</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/akCtSzs2ln4lFWODXZ412lmsjUe.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/oE6bhqqVFyIECtBzqIuvh6JdaB5.jpg</fanart>
<date>2021</date>
<genre>Ciencia ficción. Drama</genre>
<extra>NA</extra>
<info>Tom Hanks interpreta a Finch, un ingeniero especializado en robótica y uno de los pocos supervivientes de un cataclismo solar que ha convertido al mundo en un páramo. Pero Finch, que lleva una década viviendo en un búnker subterráneo, ha construido un mundo propio que comparte con su perro, Goodyear, y con un robot, interpretado por Caleb Landry Jones, que creó para que cuide de Goodyear cuando él ya no pueda hacerlo. Cuando el trío se embarca en un peligroso viaje al desolado oeste americano, Finch se esfuerza por mostrar a su creación, que se bautizó como Jeff, y la alegría y la maravilla que representan estar vivo. Su viaje está plagado de retos y de humor, ya que a Finch le resulta tan difícil lograr que Jeff y Goodyear se lleven bien como a él lidiar con los peligros de este nuevo mundo.</info>
 
 
<page>11</page><title>FIRST MAN</title>
<microhd>4a8c54fb2919e2c790e372f18552f3bad2cb92b4</microhd>
<fullhd>e5271b7d46a40ed584c69ce13150c8f0e5159048</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/6LKINapQLIOtRuvAGO30FUuUJqN.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/z6ZANhPCcPl1HsKGjY5iZALf65V.jpg</fanart>
<date>2018</date>
<genre>Aventuras. Drama</genre>
<extra>NA</extra>
<info>Cuenta la historia de la misión de la NASA que llevó al primer hombre a la luna, centrada en Neil Armstrong (Ryan Gosling) y el periodo comprendido entre los años 1961 y 1969. Un relato en primera persona, basado en la novela de James R. Hansen, que explora el sacrificio y el precio que representó, tanto para Armstrong como para los Estados Unidos, una de las misiones más peligrosas e importantes de la historia de la humanidad.</info>
 
 
<page>11</page><title>FLASH</title>
<microhd>NA</microhd>
<fullhd>f5f4656da7d321ef4942e9978fc160499a5705dc</fullhd>
<tresd>NA</tresd>
<cuatrok>ff67dcb0a5368ef641885effbb31ec150ed212df</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/rktDFPbfHfUbArZ6OOOKsXcv0Bm.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/yF1eOkaYvwiORauRCPWznV9xVvi.jpg</fanart>
<date>2023</date>
<genre>Ciencia ficción. Acción. Fantástico. Comedia </genre>
<extra>NA</extra>
<info>Los mundos chocan en "Flash" cuando Barry utiliza sus superpoderes para viajar en el tiempo y cambiar los acontecimientos del pasado. Barry intenta salvar a su familia, pero sin saberlo altera el futuro y queda atrapado en una realidad en la que el general Zod ha regresado y amenaza con la aniquilación, pero en la que no hay Superhéroes a los que recurrir. A menos que Barry pueda persuadir a un Batman muy diferente para que salga de su retiro y rescate a un kryptoniano encarcelado... aunque no sea el que está buscando. En última instancia, para salvar el mundo en el que se encuentra y regresar al futuro que conoce, la única esperanza de Barry es luchar por seguir vivo. Pero ¿este último sacrificio será suficiente para reiniciar el universo?</info>
 
 
<page>11</page><title>FORREST GUMP</title>
<microhd>NA</microhd>
<fullhd>1adbaf3f59a5c9ad012ba25a2f6bcf93f6836e32</fullhd>
<tresd>NA</tresd>
<cuatrok>ebf0ae6e8cd226857b0c6d8942995750ea429a64</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/azV6hV99lYkdhydsQbJCI6FqMl4.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/7c9UVPPiTPltouxRVY6N9uugaVA.jpg</fanart>
<date>1999</date>
<genre>Comedia. Drama. Romance</genre>
<extra>Culto</extra>
<info>Forrest Gump (Tom Hanks) sufre desde pequeño un cierto retraso mental. A pesar de todo, gracias a su tenacidad y a su buen corazón será protagonista de acontecimientos cruciales de su país durante varias décadas. Mientras pasan por su vida multitud de cosas en su mente siempre está presente la bella Jenny (Robin Wright), su gran amor desde la infancia, que junto a su madre será la persona más importante en su vida. </info>
 
 
<page>11</page><title>FORTALEZA: EL OJO DEL FRANCOTIRADOR</title>
<microhd>c26cfb170432e80b24d234ec7e5863532d5a91b0</microhd>
<fullhd>a73f91f8aa9602b98b4b3cf3b0780611a7732655</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/9y2ymS09bqTSM7L2DZJi7aIxor6.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/uOGcYv0Su01CFpfhXFVVKia6j3U.jpg</fanart>
<date>2022</date>
<genre>Acción. Thriller</genre>
<extra>NA</extra>
<info>Semanas después del asalto mortal a Fortress Camp, Robert realiza un atrevido rescate para salvar a Sasha, la viuda de su antiguo némesis Balzary. Pero de vuelta en el búnker de mando del campamento, parece que Sasha puede tener sus propios planes tortuosos. Cuando estalla un nuevo ataque, Robert se enfrenta a un rostro familiar que pensó que nunca volvería a ver.</info>
 
 
<page>11</page><title>FRACTURA</title>
<microhd>586f47f0b2011e833d753c002232696ece5f5a96</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/paZNRffT3kUckuRFKbeDBuX1YcZ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/bk6k1DV4v73bOjvDdbdtU4XUNHL.jpg</fanart>
<date>2019</date>
<genre>Intriga</genre>
<extra>NA</extra>
<info>Mientras viajan a través del país, Ray (Worthington), su mujer y su hija hacen una parada en una zona de descanso y la niña tropieza, rompiéndose el brazo. Los tres ponen rumbo al hospital y, tras varias horas de trayecto, por fin logran que su hija sea atendida. Agotado, Ray se queda dormido esperando a los resultados sobre la gravedad de las lesiones. Cuando despierta, nadie del hospital recuerda haber visto a su familia, ni existen datos de que alguna vez hayan ingresado en él...</info>
 
 
<page>11</page><title>FRANKENWEENIE</title>
<microhd>3279b0ee3824c7786c31ac5d9b01581933c23b1f</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/owPblpHrKTpzATIOti4Wggw3mhH.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/yfTZ2MSRWc06cBrFOsAF4Lebi4.jpg</fanart>
<date>2012</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Película basada en el cortometraje homónimo que el propio Burton realizó en 1984. El experimento científico que lleva a cabo el pequeño Victor para hacer resucitar su adorado perro Sparky, lo obligará a afrontar terribles situaciones cuyas consecuencias son imprevisibles.</info>
 
 
<page>11</page><title>FREAKS</title>
<microhd>429e03b0141ea2feb1a60a1065e21053103bef20</microhd>
<fullhd>89e51ab280328d385f90ba94a34de9cca64c89da</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/zUYuOVHrxrfaumZIMYt2MMYKAZB.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/mPeyCA2625dLDbPSDjfxpNKL8Uv.jpg</fanart>
<date>2018</date>
<genre>Ciencia ficcion. Thriller</genre>
<extra>NA</extra>
<info>Un padre trastornado (Hirsch) encierra a su hija de 7 años en una casa, advirtiéndola de los peligros que hay fuera. Pero el misterioso Sr. Snowcone (Dern) la convence para que se escape y se una a él en una búsqueda profunda de lazos familiares, libertad y venganza. </info>
 
 
<page>11</page><title>FREE GUY</title>
<microhd>e5011c981400ffce01b1b3e228307bd3579b08f6</microhd>
<fullhd>b576bac5610503b8afb4bb10c7445598673f0cfe</fullhd>
<tresd>NA</tresd>
<cuatrok>725dd76080b8b6c62e1dfcf08f380060b9412e68</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/suaooqn1Mnv60V19MoGxneMupJs.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/zIZYt2DRIzm1qVZGy3xMUqN7HJU.jpg</fanart>
<date>2021</date>
<genre>Fantástico. Comedia. Acción. Aventuras. Ciencia ficción</genre>
<extra>NA</extra>
<info>Guy (Ryan Reynolds) trabaja como cajero de un banco, y es un tipo alegre y solitario al que nada la amarga el día. Incluso si le utilizan como rehén durante un atraco a su banco, él sigue sonriendo como si nada. Pero un día se da cuenta de que Free City no es exactamente la ciudad que él creía. Guy va a descubrir que en realidad es un personaje no jugable dentro de un brutal videojuego.</info>
 
 
<page>11</page><title>FRESH</title>
<microhd>NA</microhd>
<fullhd>c1411dc6d27850176656e3412e89d396d2e2efe0</fullhd>
<tresd>NA</tresd>
<cuatrok>8d14ffdf03e6f187bfc080bd54c4480b04674ebf</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/5Wbzh4RyxPoboS4xllRi7JZVhvx.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/oVv5l6L5Wi5j3gd4P2vt0YvVMOg.jpg</fanart>
<date>2022</date>
<genre>Thriller. Terror. Drama</genre>
<extra>NA</extra>
<info>Thriller sobre los horrores de las citas modernas vistos a través de la desafiante batalla de una joven para sobrevivir a los inusuales apetitos de su nuevo novio.</info>
 
 <page>12</page><title>FROZEN 2</title>
<microhd>338d43ad3edb738eb52c0210e689f21ee7b450ef</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/jnFCk7qGGWop2DgfnJXeKLZFuBq.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/cnA434G2b8m95dp76AKMhM1ufku.jpg</fanart>
<date>2019</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>¿Por qué nació Elsa con poderes mágicos? La respuesta le está llamando y amenaza su reino. Junto con Anna, Kristoff, Olaf y Sven emprenderá un viaje peligroso e inolvidable. En 'Frozen', Elsa temía que sus poderes fueran demasiado para el mundo; ahora deseará que sean suficientes. Secuela de "Frozen. El reino del hielo" (2013), el film de animación más taquillero de la historia del cine, ganador del Oscar a la mejor película animada. Reúne al mismo equipo artístico y técnico de la original.</info>
 
 
<page>12</page><title>FROZEN EL REINO DE HIELO</title>
<microhd>07013b8b1e5f22599cd10e2898fb2e3c34e31576</microhd>
<fullhd>NA</fullhd>
<tresd>2eef96a50636a01448cec5fc29deaeb63355649d</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/8TEsWQbWxMKj8eir8COWfw2HYyC.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/8wljpESP1YTtC0wWRK4eVIPcD3B.jpg</fanart>
<date>2013</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Cuando una profecía condena a un reino a vivir un invierno eterno, la joven Anna, el temerario montañero Kristoff y el reno Sven emprenden un viaje épico en busca de Elsa, hermana de Anna y Reina de las Nieves, para poner fin al gélido hechizo. Adaptación libre del cuento "La reina de las nieves"</info>
 
 
<page>12</page><title>FUGA DE ALCATRAZ</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>5408bd9f250bcce4be481b7f937ada05efa1dd6d</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/qoQUnrxL3vwIMFV1L5tHWR5ZJHH.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/aICJb2UWajq1Vaui7A20qZZsZ0u.jpg</fanart>
<date>1979</date>
<genre>Drama. Acción</genre>
<extra>Mis</extra>
<info>San Francisco, enero de 1960. Frank Lee Morris (Eastwood), un preso muy inteligente que se ha fugado de varias prisiones, es trasladado a Alcatraz, cárcel de máxima seguridad situada en una isla rocosa en medio de la Bahía de San Francisco. A pesar de que nadie ha conseguido nunca evadirse de allí, Frank y otros reclusos empiezan a preparar minuciosamente un plan de fuga. </info>
 
 
<page>12</page><title>GALVESTON</title>
<microhd>110e73f9d605351dafd06dc76c179378f119f902</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/u3hDPc70JEWbrLruk6sni1OcODe.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/iI0xbqvUHHq67fyluqyQXzlrdJQ.jpg</fanart>
<date>2018</date>
<genre>Drama. Thriller</genre>
<extra>NA</extra>
<info>Después de escapar de una emboscada, un sicario enfermo vuelve a su ciudad natal de Galveston donde planea su venganza. Roy Cady es un asesino a sueldo en New Orleans al que le fue diagnosticado cáncer de pulmón a los 40 años. Ante la sospecha de que su jefe quiere eliminarle, Roy trata de huir hacia el horizonte desconocido. En su camino, se encontrará con una joven desamparada que le da la oportunidad de darle un nuevo sentido a su vida... Basada en la novela del creador de 'True Detective'.</info>
 
 
<page>12</page><title>GANGS OF NEW YORK</title>
<microhd>3e54580be60c091c5e66d0340b9483e9d1012cc7</microhd>
<fullhd>2ec278238925513938931f59113e226b55e02198</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/lemqKtcCuAano5aqrzxYiKC8kkn.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/ykqSwE2vUrcocu4w9e99bxLBrXd.jpg</fanart>
<date>2002</date>
<genre>Drama </genre>
<extra>Mis</extra>
<info>Nueva York, 1863. La ciudad está dominada por la corrupción política, y la guerra entre bandas provoca muertos y disturbios. En este contexto, el joven inmigrante irlandés Amsterdam Vallon (Leonardo DiCaprio) quiere vengarse de William Cutting, "Bill el carnicero" (Daniel Day-Lewis), el hombre que mató a su padre (Liam Neeson).</info>
 
 
<page>12</page><title>GARCIA Y GARCIA</title>
<microhd>NA</microhd>
<fullhd>33707D648C69D724F0A31D136EB231FEDC3850A7</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/8V8ctsflyc03T0gN5Sr79rAbIof.jpg</thumbnail>
<fanart>https://i.imgur.com/aGmUXHk.jpg</fanart>
<date>2021</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Hispavia, una aerolínea low cost de poca monta, se encuentra en graves dificultades. Ni sus números cuadran, ni sus aviones vuelan. En un intento desesperado por salvar la compañía, deciden contratar simultáneamente a un prestigioso consultor de aerolíneas y a un experto mecánico en paro. Los dos se llaman Javier García. La casualidad y la desorganización de la empresa harán que sean confundidos e intercambien sus papeles, y mientras el mecánico es atendido por el dueño de la compañía y alojado en hoteles de lujo, el ejecutivo acaba en el hangar, enfundado en un mono grasiento. Perplejos y sin saber qué está sucediendo, ambos se van enfrentando a los cometidos del otro, hasta que los dos Javier García se encuentran y descubren el error.</info>
 
 
<page>12</page><title>GARRA (HUSTLE)</title>
<microhd>1ee9cec28e9843d11d0045e3e13be84347310ce7</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/An8C3wAxdB2Oo3fOG70PZ10PxTN.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/pjRRpuH2NqFmkoegLTAox1qll15.jpg</fanart>
<date>2022</date>
<genre>Drama. Comedia</genre>
<extra>NA</extra>
<info>Sandler interpreta a un cazatalentos de baloncestistas en horas bajas que, estando en el extranjero, descubre un jugador con enorme talento pero con un pasado difícil. Sin la aprobación de su equipo, decide llevarse el fenómeno con él, dándoles a ambos una última oportunidad para demostrar que son dignos de la NBA.</info>
 
 
<page>12</page><title>GATTACA</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>c4c85e5ae77994ed59462e7bbb061b3cc29dcaae</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/gtq8SK3FDzv8heeo87gBHQgBmd.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/ynJwGLbjx9ypfkHoDOMbc0IOIti.jpg</fanart>
<date>1997</date>
<genre>Ciencia ficción. Intriga</genre>
<extra>Culto</extra>
<info>Ambientada en una sociedad futura, en la que la mayor parte de los niños son concebidos in vitro y con técnicas de selección genética. Vincent (Ethan Hawke), uno de los últimos niños concebidos de modo natural, nace con una deficiencia cardíaca y no le auguran más de treinta años de vida. Se le considera un inválido y, como tal, está condenado a realizar los trabajos más desagradables. Su hermano Anton, en cambio, ha recibido una espléndida herencia genética que le garantiza múltiples oportunidades. Desde niño, Vincent sueña con viajar al espacio, pero sabe muy bien que nunca será seleccionado. Durante años ejerce toda clase de trabajos hasta que un día conoce a un hombre que le proporciona la clave para formar parte de la élite: suplantar a Jerome (Jude Law), un deportista que se quedó paralítico por culpa de un accidente. De este modo, Vincent ingresa en la Corporación Gattaca, una industria aeroespacial, que lo selecciona para realizar una misión en Titán. Todo irá bien, gracias a la ayuda de Jerome, hasta que el director del proyecto es asesinado y la consiguiente investigación pone en peligro los planes de Vincent.</info>
 
 
<page>12</page><title>GEMINIS</title>
<microhd>aab93fc890fc4a544cb5cac348121580707765db</microhd>
<fullhd>b35e3e35a0b52a1dbe26d43ddad5469b6dae3e5a</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/8QmJs80FDNQ9dWzWdEAStZnadjn.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/tbCI2Sw6peDm1S22fLQUlhd4rJY.jpg</fanart>
<date>2019</date>
<genre>Ciencia ficcion. Accion. Thriller. Drama</genre>
<extra>NA</extra>
<info>Henry Brogan, un asesino a sueldo ya demasiado mayor para seguir con su duro trabajo, decide retirarse. Pero esto no le va a resultar tan fácil, pues tendrá que enfrentarse a un clon suyo, mucho más joven.</info>
 
 
<page>12</page><title>GHOST</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>d71b7f791743d0f9f943ed97ccc535e984803e77</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/eapsL757qbyT1c2ZQDyJ1oxaqdt.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/zOyXP0MS4jvU01yIxzRV394ZUJw.jpg</fanart>
<date>1990</date>
<genre>Romance. Drama. Fantástico</genre>
<extra>Mis</extra>
<info>Una pareja de enamorados ve truncada su felicidad cuando él es asesinado por un ladrón. La necesidad de salvar la vida a la chica hace que él permanezca en la Tierra en forma de fantasma e intente advertirla del peligro que corre. Su único medio de comunicación es una alocada vidente.</info>
 
 
<page>12</page><title>GHOST IN THE SHELL</title>
<microhd>b260d79649459e7c6aa2560b474f026607ba185d</microhd>
<fullhd>NA</fullhd>
<tresd>7bd2cc703ebae3f7895679ac6bbfaac25367031b</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/F8Wb1Qi5IiFEpTsnSnKYEfX0Ki.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/cDOv649oRCxLxsrMtboqSFOb6aB.jpg</fanart>
<date>2017</date>
<genre>Ciencia ficcion. Accion</genre>
<extra>NA</extra>
<info>En un Japón futurista la joven Motoko Kusanagi (Scarlett Johansson), también conocida como 'the Major' Mira Killian, es la líder de grupo operativo de élite, Sección 9, cuyo objetivo es luchar contra el ciberterrorismo y los crímenes tecnológicos. Al mando de esta unidad de operaciones encubiertas está Aramaki (Takeshi Kitano), y destaca Batou (Pilou Asbæk), un exmilitar considerado como uno de los agentes más salvajes del grupo. Pero, después de un peligrosa misión, el cuerpo de Kusanagi queda dañado, siendo sometida a una operación quirúrgica para trasplantar su cerebro en un cuerpo robótico. Este nuevo cuerpo artificial le permitirá ser capaz de realizar hazañas sobrehumanas especialmente requeridas para su trabajo... Basada en la aclamada saga homónima de ciencia ficción.</info>
 
 
<page>12</page><title>GHOSTING</title>
<microhd>11a7e257d74fe11d9824aaa21067e2c754209dde</microhd>
<fullhd>0735170c6251b1e33e1cca1836a5671ac32e8a50</fullhd>
<tresd>NA</tresd>
<cuatrok>8a3ede65a3598c17940dcbb5b09258292eb00310</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/zSw2JeQ03GivcS4VKJmWK5sYi1F.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/b9UCfDzwiWw7mIFsIQR9ZJUeh7q.jpg</fanart>
<date>2023</date>
<genre>Acción. Romance</genre>
<extra>NA</extra>
<info>Cole, un tipo campechano, se enamora perdidamente de la emigmática Sadie, quien, para su enorme sorpresa, resulta ser una agente secreta. Antes de que pueda surgir una segunda cita, los dos deben embarcarse en una aventura internacional para salvar el mundo.</info>
 
 
<page>12</page><title>GHOSTLAND</title>
<microhd>6702e322734fc92a166bc0e370e4228ce8aeba2d</microhd>
<fullhd>ccd27cde8a5560d2e1ada3664e0fcb5c35888573</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ma9gY6OikBev8MsMaeA6h2EIdGo.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/gl3RKxaM65gyrvRoG3MffTYBoo5.jpg</fanart>
<date>2018</date>
<genre>Terror</genre>
<extra>NA</extra>
<info>Una madre y sus dos hijas heredan una casa. Pero en su primera noche, aparecen unos asesinos y la madre se ve obligada a luchar para salvar a sus hijas.</info>
 
 
<page>12</page><title>GLADIATOR</title>
<microhd>ad981c08bed0de6811516707a887d1e7291af2bc</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>1fc5943f03d94071a2ebe806e6f245abcd08e52f</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/w4bXRLYNpuAU9XiwC7NN36GOYtO.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/u1CC2m4owXbAbQfGzXNMcbnUv6G.jpg</fanart>
<date>2000</date>
<genre>Acción. Aventuras. Drama </genre>
<extra>Mis</extra>
<info>En el año 180, el Imperio Romano domina todo el mundo conocido. Tras una gran victoria sobre los bárbaros del norte, el anciano emperador Marco Aurelio (Richard Harris) decide transferir el poder a Máximo (Russell Crowe), bravo general de sus ejércitos y hombre de inquebrantable lealtad al imperio. Pero su hijo Cómodo (Joaquin Phoenix), que aspiraba al trono, no lo acepta y trata de asesinar a Máximo.</info>
 
 
<page>12</page><title>GLASS</title>
<microhd>7a701a1edb067ab0d67f25d7e7abff9613a2ecb2</microhd>
<fullhd>a2cf7d0e1d8c4bbc0d74d2bb9b8485ea279b4669</fullhd>
<tresd>NA</tresd>
<cuatrok>b61edd472c23abce0811b0b0bb64a25cc9e9758e</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/giUuSG5fWmAOWqyY8bPm57cGkpX.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/lvjscO8wmpEbIfOEZi92Je8Ktlg.jpg</fanart>
<date>2019</date>
<genre>Intriga. Thriller</genre>
<extra>NA</extra>
<info>Continuando desde donde lo dejó "Múltiple", "Glass" sigue los pasos de David Dunn (Bruce Willis) en su búsqueda de la figura superhumana de "La Bestia". En la sombra, Elijah Price (Samuel L. Jackson) parece emerger como una figura clave que conoce los secretos de ambos. Secuela de "El protegido" y "Múltiple", de M. Night Shyamalan.</info>
 
 
<page>12</page><title>GODZILLA VS. KONG</title>
<microhd>97cc3716d581effe80d8e2c9134c23dd790c4ad8</microhd>
<fullhd>41ee5fc2d492605aea0287b8e1440d79b6dfcfdb</fullhd>
<tresd>NA</tresd>
<cuatrok>9244074b64fc399695310c908ce40c3f74210860</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/yJTk4eqQd9Yo5REpFbTSOMkbSgn.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/pX0xvPSj6GUBRpf0thzZSZsekQK.jpg</fanart>
<date>2021</date>
<genre>Acción. Fantástico. Ciencia ficción </genre>
<extra>NA</extra>
<info>Godzilla y Kong, dos de las fuerzas más poderosas de un planeta habitado por todo tipo de aterradoras criaturas, se enfrentan en un espectacular combate que sacude los cimientos de la humanidad. Kong y sus protectores emprenderán un peligroso viaje para encontrar su verdadero hogar. Con ellos está Jia, una joven huérfana con la que el gigante tiene un vínculo único y poderoso. En el camino se cruzan inesperadamente con el de un Godzilla enfurecido que va causando destrucción a su paso por el mundo. El choque épico entre los dos titanes -provocado por fuerzas invisibles- es solo el comienzo del misterio que se esconde en las profundidades del núcleo de la Tierra.</info>
 
 
<page>12</page><title>GOLD</title>
<microhd>1b2232fdde3cfadb823cc1b5fa8b9f053fed6ad0</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>0158ddb42647481474cee462bd4199b2d15d8250</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/c0r75XaNHA4QMlDcitEscQW2Fn6.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/5Ou7l7iA3Ud2DzCmwU3OgqU8jIx.jpg</fanart>
<date>2022</date>
<genre>Thriller. Drama </genre>
<extra>NA</extra>
<info>Cuando dos hombres que viajan por el desierto descubren el pedrusco de oro más grande que han visto en su vida, empiezan a soñar con la riqueza que les traerá, pero la avaricia toma las riendas. Tienen que extraer el oro de la tierra y para ello trazan un plan en el que uno de ellos tiene que ir a por el equipo necesario y el otro se tiene que quedar allí solo a su suerte. </info>
 
 
<page>12</page><title>GORKY PARK</title>
<microhd>NA</microhd>
<fullhd>9f3e8cd2af15e5f3b0e84f5163eb9022170d5e93</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/elFLQIzkbgLZgAykmqPvhYzQJqy.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/dI7U0PNBOcCypF2VXiyqz6fEwte.jpg</fanart>
<date>1983</date>
<genre>Intriga</genre>
<extra>Mis</extra>
<info>Tres cadáveres desfigurados aparecen en el parque Gorky, en el centro de Moscú. El detective de policía Arkady Renko se hace cargo de la investigación, pero tropieza con ciertas trabas burocráticas. Por otra parte, el estado de los cuerpos, que presentan numerosas mutilaciones, impide que sean identificados. A medida que la investigación avanza, Renko empieza a sospechar que ciertos miembros de las altas esferas políticas están relacionados con estos asesinatos. Una historia de intriga, espionaje y romance durante la época de la Guerra Fría. </info>
 
 
<page>12</page><title>GORRION ROJO</title>
<microhd>3fe4ea4290d9b5276d0e957ab2854d0b159a11bc</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/bitgVjyiz4FszOvgQJ9ORHlRhA8.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/sGzuQYSTwJvLBc2PnuSVLHhuFeh.jpg</fanart>
<date>2018</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>Dominika Egorova (Jennifer Lawrence) es reclutada contra su voluntad para ser un “gorrión”, una seductora adiestrada del servicio de seguridad ruso. Dominika aprende a utilizar su cuerpo como arma, pero lucha por conservar su sentido de la identidad durante el deshumanizador proceso de entrenamiento. Hallando su fuerza en un sistema injusto, se revela como uno de los activos más sólidos del programa. Su primer objetivo es Nate Nash (Joel Edgerton), un funcionario de la CIA que dirige la infiltración más confidencial de la agencia en la inteligencia rusa. Los dos jóvenes agentes caen en una espiral de atrAccion y engaño que amenaza sus carreras, sus lealtades y la seguridad de sus respectivos países.</info>
 
 
<page>12</page><title>GRAN TORINO</title>
<microhd>f59eef5b14e4d207b2b74cb2328f48ba16865579</microhd>
<fullhd>7c3b6d2686076a8c92332546c551f458226e9a62</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/7w54KRHdRokgXqxL59GKVau8e6l.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/sP4vNXFhPviUJevrD7X8fJV1OD1.jpg</fanart>
<date>2008</date>
<genre>Drama</genre>
<extra>Mis</extra>
<info>Walt Kowalski (Clint Eastwood), un veterano de la guerra de Corea (1950-1953), es un obrero jubilado del sector del automóvil que ha enviudado recientemente. Su máxima pasión es cuidar de su más preciado tesoro: un coche Gran Torino de 1972. Es un hombre inflexible y cascarrabias, al que le cuesta trabajo asimilar los cambios que se producen a su alrededor, especialmente la llegada de multitud de inmigrantes asiáticos a su barrio. Sin embargo, las circustancias harán que se vea obligado a replantearse sus ideas</info>
 
 
<page>12</page><title>GRAN TURISMO</title>
<microhd>NA</microhd>
<fullhd>a7231c64fae7a095fdd9448a95377f621ff22d18</fullhd>
<tresd>NA</tresd>
<cuatrok>453341dccfd669c5444015a9f991ba531c93cefa</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/jlaJlnxr6LkTmeHsrcbnai5tn6l.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/3wtifTCVwqSyXF924FLkRpVRjYw.jpg</fanart>
<date>2023</date>
<genre>Acción. Drama</genre>
<extra>Estreno</extra>
<info>Basada en una historia real, la película cuenta cómo cumplió su sueño un adolescente que jugaba a 'Gran Turismo', videojuego en el que ganó una serie de competiciones patrocinadas por Nissan, y que le sirvió de trampolín para acabar convirtiéndose en un piloto de carreras profesional.</info>
 
 
<page>12</page><title>GRANUJAS A TODO RITMO (THE BLUES BROTHERS)</title>
<microhd>39e3f592cef29d5672a800d88bbc87e9394a8930</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/TtkkCYdchVH162OdWenl3SnxZp.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/eIHRJK4Pz0RWO6T8h18mBAS3qvT.jpg</fanart>
<date>1980</date>
<genre>Comedia. Musical</genre>
<extra>Culto</extra>
<info>Tras pasar años en la cárcel por robo a mano armada, Jake Blues es puesto en libertad por buen comportamiento. Sale de prisión vestido de la misma forma en la que entró: traje y sombrero negro y gafas oscuras. Vestido de idéntica manera, lo espera en la puerta de la prisión su hermano Elwood, que lo informa de que el orfanato de Santa Elena, el único hogar que conocieron, corre el peligro de desaparecer por razones económicas. Sin dudarlo ni un momento los hermanos se ponen manos a la obra. </info>
 
 
<page>12</page><title>GREASE</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>4aca7a3240761549e30f46adb84f07aba366e736</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/9L4p8N1nQmAdAAfiaWlLuYs7dZt.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/koSWhCzMJfJOramu4KD0ZZifqpi.jpg</fanart>
<date>1978</date>
<genre>Musical. Romance. Comedia</genre>
<extra>Culto</extra>
<info>Verano de 1959. Sandy (Olivia Newton John) y Danny (John Travolta) han pasado un romántico y maravilloso verano juntos, pero, cuando las vacaciones se acaban, sus caminos se separan. Inesperadamente, vuelven a verse en el instituto Rydell, pero la actitud de Danny ya no es la misma: ya no es el chico encantador y atento que encandiló a Sandy; ahora es engreído e insensible.</info>
 
 
<page>12</page><title>GREEN BOOK</title>
<microhd>d96e41946f20c2400e3cb06b8660d074cf79430d</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/od2A7qPtpimcYfqfKXkpHqoKyuS.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/205t5YwbjdcLtxfsds09ZifIlFZ.jpg</fanart>
<date>2018</date>
<genre>Drama. Comedia</genre>
<extra>NA</extra>
<info>Año 1962. Tony Lip (Viggo Mortensen) es un rudo italoamericano del Bronx que es contratado como chófer del virtuoso pianista negro Don Shirley (Mahershala Ali). Ambos emprenderán un viaje para una gira de conciertos por el Sur de Estados Unidos, donde Tony deberá tener presente "El libro verde", una guía que indicaba los pocos establecimientos donde se aceptaba a los afroamericanos. Son dos personas que tendrán que hacer frente al racismo y los prejuicios, pero a las que el destino unirá, obligándoles a dejar de lado las diferencias para sobrevivir y prosperar en el viaje de sus vidas.</info>
 
 
<page>12</page><title>GREENLAND: EL ULTIMO REFUGIO</title>
<microhd>f8a0133f1d2d352d397657fbcd6fcc625076d22d</microhd>
<fullhd>280fe38d2e1c8ae6d28843687c149abbbf64ac91</fullhd>
<tresd>NA</tresd>
<cuatrok>968b88d79d298a5f0f75587aac552068ae24da2b</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/9iFkwShRf5M29yLABAdsEZyr9w6.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/2Fk3AB8E9dYIBc2ywJkxk8BTyhc.jpg</fanart>
<date>2020</date>
<genre>Thriller. Acción</genre>
<extra>NA</extra>
<info>Cuando el mundo es consciente de que el asteroide más grande de la historia va a impactar en la Tierra y aniquilar todo rastro de vida, los gobiernos de todo el mundo realizan un sorteo en el cual los afortunados podrán sobrevivir en refugios secretos. Esta decisión desata un caos a nivel mundial. Muchos tendrán que emprender un peligroso viaje donde se enfrentarán a los más imponentes peligros de la naturaleza, lo que les obligará a encontrar la manera de mantenerse unidos mientras encuentran la forma de sobrevivir.</info>
 
 
<page>12</page><title>GRETEL Y HANSEL</title>
<microhd>a5d8e0044ed54594c62b29978855cb3bb6d71ebb</microhd>
<fullhd>c5a9c20f70771940e93d357f3830508fbada7d0f</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/4LB4oRyWWwv08C2xxmbx1wTj7CU.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/jEPEVO48hKQB0EUNFQOSv6qtKNW.jpg</fanart>
<date>2020</date>
<genre>Terror. Fantástico</genre>
<extra>NA</extra>
<info>En Baviera, a principios del siglo XIV, Gretel y Hansel, de 13 y 9 años respectivamente, viven en la miseria más absoluta. Su padre murió hace años y su madre está casada ahora con un hombre malvado. Debido a la falta de recursos y al creciente miedo que les produce su padrastro, Gretel y Hansel deciden huir del pueblo en busca de un futuro mejor. En el bosque se encuentran con varias personas que, de una forma u otra, intentan aprovecharse de ellos. Huyendo de unos y otros, conocen a un amigable cazador, que por fin les indica cual es el camino seguro a seguir. Así, los dos hermanos llegan a la cabaña de Holda, una amable mujer mayor que decide acogerlos. Lo que Gretel y Hansel no pueden imaginarse es que en esa cabaña tendrán que enfrentarse a sus peores miedos si quieren sobrevivir.</info>
 
 
<page>12</page><title>GREYHOUND: ENEMIGOS BAJO EL MAR</title>
<microhd>e1c2b7a043dd90e9aa2861b7e098b38d7a441977</microhd>
<fullhd>aaadcfb76d69825c77da5eea002954122fe6b51a</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/qHmGs4uAIOdPgseCgOH0eEsmTKD.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/xXBnM6uSTk6qqCf0SRZKXcga9Ba.jpg</fanart>
<date>2020</date>
<genre>Bélico</genre>
<extra>NA</extra>
<info>Año 1942. Durante los primeros días de la participación de Estados Unidos en la Segunda Guerra Mundial, un convoy internacional de 37 barcos aliados, encabezado por el comandante Ernest Krause, cruza el Atlántico Norte mientras es perseguido por submarinos alemanes.</info>
 
 
<page>12</page><title>GRITA LIBERTAD</title>
<microhd>NA</microhd>
<fullhd>e70df9247b4c9e1c146266cc43b50d28321ea357</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/epubeiqjYJsPQP3WipxzcNjd57r.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/A7KOU9BGrRRjkaBuVXAXzPYay4Q.jpg</fanart>
<date>1987</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Donald Woods (Kevin Kline) es un periodista blanco y liberal que empieza a seguir las actividades de Stephen Biko (Denzel Washington), un activista negro antiapartheid, en un momento clave de la historia del país. Donald conoce a Stephen y se hacen amigos hasta que éste es brutalmente asesinado en 1977, a manos de la policía. Afligido por la muerte de su amigo, Donald decide dar a conocer la verdad sobre su vida y su muerte. Sin embargo, tanto él como su familia deberán pagar un precio muy alto que significó abandonar todo aquello que amaban.</info>
 
 
<page>12</page><title>GUARDIANES DE LA GALAXIA VOL. 3</title>
<microhd>NA</microhd>
<fullhd>4ktwxlt5ix54g5cnfpjuio7edksrp5wc</fullhd>
<tresd>NA</tresd>
<cuatrok>akealmlhtcdkkienx5ab5vqyv4pdijo4</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/6GkKzdNosVAL7UGgwTtCHSxLQ67.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/hoVj2lYW3i7oMd1o7bPQRZd1lk1.jpg</fanart>
<date>2023</date>
<genre>Ciencia ficción. Aventuras. Acción. Comedia. Drama</genre>
<extra>NA</extra>
<info>La querida banda de los Guardianes se instala en Knowhere. Pero sus vidas no tardan en verse alteradas por los ecos del turbulento pasado de Rocket. Peter Quill, aún conmocionado por la pérdida de Gamora, debe reunir a su equipo en una peligrosa misión para salvar la vida de Rocket, una misión que, si no se completa con éxito, podría muy posiblemente conducir al final de los Guardianes tal y como los conocemos. </info>
 
 
<page>12</page><title>GUARDIANES DE LA GALAXIA: ESPECIAL FELICES FIESTAS</title>
<microhd>NA</microhd>
<fullhd>6f07c9a88eb689bb31f31c754366125717e27b2d</fullhd>
<tresd>NA</tresd>
<cuatrok>ccf836e2114ba3a4a32eac38fb02e807d8075915</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/wVrfgf4rHU0dHf5iWmtBH9MibB.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/rfnmMYuZ6EKOBvQLp2wqP21v7sI.jpg</fanart>
<date>2022</date>
<genre>Acción. Aventuras. Comedia. Ciencia ficción</genre>
<extra>NA</extra>
<info>Antes de que Star-Lord, Gamora, Drax, Rocket, Mantis y Groot lleguen a la pantalla grande con Guardianes de la Galaxia Vol. 3 en 2023, mira a los Guardianes participar en algunas travesuras enérgicas en un especial original completamente nuevo, creado para Disney+, durante la temporada navideña de 2022.</info>
 
 
<page>12</page><title>GUERRA MUNDIAL Z</title>
<microhd>10cc85d9a0821517be364ed62e01959c8e7c0706</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>dc710abcfbc01548592e1d13c956d6b88aa00b0c</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/rQxQh36ZWtMmPveVxmSRa6qUMLh.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/upsk7nfUjf8ZSYuokwa5U5YXERm.jpg</fanart>
<date>2013</date>
<genre>Acción. Ciencia ficción. Thriller. Terror</genre>
<extra>Mis</extra>
<info>Cuando el mundo comienza a ser invadido por una legión de muertos vivientes, Gerry Lane (Brad Pitt), un experto investigador de las Naciones Unidas, intentará evitar el fin de la civilización en una carrera contra el tiempo y el destino. La destrucción a la que se ve sometida la raza humana lo lleva a recorrer el mundo entero buscando la solución para frenar esa horrible epidemia.</info>
 
 
<page>12</page><title>GUNPOWDER MILKSHAKE (COCTEL EXPLOSIVO)</title>
<microhd>bcaa6d1d8572315ebd5d1adbfedb168ebfa12d18</microhd>
<fullhd>413fa63452d4694c17f2ec1eab221a0325bdd297</fullhd>
<tresd>NA</tresd>
<cuatrok>294034f4b2677b07dbbdc55208d600b79c6a2dbe</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/54rcNkxmG3EEBU5GuI3NL0W6gz.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/1GVwnspfEEMOHkSWRcibipPT0Pc.jpg</fanart>
<date>2021</date>
<genre>Acción. Thriller</genre>
<extra>NA</extra>
<info>Eva (Karen Gillan) era lo más importante en la turbulenta vida de su madre, Scarlet (Lena Headey). Pero sus enemigos obligaron a Scarlet a huir abandonándolo todo, incluida a su hija. Años más tarde, Eva se convierte en una asesina a sangre fría siguiendo los pasos de su madre. Después de perder el control en una misión, poniendo a una inocente niña de 8 años en peligro, Eva no tiene más remedio que enfrentarse sin escrúpulos a sus antiguos compinches. Y Scarlet y su banda no tienen más remedio que volver para intentar ayudarla.</info>
 
 
<page>12</page><title>GUNS AKIMBO</title>
<microhd>5cab82c0cb540b048ae04df4c04bbac5ce86d992</microhd>
<fullhd>b95376794d2724edd06e6b696716852e0a10dca8</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/luZH1jlEYtEaRUmDvc88mV6PlGO.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/xBHTHkNL1Urhc8K8NHzIih7unjs.jpg</fanart>
<date>2019</date>
<genre>Accion. Comedia</genre>
<extra>NA</extra>
<info>Miles (Daniel Radcliffe) se siente atascado en la vida: su trabajo no tiene futuro y sigue enamorado de su exnovia Nova. Un día descubre que un grupo de mafiosos llamado "Skizm" planea celebrar una peligrosa competición que reúne a extraños de distintos puntos de la ciudad con el propósito de comprobar cuál de ellos logra una mayor cantidad de espectadores online. Aunque al principio tiene dudas, pronto descubre que Nora ha sido secuestrada por Nix, un grupo armado que participa en el concurso, por lo que Miles decide dejar atrás sus miedos para participar en el torneo.</info>
 
 
<page>12</page><title>HA NACIDO UNA ESTRELLA</title>
<microhd>07b30421877392be620d7b1a60f057e61413f779</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/sNQyj8mLTMY4fsvr7qvuIiNFeaG.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/840rbblaLc4SVxm8gF3DNdJ0YAE.jpg</fanart>
<date>2018</date>
<genre>Drama. Romance. Musical</genre>
<extra>NA</extra>
<info>Jackson Maine (Bradley Cooper) es una estrella consagrada de la música que una noche conoce y se enamora de Ally (Lady Gaga), una joven artista que lucha por salir adelante en el mundo del espectáculo. Justo cuando Ally está a punto de abandonar su sueño de convertirse en cantante, Jack decide ayudarla en su carrera hacia la fama. Pero el camino será más duro de lo que imagina.</info>
 
 
<page>12</page><title>HACIA LA LIBERTAD</title>
<microhd>NA</microhd>
<fullhd>28141b45f03d761a53bdeedf3dd26c1dcbefbf83</fullhd>
<tresd>NA</tresd>
<cuatrok>03dd5b4acb20c39335e6809ee5ff3d129b191e63</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/zQe7paVC4tSwb8vBwxSqajJ22Ue.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/9GcSC43iof4nIdxoVIo2Xzd0nxM.jpg</fanart>
<date>2022</date>
<genre>Aventuras. Acción. Drama. Bélico. Thriller</genre>
<extra>NA</extra>
<info>Inspirada en la conmovedora historia real de un hombre dispuesto a cualquier cosa por su familia, y por la libertad. Cuando Peter, un hombre esclavizado, arriesga su vida por escapar y regresar con su familia, se embarca en una peligrosa travesía de amor y resiliencia.</info>
 
 
<page>12</page><title>HALLOWEEN KILLS</title>
<microhd>7be181cdfc13d615f984bc6a39ece24418ae50d3</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<thumbnail>https://www.themoviedb.org/t/p/original/qEzLir8LN2nDSmvEbx9p3GOg6VF.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/tDYwYktjFmpj92S2Qn4T3BcRgL.jpg</fanart>
<date>2021</date>
<genre>Terror. Thriller</genre>
<extra>NA</extra>
<info>La noche de Halloween en la que Michael Myers regresa no ha acabado todavía. Minutos después de que Laurie Strode, su hija Karen y su nieta Allyson dejen encerrado y ardiendo a este monstruo enmascarado, Laurie se dirige rápidamente al hospital para tratar sus heridas, creyendo que todo ha terminado. Pero cuando Michael consigue liberarse, su ritual sangriento continúa. Mientras Laurie lidia con su dolor y se prepara para defenderse, sirve de inspiración para que todo Haddonfield se levante contra este monstruo imparable. </info>
 
 
<page>12</page><title>HALLOWEEN: EL FINAL</title>
<microhd>NA</microhd>
<fullhd>1c715aca77cabab81a4539ccdf8322ab3c0813d5</fullhd>
<tresd>NA</tresd>
<cuatrok>5a233a06751a370917a199f290548b5626569d74</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/licjj6TbWOaT2KhtK66zRhMLRt.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/bvTmwkF1rAVncrQz1zIASiVNdzm.jpg</fanart>
<date>2022</date>
<genre>Terror. Thriller. Saga</genre>
<extra>NA</extra>
<info>Cuatro años después de los acontecimientos de Halloween Kills, Laurie vive con su nieta Allyson (Andi Matichak) y está a punto de terminar de escribir sus memorias. Nadie ha vuelto a ver a Michael Myers desde entonces. Laurie, después de permitir que el espectro de Myers controlara su realidad durante décadas, ha decidido por fin dejar atrás el miedo y la rabia para dedicarse a vivir. Pero cuando acusan a Corey Cunningham (Rohan Campbell) de matar al niño al que cuidaba, se desencadena una cascada de violencia que obligará a Laurie a enfrentarse de una vez por todas con una maldad que no puede controlar. </info>
 
 
<page>12</page><title>HARRY EL SUCIO</title>
<microhd>a911242e73578cf3eaa9a948c0e021fea1bfb1dc</microhd>
<fullhd>e822aae0d78077ae7cb37c4b5ffa88eead478490</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ojkaEfFJ77cVPcQ4Hk8Bl0CrFfB.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/5kex1QQCRjNFKbsaFLaX4j3IDKa.jpg</fanart>
<date>1971</date>
<genre>Thriller. Acción</genre>
<extra>Mis</extra>
<info>Harry Callahan (Clint Eastwood) es un duro policía que se ha criado en la calles de San Francisco. Sus compañeros le llaman Harry el Sucio por sus particulares métodos de lucha contra el crimen y porque siempre se encarga de los trabajos más desagradables. Cuando un francotirador que se hace llamar 'Scorpio' asesina a una mujer desde una azotea y promete matar a más personas si la ciudad no le paga 100.000 dólares, el inspector Callahan será el encargado de intentar resolver el caso. </info>
 
 
<page>12</page><title>HARRY POTTER : ANIMALES FANTASTICOS LOS CRIMENES DE GRINDELWALD</title>
<microhd>NA</microhd>
<fullhd>83e1a34298ad3837cbc852e3d25e9ecc090cc2a6</fullhd>
<tresd>NA</tresd>
<cuatrok>a9e312efbef6495c948a6866449a2a7f70545ea1</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/zs6LFuE4aB1I8crKjAhlPVTHAOS.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/wDN3FIcQQ1HI7mz1OOKYHSQtaiE.jpg</fanart>
<date>2018</date>
<genre>Infantil.Fantastico. Aventuras.Saga</genre>
<extra>NA</extra>
<info>Cumpliendo con su amenaza, Grindelwald escapa de su custodia y ha comenzado a reunir seguidores, la mayoría de los cuales no sospechan sus verdaderas intenciones: alzar a los magos purasangre para reinar sobre todas las criaturas no mágicas. En un esfuerzo por frustrar los planes de Grindelwald, Albus Dumbledore (Jude Law) recluta a su antiguo estudiante Newt Scamander, quien accede a prestar su ayuda, sin conocer los peligros que aguardan. Las líneas quedan marcadas mientras el amor y la lealtad son puestos a prueba, incluso entre los amigos más cercanos y la familia, en un mundo mágico cada vez más dividido.</info>
 
 
<page>12</page><title>HARRY POTTER : ANIMALES FANTASTICOS: LOS SECRETOS DE DUMBLEDORE</title>
<microhd>NA</microhd>
<fullhd>9150766af2d131400c85a3c2ff32b3a8306e44f9</fullhd>
<tresd>NA</tresd>
<cuatrok>3f6c63fee1f13a455eee8a3b87af0267d9f15f7b</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/yOeuJdwag4bAlnvgrdweRoiuXGC.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/mZ3dl55cY8xtbzX06MOh31pV84Y.jpg</fanart>
<date>2022</date>
<genre>Fantástico. Aventuras. Saga</genre>
<extra>NA</extra>
<info>El profesor Albus Dumbledore (Jude Law) sabe que el poderoso mago oscuro Gellert Grindelwald (Mads Mikkelsen) está haciendo planes para apoderarse del mundo mágico. Incapaz de detenerlo él solo, confía en el Magizoólogo Newt Scamander (Eddie Redmayne) para dirigir un intrépido equipo de magos, brujas y un valiente panadero Muggle en una misión peligrosa, donde se encuentran con antiguos y nuevos animales y se enfrentan a una legión cada vez más numerosa de seguidores de Grindelwald. Hay mucho en juego así que nos preguntamos hasta cuándo podrá permanecer Dumbledore al margen. </info>
 
 
<page>12</page><title>HARRY POTTER : Y EL CALIZ DE FUEGO</title>
<microhd>a42bc2e1c32b616c4cbfeecb137a11536f309391</microhd>
<fullhd>616f0d0cad6a9ff2ca1488df2d0f36c1ec7377f8</fullhd>
<tresd>NA</tresd>
<cuatrok>03d538a4432e4e6e571300e6633f2a0a12a58d55</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/6Cn5Lx9kqhXzTNV5QXwZ3RW5pBg.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/8f9dnOtpArDrOMEylpSN9Sc6fuz.jpg</fanart>
<date>2005</date>
<genre>Fantastico. Aventuras. Drama .Saga</genre>
<extra>NA</extra>
<info>La cuarta parte de la serie del niño mago comienza con la Copa Internacional de Quidditch. Cuenta también el inicio de la atracción por Cho Chang y otro año de magia, en el que una gran sorpresa obligará a Harry a enfrentarse a muchos desafíos temibles. También habrá un torneo de magia para tres escuelas, y el temido regreso de "Aquel-que-no-debe-ser-nombrado".</info>
 
 
<page>12</page><title>HARRY POTTER: ANIMALES FANTASTICOS Y DONDE ENCONTRARLOS</title>
<microhd>a42bc2e1c32b616c4cbfeecb137a11536f309391</microhd>
<fullhd>77505a7d4f9ef577b85b4ffa3d9a0789a40d4de5</fullhd>
<tresd>949fa3d7136e2a34e0a4ce35e24abe49e6cc0a2e</tresd>
<cuatrok>9561384f81b5c73393311cc679929a34cb5add80</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/w22GrhGKpi5jdAmE6tbWCHApiG4.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/pLgSLx9dn2YYbwVk8p0F28KIU8C.jpg</fanart>
<date>2016</date>
<genre>Fantastico. Aventuras.Saga</genre>
<extra>NA</extra>
<info>Año 1926. Newt Scamander acaba de completar un viaje por todo el mundo para encontrar y documentar una extraordinaria selección de criaturas mágicas. Llegando a Nueva York para hacer una breve parada en su camino, donde podría haber llegado y salido sin incidentes…pero no para un Muggle llamado Jacob, un caso perdido de magia, y la fuga de algunas criaturas fantásticas de Newt, que podrían causar problemas el mundo mágico y en el mundo Muggle.</info>
 
 
<page>12</page><title>HARRY POTTER: Y EL MISTERIO DEL PRINCIPE</title>
<microhd>a42bc2e1c32b616c4cbfeecb137a11536f309391</microhd>
<fullhd>ef5338c1496304c2825e4cc7f78f295c889a208d</fullhd>
<tresd>NA</tresd>
<cuatrok>195739dcb7e695608f0d96748f9aca58094bf213</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/nSb3nNiL6pOEDK34T0U6RKTGzlJ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/kRyojLYtWPsBKfDhphhhl1FdM5a.jpg</fanart>
<date>2009</date>
<genre>Fantastico. Aventuras. Drama .Saga</genre>
<extra>NA</extra>
<info>Con dieciséis años cumplidos, Harry inicia el sexto curso en Hogwarts en medio de terribles acontecimientos que asolan Inglaterra. Elegido capitán del equipo de Quidditch, los entrenamientos, los exámenes y las chicas ocupan todo su tiempo, pero la tranquilidad dura poco. A pesar de los férreos controles de seguridad que protegen la escuela, dos alumnos son brutalmente atacados. Dumbledore sabe que se acerca el momento, anunciado por la Profecía, en que Harry y Voldemort se enfrentarán a muerte. El anciano director pedirá ayuda a Harry y juntos emprenderán un peligroso viaje. Para debilitar al enemigo, el joven mago cuenta con la ayuda de un viejo libro de pociones perteneciente a un misterioso personaje que se hace llamar el Príncipe Mestizo.</info>
 
 
<page>12</page><title>HARRY POTTER: Y EL PRISIONERO DE AZKABAN</title>
<microhd>a42bc2e1c32b616c4cbfeecb137a11536f309391</microhd>
<fullhd>aba62329b3671435eef4c2e68e39bbe72183a9b3</fullhd>
<tresd>NA</tresd>
<cuatrok>8765a59c4a4fa088c931bc1dfd5aa2334c8018ef</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/wF9aoo4YZmpKP4bZPSy4Zwwek6G.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ve4TEHHmJdnEKVWSYlRa3DdoeKz.jpg</fanart>
<date>2004</date>
<genre>Fantastico. Aventuras. Drama.Saga</genre>
<extra>NA</extra>
<info>Cuando Harry Potter y sus amigos vuelven a Hogwarts para cursar su tercer año de estudios, se ven involucrados en un misterio: de la prisión para magos de Azkaban se ha fugado Sirius Black, un peligroso mago que fue cómplice de Lord Voldemort y que intentará vengarse de Harry Potter. El joven aprendiz de mago contribuyó en gran medida a la condena de Sirius, por lo que hay razones para temer por su vida</info>
 
 
<page>12</page><title>HARRY POTTER: Y LA CAMARA SECRETA</title>
<microhd>a42bc2e1c32b616c4cbfeecb137a11536f309391</microhd>
<fullhd>4e0b0c81f15c50e592657fb37b822786ec12b6c3</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/bhCfAzeMMg7GyLDT11yVM2i1NPh.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/bvRnPaai6JL7XHF4K6414DdSHro.jpg</fanart>
<date>2002</date>
<genre>Fantastico. Aventuras.Saga</genre>
<extra>NA</extra>
<info>Terminado el verano, Harry (Radcliffe) no ve la hora de abandonar la casa de sus odiosos tíos, pero, inesperadamente se presenta en su dormitorio Dobby, un elfo doméstico, que le anuncia que correrá un gran peligro si vuelve a Hogwarts. A pesar de los esfuerzos del elfo por retenerlo, Harry es rescatado por Ron y sus hermanos, con la ayuda de un coche volador, y recibido con los brazos abiertos en el cálido hogar de los Weasley.A pesar de que a Harry y a Ron les impiden entrar en el andén 9 ¾ y subir al Expreso de Hogwarts, ellos se las arreglan para llegar a tiempo de empezar el nuevo curso. Las hazañas de nuestro héroe ya se han extendido por todo el colegio, convirtiéndolo en el centro de una atención no deseada. Entre sus nuevos admiradores está Ginny (Bonnie Wright), la hermana pequeña de Ron; el aspirante a fotógrafo Colin Creevey (Hugh Mitchell) y Gilderoy Lockhart (Branagh), el nuevo y vanidoso profesor de defensa contra las artes oscuras.Sin embargo, ni siquiera Lockhart puede ofrecer una explicación plausible ni una solución eficaz para combatir el terror que se está apoderando del colegio. En tal circunstancia, la atención se centra en Harry, pero todos empiezan a dudar de él. Todos, excepto Ron, Hermione y la pequeña y frágil Ginny, que vive volcada en su nuevo y misterioso diario. Pero Harry no decepcionará a sus amigos y se enfrentará a la oscura fuerza que acecha Hogwarts</info>
 
 
<page>12</page><title>HARRY POTTER: Y LA ORDEN DEL FENIX</title>
<microhd>a42bc2e1c32b616c4cbfeecb137a11536f309391</microhd>
<fullhd>a343a3aeeab39e4052fea96f1dbfb218471a54bd</fullhd>
<tresd>NA</tresd>
<cuatrok>f460c59ba3d2a08f353d452da240b8cd659ec071</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/z5bL1z1ZOAKBkvbOhVQp6GfBv4u.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/pkxPkHOPJjOvzfQOclANEBT8OfK.jpg</fanart>
<date>2007</date>
<genre>Fantastico. Aventuras. Drama.Saga</genre>
<extra>NA</extra>
<info>Las tediosas vacaciones en casa de sus tíos todavía no han acabado y Harry se encuentra más inquieto que nunca. Apenas ha tenido noticias de Ron y Hermione y presiente que algo extraño está sucediendo en Hogwarts. En efecto, cuando por fin empieza el curso, sus temores se hacen realidad. El Ministerio de Magia niega que Voldemort haya regresado e inicia una campaña de desprestigio contra Harry y Dumbledore, encomendando a la horrible profesora Dolores Umbridge la tarea de vigilar todos sus movimientos. Así, pues, además de sentirse solo e incomprendido, Harry sospecha que Voldemort puede adivinar sus pensamientos e intuye que el temible mago trata de apoderarse de un objeto secreto que le permitiría recuperar su destructivo poder.</info>
 
 
<page>12</page><title>HARRY POTTER: Y LA PIEDRA FILOSOFAL</title>
<microhd>a42bc2e1c32b616c4cbfeecb137a11536f309391</microhd>
<fullhd>8820a62a34253c05287cada6f11f6fcee03f09b0</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/7xXJ15VEf7G9GdAuV1dO769yC73.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/hziiv14OpD73u9gAak4XDDfBKa2.jpg</fanart>
<date>2001</date>
<genre>Saga.Fantastico. Aventuras. Infantil</genre>
<extra>NA</extra>
<info>El día en que cumple once años, Harry Potter se entera de que es hijo de dos destacados hechiceros, de los que ha heredado poderes mágicos. En la escuela Hogwarts de Magia y Hechicería, donde se educa con otros niños que también tienen poderes especiales, aprenderá todo lo necesario para ser mago.</info>
 
 
<page>12</page><title>HARRY POTTER: Y LAS RELIQUIAS DE LA MUERTE PARTE 1</title>
<microhd>a42bc2e1c32b616c4cbfeecb137a11536f309391</microhd>
<fullhd>3503a68aa75652e4d7bc6e6a1b3df0468d5cecc6</fullhd>
<tresd>NA</tresd>
<cuatrok>2b6995401196b1fbb96a173c8910377aef897711</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/fYyoJ4Yadk0MCBHZPFrgvMDl50o.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/fXRXpzxUApE3OuXhIqsdavQjCVa.jpg</fanart>
<date>2010</date>
<genre>Fantastico. Aventuras. Drama.Saga</genre>
<extra>NA</extra>
<info>Primera parte de la adaptación al cine del último libro de la saga Harry Potter. Es una continuación de la historia recogida en "El misterio del príncipe". Una misión casi imposible cae sobre los hombros de Harry: deberá encontrar y destruir a los últimos horrocruxes para poner fin al reinado de Lord Voldemort. En el episodio final de la saga, el joven hechicero de 17 años emprende con sus amigos Hermione Granger y Ron Weasley un peligroso viaje por Inglaterra para encontrar los objetos que contienen los fragmentos del alma del Señor Tenebroso, que son los que garantizan su longevidad. Pero la tarea no será fácil, pues el poder del lado oscuro crece cada vez más, y las más firmes lealtades serán puestas a prueba. Harry deberá usar todos los conocimientos que ha adquirido gracias a Dumbledore para enfrentarse a su enemigo y encontrar la forma de sobrevivir a esta última aventura.</info>
 
 
<page>12</page><title>HARRY POTTER: Y LAS RELIQUIAS DE LA MUERTE PARTE 2</title>
<microhd>a42bc2e1c32b616c4cbfeecb137a11536f309391</microhd>
<fullhd>d69ccf5c2eb6e2d09891bc59c92a2d6fa22719da</fullhd>
<tresd>NA</tresd>
<cuatrok>5bd36ce2a82393306b6eec7f246d66445570b746</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/aM1TuUiPtV8OAZyu61CTdy9Ymtk.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/cbcpDn6XJaIGoOil1bKuskU8ds4.jpg</fanart>
<date>2011</date>
<genre>Fantastico. Aventuras. Drama.Saga</genre>
<extra>NA</extra>
<info>El final ha llegado. Harry, Hermione y Ron tendrán que recuperar la espada de Gryffindor para encontrar y destruir los últimos horrocruxes. Mientras tanto, Lord Voldemort está a punto de apoderarse por completo de Hogwarts y de conseguir su objetivo: matar a Harry Potter. La única esperanza de Harry es encontrar los horrocruxes antes de que Voldemort lo encuentre a él. Buscando pistas, descubre una antigua y olvidada historia: la leyenda de las reliquias de la muerte, que podría dar al malvado Lord el poder definitivo. Pero el futuro de Harry está escrito desde que nació e incluye una misión para la que se ha estado preparando desde que llegó a Hogwarts: la batalla final contra Voldemort. </info>
 
 
<page>12</page><title>HASTA EL CIELO</title>
<microhd>729cbe81933b26956295535026c6dace904edb0f</microhd>
<fullhd>7ef958e6ee0bc2ef26a15b44b056fd094d310820</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/4wBsYozsXBlG7Lx4NO7ycDWnnUs.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/vx4bevtt3ZVgezPZsuiVxm90l5E.jpg</fanart>
<date>2020</date>
<genre>Thriller </genre>
<extra>NA</extra>
<info>El día que Ángel habló con Estrella en aquella discoteca, su vida cambió para siempre. Tras una pelea con Poli, el posesivo novio de la chica, éste le anima a unirse a una banda de atracadores que tiene en jaque a toda la policía de Madrid. Ángel comienza a escalar rápidamente en una pirámide de atracos, dinero negro, negocios turbios y abogados corruptos que le llevarán a ser acorralado por Duque, un incansable detective. Desoyendo los consejos de su gente, Ángel consigue ascender hasta convertirse en el protegido de Rogelio, uno de los tipos que controla el mercado negro de la ciudad. Con éste y Sole, hija del capo, Ángel descubrirá que el precio del poder es alto y que pronto tendrá que decidir entre su futuro como atracador y el amor de su vida, Estrella. Un viaje que empezó en el más sucio de los suburbios y que tiene como principal objetivo lo más alto: el cielo.</info>
 
<page>13</page><title>HASTA EL HORIZONTE</title>
<microhd>ff4706012fe2b148fde8868f79c27448bb731d47</microhd>
<fullhd>4fec1167a1aa75a5f8d5771474fb3e81d1c4d505</fullhd>
<tresd>NA</tresd>
<cuatrok>b8228edf203785436a8ad8864a02506975b0086c</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/vWGkIHeCLw9wWvPXnPTOM9d5GiK.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/2KX79ljurDq6bBXcyoQLROVxkU8.jpg</fanart>
<date>2020</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>Tiempo después de su ruptura, Sara y Jackson se reencuentran unos días antes de la boda de un amigo en común que se casa en una isla remota. Juntos atraviesan el océano camino a la boda, pero cuando el piloto de la avioneta sufre un ataque al corazón, ellos deberán hacer lo imposible para mantener la avioneta en el aire.</info>
 
 
<page>13</page><title>HASTA EL ULTMO HOMBRE</title>
<microhd>357e072c7cf16342e39509453227e2047538f1b9</microhd>
<fullhd>B63544343488A8C9289EF6AD9B6D3D5DEAF1A5EB</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/m3jj8IJ7uP5p4MqMzgtGW5l4ECd.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/pqv1Li4kUc96LmrLutdJm6aQi0y.jpg</fanart>
<date>2016</date>
<genre>Bélico. Drama</genre>
<extra>Mis</extra>
<info>Narra la historia de Desmond Doss, un joven médico militar que participó en la sangrienta batalla de Okinawa, en el Pacífico durante la II Guerra Mundial, y se convirtió en el primer objetor de conciencia en la historia estadounidense en recibir la Medalla de Honor del Congreso. Doss quería servir a su país, pero desde pequeño se había hecho una promesa a sí mismo: no coger jamás ningún arma.</info>
 
 
<page>13</page><title>HASTA QUE LA BODA NOS SEPARE</title>
<microhd>837204652c4ccbd0d31e65a66d9cbbccd77a0bf9</microhd>
<fullhd>010e2f69594ebd6b91f501279095f0984740dd36</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/bkz7ZoqJMFziqF8yQzkmfMRUEG.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/1D31jXT9Wqq84xGELwl4cgTZtsd.jpg</fanart>
<date>2020</date>
<genre>Comedia. Romance</genre>
<extra>NA</extra>
<info>Marina es una treintañera que se gana la vida organizando bodas. A diferencia de sus clientes, ella disfruta de una vida sin ataduras ni compromisos, hasta que una noche conoce a Carlos, un affaire más para ella y un momento de debilidad para él. Porque él tiene novia: Alexia, una joven perfecta y amiga de infancia de Marina. Cuando Alexia descubre la tarjeta de visita de Marina entre las cosas de Carlos, lo interpreta como una propuesta de matrimonio y dice que sí de inmediato.</info>
 
 
<page>13</page><title>HEAT</title>
<microhd>NA</microhd>
<fullhd>40b38b34864fc9eb680f5499a81d457e9455bd94</fullhd>
<tresd>NA</tresd>
<cuatrok>ce1e689f838d9e4f70568b84645d54d608efbd83</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/bVKiGEV8qlQ4QfIIhgay7v6WWto.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/kYQAOe9uveHOkHrGI1d6QQlEWSy.jpg</fanart>
<date>1995</date>
<genre>Thriller. Accion. Intriga</genre>
<extra>NA</extra>
<info>Neil McCauley (Robert De Niro) es un experto ladrón. Su filosofía consiste en vivir sin ataduras ni vínculos que puedan constituir un obstáculo si las cosas se complican. Su banda la forman criminales profesionales tan cualificados que pueden incluso impresionar al detective Vincent Hanna (Al Pacino), un hombre que vive tan obsesionado con su trabajo que llega a poner en peligro su vida sentimental. Cuando la banda de McCauley prepara el golpe definitivo, y el equipo de Hanna se dispone a evitarlo, cada uno de ellos comprende que tiene que vérselas con la mente más brillante a la que se ha enfrentado en su carrera. </info>
 
 
<page>13</page><title>HELL FEST</title>
<microhd>NA</microhd>
<fullhd>5ff7625d11a44aa5b2a1999ce58bd8780883559f</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/iJkvu8hinbqH9OeQYSppqY3BuFn.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/vIsKcpscdELfbBHHsfLvfTTMIc3.jpg</fanart>
<date>2018</date>
<genre>Terror</genre>
<extra>NA</extra>
<info>Un asesino en serie enmascarado convierte un parque de atracciones en su patio de recreo aterrorizando a un grupo de amigos. Mientras tanto, un grupo de patrocinadores que está ese día en el parque se divierte pensando que todo forma parte del espectáculo.</info>
 
 
<page>13</page><title>HELLBOY</title>
<microhd>45a22bd256dc1a8ef0ac2011fda421a9d473302f</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/tNSpy125u15oQ9EOsDLzmMSvvub.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/8jfaT1cCZWQgWoVE5R1E8I4oA8N.jpg</fanart>
<date>2019</date>
<genre>Fantastico. Accion</genre>
<extra>NA</extra>
<info>La Agencia para la Investigación y Defensa Paranormal (AIDP) encomienda a Hellboy la tarea de derrotar a un espíritu ancestral: Nimue, conocida como "La Reina de la Sangre". Nimue fue la amante del mismísimo Merlín durante el reinado del Rey Arturo, de él aprendió los hechizos que la llevaron a ser una de las brujas más poderosas… Pero la locura se apoderó de ella y aprisionó al mago para toda la eternidad. Hace siglos consiguieron acabar con esta villana, enterrándola profundamente, pero ha vuelto de entre los muertos con la intención de destruir a la humanidad con su magia negra.</info>
 
 
<page>13</page><title>HEREDITARY</title>
<microhd>f184621f5655bd9a98ac0d0ddd29bfcda8f26867</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>56d8dae7ec086a05bca5bd2eee8f487cf7bb8127</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/fCVCaO5pCXChfXbTcqEqeyj2z9N.jpg</thumbnail>
<fanart>https://i.imgur.com/PLsw3N3.jpg</fanart>
<date>2018</date>
<genre>Terror</genre>
<extra>NA</extra>
<info>Cosas extrañas comienzan a suceder en casa de los Graham tras la muerte de la abuela y matriarca, que deja en herencia su casa a su hija Annie. Annie Graham, una galerista casada y con dos hijos, no tuvo una infancia demasiado feliz junto a su madre, y cree que la muerte de ésta puede hacer que pase página. Pero todo se complica cuando su hija menor comienza a ver figuras fantasmales, que también empiezan a aparecer ante su hermano.</info>
 
 
<page>13</page><title>HISTORIAS DE FANTASMAS</title>
<microhd>NA</microhd>
<fullhd>129ce3ad496f7ecdb03055c0bdd523be8415e6c0</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/A5urWIklagQnAaJ8ZfNuq1KyPEu.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/7vyzEMfcEpBChEyK7cP6XW6B7hi.jpg</fanart>
<date>2017</date>
<genre>Terror. Drama</genre>
<extra>NA</extra>
<info>El escéptico profesor Phillip Goodman (Andy Nyman) se embarca en un viaje hacia lo terrorífico tras encontrarse un archivo con detalles de tres casos inexplicables de apariciones.</info>
 
 
<page>13</page><title>HISTORIAS DE MIEDO</title>
<microhd>dfc58648aa36ae390b3c2a5105b72d187c93c447</microhd>
<fullhd>a564833bf45e3edaacd67c376b19922b38a85465</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/2o2thWyv4q1s5CLkc1ffxyVFkti.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/8m9JTau4ZBFQDVX7fyFaxp8yFow.jpg</fanart>
<date>2019</date>
<genre>Terror</genre>
<extra>NA</extra>
<info>Un grupo de adolescentes debe resolver el misterio que rodea a una serie de repentinas y macabras muertes que suceden en su pueblo. Producida por Guillermo del Toro. </info>
 
 
<page>13</page><title>HISTORIAS PARA NO CONTAR</title>
<microhd>NA</microhd>
<fullhd>ca973d51ea7dd2242f132cc648aea0c7142f973a</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/DDpLKrJahb6wexz4ssAOWLZMK6.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/jKSCK59bJwAkNZzG9mfQzPCI2N9.jpg</fanart>
<date>2022</date>
<genre>Comedia. Drama</genre>
<extra>NA</extra>
<info>Narra situaciones en las que nos podemos reconocer y que preferiríamos no explicar, o incluso olvidar. Encuentros inesperados, momentos ridículos o decisiones absurdas, cinco historias con una mirada ácida y compasiva a la incapacidad para gobernar nuestras propias emociones.</info>
 
 
<page>13</page><title>HOGAR</title>
<microhd>40f9d5b0745f0d7240b97599f8d41eba5653c374</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/tUrF8viLUb6sJec6t2pzidC3VGb.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/yjLjZxfL8FtRJd7o3iiUHqL01pH.jpg</fanart>
<date>2020</date>
<genre>Thriller. Intriga. Drama</genre>
<extra>NA</extra>
<info>Javier Muñoz era un ejecutivo publicitario de éxito pero, tras un año en el paro, él y su familia se ven obligados a dejar el piso que ya no se pueden permitir. Un día, Javier descubre que aún conserva un juego de llaves de su antigua casa, y empieza a espiar a la joven pareja que ahora vive allí. Poco a poco, Javier empezará a infiltrarse en la vida de los nuevos propietarios, decidido a intentar recuperar la vida que ha perdido… a costa de quien sea.</info>
 
 
<page>13</page><title>HOMBRES DE NEGRO II (MEN IN BLACK 2)</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>c9e44974d1145dcdd7904acf92440fb0973e5921</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/zX1Hi0j7Yn4jv2eDHyIMlHP8lDb.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/gaObzSfuj8cLw2xpfphDVEs9aof.jpg</fanart>
<date>2002</date>
<genre>Ciencia ficción. Fantástico. Comedia. Acción. Saga</genre>
<extra>Mis</extra>
<info>Cuatro años después de evitar un desastre intergaláctico, el agente K ha vuelto a trabajar en el servicio postal mientras que el agente J sigue persiguiendo alienígenas. Pero cuando la integridad de la Tierra vuelve a estar en peligro, J tendrá que convencer a K para que se aliste de nuevo en los Hombres de Negro antes de que sea demasiado tarde.</info>
 
 
<page>13</page><title>HONEY BOY</title>
<microhd>01775da1418c9ea2ca1285713a2194908530ac80</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/3BZ2rBn31kWER45ZMj7OTe9keMm.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/sOLlj8gmpojgm1zUCMds5wq9yAO.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Otis es un niño de 12 años que descubre desde muy joven la cara oculta de Hollywood, ejerciendo de especialista en shows televisivos. Su padre es un antiguo payaso de rodeo con diversos problemas, ahora sin trabajo, que decide convertirse en su guardián. Cuando Otis no está grabando sus escenas como doble, pasa el rato con él en hoteles de poca monta situados en las afueras de las ciudades donde se alojan. La convivencia entre ambos es muy compleja, y Otis anhela que su padre se comporte como tal.</info>
 
 
<page>13</page><title>HOSTILES</title>
<microhd>728441ebc8af882110528ed247eaceadf5f944b8</microhd>
<fullhd>44e5223a07c60284237f131da90d540d96762cc8</fullhd>
<tresd>NA</tresd>
<cuatrok>94b0d713273007808e5a06739079b54f3552ca49</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/6FxhTzubzDdFE6ZyzbxgTwIyx7l.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/vBSsIyWExRpe3Robi8SAnayrMq1.jpg</fanart>
<date>2017</date>
<genre>Western. Drama</genre>
<extra>NA</extra>
<info>El legendario capitán del ejército, Joseph J. Blocker (Christian Bale), se ve en la obligación de acompañar a un moribundo jefe cheyenne (Wes Studi), y a su familia, de regreso a las tierras de su tribu. Para ello tendrán que emprender un peligroso viaje por las praderas montañosas de Nuevo México, dónde se encontrarán con una joven viuda (Rosamund Pike) cuya familia fue asesinada en las llanuras por un grupo de comanches que aún rondan por la zona. Juntos tendrán que unir fuerzas para sobrevivir al castigador paisaje y a las hostiles tribus comanche que se encuentran por el camino.</info>
 
 
<page>13</page><title>HOTEL ARTEMIS</title>
<microhd>ea957a62305e783ee44dbe7eedfba4422bfa064f</microhd>
<fullhd>3e89d1b68e6b7d8e3e97e868e0ae39db58dd2808</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/aYvA8WoNOfNxxO7U7DMg6NNKzJf.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/E0QuvtI9RCDfi79WnQSPKIzc61.jpg</fanart>
<date>2018</date>
<genre>Ciencia ficcion. Thriller</genre>
<extra>NA</extra>
<info>Los Ángeles, año 2028. El Hotel Artemis es un hospital exclusivo para delincuentes, regentado por Jean Thomas (Jodie Foster) y ceñido a unas reglas muy estrictas. Cuando el centro es atacado, dará paso a una espiral de violencia difícil de contener.</info>
 
 
<page>13</page><title>HOTEL TRANSILVANIA</title>
<microhd>NA</microhd>
<fullhd>7fb1006d67523c7f4d57eb90549cc642cfb60dd5</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/2YYH5i78lNNlllz7mlj6tEjWaSZ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/sETTal2HPPkUC3lWc27HL1AFu9C.jpg</fanart>
<date>2012</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Desde su nacimiento, Mavis vive en el lujoso hotel Transylvania, que fue creado para proteger a los monstruos de la amenaza de los seres humanos. Cuando Mavis está a punto de cumplir la mayoría de edad (118 años), su padre, el conde Drácula, le prepara una fiesta a la que acuden los mejores amigos de la familia: desde Frankenstein al Hombre Invisible o la Momia; pero también asiste a la celebración un hombre que se enamora de Mavis</info>
 
 
<page>13</page><title>HOTEL TRANSILVANIA 2</title>
<microhd>726d54b01f263842a2bec801d2d52b9c3b8312f2</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ndYVcm1k3h81MRDhzZCI4SxDzGp.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/2OQYAP64ybQZBiOcjthMVFcQRE.jpg</fanart>
<date>2015</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Todo parece mejorar en el Hotel Transilvania. La rígida norma establecida por Drácula de “sólo para monstruos” se ha suprimido y se aceptan también huéspedes humanos. Lo que preocupa al conde es que que su adorable nieto Dennis, medio humano y medio vampiro, no dé señal alguna de vampirismo. Aprovechando que Mavis ha ido a visitar a sus suegros humanos, Drácula recluta a sus amigos Frank, Murray, Wayne y Griffin para hacer que Dennis pase por un campamento de “entrenamiento de monstruos”. Lo que ignoran es que Vlad, el gruñón padre de Drácula, está a punto de llegar al hotel. Y cuando descubra que su bisnieto no es de sangre pura y que los humanos pueden frecuentar el establecimiento las cosas se complicarán. Secuela de Hotel Transilvania (2012).</info>
 
 
<page>13</page><title>HOTEL TRANSILVANIA 3</title>
<microhd>7248a674f28863966c6ea4718f09b00b74c0b371</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/r69lcBWIqjN1wU0sKuxyubbtyF.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/m03jul0YdVEOFXEQVUv6pOVQYGL.jpg</fanart>
<date>2018</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Nuestra familia de monstruos favorita se embarca en un crucero de lujo para que por fin Drac pueda tomarse un descanso de proveer de vacaciones al resto en el hotel. Es una navegación tranquila para la pandilla de Drac, ya que los monstruos se entregan a toda la diversión a bordo que ofrece el crucero, desde el voleibol de monstruos y las excursiones exóticas, a ponerse al día con sus bronceados de luna. Pero las vacaciones de ensueño se convierten en una pesadilla cuando Mavis se da cuenta de que Drac se ha enamorado de la misteriosa capitana de la nave, Ericka, quien esconde un peligroso secreto que podría destruir a todos los monstruos.</info>
 
 
<page>13</page><title>HOTEL TRANSILVANIA: TRANSFORMANIA</title>
<microhd>a36bf674b963583611adba93e64f0b01f2157843</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/xNF8AxJc966FWk4SYqXxGHaZLHZ.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/7QabKu8tizoqy8qCZJXljdSpP4A.jpg</fanart>
<date>2022</date>
<genre>Infantil. Saga</genre>
<extra>NA</extra>
<info>Drac se enfrentará a una de las situaciones más aterradoras vividas hasta el momento. Cuando el misterioso invento de Van Helsing, el "Rayo Monstrificador", se vuelve totalmente fuera de control, Drac y sus monstruosos amigos se transforman en humanos, ¡y Johnny se convierte en un monstruo! Cuarta y última entrega de la franquicia animada "Hotel Transilvania". </info>
 
 
<page>13</page><title>HUERFANOS DE BROOKLYN</title>
<microhd>8c84bb54420997568e309b96350fc8384ba9039a</microhd>
<fullhd>06072a9f2c9088f17d32e2e23123d0e59a209e98</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/2X86f3KRbdStQXk0f8pZovSRpal.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/2X86f3KRbdStQXk0f8pZovSRpal.jpg</fanart>
<date>2019</date>
<genre>Thriller. Drama </genre>
<extra>NA</extra>
<info>Nueva York. Años 50. Lionel Essrog es un solitario detective privado, afectado por el Síndrome de Tourette, que se aventura a intentar resolver el asesinato de su mentor y único amigo, Frank Minna.</info>
 
 
<page>13</page><title>HUIDA DE MOGADISCIO</title>
<microhd>f54a63fdba35c637e1791d2dd337bd97803d172e</microhd>
<fullhd>b43e2fba10b40d82d013edf78a47a1fd448fbbfa</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ohN2XO36CiWl5MvK4dO7NtZlt0Q.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/3zpVRXBswrbKQQIXHDqmR3gpH0h.jpg</fanart>
<date>2021</date>
<genre>Acción. Drama. Thriller</genre>
<extra>NA</extra>
<info>En 1991, Mogadiscio, capital y ciudad más poblada de Somalia, estaba pasando por una guerra civil. El personal y las familias de la embajada de Corea del Sur, aislados sin poder comunicarse, aguantaron como pudieron. Una noche, el personal de la embajada de Corea del Norte llamó a la puerta pidiendo ayuda. El objetivo de ambas embajadas era huir de la ciudad.</info>
 
 
<page>13</page><title>I AM MOTHER</title>
<microhd>185B05826C12F7288A4261B4C4567B2FEC3DD01F</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/zWdDG1T0plbF84Urm2uNawyiwSl.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/1ShvGfnSgnFUgygharI833EhfWB.jpg</fanart>
<date>2019</date>
<genre>Ciencia ficcion. Thriller </genre>
<extra>NA</extra>
<info>Una chica adolescente (Rugaard) es criada en un refugio subterráneo por una madre robot. Pero el vínculo entre ambas se ve amenazado cuando entra en escena una mujer desconocida y ensangrentada (Swank) que pone en tela de juicio todo lo que su madre robótica le ha enseñado sobre el mundo exterior.</info>
 
 
<page>13</page><title>I AM WOMAN</title>
<microhd>NA</microhd>
<fullhd>61f4bd9ed656aa1606d7ed1f0380bcf10338e71c</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/xpIEx5hFp9RNhuXa4t7DobgBpft.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/pgpOc0sGOiZ59omUh6xLMpbghiz.jpg</fanart>
<date>2019</date>
<genre>Drama . Musical</genre>
<extra>NA</extra>
<info>Cuenta la historia de la cantante y activista Helen Reddy, que se dio a conocer durante la década de los años 70</info>
 
 
<page>13</page><title>I CARE A LOT</title>
<microhd>b69126f326c0ac29dd3b708f832bf02757f32535</microhd>
<fullhd>385a120e6e6c5af1f04bd5381647c4c22a509d96</fullhd>
<tresd>NA</tresd>
<cuatrok>cd661b59bdf46b0687e0dd6db49e817243d778e3</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/92JqGDguuNiGT1njV8ieyik5qpf.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/vX5JtEcumMQvMCLVcIqfetc7hdg.jpg</fanart>
<date>2020</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Marla Grayson (Rosamund Pike) no tiene escrúpulos a la hora de beneficiarse de los demás. Después de haberse aprovechado de docenas de jubilados como tutora legal, ella y su compañera Fran (Eiza González) ven a Jennifer Peterson (Dianne Wiest) como la nueva víctima: una gallina de los huevos de oro a la que pueden desplumar fácilmente. Pero mientras intentan llevar a cabo su plan, Marla y Fran descubren que la señora Peterson no es lo que creían, y que sus actos han entorpecido la labor de un importante criminal (Peter Dinklage)</info>
 
 
<page>13</page><title>I LOVE DOGS</title>
<microhd>201202fd499e90246b0e2e8971adfe78aff2d6e9</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/3IowUP2HzgPbXtBBplsCVTG5HsM.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/edbShQ3rrezoDrsWoLUdcsZkyj2.jpg</fanart>
<date>2018</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Sigue la vida de varios dueños de perros y a las propias mascotas en la soleada Los Ángeles. Cuando los caminos de estos humanos y sus perros comienzan a entrelazarse, sus vidas comienzan a cambiar en formas que nunca esperaron.</info>
 
 
<page>13</page><title>ICE AGE 1: LA EDAD DE HIELO</title>
<microhd>517fa753c26e3043601130804d9e987dbc06df31</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/7u2LiFrV0wJ4heTMh5HjeCkII3f.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/a9ykRAWtQnI3SsZDfh8sCJo5cWI.jpg</fanart>
<date>2002</date>
<genre>Infantil. Saga</genre>
<extra>NA</extra>
<info>En la epoca glacial de la prehistoria un mamut, un perezoso gigante y un tigre se ocuparán de cuidar un bebé humano extraviado por su familia</info>
 
 
<page>13</page><title>ICE AGE 2: EL DESHIELO</title>
<microhd>517fa753c26e3043601130804d9e987dbc06df31</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/wnBEdWq5gplaAgTmYFg4cq89mWi.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/geKp17rEoImTIZp5nlzk1vpPrqP.jpg</fanart>
<date>2006</date>
<genre>Infanti. Saga</genre>
<extra>NA</extra>
<info>Continuación de las aventuras de los animales prehistóricos en la "edad de hielo". Esta vez Manny, Sid y Diego tendrán la misión de informar a los demás animales que la edad de hielo se acaba, pero que una gigantesca inundación podría acabar con la preciosa pradera. </info>
 
 
<page>13</page><title>ICE AGE 3: EL ORIGEN DE LOS DINOSAURIOS + CORTOS</title>
<microhd>b9c65be8f06b66fca9d9b1fd06258d8b46904043</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/7BAJh4R1pdSxvZO7JeV4PmDXjcY.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/gkxSIlZPEwGPimQ8TEE8C52cOSO.jpg</fanart>
<date>2009</date>
<genre>Infantil. Saga</genre>
<extra>NA</extra>
<info>Tercera parte de las aventuras de Diego, Scrat y compañía. En esta ocasión, les tocará lidiar con varios dinosaurios. Scrat sigue intentando apoderarse de la muy escurridiza bellota (mientras, tal vez, encuentra el verdadero amor); Manny y Ellie esperan el nacimiento de su mini-mamut; Sid, el perezoso, se mete en un buen lío al formar de repente una familia propia tras encontrarse unos huevos de dinosaurio; y Diego, el tigre de dientes de sable, se pregunta si pasar tanto tiempo con sus amigos no le estará volviendo demasiado "blandito". Con el fin de salvar al infortunado Sid, la pandilla se adentra en un misterioso mundo subterráneo, donde tienen algunos enfrentamientos con los dinosaurios, lidian con una enloquecida flora y fauna, y conocen a una comadreja tuerta e implacable cazadora de dinosaurios llamada Buck. </info>
 
 
<page>13</page><title>ICE AGE 4: LA FORMACION DE LOS CONTINENTES  + CORTOS</title>
<microhd>b9c65be8f06b66fca9d9b1fd06258d8b46904043</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/58chkjMfhylCtdZ29ofw8dp7ot6.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/cC3nr0KkXHPChZnFMBvFPdAYNyV.jpg</fanart>
<date>2012</date>
<genre>Infantil. Saga</genre>
<extra>NA</extra>
<info>La loca persecución de Scrat por su escurridiza bellota desde el principio de los tiempos, tiene consecuencias que cambiarán el mundo: un cataclismo continental que desencadenará la mayor de las aventuras para Manny, Diego y Sid. En medio de estas agitaciones, Sid se reencuentra con su Abuelita Gruñona y la manada tropieza con una extraña cuadrilla de piratas de altamar decididos a impedirles su regreso a casa. </info>
 
 
<page>13</page><title>ICE AGE:  EL GRAN CATACLISMO</title>
<microhd>817c0c97aec34cb1ede9eab71e5a9d565eeecf1f</microhd>
<fullhd>6da8c6e0faf13ebe50836ea2d63491d09fb86fa2</fullhd>
<tresd>82c57c3ad6744e1fade9921a001106517a4ac490</tresd>
<cuatrok>e725642ab0a2c6beae8c1133eb0f08dea3388e7c</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/wjgvznDSs2YZI0pr0dEe0NzDI7d.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/crI3XE2CQzeYwsMkrNzQoYmv9mV.jpg</fanart>
<date>2016</date>
<genre>Infantil.Saga</genre>
<extra>NA</extra>
<info>La obsesiva búsqueda de bellotas por parte de Scrat acaba provocando un accidente: que un asteroide se dirija a a la Tierra, amenazando con acabar con la Edad de Hielo. Sid, Manny, Diego y los demás miembros de la manada intentarán impedirlo por todos los medios.</info>
 
 
<page>13</page><title>ICE AGE: EN BUSCA DEL HUEVO</title>
<microhd>320021185eeae0008ea2026d1ee19ff053075e7e</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/piPjnB8SvuQoV1C3h7WFV4jw4PR.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/obng5fTitTqAXwLUSBQXjpsXeAG.jpg</fanart>
<date>2016</date>
<genre>Infantil. Saga</genre>
<extra>NA</extra>
<info>Cuando Sid toma un trabajo como una niñera de un huevo, él es inconsciente que un viejo enemigo tiene sus propios planes. Las travesuras conducen a la primera búsqueda de huevos y la creación de las tradiciones populares de Pascua.</info>
 
<page>13</page><title>ICE ROAD</title>
<microhd>c6d33c78436c25098118a9330112fcd0dc796fa4</microhd>
<fullhd>f640513eae30b366ef80fabb7abbb2b1e0701427</fullhd>
<tresd>NA</tresd>
<cuatrok>69a1970d81db69b19bdec992bd7a56b78c4ce021</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/lzJlE1mlHY2ISm9gUt75bXdGJom.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/y9rrNKSLkS7imiR46hQWRnO0n2m.jpg</fanart>
<date>2021</date>
<genre>Acción. Thriller</genre>
<extra>NA</extra>
<info>Una remota mina de diamantes ha colapsado dejando atrapados a un grupo de mineros en una región lejana y helada de Canadá. Como parte de un equipo contratado para rescatarlos, el experimentado conductor de una quitahielos emprende un rescate imposible, teniendo que luchar contra una carretera helada, contra las aguas que están descongelándose y contra una amenaza que no ve venir.</info>
 
 
<page>13</page><title>IDENTIDAD BORRADA</title>
<microhd>1215fd115a90910771473ad5d6b7deb39b19abdc</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/s8uSSxbk1m0h8PEnyhtoWeoeCCT.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/2ePViXIOnifrugvnjlFxzmvjwKL.jpg</fanart>
<date>2018</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>El hijo (Lucas Hedges) de un predicador baptista de una pequeña ciudad norteamericana, se ve obligado a participar en un programa para "curar" su homosexualidad, apoyado por la Iglesia. Cuando a los 19 años Jared Eamons (Lucas Hedges) cuenta a sus padres Nancy y Marshall Eamons (Nicole Kidman y Russell Crowe) que es gay, el joven comienza a ser presionado para que asista a un programa de terapia de conversión gay, o de lo contrario será rechazado por su familia, su amigos y la iglesia. Dentro del programa Jared entrará en conflicto con el terapeuta jefe Victor Sykes (Joel Edgerton).</info>
 
<page>13</page><title>INDIANA JONES EN BUSCA DEL ARCA PERDIDA</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>c6699d42f860ccd9d7bad858caf610137a027311</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/9QvmNoet3sZhuR225rDjTh1Hhb4.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/oxK0OlJ5brTZMLQu5AD1oLDgwt3.jpg</fanart>
<date>1981</date>
<genre>Aventuras. Acción . Saga</genre>
<extra>NA</extra>
<info>Año 1936. Indiana Jones es un profesor de arqueología, dispuesto a correr peligrosas aventuras con tal de conseguir valiosas reliquias históricas. Después de una infructuosa misión en Sudamérica, el gobierno estadounidense le encarga la búsqueda del Arca de la Alianza, donde se conservan las Tablas de la Ley que Dios entregó a Moisés. Según la leyenda, quien las posea tendrá un poder absoluto, razón por la cual también la buscan los naSis.</info>
 
<page>13</page><title>INDIANA JONES Y EL DIA DEL DESTINO</title>
<microhd>NA</microhd>
<fullhd>6a55c55a983dfc87c9116810492943ba89d7fef5</fullhd>
<tresd>NA</tresd>
<cuatrok>b1fa1effc49de1872b05a4695ac09b62fd3b7001</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/3Sj5ubQ0BLXd6JuECq52Ira8Oor.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/9m161GawbY3cWxe6txd1NOHTjd0.jpg</fanart>
<date>2023</date>
<genre>Aventuras. Acción. Saga</genre>
<extra>NA</extra>
<info>El arqueólogo Indiana Jones deberá emprender otra aventura contra el tiempo para intentar recuperar un dial legendario que puede cambiar el curso de la historia. Acompañado por su ahijada, Jones pronto se encuentra enfrentándose a Jürgen Voller, un ex naSi que trabaja para la NASA.</info>
 
 
<page>13</page><title>INDIANA JONES Y EL REINO DE LA CALAVERA DE CRISTAL</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>5aa95f7dd6db3393e7248c043f82ce91f67eaca0</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/eahWIgjttYQu4Ih8HwhF9sdexYk.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/p96949hcwUG2BwE6MgKmw0uYNCx.jpg</fanart>
<date>2008</date>
<genre>Aventuras. Acción. Ciencia ficción. Saga</genre>
<extra>NA</extra>
<info>Año 1957, en plena guerra fría. Indiana Jones (Harrison Ford) consigue de milagro salir ileso de una explosiva situación con unos agentes soviéticos en un remoto desierto al que llegó detenido junto a su amigo Mac (Ray Winstone). El decano de la Universidad (Jim Broadbent) le confiesa a su amigo el profesor Jones que las últimas misiones de Indy han fracasado, y que está a punto de ser despedido. Mientras tanto, Indiana conoce a Mutt (Shia LaBeouf), un joven rebelde que le propone un trato: si le ayuda a resolver un problema personal, él, a cambio, le facilitaría uno de los descubrimientos más espectaculares de la historia: la Calavera de Cristal de Akator, que se encuentra en un lugar remoto del Perú. Pero los agentes soviéticos, dirigidos por la fría y bella Irina Spalko (Cate Blanchett), tienen el mismo objetivo.</info>
 
 
<page>13</page><title>INDIANA JONES Y EL TEMPLO MALDITO</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>5b3b13c44cf9fd60f0d75b60d8fe67f0e8d38407</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/6UrznkPL4Y2f66zkoFAiSwfAYtE.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/2hMt6zKQsvYvH3ZRe8T6RzAD2XB.jpg</fanart>
<date>1984</date>
<genre>Aventuras. Acción. Saga</genre>
<extra>NA</extra>
<info>1935. Shanghai. El intrépido arqueólogo Indiana Jones, tras meterse en jaleos en un local nocturno, consigue escapar junto a una bella cantante y su joven acompañante. Tras un accidentado vuelo, los tres acaban en la India, donde intentarán ayudar a los habitantes de un pequeño poblado, cuyos niños han sido raptados.</info>
 
 
<page>13</page><title>INDIANA JONES Y LA ULTIMA CRUZADA</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>9ba556924bd4b17a886345aa8997a2b1a7a934c8</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/osKZUgKRUK1jwYMdsmlevK7zZIY.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/267Mdb5i1AXuVN64DhVEOUPmM25.jpg</fanart>
<date>1989</date>
<genre>Aventuras. Acción . Saga</genre>
<extra>NA</extra>
<info>En esta tercera entrega, el padre del protagonista (Harrison Ford), Henry Jones, también arqueólogo (Sean Connery), es secuestrado cuando buscaba el Santo Grial. Indiana tendrá que ir a rescatarlo y, de paso, intentar hacerse con la preciada reliquia, que también ambicionan los naSis.</info>
 
<page>13</page><title>INFIERNO BAJO EL AGUA</title>
<microhd>f83422020c4b0b0d2286ac9e4d8cfb07b066896f</microhd>
<fullhd>61ef4dbf24f43c0bfd85bb58a84dc4564cc62eee</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/vPCQHc1LqLEC2ozuFQngqvc2SIO.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/lm4xH0YwFbVvTgdtau1thNK5S6J.jpg</fanart>
<date>2019</date>
<genre>Terror</genre>
<extra>NA</extra>
<info>Cuando un enorme huracán de categoría 5 llega a su pueblo en Florida, Haley (Kaya Scodelario) se desentiende de las órdenes de evacuación para buscar a su padre (Barry Pepper), que ha desaparecido. Tras encontrarle gravemente herido en el entresuelo de su casa, los dos quedan atrapados por la inundación que cubre rápidamente el terreno. Prácticamente sin tiempo para escapar de la tormenta que arrecia, Haley y su padre descubren que la subida del nivel del agua es el menor de sus problemas...</info>
 
<page>13</page><title>INFILTRADO EN EL KKKLAN</title>
<microhd>862dd1920721995e6402c5d556a24dce386468fe</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/dYVHHvrflaszZ6sy2vmEol5fNuF.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/gMVdhfQ7q9DFHhDkehrququjGPd.jpg</fanart>
<date>2018</date>
<genre>Drama. Comedia</genre>
<extra>NA</extra>
<info>A principios de los años setenta, una época de gran agitación social con la encarnizada lucha por los derechos civiles como telón de fondo, Ron Stallworth se convierte en el primer agente negro del departamento de policía de Colorado Springs. Pero es recibido con escepticismo y hostilidad por los mandos y algunos de los agentes. Sin amedrentarse, decide seguir adelante y hacer algo por su comunidad llevando a cabo una misión muy peligrosa: infiltrarse en el KKKlan y exponerlo ante la ciudad.</info>
 
 
<page>13</page><title>INFILTRADOS</title>
<microhd>53628a70d1cd0d94f779080ba7e5b87cb0b23816</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/mupWl1yjc7S8nvce6KpYksZdlTy.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/j0stmN6ktCpqoWE5nT6xHCyDpwP.jpg</fanart>
<date>2006</date>
<genre>Thriller. Drama. Accion</genre>
<extra>NA</extra>
<info>El Departamento de Policía de Massachussets se enfrenta a la mayor banda de crimen organizado de la ciudad de Boston. La estrategia consiste en acabar desde dentro con Frank Costello, el poderoso jefe de la mafia irlandesa (Jack Nicholson). El encargado de infiltrarse en la banda es un joven novato, Billy Costigan (Leonardo DiCaprio). Mientras Billy intenta ganarse la confianza de Costello, otro joven policía, Colin Sullivan (Matt Damon), sube rápidamente de categoría y ocupa un puesto en la unidad de Investigaciones Especiales, grupo de élite cuya misión también es acabar con Costello. Lo que nadie sabe es que Colin es un topo infiltrado en la policía por el propio Costello. </info>
 
<page>13</page><title>INFINITE</title>
<microhd>cc6beab12f3ab62edd2e899542c6209af5f1f1a2</microhd>
<fullhd>1cdf14d602a62b74d1e78f1756a8d73d25ea9d6c</fullhd>
<tresd>NA</tresd>
<cuatrok>27b0625e40f6178dd4328b2b4afd97a3e85df23c</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/niw2AKHz6XmwiRMLWaoyAOAti0G.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/wjQXZTlFM3PVEUmKf1sUajjygqT.jpg</fanart>
<date>2021</date>
<genre>Ciencia ficción. Thriller</genre>
<extra>NA</extra>
<info>Un hombre descubre que sus alucinaciones son en realidad visiones de vidas pasadas.</info>
 
 
<page>13</page><title>INMUNE</title>
<microhd>3E82BC50DE7F54453696A0F410EB43222224C4A2</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/1H4yFajd6eovh8BQtYbjx9J67mf.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/aqm0i13UOmow0K30gB1hrNYpJca.jpg</fanart>
<date>2020</date>
<genre>Thriller. Drama. Romance. Intriga</genre>
<extra>NA</extra>
<info>Año 2024. Han pasado dos años desde que una pandemia mundial asoló el mundo. El virus COVID23 ha sufrido una nueva y peligrosa mutación con un 50% de mortalidad que obligó a los Gobiernos a ordenar un nuevo confinamiento mundial. Nico (KJ Apa) es un repartidor que ha desarrollado una poco común inmunidad al virus, lo que le permite seguir trabajando. Nico quiere estar junto a su novia Sara (Sofia Carson), pero para ello deberá superar la ley marcial y enfrentarse a gente peligrosa, poderosa y muy desesperada.</info>
 
 
<page>13</page><title>INSIDIOUS</title>
<microhd>3268fa9807f5ac38cabde16977b1341d04bdbce4</microhd>
<fullhd>be1c41c85127ec9250745b2821bb37a9cc64cc0d</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/dVX16rCij8lTt0rAAMctdr0a0S4.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/yZP6GyrgzdpjH1AxPHlb8ACLkiA.jpg</fanart>
<date>2010</date>
<genre>Saga.Terror</genre>
<extra>NA</extra>
<info>Josh (Patrick Wilson), su esposa Renai (Rose Byrne) y sus tres hijos acaban de mudarse a una vieja casa. Pero, tras un desgraciado accidente, uno de los niños entra en coma y, al mismo tiempo, empiezan a producirse en la casa extraños fenómenos que aterrorizan a la familia.</info>
 
 
<page>13</page><title>INSIDIOUS: CAPITULO 2</title>
<microhd>f1d6acb8b2e54b4bb89d6931842b191511e5366e</microhd>
<fullhd>f1d6acb8b2e54b4bb89d6931842b191511e5366e</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/xPjn09um3hvGOfd1zYoUp4aDvMJ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/uwivrZ7vIeP0HNkE5gKavzhZNpG.jpg</fanart>
<date>2013</date>
<genre>Saga.Terror</genre>
<extra>NA</extra>
<info>Josh y Renai Lambert se han mudado con sus hijos a la casa de Lorraine, la madre de Josh, esperando olvidar el pasado y comenzar una nueva vida. Pero pronto la familia empieza a percibir extrañas visiones y fenómenos amenazantes que los obligarán a desentrañar un oscuro secreto.</info>
 
 
<page>13</page><title>INSIDIOUS: CAPITULO 3</title>
<microhd>5eb9bad66fa229e4abfade02529b55a1317116d1</microhd>
<fullhd>7c69dddf79611616e9cd9a9cb67a47d0101d63ee</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/qnVtVZIiQ59MsZHKhxYenjAIBXo.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/r5cMwEIkkGs0jeNDAZCu9kClxMq.jpg</fanart>
<date>2015</date>
<genre>Saga.Terror</genre>
<extra>NA</extra>
<info>Elise Rainier (Lin Shaye) acepta a regañadientes utilizar su capacidad de ponerse en contacto con los muertos a fin de ayudar a una adolescente (Stefanie Scott) que se ha convertido en el blanco de una peligrosa entidad sobrenatural.</info>
 
 
<page>13</page><title>INSIDIOUS: LA PUERTA ROJA</title>
<microhd>NA</microhd>
<fullhd>2cfbc5365e8c055fcbf446b0f8112835b7f43ef2</fullhd>
<tresd>NA</tresd>
<cuatrok>f1d8431c5dfb2018dd10c8a371213fe767545a66</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/wD4eLIHUaTvrXQqAzlfduHQ1NYg.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/sgxrLG0oax0fzsuhlmkorXm1gnC.jpg</fanart>
<date>2023</date>
<genre>Terror. Saga</genre>
<extra>NA</extra>
<info>Para enterrar sus demonios de una vez por todas, Josh Lambert y un ya universitario Dalton Lambert, deberán profundizar en El Mas Allá (The Further) más que nunca, enfrentándose al oscuro pasado familiar y a un huésped de nuevos horrores terroríficos que acechan tras la puerta roja.</info>
 
 
<page>13</page><title>INSIDIOUS: LA ULTIMA LLAVE</title>
<microhd>1bf5de5e0b95739ea9a1696c7470e789a2c85c6e</microhd>
<fullhd>23a66b2452131ad5b0eb369525f1b40f92134ce6</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/wtLB9IRTC9GwbktR3EBFu2Pux8z.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/PwI3EfasE9fVuXsmMu9ffJh0Re.jpg</fanart>
<date>2018</date>
<genre>Terror.Saga</genre>
<extra>NA</extra>
<info>La parapsicóloga Elise Rainier afronta su caso sobrenatural más terrorífico: en su propia casa. Cuarta entrega de la saga 'Insidious'</info>
 
 
<page>13</page><title>INSPECTOR SUN Y LA MALDICIÓN DE LA VIUDA NEGRA</title>
<microhd>NA</microhd>
<fullhd>kl4c5ywrerbygtrro26dfwaww5nyu2vc</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/aGp9ghdIcw74K2VJ1Godcsb9MQo.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/2exCXt0iHOHkbkgT6ErgkKGDD1b.jpg</fanart>
<date>2022</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Shanghái, 1934. En un mundo de insectos paralelo al de los humanos, el veterano Inspector Sun, una araña solitaria, lidera su última misión contra su archienemigo Langosta Roja, antes de embarcarse en un merecido retiro. Sus vacaciones a bordo de un avión Pan Am clipper entre Shanghai y San Francisco acaban abruptamente cuando el millonario Dr. Spindelthorp aparece asesinado. Lo que comienza como un caso rutinario para Sun, decidirá finalmente el destino de la humanidad.</info>
 
 
<page>13</page><title>INSTINTO BASICO</title>
<microhd>3836c98bb7d1b74aa37e668a7246adbcb397652f</microhd>
<fullhd>2248bd22278676cce950f437e1981f968aca1a31</fullhd>
<tresd>NA</tresd>
<cuatrok>6d9298147e3f2d59b31c2115ee67307b34c2bbdc</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/2zfMa4IHBRPVV5zv6pE6epkzoBW.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/23NVxWSXvlCFA60gPqf0xXrAObB.jpg</fanart>
<date>1992</date>
<genre>Thriller. Intriga</genre>
<extra>Culto</extra>
<info>Johnny Boz, antiguo cantante de rock y propietario de un nightclub de San Francisco, aparece brutalmente asesinado en su cama. La última vez que se le vio estaba con su novia, Catherine Tramell, una atractiva escritora de novelas de intriga. El agente Nick Curran, que atraviesa un mal momento, pues acaba de desintoxicarse de su adicción al alcohol y a las drogas, recibe el encargo de vigilar a Catherine, principal sospechosa del crimen.</info>
 
<page>14</page><title>INSTINTO PELIGROSO</title>
<microhd>3fe94eac0b5f8a14efae77559a24686dcba0c541</microhd>
<fullhd>3a04e261fa2c5d3861a06d33d24a31af1b8fdb2c</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ubeZN7kWlr7Bl1OxdBPck43FhD3.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/mo57hzhW3BcZL1f7MNteWKHsmlN.jpg</fanart>
<date>2021</date>
<genre>Acción. Thriller</genre>
<extra>NA</extra>
<info>Un sociópata reformado viaja a una isla remota para investigar el misterio detrás de la desaparición de su hermano, pero pronto termina enfrentándose con más de lo que esperaba.</info>
 
 
<page>14</page><title>INTEMPERIE</title>
<microhd>8663aeb83435b290bf4f7bc2c5b0973132a7253b</microhd>
<fullhd>981c4314e631d4af0feae92e349f57f7e480f6cf</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/nHM2gJJpFbiBpRNBel7u6noAc6L.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/sNfedix0Vt65kjcnwU3zlnk1glI.jpg</fanart>
<date>2019</date>
<genre>Drama. Thriller. Western</genre>
<extra>NA</extra>
<info>Un niño que ha escapado de su pueblo escucha los gritos de los hombres que le buscan. Lo que queda ante él es una llanura infinita y árida que deberá atravesar si quiere alejarse definitivamente del infierno del que huye. Ante el acecho de sus perseguidores al servicio del capataz del pueblo, sus pasos se cruzarán con los de un pastor que le ofrece protección y, a partir de ese momento, ya nada será igual para ninguno de los dos.</info>
 
 
<page>14</page><title>INTERSTELLAR</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>7656f70d19bace86bad4bbbd5ecf866d4a097337</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/fbUwSqYIP0isCiJXey3staY3DNn.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/rAiYTfKGqDCRIIqo664sY9XZIvQ.jpg</fanart>
<date>2014</date>
<genre>Ciencia ficción. Drama. Aventuras</genre>
<extra>Mis</extra>
<info>Al ver que la vida en la Tierra está llegando a su fin, un grupo de exploradores dirigidos por el piloto Cooper (McConaughey) y la científica Amelia (Hathaway) emprende una misión que puede ser la más importante de la historia de la humanidad: viajar más allá de nuestra galaxia para descubrir algún planeta en otra que pueda garantizar el futuro de la raza humana.</info>
 
 
<page>14</page><title>IP MAN 4 : EL FINAL</title>
<microhd>6edd0642a019f70c19dc03b180a115119370dd4a</microhd>
<fullhd>1922917c6d4629327d58ebf3b98013e0df3f12be</fullhd>
<tresd>NA</tresd>
<cuatrok>464a6f5eff3165e328a886ce828c4d65bda4e9cc</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/swmjF0CQc59K3jdUFzOirXESeGN.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/2erlehiUjxU4Aw64lZYsoWOgbwL.jpg</fanart>
<date>2019</date>
<genre>Acción. Drama</genre>
<extra>NA</extra>
<info>El maestro de Kung Fu viaja a los Estados Unidos, donde uno de sus estudiantes ha molestado a la comunidad de artes marciales al abrir una escuela de Wing Chun.</info>
 
 
<page>14</page><title>ISLA LARVA: LA PELICULA</title>
<microhd>a80f8ed9d9b93fba31e432551d248f26fd06de7e</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/uHdxICUIRGoJjK67aOp3ykvL8ZW.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/53aUwtolUZxgzMr2KbDpOnD8jo3.jpg</fanart>
<date>2020</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Al volver a casa, Chuck le cuenta a una reportera cómo es la vida en la isla con sus chiflados amigos Rojo y Amarillo. Secuela de la serie "Larva: Aventuras en la isla"</info>
 
 
<page>14</page><title>IT</title>
<microhd>NA</microhd>
<fullhd>b970fcb16620c8ab486ca23214a6a27e6ff3b3b2</fullhd>
<tresd>NA</tresd>
<cuatrok>b47f2dbb1c67cde2988952098e53b72e2f726f6d</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/sSrj4lnhrb113DOPEPTaO2jaDk3.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/tcheoA2nPATCm2vvXw2hVQoaEFD.jpg</fanart>
<date>2017</date>
<genre>Terror. Fantastico</genre>
<extra>NA</extra>
<info>Cuando empiezan a desparecer niños en el pueblo de Derry (Maine), un pandilla de amigos lidia con sus mayores miedos al enfrentarse a un malvado payaso llamado Pennywise, cuya historia de asesinatos y violencia data de siglos. Adaptación cinematográfica de la conocida novela de Stephen King "It".</info>
 
 
<page>14</page><title>IT CAPITULO 2</title>
<microhd>085b725be40aadfbcd2b231c0c99951c40df380b</microhd>
<fullhd>6e2af09ac5d219920f7d6c3a43ed3e263b390839</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/9oERKIVyTWpHNum3STVsAGD4ojz.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/8moTOzunF7p40oR5XhlDvJckOSW.jpg</fanart>
<date>2019</date>
<genre>Terror. Fantastico</genre>
<extra>NA</extra>
<info>Han pasado casi 30 años desde que el "Club de los Perdedores", formado por Bill, Berverly, Richie, Ben, Eddie, Mike y Stanley, se enfrentaran al macabro y despiadado Pennywise (Bill Skarsgård). En cuanto tuvieron oportunidad, abandonaron el pueblo de Derry, en el estado de Maine, que tantos problemas les había ocasionado. Sin embargo, ahora, siendo adultos, parece que no pueden escapar de su pasado. Todos deberán enfrentarse de nuevo al temible payaso para descubrir si de verdad están preparados para superar sus traumas de la infancia.</info>
 
 
<page>14</page><title>JACK REACHER</title>
<microhd>53db7eb9e4668909deeedefa98a453d4c12be30d</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>f243b80b059590ecb961077ca8e45e22ac5175ad</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/eFqrTyVm4ZalRveBMk86ry6WQpm.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/gE8Dow9HTVYLQkRRik72o9iv9xw.jpg</fanart>
<date>2012</date>
<genre>Acción. Thriller .Saga</genre>
<extra>NA</extra>
<info>Jack Reacher (Tom Cruise), un antiguo policía militar que vive como un vagabundo y que trabaja por su cuenta, decide investigar el caso de un francotirador que ha sido acusado de matar a cinco personas en un tiroteo. Al ser interrogado, el francotirador exige la presencia de Jack Reacher.</info>
 
 
<page>14</page><title>JACK REACHER:  NUNCA VUELVAS LA VISTA ATRÁS</title>
<microhd>df68f616c6b8674fd5f8a9ca66189bccd5d34ea3</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>44fc351b70c0643ff052969e4fc32ad02eb9349c</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/eLzStFuergouErSQlfABthuQHCJ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ww1eIoywghjoMzRLRIcbJLuKnJH.jpg</fanart>
<date>2016</date>
<genre>Acción. Thriller. Saga</genre>
<extra>NA</extra>
<info>La Mayor Susan Turner, líder de la antigua unidad militar de Reacher, es falsamente acusada de traición. Jack Reacher tendrá que sacarla de prisión y descubrir la verdad detrás de una conspiración gubernamental para limpiar sus nombres y salvar sus vidas. Durante el escape, Reacher descubrirá un secreto de su pasado que podría cambiar su vida para siempre. Secuela de "Jack Reacher" (2012).</info>
 
<page>14</page><title>JACKAZS FOREVER</title>
<microhd>8d23d096621d7b79d1886ad809c9a980800830ad</microhd>
<fullhd>2b9b9449efacf7278e9d6d6c73241f4d95ec8490</fullhd>
<tresd>NA</tresd>
<cuatrok>42e59b7e728b7715d02fd2a6304e0733d29e9bad</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/mlOXyDGxqBGpEmzV9LPQMudMgng.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/uCDXCSet5nwV9HdVwcvfaoB5UA2.jpg</fanart>
<date>2022</date>
<genre>Comedia </genre>
<extra>tt11466222</extra>
<info>El equipo de "Burro por Siempre" ha vuelto para su cruzada final. Johnny Knoxville, Steve-O y el resto de la pandilla regresan para otra ronda de divertidas, salvajemente absurdas y a menudo, peligrosos retos.</info>
 
<page>14</page><title>JACKIE BROWN</title>
<microhd>NA</microhd>
<fullhd>ab9e9d73d0f34f14ea459383456697db9cba638b</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/l6bCO7BhJJCQehaEqHc4fx80kiG.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/6fPdTjpVOwkNyQBYWXL8JvG7H2H.jpg</fanart>
<date>1997</date>
<genre>Thriller. Drama</genre>
<extra>Mis</extra>
<info>Jackie Brown (Pam Grier) es una azafata de vuelo que necesita dinero y hace de correo para Robbie, un mafioso buscado por la policía. Un día es sorprendida en la aduana y acusada de tráfico de drogas y evasión de capital. Sólo podrá evitar su ingreso en prisión, si acepta una propuesta de la policía: ayudarles a llegar hasta Robbie.</info>
 
 
<page>14</page><title>JAMES BOND: CASINO ROYALE</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>d6d2648c270ef3068c88cb8da2194947af41588e</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ta2BX3THwYXytWuVVozaT0NsMM8.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/sbdSz5nOtPSV7FZKFf8SjPN3hy6.jpg</fanart>
<date>2006</date>
<genre>Acción. Thriller. Saga</genre>
<extra>NA</extra>
<info>La primera misión del agente británico James Bond (Daniel Craig) como agente 007 lo lleva hasta Le Chiffre (Mads Mikkelsen), banquero de los terroristas de todo el mundo. Para detenerlo y desmantelar la red terrorista, Bond debe derrotarlo en una arriesgada partida de póquer en el Casino Royale. Al principio a Bond le disgusta Vesper Lynd (Eva Green), la hermosa oficial del Tesoro que debe vigilar el dinero del gobierno. Pero, a medida que Bond y Vesper se ven obligados a defenderse juntos de los mortales ataques de Le Chiffre y sus secuaces, nace entre ellos una atracción mutua.</info>
 
 
<page>14</page><title>JAMES BOND: QUANTUM OF SOLACE</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>82c914e06f6f4584c750492922d9d3628e603048</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/f2rCskvSSr7ywPAnFGhk4FT6Piz.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/wHeArLnzZpv2UeHa9SoAKeVhQhX.jpg</fanart>
<date>2008</date>
<genre>Acción. Saga</genre>
<extra>NA</extra>
<info>Traicionado por Vesper, la mujer a la que amaba, 007 se plantea su nueva misión como algo personal. Durante su investigación, Bond y M interrogan a Mr. White, que les revela que la organización que chantajeó a Vesper es mucho más compleja y peligrosa de lo que imaginan. El servicio de inteligencia forense vincula a un traidor del Mi6 con una cuenta bancaria en Haití, donde Bond conoce a la bella pero combativa Camille, una mujer que tiene sus propios motivos para vengarse. Camille pone a Bond tras la pista de Dominic Greene, un despiadado hombre de negocios y miembro importante de la misteriosa organización. Bond averigua que el objetivo de Green es controlar uno de los recursos naturales más importantes del mundo; pero, para ello, tiene que derrocar al gobierno de un país sudamericano.</info>
 
 
<page>14</page><title>JAMES BOND: SKYFALL</title>
<microhd>NA</microhd>
<fullhd>78e3466b9939e0551661a52ff3024a964b698c86</fullhd>
<tresd>NA</tresd>
<cuatrok>a3192d9701eaed05d3389eac063616d8405f2b0d</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/5cHyNrZzi5txPyseWEIba9WK7NA.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/68uaSouhcLaEhqCW1iuZORgrRPi.jpg</fanart>
<date>2012</date>
<genre>Acción. Thriller.Saga</genre>
<extra>NA</extra>
<info>La lealtad de James Bond (Daniel Craig), el mejor agente de los servicios secretos británicos, por su superiora M (Judi Dench) se verá puesta a prueba cuando episodios del pasado de ella vuelven para atormentarla. Al mismo tiempo, el MI6 sufre un ataque, y 007 tendrá que localizar y destruir el grave peligro que representa el villano Silva (Javier Bardem). Para conseguirlo contará con la ayuda de la agente Eve (Naomie Harris). </info>
 
 
<page>14</page><title>JAMES BOND: SPECTRE</title>
<microhd>aa0b0862435e27aef9efb97efb0a4903c723a6c6</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>dd39c2f1636a1795bebfa28132485291d74bc795</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/672kUEMtTHcaVYSVY4eiHEliHFa.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/2MHguLY6RoCduvRADUBJzQWPhDO.jpg</fanart>
<date>2015</date>
<genre>Acción. Aventuras. Thriller. Saga</genre>
<extra>NA</extra>
<info>James Bond recibe un críptico mensaje del pasado que le confía una misión secreta que lo lleva a México D.F. y a Roma, donde conoce a Lucía Sciarra, la hermosa viuda de un infame criminal. Bond se infiltra en una reunión secreta y descubre la existencia de una siniestra organización conocida como SPECTRE. Mientras tanto, en Londres, el nuevo director del Centro para la Seguridad Nacional cuestiona las acciones de Bond y pone en duda la importancia del MI6, encabezado por M. De modo encubierto Bond recluta a dos colaboradores para que le ayuden a encontrar a Madeleine Swann, la hija de su gran enemigo, el Sr. White, pues quizá tenga la clave para desentrañar el misterio de SPECTRE. A medida que Bond avanza en su investigación, descubre una estremecedora conexión entre él y el enemigo que busca.</info>
 
 
<page>14</page><title>JAULA</title>
<microhd>NA</microhd>
<fullhd>c3fc18e1ec30316b4ddee4c5fc6fbaf540ed3e06</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/nOmLxwOnUFL5NQSCSY9cwtXFR1m.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/54x6WfreK5gIr6VlXpQYB7u7e1b.jpg</fanart>
<date>2022</date>
<genre>Intriga. Thriller. Drama</genre>
<extra>NA</extra>
<info>Paula (Elena Anaya) y su marido (Pablo Molinero) regresan en coche de una cena. Repentinamente, se topan con una niña (Eva Tennear) deambulando sola por la carretera. Dos semanas más tarde, tras conocer que nadie la reclama, deciden acogerla en su casa temporalmente y así también dar un giro a su vida de pareja. No será fácil, ya que la niña vive obsesionada con la fantasía de un monstruo que la castigará si sale de un cuadrado de tiza pintado en el suelo. Tras el fuerte vínculo que crean ambas, Paula iniciará un viaje por caminos oscuros para intentar descubrir el enigma del pasado de la niña.</info>
 
 
<page>14</page><title>JINETES DE LA JUSTICIA</title>
<microhd>a94d7de04594f044e9763a3de0e097b667c8237b</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/2SfoAbAcXCcNb1V3GpPPJG6OLFB.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/3LFIil3YCVpyajp8nxJ9XvfNupv.jpg</fanart>
<date>2020</date>
<genre>Thriller. Comedia. Drama</genre>
<extra>NA</extra>
<info>El militar Markus debe regresar a casa con su hija adolescente, Mathilde, cuando su esposa muere en un trágico accidente de tren. Todo parece ser a causa de la mala suerte, hasta que Otto, experto en matemáticas y también pasajero de tren siniestrado, aparece con sus dos excéntricos colegas, Lennart y Emmenthaler. Otto está convencido de que alguien está detrás de todo esto.</info>
 
 
<page>14</page><title>JOHN WICK: CAPITULO 3 - PARABELLUM</title>
<microhd>c047c3d3034b8ab01cbcf795de781bd4a7ea8b38</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/38d32uG1x7iiN2jdK0cRX0Bk423.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/jEjyjX4izUucoq7A9jmbcT6q3e1.jpg</fanart>
<date>2019</date>
<genre>Saga. Accion. Thriller</genre>
<extra>NA</extra>
<info>John Wick (Keanu Reeves) regresa a la acción, solo que esta vez con una recompensa de 14 millones de dólares sobre su cabeza y con un ejército de mercenarios intentando darle caza. Tras asesinar a uno de los miembros del gremio de asesinos al que pertenecía, Wick es expulsado de la organización, pasando a convertirse en el centro de atención de multitud de asesinos a sueldo que esperan detrás de cada esquina para tratar de deshacerse de él. </info>
 
 
<page>14</page><title>JOHN WICK: CAPITULO CUARTO</title>
<microhd>d002d3a5f016b2586e6b8c4bdc1210a386940761</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/mj2Z9HnRSIEk3n7yVPoOY4Uzzfh.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/h8gHn0OzBoaefsYseUByqsmEDMY.jpg</fanart>
<date>2023</date>
<genre>Acción. Thriller. Saga</genre>
<extra>NA</extra>
<info>John Wick, legendario asesino retirado, vuelve de nuevo a la acción impulsado por una incontrolable búsqueda de venganza. Al tener que luchar contra asesinos sedientos de sangre que le persiguen, John tendrá que llevar sus habilidades al límite si quiere salir esta vez con vida. </info>
 
 
<page>14</page><title>JOHN WICK: OTRO DIA PARA MORIR</title>
<microhd>2cfa9a53a8167043da1fe8e2ccc13e049aad90ba</microhd>
<fullhd>a45246df0cf8d4ae83d3790cd155789a0c5922a8</fullhd>
<tresd>NA</tresd>
<cuatrok>513c8f4e72226ced8961e015ba5a489c0c757862</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/4A9KmccvMqJA8MQzIes0G9Uobh5.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ff2ti5DkA9UYLzyqhQfI2kZqEuh.jpg</fanart>
<date>2014</date>
<genre>Accion. Thriller.Saga</genre>
<extra>NA</extra>
<info>En Nueva York, John Wick, un asesino a sueldo retirado, vuelve otra vez a la acción para vengarse de los gángsters que le quitaron todo</info>
 
 
<page>14</page><title>JOHN WICK: PACTO DE SANGRE</title>
<microhd>ca4e57f281e52c3c0b0bf3cfa55cd7628f88f10c</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/kTvpQimqkTXPYvP4F4t8nGqMYg6.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/fRLftkwv8jm3DLxXOf2NbngHjPB.jpg</fanart>
<date>2017</date>
<genre>Accion. Thriller.Saga</genre>
<extra>NA</extra>
<info>El legendario asesino John Wick (Keanu Reeves) se ve obligado a salir del retiro por un ex-asociado que planea obtener el control de un misterioso grupo internacional de asesinos. Obligado a ayudarlo por un juramento de sangre, John emprende un viaje a Roma lleno de adrenalina estremecedora para pelear contra los asesinos más peligrosos del mundo.</info>
 
 
<page>14</page><title>JOHNNY ENGLISH DE NUEVO EN ACCION</title>
<microhd>NA</microhd>
<fullhd>7d2b0b63344b44f7e7672c67891951ed11b410c6</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/2TFVgCZkOUF0JhPK2O35NmV42TO.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/yCOLqh5MOGyYdo58Ap0aWvKop9h.jpg</fanart>
<date>2018</date>
<genre>Comedia. Accion</genre>
<extra>NA</extra>
<info>Cuando un ciberataque revela la identidad de todos los agentes secretos en activo de Reino Unido, Johnny English se convierte en la única esperanza del servicio secreto. Para para encontrar al hacker, esto fuerza su regreso después de retirarse, pero como sus habilidades son bastante limitadas English tendrá que esforzarse para superar los desafíos tecnológicos de la era moderna.</info>
 
 
<page>14</page><title>JOJO RABBIT</title>
<microhd>8263a00aa1cea948e33f0256944636dcdabadd8b</microhd>
<fullhd>8c446fb5853723c72eb4c4b66ecc2b7eeaa10db9</fullhd>
<tresd>NA</tresd>
<cuatrok>d6b70dda27224d37f028809d4fb45290385c84e3</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/1mqL7VG4Ix8wmxwypmCA1HTHBky.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/uSYkZt12ewdg8eT4aOsAnh22IYw.jpg</fanart>
<date>2019</date>
<genre>Comedia. Drama</genre>
<extra>NA</extra>
<info>Jojo "Rabbit" Betzler (Roman Griffin Davis) es un solitario niño alemán perteneciente a las Juventudes Hitlerianas que ve su mundo puesto patas arriba cuando descubre que su joven madre Rosie (Scarlett Johansson) esconde en su ático a una niña judía (Thomasin McKenzie). Con la única ayuda de su mejor amigo imaginario, el mismísimo Adolf Hitler (Taika Waititi), Jojo deberá afrontar su ciego nacionalismo con las contradicciones de una guerra absurda.</info>
 
 
<page>14</page><title>JOKER</title>
<microhd>fbc594b0409fb6767d41240793228e652ce51c09</microhd>
<fullhd>68e197aaa808f5cf09657e0269149773b5216009</fullhd>
<tresd>NA</tresd>
<cuatrok>537df65fadd0caee3301858927bd583dbe0abc8b</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/tgcrYiyG75iDcyk3en9NzZis0dh.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/qR1yyZe3X14opqBmeHRLXmfVcot.jpg</fanart>
<date>2019</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Arthur Fleck (Phoenix) vive en Gotham con su madre, y su única motivación en la vida es hacer reír a la gente. Actúa haciendo de payaso en pequeños trabajos, pero tiene problemas mentales que hacen que la gente le vea como un bicho raro. Su gran sueño es actuar como cómico delante del público, pero una serie de trágicos acontecimientos le hará ir incrementando su ira contra una sociedad que le ignora.</info>
 
 
<page>14</page><title>JOLT</title>
<microhd>9627b4895bb1796dce8053fa408edd00150068e7</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>8451cf916b080f6d01dd5d5711f21c266b19cb72</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/kprsFE8m4NgRstmr3BzTyxZx7Pp.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/wPjtacig0kIkVcTQmXoNt6jbMwo.jpg</fanart>
<date>2021</date>
<genre>Accion</genre>
<extra>NA</extra>
<info>Lindy es una mujer con un doloroso secreto: debido a un raro trastorno neurológico crónico, experimenta esporádicos impulsos de ira extremadamente violentos que solo pueden detenerse cuando se aplica una descarga a sí misma con un dispositivo especial de electrodos. Aunque es incapaz de encontrar el amor y de encajar en un mundo que teme su extraña condición, por fin logra confiar lo suficiente en un hombre como para enamorarse, solo para encontrarlo muerto al día siguiente. Desconsolada y enfurecida, se embarca en una misión de venganza para encontrar a su asesino, mientras es perseguida por la policía como principal sospechosa del crimen.</info>
 
 
<page>14</page><title>JUDAS Y EL MESIAS NEGRO</title>
<microhd>9b0cbe420b0a668d10ed5e662f96046bfc23010a</microhd>
<fullhd>facd79915705305c13dd183453948ea2a9728b7d</fullhd>
<tresd>NA</tresd>
<cuatrok>d79551d21c302282fb5553bc9c2d59af8515f106</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/iIgr75GoqFxe1X5Wz9siOODGe9u.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/fatz1aegtBGh7KS0gipcsw9MqUn.jpg</fanart>
<date>2021</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Fred Hampton tenía 21 años cuando fue asesinado por el FBI, quien coaccionó a un delincuente menor llamado William O'Neal para ayudarlos a silenciarlo a él y al Partido Pantera Negra. Pero no pudieron matar el legado de Fred Hampton.</info>
 
 
<page>14</page><title>JUDY AND PUNCH</title>
<microhd>c576ddd76cea112f711c78ca65db5b5296d42786</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/907CTQB72oca77tbWhDhg22yTsX.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ao9UJkUs7hcIkqv02BquWxP49RR.jpg</fanart>
<date>2019</date>
<genre>Drama. Comedia</genre>
<extra>NA</extra>
<info>En el anárquico pueblo de Seaside, los marionetistas Judy (Mia Wasikowska) y Punch (Damon Herriman) hacen todo lo posible por volver a tener éxito con su espectáculo. Aunque el show no tarda en convertirse en un éxito gracias a las increíbles habilidades de Judy en su oficio, la desmedida ambición de Punch y su gusto por el alcohol dan como resultado una inevitable tragedia... De la que Judy decide vengarse. </info>
 
 
<page>14</page><title>JUEGO DE ESPIAS</title>
<microhd>f8012f2ae8f1f75a69afb4bb06523441a98add50</microhd>
<fullhd>6e27f59f64df761404a33d60b784a907b1effcf1</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/dhlpHGLpIeQCOTKFMRGTC95sW5a.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/bFxGD1TKrJq3Dr5Pjnn739gBhQF.jpg</fanart>
<date>2020</date>
<genre>Comedia. Infantil</genre>
<extra>NA</extra>
<info>Un operativo de la CIA que cree no tener sentimientos (Dave Bautista) queda a merced de una niña de 9 años (Kristen Schaal) después de que se le asignase la misión de vigilar a su familia.</info>
 
 
<page>14</page><title>JUEGO DE LADRONES. EL ATRACO PERFECTO</title>
<microhd>b0f16572ed918bdd72613ec55d41cb098c094ac9</microhd>
<fullhd>960b7db20c159cd9a2fae89fb146ef0a06b734c9</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/4OAnIfMibW3GeNNItTqL2ynuJqj.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/s3FDBLH4qc1IcjexB05Qvbn3wxO.jpg</fanart>
<date>2018</date>
<genre>Thriller. Accion. Drama</genre>
<extra>NA</extra>
<info>Todos los días, el banco de la Reserva Federal de Los Ángeles saca de la circulación 120 millones de dólares en efectivo. Un grupo de experimentados ladrones planea su gran atraco final: robar esos 120 millones, pero el departamento del Sheriff de Los Ángeles, la brigada más temida de la ciudad liderada por 'El gran Nick' (Gerard Butler), no está dispuesto a ponérselo fácil.</info>
 
 
<page>14</page><title>JUMANJI BIENVENIDOS A LA JUNGLA</title>
<microhd>a32207de7f0ef8062fe7836f59aa01e96f518f5c</microhd>
<fullhd>NA</fullhd>
<tresd>04a33c2f032bb8662010c2c485507f9d49544fed</tresd>
<cuatrok>5a8a48179c83344e55a45e295b682de9e6c35115</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/1uQaSgtHyTN3r2fecL0mSs6geQO.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/cpz070zEKbPGXzCWuQsNt42PqXY.jpg</fanart>
<date>2017</date>
<genre>Aventuras. Fantastico. Comedia</genre>
<extra>NA</extra>
<info>Cuatro adolescentes son absorbidos por un videojuego, en el que se convierten en avatares de personajes arquetípicos. Allí vivirán múltiples aventuras, al tiempo que buscan cómo salir de allí para volver a su mundo.</info>
 
 
<page>14</page><title>JUMANJI: SIGUIENTE NIVEL</title>
<microhd>97a0d7242e40016ec4aa8a8fd4f01c589f11c582</microhd>
<fullhd>2d30f363f42c54c5ad278af6d9bdae05d318553a</fullhd>
<tresd>NA</tresd>
<cuatrok>bcec5944bead6b3e48d3522f61862bea7f082483</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/b1bTpxh0lRfw7kwRrWPeMOo7jbY.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/zTxHf9iIOCqRbxvl8W5QYKrsMLq.jpg</fanart>
<date>2019</date>
<genre>Aventuras. Fantastico. Comedia. Accion</genre>
<extra>NA</extra>
<info>En esta ocasión, los 'jugadores' vuelven al juego, pero sus personajes se han intercambiado entre sí, lo que ofrece un curioso plantel: los mismos héroes con distinta apariencia. Pero, ¿dónde está el resto de la gente? Los participantes sólo tienen una opción: jugar una vez más a esta peligrosa partida para descubrir qué es realmente lo que está sucediendo. </info>
 
 
<page>14</page><title>JUNGLE CRUISE</title>
<microhd>22d5c52dc892952c837397d036deade8c4073b51</microhd>
<fullhd>a8b17cc8eedb3f4ebbbce8dcbc209ea769601e84</fullhd>
<tresd>NA</tresd>
<cuatrok>21c4dd290be908a8a3e5243cec99fd96858a27bd</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/dtD417HuGTwjYtBILXZqObweL51.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/7WJjFviFBffEJvkAms4uWwbcVUk.jpg</fanart>
<date>2021</date>
<genre>Aventuras. Fantástico. Comedia</genre>
<extra>NA</extra>
<info>Principios del siglo XX. Frank (Dwayne Johnson) es el carismático capitán de una peculiar embarcación que recorre la selva amazónica. Allí, a pesar de los peligros que el río Amazonas les tiene preparados, Frank llevará en su barco a la científica Lily Houghton (Emily Blunt) y a su hermano McGregor Houghton (Jack Whitehall). Su misión será encontrar un árbol místico que podría tener poderes curativos. Claro que su objetivo no será fácil, y en su aventura se encontrarán con toda clase de dificultades, además de una expedición alemana que busca también este árbol con propiedades curativas. Película basada en una atracción de Disneylandia.</info>
 
 
<page>14</page><title>JURASSIC PARK</title>
<microhd>NA</microhd>
<fullhd>a27b707a3079b4e816d38f40ad5418cb4e7d846f</fullhd>
<tresd>9089c4e827ca6211e5d9b724548360db9898920a</tresd>
<cuatrok>8f643acc68da0515794bb9f3c3a1a2b0e5e342ea</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/1r8TWaAExHbFRzyqT3Vcbq1XZQb.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/95QcOsRGRzJ2cLaqaR4pEfwjezF.jpg</fanart>
<date>1993</date>
<genre>Ciencia ficcion. Aventuras.Saga</genre>
<extra>Culto</extra>
<info>El multimillonario John Hammond consigue hacer realidad su sueño de clonar dinosaurios del Jurásico y crear con ellos un parque temático en una isla remota. Antes de abrirlo al público, invita a una pareja de eminentes científicos y a un matemático para que comprueben la viabilidad del proyecto. Pero las medidas de seguridad del parque no prevén el instinto de supervivencia de la madre naturaleza ni la codicia humana.</info>
 
 
<page>14</page><title>JURASSIC PARK: EL MUNDO PERDIDO</title>
<microhd>31c8da670d3456e3e9e6bd988769d0dabef26624</microhd>
<fullhd>48974CE5BC091F69B2C22449059CE5F1C50FB98A</fullhd>
<tresd>c29730b9c89719c9b0750a62cfebd8e4e8ccb445</tresd>
<cuatrok>90fcf707912c9e7a08a63e7a05e19277d6aafe83</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/zeNCIgB4cfAN7dKRsCuDeLIgrYy.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/w9rBhYquLHVnzBnekQQBofTpXcR.jpg</fanart>
<date>1997</date>
<genre>Ciencia ficcion. Aventuras. Fantastico.Saga</genre>
<extra>NA</extra>
<info>Cuatro años después del desastre ocurrido en el Parque Jurásico de la isla Nublar, John Hammond revela a Ian Malcolm que existe otra isla en la que se criaban los dinosaurios antes de ser transportados a la isla Nublar. Ian, acompañado por dos expertos, deberá acudir a la isla Sorna para rescatar a una científica, aunque los planes cambiarán drásticamente.</info>
 
 
<page>14</page><title>JURASSIC PARK: JURASSIC PARK 3</title>
<microhd>NA</microhd>
<fullhd>878ead18a4db830c2496f4a31f70f477a9c8904f</fullhd>
<tresd>NA</tresd>
<cuatrok>f2d453095e3ef515c8dd30657800e434315087bd</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/5GM4Nqc79PFE3IOqNKwmOxAWkX1.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/rl2mFCm04GqHOz3RjSzrXqWB3M9.jpg</fanart>
<date>2001</date>
<genre>Ciencia ficcion. Aventuras. Fantastico.Saga</genre>
<extra>NA</extra>
<info>El doctor Alan Grant, ansioso por conseguir fondos que financien su estudio sobre la inteligencia del velociraptor, acepta la oferta de una pareja de millonarios, Paul y Amanda Kirby, para sobrevolar la Isla Sorna (Costa Rica), poblada por dinosaurios creados genéticamente. Tras un aterrizaje forzoso en la isla, Alan descubre que los Kirby estaban buscando a su hijo adolescente, perdido en la isla tras un accidente de parapente.</info>
 
 
<page>14</page><title>JURASSIC WORLD EL REINO CAIDO</title>
<microhd>31c8da670d3456e3e9e6bd988769d0dabef26624</microhd>
<fullhd>35ef02121fb76ef5591dd7d26db5b59030c2aeca</fullhd>
<tresd>c29730b9c89719c9b0750a62cfebd8e4e8ccb445</tresd>
<cuatrok>d4d115f19a1aa292c56681f49178ed807bdcff59</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/zeNCIgB4cfAN7dKRsCuDeLIgrYy.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/3s9O5af2xWKWR5JzP2iJZpZeQQg.jpg</fanart>
<date>2018</date>
<genre>Ciencia ficcion. Aventuras. Accion</genre>
<extra>NA</extra>
<info>Una erupción volcánica amenaza a los dinosaurios restantes en la Isla Nublar, donde las criaturas han vagado libremente durante años tras de la desaparición del parque temático "Jurassic World". Claire Dearing, ex gerente del parque, ahora fundó el Grupo de Protección de Dinosaurios, una organización dedicada a intentar preservarlos. Cuando a Claire le ofrecen la oportunidad de rescatar algunos ejemplares de la isla, acude a Owen Grady, el ex entrenador de dinosaurios que trabajó en el parque, para que la ayude a evitar la extinción de los dinosaurios... Secuela de "Jurassic World".</info>
 
 
<page>14</page><title>JURASSIC WORLD: DOMINION</title>
<microhd>NA</microhd>
<fullhd>c1717bb6d7c1d3cbc368f69070faa2693b6eb158</fullhd>
<tresd>NA</tresd>
<cuatrok>d49c315b32b422f0b32285a2a9ceed5a5b96fbff</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/vyTTjx1aHU9SOcEyF4kdcufjKqx.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/7bhvI1tM7JBmqP8HSevIsebSBbh.jpg</fanart>
<date>2022</date>
<genre>Ciencia ficción. Acción. Fantástico. Aventuras. Saga</genre>
<extra>NA</extra>
<info>Cuatro años después de la destrucción de Isla Nublar, los dinosaurios ahora conviven -y cazan- con los seres humanos en todo el mundo. Este frágil equilibrio remodelará el futuro y determinará, de una vez por todas, si los seres humanos seguirán en la cúspide de los depredadores en un planeta que comparten con los animales más temibles de la creación. </info>
 
 
<page>14</page><title>KEEPERS, EL MISTERIO DEL FARO</title>
<microhd>399cef40970710a25ebddcae08f9161efe48223e</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/lXSiEbWSk7RqZQtwobZNTZKp8IH.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/zqftCJ83Gw3jWZFYZrf0nowDuXB.jpg</fanart>
<date>2019</date>
<genre>Thriller. Intriga</genre>
<extra>NA</extra>
<info>Tres fareros llegan a una isla despoblada. Thomas, James y Donald comienzan sus tareas en el faro hasta que un día topan con algo extraño, algo que no es exactamente lo que un farero debería vigilar. Las cosas se complican cuando avistan un barco que podría tener todas las respuestas, desatando una serie de sucesos progresivamente trágicos. Basada en la leyenda de la isla de Flannan.</info>
 
 
<page>14</page><title>KENSHIN, EL GUERRERO SAMURAI: EL FINAL</title>
<microhd>31330c05d90546d78675b968a54b92c8f6256d16</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/w4eGNABnQLA38Lg7agGk9Agpaj5.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/d17qispxVlOb1TLXMCBhLfe641W.jpg</fanart>
<date>2021</date>
<genre>Acción. Drama</genre>
<extra>NA</extra>
<info>Kenshin Himura (Takeru Satoh) es un legendario espadachín. Tras la Restauración Meiji, ha dejado de matar con la espada. Intenta llevar una vida tranquila con Kaoru Kamiya que dirige una escuela de espadachines en el pueblo. Todo cambia cuando el restaurante Akabeko, que es el lugar favorito de Kenshin para comer, es destruido y él encuentra una nota en la que pone "Junchu".</info>
 
 
<page>14</page><title>KILL BILL: VOLUMEN 1</title>
<microhd>NA</microhd>
<fullhd>3b0a673e5c2767051b42c79f87b4f68dd906f87e</fullhd>
<tresd>NA</tresd>
<cuatrok>4334ea87fdc8f3b3cfa3827c4eef74762758a330</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/lfj709InbmljVqAXgUk2qjnujNN.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/fQ1IUeZCCLq7vI5xtylj6bHIuZF.jpg</fanart>
<date>2003</date>
<genre>Acción. Thriller</genre>
<extra>Culto</extra>
<info>El día de su boda, una asesina profesional (Thurman) sufre el ataque de algunos miembros de su propia banda, que obedecen las órdenes de Bill (David Carradine), el jefe de la organización criminal. Logra sobrevivir al ataque, aunque queda en coma. Cuatro años después despierta dominada por un gran deseo de venganza.</info>
 
 
<page>14</page><title>KILL BILL: VOLUMEN 2</title>
<microhd>NA</microhd>
<fullhd>184dfc49a6c348b035e616b5061286f1f724be75</fullhd>
<tresd>NA</tresd>
<cuatrok>af4b0cc551dcdcc83a865cac56ad281817ac2136</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/oEbYyyl3w2dptDZCfXr8NHEHFkl.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/e5BEz8Jq7xR2kzWab1GllAlLUf7.jpg</fanart>
<date>2004</date>
<genre>Acción. Thriller</genre>
<extra>Culto</extra>
<info>Tras eliminar a algunos miembros de la banda que intentaron asesinarla el día de su boda, "Mamba Negra" (Uma Thurman) intenta acabar con los demás, especialmente con Bill, su antiguo jefe (David Carradine), que la había dado por muerta.</info>
 
 
<page>14</page><title>KILLERMAN</title>
<microhd>ba72e9c46b5da9128e28e9ae1e821f5d1aca25ec</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/3KhJs7tFazpMfhOJLGbo0dbw9ku.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/sKppjHk5ZiNIMcl6Tnb44ZgoAXG.jpg</fanart>
<date>2019</date>
<genre>Acción. Drama</genre>
<extra>NA</extra>
<info>En Nueva York, Moe Diamond (Liam Hemsworth) se dedica al blanqueo de dinero negro. Un día se verá buscando respuestas desesperadamente tras despertarse sin memoria, con un montón de dinero y drogas y un violento grupo de policías corruptos persiguiéndole</info>
 
 
<page>14</page><title>KIMI</title>
<microhd>NA</microhd>
<fullhd>ab3873157f9f529bb72c3cd78de14e07778d2e26</fullhd>
<tresd>NA</tresd>
<cuatrok>d7e4e18e723efe76d1ccd79ec69543e6bca8effe</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/im91Ho4WkhVroehZJiIUoOF6UIl.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/mruT954ve6P1zquaRs6XG0hA5k9.jpg</fanart>
<date>2022</date>
<genre>Thriller. Intriga. Drama</genre>
<extra>NA</extra>
<info>La historia se centra en una trabajadora de tecnología agorafóbica que descubre evidencia registrada de un violento crimen durante una revisión del flujo de datos. Después de ser golpeada con resistencia y burocracia por el cambio de mando de su compañía, determina combatir sus miedos para involucrarse.</info>
 
 
<page>14</page><title>KIN</title>
<microhd>7326449961d9dfe3983b8e3cbc8a417b9d96e1bb</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/prR1AWvPHFKltNqoSMXMSEH2lKI.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/4QzljPSmLLjncdplcZeFdpZ0Bcm.jpg</fanart>
<date>2018</date>
<genre>Ciencia ficcion. Accion. Thriller. Drama</genre>
<extra>NA</extra>
<info>Perseguidos por un criminal vengativo (James Franco) y una banda de soldados sobrenaturales,​ un exconvicto recién liberado (Jack Reynor) y su hermano adolescente adoptado (Myles Truitt) se ven obligados a escapar con un arma de origen misterioso que es su única protección.</info>
 
 
<page>14</page><title>KINGS</title>
<microhd>1065c5de4c22a1ccc8055a9b943df8425631e689</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/sEQIshX0y64hYjjDbE1KqevW04e.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/qTRg8UGaf4KZDleEKVGomv2T4tk.jpg</fanart>
<date>2017</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>En 1992, una madre de familia afroamericana vive en el distrito de South Central, donde hay grandes disturbios después de la absolución de los policías blancos acusados ​​de golpear a Rodney King.</info>
 
 
<page>14</page><title>KINGSMAN EL CIRCULO DE ORO</title>
<microhd>e387f3cc4c4e9e0c8d179bd97322c1429143552e</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>b1fdaa358e559e27a9014063d239cf2c0da03ca4</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/gYdCXdyWCeHIrJUwaTWltItEzah.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/uExPmkOHJySrbJyJDJylHDqaT58.jpg</fanart>
<date>2017</date>
<genre>Thriller. Accion. Comedia</genre>
<extra>NA</extra>
<info>Cuando el cuartel general de la agencia secreta es destruido, se descubre una organización de espionaje aliada en EE.UU. llamada Statesman, cuyo origen se remonta a la fecha en que ambas fueron fundadas. En una nueva aventura que pone a prueba la fuerza y el ingenio de sus agentes, ambas organizaciones secretas de élite aúnan sus esfuerzos para intentar derrotar a su enemigo común y salvar al mundo... algo que está convirtiéndose en una especie de hábito para Eggsy.</info>
 
 
<page>14</page><title>KONG LA ISLA CALAVERA</title>
<microhd>117e482ac44571f1c5e4cf85b6404513c7aefc2f</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/myOdovPayRRitZDZSutI07eNANM.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/I2AupZs0YNqLFL2cyetGon91OL.jpg</fanart>
<date>2017</date>
<genre>Aventuras. Fantastico. Accion</genre>
<extra>NA</extra>
<info>En los años 70, un variopinto grupo de exploradores y soldados es reclutado para viajar a una misteriosa isla del Pacífico. Entre ellos están el capitán James Conrad (Tom Hiddleston), el teniente coronel Packard (Samuel L. Jackson) y una fotoperiodista (Brie Larson). Pero al adentrarse en esta bella pero traicionera isla, los exploradores encontrarán algo absolutamente sorprendente. Sin saberlo, estarán invadiendo los dominios del mítico Kong, el gorila gigante rey de esta isla. Será Marlow (John C. Reilly), un peculiar habitante del lugar, quien les enseñe los secretos de Isla Calavera, además del resto de seres monstruosos que la habitan.</info>
 
 
<page>14</page><title>LA ABUELA</title>
<microhd>c7681ec53efbc270d73ffa30e139785c149fa194</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/cdzUKycvzn0X6mQMxDZP9el81lz.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/An1nKjXahrChfEbZ3MAE8fsiut1.jpg</fanart>
<date>2022</date>
<genre>Terror. Fantástico</genre>
<extra>NA</extra>
<info>Susana (Almudena Amor) tiene que dejar su vida en París, donde trabaja como modelo, para regresar a Madrid, debido a que su abuela Pilar (Vera Valdez) acaba de sufrir un derrame cerebral. Años atrás, cuando los padres de Susana murieron, su abuela la crió como si fuese su propia hija. Susana necesita encontrar a alguien que cuide de Pilar, pero lo que deberían ser solo unos días con su abuela se acabarán convirtiendo en una terrorífica pesadilla</info>
 
 
<page>14</page><title>LA ALFOMBRA MAGICA</title>
<microhd>f4d62dcc2e12ead7ac3a087273e3d370233df782</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/9JGRCYbsQ2EX4f417I7inI1MBVt.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/9Tm67GNnHR6HyLw70qwnkaZ8wcf.jpg</fanart>
<date>2018</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Hodja es un niño de Pjort, que toma prestada una alfombra voladora para ver el mundo. A cambio, tiene que encontrar y devolver el "diamante" a su abuelo.</info>
 
 
<page>14</page><title>LA APARIENCIA DE LAS COSAS</title>
<microhd>NA</microhd>
<fullhd>e560f84e432d1648b3320876a707d56a4edb27f4</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/1wDmFicGRYetVMXcDmz0CvM7ZOJ.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/owOBJlzH3j3YHUbr3XqSH10CWzo.jpg</fanart>
<date>2021</date>
<genre>Terror. Thriller </genre>
<extra>NA</extra>
<info>Una pareja de Manhattan se muda a una aldea histórica en el valle del Hudson y acaba descubriendo que su matrimonio oculta una siniestra oscuridad que rivaliza con la historia de su nuevo hogar.</info>
 
 
<page>15</page><title>LA BALLENA</title>
<microhd>ad68af3edc7e4f526e3d29cecbafa2be0e0f8c52</microhd>
<fullhd>c0ab9a6bc29137681e0ddf3f8a0e5552b124991d</fullhd>
<tresd>NA</tresd>
<cuatrok>03ce9a93ec32de819731f718deaabc3fadb1e489</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/dbNRiP2slaz7fk42PH5sEPNDij7.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/zmfsh2BmeeKNJToxGEYg25H3Ydw.jpg</fanart>
<date>2022</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Un solitario profesor de inglés con obesidad severa (Brendan Fraser) intenta reconectar con su hija adolescente en una última oportunidad de redención.</info>
 
 
<page>15</page><title>LA BELLA Y LA BESTIA</title>
<microhd>8f48bbf414a35ae33b8a17296f6c15c7515ef7d3</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/1FxMtEUc6DP1MXsTBftOFaoCVVO.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/3mNA9xRqd5WfDj1hBpbbniFcI2T.jpg</fanart>
<date>1991</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Una hermosa joven llamada Bella acepta alojarse en un castillo con una bestia a cambio de la libertad de su padre. La bestia es, en realidad, un príncipe encantado. Para romper el hechizo, deberá ganarse el amor de una preciosa dama antes de que caiga el último pétalo de una rosa encantada.</info>
 
 
<page>15</page><title>LA BELLA Y LA BESTIA</title>
<microhd>4bfed8eb986c8bcfe920ed561ff803232a9af715</microhd>
<fullhd>f1b89cf58f40ac7cedc85e13961f7f022fd56548</fullhd>
<tresd>5c723692afc0dc0365eb0b0c08b3b955890f7a9f</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/aGJkPbMixqD6j0OjbbGouSMbiZO.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/n28rdNRdhr5sTcskuzPRwczfP2t.jpg</fanart>
<date>2017</date>
<genre>NA</genre>
<extra>NA</extra>
<info>Adaptación en imagen real del clásico de Disney "La bella y la bestia", que cuenta la historia de Bella (Emma Watson), una joven brillante y enérgica, que sueña con aventuras y un mundo que se extiende más allá de los confines de su pueblo en Francia. Independiente y reservada, Bella no quiere saber nada con el arrogante y engreído Gastón, quien la persigue sin descanso. Todo cambia un día cuando su padre Maurice (Kevin Kline) es encarcelado en el castillo de una horrible Bestia, y Bella se ofrece a intercambiarse con su padre y queda recluida en el castillo. Rápidamente se hace amiga del antiguo personal del lugar, que fue transformado en objetos del hogar tras una maldición lanzada por una hechicera. </info>
 
 
<page>15</page><title>LA CAJA 507</title>
<microhd>NA</microhd>
<fullhd>265f66e56331c163f018a9946e08578dbb5c91b0</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/yolTbY4f0E5d4A0fVBL1fbGm6UU.jpg</thumbnail>
<fanart>https://i.imgur.com/NUXtyZq.jpg</fanart>
<date>2002</date>
<genre>Thriller</genre>
<extra>Misc</extra>
<info>Modesto, un hombre honrado y trabajador, dirige una sucursal bancaria en la Costa del Sol. Un día, unos atracadores entran en el banco, revientan las cajas de seguridad y lo dejan atrapado dentro. Casualmente, al mirar el contenido de la caja 507, descubre que la muerte de su hija, ocurrida hace algunos años, no fue accidental. Del contenido de esa caja depende también la vida y la seguridad de Rafael, un ex-policía corrupto y sin escrúpulos, que tratará de recuperar esos documentos como sea.</info>
 
 
<page>15</page><title>LA CALLE DEL TERROR PARTE 1 1994</title>
<microhd>d27c95b94fad2c12164a6f2e9fd9bdd012983c12</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/2JG46f4JtCJTUf2z9UbiL9qusOP.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/5C8bfwglg91uZhc2fbfpSjNGamV.jpg</fanart>
<date>2021</date>
<genre>Terror. Thriller. Saga</genre>
<extra>NA</extra>
<info>En 1994, un grupo de adolescentes descubre que los sucesos que aterrorizan su ciudad desde hace generaciones podrían estar conectados. Además, puede que ahora les toque a ellos protagonizar la pesadilla. Primera parte de la trilogía basada en las famosas novelas de miedo de R. L. Stine.</info>
 
 
<page>15</page><title>LA CALLE DEL TERROR PARTE 2 1978</title>
<microhd>b374a23336daa05559e76ec514035eaecb14385a</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/i9CNNS5htRdTf11A63lNKQyYyhj.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/7jpj6ek9dftr3IXm7Pnq2pkhir3.jpg</fanart>
<date>2021</date>
<genre>Terror. Thriller. Saga</genre>
<extra>NA</extra>
<info>En 1978, Camp Nightwing está dividido en campistas que vinieron del pueblo oprimido de Shadyside y consejeros que vinieron del próspero pueblo de Sunnyvale. Cuando los horrores del pasado de ambos pueblos cogen fuerza, estos dos grupos deberán unirse para resolver un misterio terrorífico.</info>
 
 
<page>15</page><title>LA CALLE DEL TERROR PARTE 3 1666</title>
<microhd>c1219961be4588d983eda8040cae11f1c21088a1</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/aL0qzDQHjw0RQdMilhOQZSwK0KE.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/6viJBbovWxJYavdjPy7UPTLvaOS.jpg</fanart>
<date>2021</date>
<genre>Terror. Thriller. Saga</genre>
<extra>NA</extra>
<info>En 1666, un pueblo colonial sufre una caza de brujas que tiene consecuencias letales durante siglos. Está en manos de los jóvenes de 1994 poner fin a esta maldición.</info>
 
 
<page>15</page><title>LA CARRERA DE LA MUERTE DEL AÑO 2000</title>
<microhd>827E2C2DA74CDA3D6DF489E5A49AC62F7B706E11</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/tvp70ZkSWzAsvrKHHIEjV8Sy5ir.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/1WgFbHfTPSXKoqYdB9SbcAWpqrO.jpg</fanart>
<date>1975</date>
<genre>Acción. Ciencia ficción</genre>
<extra>Mis</extra>
<info>Film sobre "violentas" carreras de coches. La leyenda reza: "En el año 2000 atropellar conduciendo no es un crimen, es el deporte nacional".</info>
 
 
<page>15</page><title>LA CASA DEL CARACOL</title>
<microhd>6a68705c2c1587dbf9444d42d4cd468fbcede436</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/bfijc3b5JLnnXlVdgLDxheX2Hu1.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/1japeIpy8ZW4ql3AvAxB5cd5Itq.jpg</fanart>
<date>2021</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>El escritor Antonio Prieto decide pasar el verano en un pueblo de la serranía malagueña, donde espera encontrar tranquilidad e inspiración para su siguiente novela. Allí conoce a Berta, una mujer por la que siente una atracción instantánea, así como a algunos personajes peculiares sobre los que empieza a escribir e investigar. Antonio comienza a descubrir que los locales guardan numerosos secretos y una perturbadora leyenda oculta. Lo que vivirá en esos días le hará ir dándose cuenta que, a veces, la realidad supera con creces a los mitos.</info>
 
 
<page>15</page><title>LA CASA DEL RELOJ EN LA PARED</title>
<microhd>002b5c438b32701d945addf111a4b768bdacdb31</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/A2xteTlt6kJAcevHc5TyA95oU7B.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/meyZ0ZZLLNaKIrvbmVpuXzZ11sD.jpg</fanart>
<date>2018</date>
<genre>Fantastico. Aventuras. Infantil Familiar</genre>
<extra>NA</extra>
<info>La historia cuenta el mágico y escalofriante relato de Lewis (Owen Vaccaro), un niño de 10 años quien, tras quedar huérfano, se muda a la vieja y rechinante casa de su tío, la cual tiene un misterioso reloj. Pronto, la aburrida y tranquila vida de su nuevo pueblo se ve interrumpida cuando accidentalmente, Lewis despierta a magos y brujas de un mundo secreto.</info>
 
 
<page>15</page><title>LA CASA GUCCI</title>
<microhd>545F7CF237D2CFFE5433160EEAEDF2549F29EEB8</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>4982a9a41ec937abf8fedbdb6f7c48d720085d89</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/o0XF8y2cHw8DfhY8ktFZY8lNHXn.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/6VZF8JVNOgJX56WCapYaqcVQiAw.jpg</fanart>
<date>2021</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Drama criminal en torno al asesinato, en 1995, de Maurizio Gucci, nieto del fundador del imperio de la moda Gucci, que apareció asesinado por orden de su exmujer Patrizia Reggiani, conocida como "la viuda negra de Italia". Adaptación del libro de Sara Gay Forden, publicado en 2001, 'The House of Gucci: A Sensational Story of Murder, Madness, Glamour, and Greed'.</info>
 
 
<page>15</page><title>LA CASA TORCIDA</title>
<microhd>379B2C2F03438D0668E826A1EAD00AF677CBDECA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/cV8UQKK8giYB2kA7wL6rtyoISD.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ynJO5IHwgrLjBFDGkvBV8vKAvGe.jpg</fanart>
<date>2017</date>
<genre>Intriga. Drama</genre>
<extra>NA</extra>
<info>Aristides Leónides es el patriarca de una adinerada familia de origen griego que será asesinado poco después de que su nieta presente a la familia a su prometido, hijo de un detective de Scotland Yard, que será quien deberá resolver el crimen. Adaptación de la novela de Agatha Christie.</info>
 
 
<page>15</page><title>LA CAZA</title>
<microhd>27990047c7f4101ffa0827a7e4fca30ce5d8c622</microhd>
<fullhd>581cc96336a0bc9a3f4aa792ada0132da64ce3e5</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/5WCuPRfa0h8jMT4Iu2jIIQXqllM.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/9R2FuGT0BeZl8usfsfqIcO7KK7v.jpg</fanart>
<date>2020</date>
<genre>Thriller. Acción. Terror. Comedia</genre>
<extra>NA</extra>
<info>Un grupo de personas, desconocidas entre sí, despiertan en un lugar deshabitado. Pronto se dan cuenta de que alguien intenta, literalmente, cazarlos como a animales.</info>
 
 
<page>15</page><title>LA CENICIENTA. TRILOGIA</title>
<microhd>a3697388c698e647df32ceb4ab5bad3b3d1e733d</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/vqzeSm5Agvio7DahhKXaySUbUUW.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/puJKgNcWaGgMk5VHanSSomUTpmw.jpg</fanart>
<date>1950 - 2007</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Cenicienta era una hermosa y bondadosa joven, a quien su cruel madrastra y sus dos hermanastras obligaban a ocuparse de las labores más duras del palacio, como si fuera la última de las criadas. Sucedió que el hijo del Rey celebró un gran baile. Cenicienta ayudó a sus egoístas hermanastras a vestirse y peinarse para la fiesta. Cuando se hubieron marchado, la pobre niña se echó a llorar amargamente porque también le hubiera gustado ir al baile. Pero hete aquí que su hada madrina le hizo una carroza con una calabaza, convirtió seis ratoncitos en otros tantos caballos, una rata en un grueso cochero, y seis lagartos en elegantes lacayos. Después tocó a Cenicienta con su varita mágica y sus harapos se convirtieron en un vestido resplandeciente, y sus alpargatas en preciosos zapatitos de cristal. Pero el hada advirtió a Cenicienta que a medianoche, todo volvería a ser como antes. Cuando llegó a la fiesta, su radiante belleza causó asombro y admiración. El Príncipe no se apartó de ella ni un solo instante. Poco antes de la doce, Cenicienta se retiró. Al día siguiente, seguían los festejos principescos y todo se repitió de igual manera que la víspera. Pero la pobre Cenicienta, tan feliz con su Príncipe, se olvidó de que a las doce terminaba el hechizo. Cuando oyó la primera campanada de la medianoche, echó a correr y perdió uno de sus zapatos de cristal.</info>
 
 
<page>15</page><title>LA CHAQUETA METALICA</title>
<microhd>NA</microhd>
<fullhd>345da9a80f49897810556f44181e272ca1476af5</fullhd>
<tresd>NA</tresd>
<cuatrok>e53a4f70a67659dc2269f2e9c6a19cf839049db7</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/7SQMNukt3oxFZymLd4bTOL5zsIU.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/v6JAlrwRLCnIujs9NFIViursbhg.jpg</fanart>
<date>1987</date>
<genre>Bélico. Drama</genre>
<extra>Culto</extra>
<info>Un grupo de reclutas se prepara en Parris Island, centro de entrenamiento de la marina norteamericana. Allí está el sargento Hartman, duro e implacable, cuya única misión en la vida es endurecer el cuerpo y el alma de los novatos, para que puedan defenderse del enemigo. Pero no todos los jóvenes están preparados para soportar sus métodos.</info>
 
 
<page>15</page><title>LA CHICA SALVAJE</title>
<microhd>NA</microhd>
<fullhd>d788951dc87b764fcd0f4dc97c1318ce9a4f98a7</fullhd>
<tresd>NA</tresd>
<cuatrok>9ba413b2da50fb7b3b0960ac01a328393cce6959</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/uyFrPliX4lN8swXd5Uytbwye8MX.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/7Y3LdmsZukXhmwxtO0UF95BFBTt.jpg</fanart>
<date>2022</date>
<genre>Intriga. Thriller. Drama. Romance</genre>
<extra>NA</extra>
<info>La historia de Kya, una niña abandonada que se crió hasta la edad adulta en los peligrosos pantanos de Carolina del Norte. Atraída por dos jóvenes de la ciudad, Kya se abre a un mundo nuevo y sorprendente; pero cuando uno de ellos es encontrado muerto, la comunidad inmediatamente la señala como la principal sospechosa.</info>
 
 
<page>15</page><title>LA CIUDAD PERDIDA</title>
<microhd>6b3b8f268300aa3593ade5d231ab68b0fbe49f29</microhd>
<fullhd>b55a64a6b2a16a91c64bab2814cacf7375188c8f</fullhd>
<tresd>NA</tresd>
<cuatrok>8fc0b7923e1c096dbaced54fdc1742789e8b9c00</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/grEVYkBAVIzQ4JmZ7ydceN9DFQR.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/1Ds7xy7ILo8u2WWxdnkJth1jQVT.jpg</fanart>
<date>2022</date>
<genre>Aventuras. Comedia. Acción</genre>
<extra>NA</extra>
<info>La carrera literaria de la brillante y algo huraña escritora de novelas Loretta Sage (Sandra Bullock) ha girado en torno a las novelas románticas de aventuras que, ambientadas en lugares exóticos, protagoniza un atractivo galán cuya imagen aparece reproducida en todas las portadas, y que en la vida real corresponde a Alan (Channing Tatum), un modelo que ha centrado su carrera en personificar al novelesco aventurero. Durante una gira para promocionar su nuevo libro junto a Alan, Loretta es raptada por un excéntrico multimillonario (Daniel Radcliffe), con la intención de que la autora le guíe hasta el tesoro de la antigua ciudad perdida sobre el que gira su último relato. Deseoso de demostrar que puede ser un héroe en la vida real, y no simplemente en las páginas de sus obras de ficción, Alan se lanza al rescate de la novelista.</info>
 
 
<page>15</page><title>LA CONFERENCIA</title>
<microhd>25abf7613346af715a524bb7c762b57579c8fc9b</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/HPY2WagRVbuU9ApYxqpJft0pAB.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/eLYJu0JY7uAu0XeltM5OL3fdPDn.jpg</fanart>
<date>2022</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>El 20 de enero de 1942, destacados representantes del régimen naSi alemán se reunieron en una villa de Berlín-Wannsee para una reunión que pasó a la historia como la Conferencia de Wannsee, en la que se decidió el asesinato sistemático de 11 millones de judíos.</info>
 
 
<page>15</page><title>LA COSA (EL ENIGMA DE OTRO MUNDO)</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>6a3b77ad9667a3cbd2ce66ea3ba84bd576a8ed8b</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/hQa3Z5CsY9Pg1rSPgL6qJoXF6iJ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/3eb63ISwxqVaJbSbkgUNfrfB2NS.jpg</fanart>
<date>1982</date>
<genre>Ciencia ficción. Terror. Fantástico</genre>
<extra>Culto</extra>
<info>En una estación experimental de la Antártida, un equipo de investigadores descubre a un ente extraño venido del espacio, que según todos los indicios ha permanecido enterrado en la nieve durante más de 100.000 años. Al descongelarse, experimenta una metamorfosis sorprendente... </info>
 
 
<page>15</page><title>LA CRONICA FRANCESA</title>
<microhd>6859cd3aaf66f171d8f4511e32410594cce9ac87</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/bnuaEPZiYgAPpUOnAs6yTVoHpul.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/ajEBFTg8odL8O20XQEChEMaJJxn.jpg</fanart>
<date>2021</date>
<genre>Comedia. Drama</genre>
<extra>NA</extra>
<info>Una carta de amor al mundo del periodismo, ambientada en la redacción de un periódico estadounidense en una ciudad francesa ficticia del siglo XX, con tres historias interconectadas entre sí.</info>
 
 
<page>15</page><title>LA DAMA Y EL VAGABUNDO</title>
<microhd>97ea2b13e7429bc910d16c8cc66de859638ae2a0</microhd>
<fullhd>0bd79a66eb7946e860018999db419282f19b992a</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ujgFJJu9t4Wx9O573X43eyqWhvC.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/eXlfDtIi7UvxKoUi9a8oxHDAYej.jpg</fanart>
<date>2019</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>En esta aventura romántica, una reinterpretación intemporal del clásico animado de 1955, Reina, una cocker spaniel inteligente y mimada en su hogar, y Golfo, un perro callejero duro y charlatán pero adorable, se embarcan en una inesperada aventura y, a pesar de sus diferencias, se hacen buenos amigos y al final comprenden el valor del hogar.</info>
 
 
<page>15</page><title>LA DELGADA LINEA ROJA</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>8b23af34d86ed648b7e08b2826749777130024cb</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/234oXaXKIcyXpWMMFISsZ94tas9.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/5nxCk1nSMNCc5ekdM8e2BbmNklO.jpg</fanart>
<date>1998</date>
<genre>Belico</genre>
<extra>Mis</extra>
<info>Año 1942, en plena Segunda Guerra Mundial en la Isla de Guadalcanal, en el Pacífico. Un grupo de hombres de la compañía de fusileros del ejército americano "C de Charlie" combate contra el ejército japonés por la conquista de una estratégica colina. Este grupo forma parte de las tropas enviadas para relevar a las unidades de infantería de la Marina, agotadas por el combate.</info>
 
 
<page>15</page><title>LA DESVIDA</title>
<microhd>f099277deb6c7199561a26f26ddeb759a223d76c</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/htmIvnWVUOxHIJK5FjLvpZEuTeI.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/htmIvnWVUOxHIJK5FjLvpZEuTeI.jpg</fanart>
<date>2022</date>
<genre>Terror</genre>
<extra>NA</extra>
<info>Una pareja de escritores dedicada a contar historias infantiles regresan a la casa en la que perdieron a su hijo. La idea es recolectar lo básico para volver a casa separados. Todo cambia cuando encuentran un mensaje del niño invitándolos a participar en un juego de pistas.</info>
 
 
<page>15</page><title>LA ESCUELA DEL BIEN Y DEL MAL</title>
<microhd>NA</microhd>
<fullhd>6B8CFA03DE0E26D3412587AD24AF0454A81669B8</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/xEOBNSrM5g1SSaOnvpioZF38cuE.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/kJdacG3ZPc76FWWmzgGy4meYkY0.jpg</fanart>
<date>2022</date>
<genre>Fantástico. Comedia</genre>
<extra>NA</extra>
<info>Charlize Theron es una glamurosa (aunque severa) maestra de villanos y Kerry Washington encarna a una alegre profesora de los futuros héroes de los cuentos en esta película fantástica ambientada en una escuela mágica. Basada en el bestseller de Soman Chainani, ha sido dirigida y coguionizada por Paul Feig</info>
 
 
<page>15</page><title>LA ESTACION DE LA FELICIDAD</title>
<microhd>767b1a327bdb405919cc7782b4fb2e2e0a3060df</microhd>
<fullhd>6647184324e12e5df827d9dd2048051ff99d22e4</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/iWkm5KikTC3kRjc5rGlktSSMHlM.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/jOWgFmD1H5t6nL7ChbmX8oDFUZl.jpg</fanart>
<date>2020</date>
<genre>Romance. Comedia. Estreno</genre>
<extra>NA</extra>
<info>Una mujer que planea pedirle matrimonio a su novia durante las vacaciones anuales con la familia descubre que los conservadores padres de su pareja aún desconocen la orientación sexual de su hija.</info>
 
<page>15</page><title>LA FAMILIA ADDAMS</title>
<microhd>e01a8573a18e2f8ff860fd251c91176e3af9fa5b</microhd>
<fullhd>204e23bb3f5f0869e209c2e933152e7d3123c72a</fullhd>
<tresd>NA</tresd>
<cuatrok>e1be78af9f6b9aaebdf41a6c06174c8a907bae76</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/307Y96L1iJ6gujCfuGPWNTqgbBd.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ur4NTeFGZmQ6Hz5uEkAMgPI3WRg.jpg</fanart>
<date>2019</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Gomez y Morticia preparan la visita de su numerosa familia para celebrar la ''Mazurka con sable'' en honor a Pugsley, un rito de iniciación para convertirse en un hombre digno de la familia Addams. Pero, mientras están inmersos en los preparativos, los Addams ignoran que su vecina, el fenómeno televisivo Margaux Needler, planea construir una comunidad prefabricada y perecta justo debajo de la suya. Y cuando por fin se levanta la niebla, Margaux entra en pánico al descubrir la mansión de la familia Addams, un claro impedimento en su sueño de vender todas las casas del barrio a personas selectas. Por otro lado, mientras Pugsley se esfuerza en aprenderse al pie de la letra la complicada ''Mazurka con sable'', Miércoles se enfrenta a la adolescencia. Se hace amiga de Parker, la hija de Margaux, e intenta ir más allá de los límites impuestos por Morticia, sacándola de quicio al volverse ''normal'', ir al instituto, ser animadora y querer vestir cosas que, sin duda, no son propias de un Addams.</info>
 
<page>15</page><title>LA FAMILIA ADDAMS 2: LA GRAN ESCAPADA</title>
<microhd>84899F08F0B308A99E6B7554B24FCD002BC650A2</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>cd023b0fd40587a2d572310d1c4deb4394da2c24</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/vCH8YJFbG0C2Ip61ZYRQrCdsGAQ.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/9e6wp707XMouPG939o2fHunXXJR.jpg</fanart>
<date>2021</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Morticia y Gomez están angustiados porque sus hijos están creciendo, saltando las cenas familiares y totalmente consumidos por el 'tiempo de los gritos'. Para recuperar su vínculo, deciden meter a Miércoles, Pugsley, el tío Fester y el resto del equipo en su caravana embrujada y salir a la carretera para unas últimas y miserables vacaciones familiares. Su aventura por Estados Unidos los saca de su hábitat y los lleva a divertidos encuentros con su icónico primo, It, así como con muchos personajes nuevos y excéntricos. Secuela de la película animada de 2019, 'La familia Addams'.</info>
 
<page>15</page><title>LA FAMILIA PERFECTA</title>
<microhd>dcb28ed09ef6f8af36eaf8fe349fa6d525f12de9</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/rvSkUeFusM34rz0jkvWP4sJAPS0.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/hPEOBD7eeQh7plm9TYoECNkK4b3.jpg</fanart>
<date>2021</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Lucía cree llevar una vida modélica y tenerlo todo bajo control. Desde que se casó, volcó todos sus esfuerzos en el cuidado de su familia ideal. Pero todo comienza a derrumbarse el día que aparece Sara, la novia de su hijo; una chica joven, libre y deslenguada y, con ella, una familia política muy diferente a la idea que Lucía siempre soñó para para su hijo. A partir de ese momento, Lucía descubrirá que la familia perfecta no era exactamente lo que ella pensaba.</info>
 
 
<page>15</page><title>LA FAMILIA QUE TU ELIGES</title>
<microhd>95df695752185f2d86e71e559b292d71e5fcb37d</microhd>
<fullhd>7e43030c031812b30eb594f7f778e9500c635c3e</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/9mj6FjC0WHi7CeOpK3VT73AuDSR.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/7HnUn1XBY615xiWqZ3tYLaHA7LR.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Zak (Gottsagen) es un joven con Síndrome de Down que se escapa de su residencia para perseguir su sueño de convertirse en un luchador profesional en una escuela de lucha. Una serie de circunstancias le llevan a encontrarse con Tyler (LaBeouf) un delincuente que también se ha dado a la fuga y que se convertirá en su entrenador y aliado. Su aventura les lleva a conocer a Eleanor (Johnson), una enfermera un tanto peculiar que se une a ellos en su viaje a través del norte de California.</info>
 
<page>15</page><title>LA FAVORITA</title>
<microhd>43c07413a3d63facc93cd0e4b150ef31763bf5ab</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/mirzwvOh7s5HN0vC7b7H6WMkO6X.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ekWMoBZ4B9rM60INZEh5FAD2HFR.jpg</fanart>
<date>2018</date>
<genre>Drama. Comedia</genre>
<extra>NA</extra>
<info>Principios del siglo XVIII. Inglaterra está en guerra contra Francia. Una reina debilitada, Anne (Olivia Colman), ocupa el trono, mientras que su amiga Lady Sarah (Rachel Weisz) gobierna en la práctica el país en su lugar, debido al precario estado de salud y al carácter inestable de la monarca. Cuando una nueva sirvienta, Abigail (Emma Stone), aparece en palacio, su encanto seduze a Sarah. Esta ayuda a Abigail, la cual ve una oportunidad para regresar a sus raíces aristocráticas. Como la política ocupa gran parte del tiempo de Sarah, Abigail empieza a acompañar con más frecuencia a la reina.</info>
 
<page>15</page><title>LA FORMA DEL AGUA</title>
<microhd>30EA850CE12E0FE9D377B20DF9B5A999BBD939F1</microhd>
<fullhd>d64e98895e06521717a47edcd6adf4e28413631e</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ahsMZ2PCIVi5qGoPKjjeG3ZznjA.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/rgyhSn3mINvkuy9iswZK0VLqQO3.jpg</fanart>
<date>2017</date>
<genre>Fantastico. Drama. Romance. Thriller</genre>
<extra>NA</extra>
<info>En un inquietante laboratorio de alta seguridad, durante la Guerra Fría, se produce una conexión insólita entre dos mundos aparentemente alejados. La vida de la solitaria Elisa (Sally Hawkins), que trabaja como limpiadora en el laboratorio, cambia por completo cuando descubre un experimento clasificado como secreto: un hombre anfibio (Doug Jones) que se encuentra ahí recluido.</info>
 
 
<page>15</page><title>LA FUERZA DE LA NATURALEZA</title>
<microhd>63b6b8815d5a8739341c089a6807fcd98e521dbb</microhd>
<fullhd>3b4425b0c90c9adb8421a6b55d97838687bc4987</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ucktgbaMSaETUDLUBp1ubGD6aNj.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/eIqyISB5j99OSRZUuvdN9g2bBsS.jpg</fanart>
<date>2020</date>
<genre>Acción</genre>
<extra>NA</extra>
<info>Una banda de ladrones planea un golpe aprovechando la llegada de un huracán, pero se encontrarán con un problema cuando la policía trata de evacuar a todo el mundo del edificio amenazado por el huracán.</info>
 
 
<page>15</page><title>LA GALLINA TURULECA</title>
<microhd>fd5ca4a10dcfa8eb8dab27f4c1728d72c80d36f0</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/kJ6t4RH4m5ly1xaf5uFMKdqE1vF.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/aZOO8jOCx2tael77wDrQ4mwxapz.jpg</fanart>
<date>2020</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Turuleca es una gallina singular. Su peculiar aspecto desata las burlas del resto del gallinero, hasta que un día, Isabel, una exprofesora de música, la lleva a vivir a su granja. Allí, feliz y en armonía, la gallina descubre su gran talento oculto con la ayuda de Isabel: ¡Turuleca no sólo puede hablar, sino que canta como jamás has oído cantar a una gallina!</info>
 
 
<page>15</page><title>LA GRAN AVENTURA DE LOS LUNNIS Y EL LIBRO MAGICO</title>
<microhd>d1f268315285338e020296592ada814e6762cb6d</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/vyIb9OAsOtH0FXTkUPeazWN1mUm.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/wl2TmIYLB81Z79fSlDfaVizH24Y.jpg</fanart>
<date>2019</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>La Imaginación y el Mundo de Fantasía están en peligro. Mar, una niña de 8 años experta en cuentos y leyendas gracias a los libros que su abuelo le lee cada noche, decide intentar salvarlos con la ayuda de sus amigos Los Lunnis y juntos deberán evitar su desaparición. El malvado Crudo odia la fantasía porque carece de imaginación y pretende acabar con el Libro Mágico que contiene todas las historias inventadas por la Humanidad, del que es guardiana Lucrecia. Para conseguir su propósito, Los Lunnis y Mar deberán entrar en el libro y ganar a Crudo y su fiel servidor, un dron llamado Alfred, en una carrera a través de los cuentos más populares. En su aventura, se encontrarán con muchos de los personajes de los cuentos más populares como Merlín, Pinocho, Alicia, el Mago de Oz, el Flautista de Hamelín o el Ratoncito Pérez, que jugarán un papel importante en su maravilloso viaje.</info>
 
 
<page>15</page><title>LA GRAN MENTIRA</title>
<microhd>9984cde0b7431fdbc12eb663a44bc835eaf395a3</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/wo6XbelpfHGvpkxg181QjcKLkex.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/z3nOqR0ap2FpCSenMRpucxQMw3P.jpg</fanart>
<date>2019</date>
<genre>Intriga. Drama. Comedia </genre>
<extra>NA</extra>
<info>Roy Courtnay (Ian McKellen) es un estafador profesional que no puede creer su suerte: ha conocido online a la adinerada viuda Betty McLeish (Helen Mirren). A medida que ella le abre su corazón, Roy se sorprende a sí mismo al darse cuenta de que alberga sentimientos hacia ella, convirtiendo lo que debería ser una estafa fácil y sencilla en una de las situaciones más complejas de su vida.</info>
 
 
<page>15</page><title>LA GUERRA DE LAS CORRIENTES</title>
<microhd>7c3bdb959e8c32b7241cb6bf24018e0a6c898b8a</microhd>
<fullhd>d9e8ee71173c73f939a91c652be24f39fcc741ed</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/91tLZNJsvsAN3WT21C7rRLLssZN.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/jypNy38bThNz4K6IWDoE7h0xlP2.jpg</fanart>
<date>2017</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>A finales del siglo XIX dos genios, Thomas Edison (Benedict Cumberbatch) y George Westinghouse (Michael Shannon), compiten -éste último junto a Nikolai Tesla (Nicholas Hoult)- para crear un sistema sustentable de electricidad y poder comercializarlo a todos los Estados Unidos en lo que se conoce como la 'guerra de las corrientes', una rivalidad entre ambos en la década de 1880 por el control del incipiente mercado de la generación y distribución de energía eléctrica.</info>
 
 
<page>15</page><title>LA GUERRA DEL MAÑANA</title>
<microhd>3a9934ebc2feb9d9257b40065ec1f961c885cee4</microhd>
<fullhd>d04bdd33302f02fcce53064a3f30f3b5cb93d103</fullhd>
<tresd>NA</tresd>
<cuatrok>3511e11ec8ac2e73e0d45eb28c7e907d1d1e7b14</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/hBcdtoe3Vu8MacviGjkJRd0U1BG.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/yizL4cEKsVvl17Wc1mGEIrQtM2F.jpg</fanart>
<date>2021</date>
<genre>Ciencia ficción. Acción</genre>
<extra>NA</extra>
<info>Un día el mundo se paraliza cuando un grupo de viajeros en el tiempo se transporta desde el año 2051 para entregar un mensaje urgente: La humanidad está perdiendo la guerra mundial contra una especie alienígena letal. La única esperanza de supervivencia es que los soldados y los ciudadanos del presente sean transportados al futuro y se unan a la lucha. Entre los reclutados está el profesor de instituto y padre de familia Dan Forester (Chris Pratt) quien, decidido a salvar el mundo por su hija, se une a una brillante científica (Yvonne Strahovski) y a su padre (J.K. Simmons) en una búsqueda desesperada por reescribir el destino del planeta. </info>
 
 
<page>15</page><title>LA HIJA DE UN LADRON</title>
<microhd>edf513430fbdd4e86908770624bb3081cbcc4ecd</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/wrbI8HBv3yD7DKMcbO3tHCktJmM.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/u0GFZlh52wfqjxQLwj5K6xHtSGB.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Sara ha estado sola toda su vida. Tiene 22 años y un bebé, su deseo es formar una familia junto a su hermano pequeño y el padre de su hijo. Su padre, Manuel, tras años de ausencia y al salir de la cárcel, decide reaparecer en sus vidas. Sara sabe que él es el principal obstáculo en sus planes y toma una decisión difícil: alejarlo de ella y de su hermano.</info>
 
 
<page>15</page><title>LA HIJA ETERNA</title>
<microhd>NA</microhd>
<fullhd>08c75a65a668943dbb8d21afd950612fd08ecd24</fullhd>
<tresd>NA</tresd>
<cuatrok>6a698bbb3862a5670aad751b5b5f65c58f3e749f</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/neh3P3A70Yisxjz0gHX2LHdjENX.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/wuvVd3vdTEG9zOCDsZv67Mcg89a.jpg</fanart>
<date>2022</date>
<genre>Drama. Intriga</genre>
<extra>NA</extra>
<info>Una mujer y su anciana madre deben enfrentarse a secretos enterrados hace mucho tiempo cuando regresan a su antigua casa familiar, una antigua gran mansión que se ha convertido en un hotel casi vacío lleno de misterio.</info>
 
 
<page>15</page><title>LA HUERFANA: PRIMER ASESINATO</title>
<microhd>NA</microhd>
<fullhd>9074d22f163f2bb327ecb9058e2b02ecb8082b12</fullhd>
<tresd>NA</tresd>
<cuatrok>d1446806cb977bd722d4659b1bdeb5bb014a777c</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/eed4qNf174pcT65it1PBbaTTL3x.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/e77LAFvZq5KteWsKxuc5nP9B6OD.jpg</fanart>
<date>2022</date>
<genre>Thriller. Intriga. Terror</genre>
<extra>NA</extra>
<info>Lena (Isabelle Fuhrman) consigue escapar del psiquiátrico ruso en el que está recluida y viaja a EE.UU., haciéndose pasar por la hija desaparecida de una familia adinerada. Pero su nueva vida como Esther no será como ella esperaba, y se enfrentará a una madre que protegerá a su familia a cualquier precio... Precuela de 'La huérfana' (2009).</info>
 
 
<page>15</page><title>LA INCREIBLE HISTORIA DE DAVID COPPERFIELD</title>
<microhd>5f07b9b833ee6cdd50f41b05181299c56416a912</microhd>
<fullhd>9799395d4568cfaffce544b602f0e222507e7b61</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/r8bitmrt5jEMmn1GvetRoYXaIl9.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/udMUwcH9UBxwjvpRD3f0ExEJq89.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Nueva adaptación de la historia de la vida de David Copperfield desde la infancia hasta la madurez, con la red de amigos y enemigos que se encuentra por el camino.</info>
 
 
<page>15</page><title>LA IRA DE BECKY</title>
<microhd>NA</microhd>
<fullhd>a3853c792c04c77f4d0db50ed0ff1974759d6d21</fullhd>
<tresd>NA</tresd>
<cuatrok>24124b2b49fc241ab55ea45e8b767b3575a652d1</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/1R0x0DEQqToCnrIQrPE7EjtUiAy.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/dYF9h2bhFHIRoNt5aZhlCx9YwoO.jpg</fanart>
<date>2023</date>
<genre>Acción. Terror. Thriller</genre>
<extra>Estreno</extra>
<info>Becky ha estado viviendo fuera de la red durante dos años. Luego se encuentra enfrentándose cara a cara con Darryl (Scott), el líder de una organización fascista, en vísperas de un ataque organizado. Secuela de 'Becky' (2020).</info>
 
 
<page>15</page><title>LA ISLA DE LAS MENTIRAS</title>
<microhd>45f362bc6186fc32edd0cdeae3b2becefc43703c</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ibuTh5t9Ex4wpZm5LJDBXf9EtgB.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/qtLNS2Zo3JYcNSmEaTXDHanWcwn.jpg</fanart>
<date>2020</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>La madrugada del 2 de enero de 1921, en medio de una espesa niebla el vapor Santa Isabel, con 260 emigrantes con destino Buenos Aires, se hundió frente a la escarpada y dura costa de la isla de Sálvora. Esa noche no había hombres en la isla, estaban celebrando las Navidades en tierra firme. La única esperanza de los supervivientes que luchaban por no estrellarse contra las rocas eran las pocas mujeres, viejos y niños que vivían en este lugar. Tres jóvenes isleñas, María (Nerea Barros), Josefa (Victoria Teijeiro) y Cipriana (Ana Oca), deciden lanzarse al mar en una pequeña embarcación tradicional. Remando sólo de oído, debido a la espesa niebla y a la noche cerrada, logran salvar a 50 personas. Todo lo que ocurre desde esa noche va a sacudir y cambiar para siempre a esta isla. Un periodista argentino, León (Darío Grandinetti), acude a Sálvora para cubrir la noticia del naufragio. Pero poco a poco comprueba que esa noche sucedieron demasiadas terribles "casualidades" en la isla. Son muchas las incógnitas que hay que resolver...</info>
 
 
<page>15</page><title>LA ISLA MINIMA</title>
<microhd>3d4b40333886700753d788a1bdd16f241e1c2f64</microhd>
<fullhd>1f67f4f3ab1f1cdf5a6a24cbc8683e4562d50344</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/v6R3cpLDHhOcuyxXNvlNOtU5kQo.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/qPrWsxKLv12UHFtt5jvUYtUvdm3.jpg</fanart>
<date>2014</date>
<genre> Thriller</genre>
<extra>NA</extra>
<info>España, a comienzos de los años 80. Dos policías, ideológicamente opuestos, son enviados desde Madrid a un remoto pueblo del sur, situado en las marismas del Guadalquivir, para investigar la desaparición de dos chicas adolescentes. En una comunidad anclada en el pasado, tendrán que enfrentarse no sólo a un cruel asesino, sino también a sus propios fantasmas.</info>
 
 
<page>15</page><title>LA JUNGLA DE CRISTAL</title>
<microhd>383f2dc1b655c84b4daafe3711f68977a252c474</microhd>
<fullhd>19a28b70db86ebf4929fcd50ef4428baae285df6</fullhd>
<tresd>NA</tresd>
<cuatrok>1e93fc3e83f31f750ad162aa7ae35445d2ca9cb6</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/m54tK4wKjc1w2dp7QInG0munGao.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/qyNEqB6gl9V2GkiT88Pu36mqHnR.jpg</fanart>
<date>1988</date>
<genre>Accion. Thriller.Saga</genre>
<extra>Culto</extra>
<info>En lo alto de la ciudad de Los Ángeles, un grupo terrorista se ha apoderado de un edificio tomando a un grupo de personas como rehenes. Sólo un hombre, el policía de Nueva York John McClane (Bruce Willis), ha conseguido escapar del acoso terrorista. Aunque está solo y fuera de servicio, McClane se enfrentará a los secuestradores. Él es la única esperanza para los rehenes.</info>
 
 
<page>15</page><title>LA JUNGLA DE CRISTAL: ALERTA ROJA</title>
<microhd>b2b208b9186bf77912db2f223b4ff398b4c6f44b</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>371ffcaec69a88197a8400549c171a4b4e24574d</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/19PBdteqhLAgN6rZUDIU0Ka6qLH.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/cDtefl7KGnKrDziEUXetMnztvqr.jpg</fanart>
<date>1990</date>
<genre>Accion. Thriller.Saga</genre>
<extra>NA</extra>
<info>Es Navidad. El teniente John McLane, de la policía de Los Ángeles, espera en el aeropuerto de Dulles, Washington, la llegada de su esposa. Mientras tanto, el coronel Stuart, un militar que ha sido expulsado del Congreso, organiza cerca de este aeropuerto con otros mercenarios profesionales una base de operaciones para liberar a un dictador sudamericano derrocado, al que se considera el mayor traficante del mundo. El dictador llegará en 58 minutos al aeropuerto de Dulles en calidad de preso político. El plan de Stuart consiste en impedir el aterrizaje de los demás aviones, que se ven así condenados a permanecer en el aire con el consiguiente riesgo de quedarse sin combustible. Exige, además un Boeing 747 para huir con el dictador previamente liberado. Pero McClane intentará frustrar sus planes... </info>
 
 
<page>15</page><title>LA JUNGLA DE CRISTAL: LA JUNGLA 4.0</title>
<microhd>a806d2752af5d2702e1c992b8f721e86ad1154f6</microhd>
<fullhd>45fd28016b5f6a2d31b65caa3b0358796cf32049</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/8jC7C4dAohb2CdWrEmAPyeTMT3f.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/aRqTPOPt8BOHE0ngppM9jnuuaeS.jpg</fanart>
<date>2007</date>
<genre>Accion. Thriller.Saga</genre>
<extra>NA</extra>
<info>Estados Unidos. Un grupo terrorista bloquea el sistema de ordenadores que controla las comunicaciones, el transporte y el suministro de energía. El cerebro de la operación había estudiado minuciosamente hasta el más mínimo detalle, pero no había contado con John McClane (Bruce Willis), un policía de la vieja escuela, pero con los conocimientos necesarios para frustrar una amenaza terrorista de esta índole.</info>
 
 
<page>15</page><title>LA JUNGLA DE CRISTAL: LA VENGANZA</title>
<microhd>63f8facfb2409cffc4cd521dcf570724b142131e</microhd>
<fullhd>dd70c1fdf059e56b8c4ccc6d061780cf655d4bf2</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/uLVcPispwVVa5LMpAWwQngFcm8.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/fBvSfLetWlAMevQVHa0nBDV1Fon.jpg</fanart>
<date>1995</date>
<genre>Accion. Thriller.Saga</genre>
<extra>NA</extra>
<info>Un tal Simon siembra el pánico en las calles de Nueva York haciendo explotar una serie de bombas y asegura que no dejará de hacerlo a menos que el agente John McClane acceda a jugar con él a un juego llamado "Simón dice". Con la ayuda de Zeus, un electricista de Harlem, el agente comienza una trepidante carrera para resolver las adivinanzas planteadas por el terrorista y, al mismo tiempo, para averiguar sus intenciones.</info>
 
 
<page>15</page><title>LA JUNGLA DE CRISTAL: UN BUEN DIA PARA MORIR</title>
<microhd>9992c78e5697373bf028c108cab6175831b6538b</microhd>
<fullhd>4299ba4e8b327ef835e010f77fedba84619af08d</fullhd>
<tresd>NA</tresd>
<cuatrok>d71da1483a2db169689e425e18fd1fc979eb04e7</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/kBxvKk4COFo78sUgHzhbE5n8hMo.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/mV1HOCbUqx7nfFPwledYsvMYHrw.jpg</fanart>
<date>2013</date>
<genre>Accion. Thriller.Saga</genre>
<extra>NA</extra>
<info>El policía John McClane (Bruce Willis) se encuentra por sorpresa en Moscú con su hijo Jack (Jai Courtney) en el lugar y momento equivocados. Con los peores elementos de los bajos fondos rusos tras ellos y luchando contrarreloj para evitar una guerra, los dos McClane descubren que sus métodos opuestos para enfrentarse a las dificultades les pueden venir bien para aunar fuerzas para mantenerse vivos.</info>
 
 
<page>15</page><title>LA LEGO PELICULA</title>
<microhd>NA</microhd>
<fullhd>01d48cc34cf01d7cd0dc516abfe06fa8f63f774d</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/9F7OQVDMmvUJVwI7s0yzGNftC2F.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/lm0rMEgS2fKZdCM6JTplP6XwGnR.jpg</fanart>
<date>2017</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Spin-off de "La LEGO Película", protagonizado por Batman. En esta ocasión, el irreverente Batman, que también tiene algo de artista frustrado, intentará salvar la ciudad de Gotham de un peligroso villano, el Joker. Pero no podrá hacerlo solo, y tendrá que aprender a trabajar con sus demás aliados.</info>
 
<page>16</page><title>LA LEGO PELICULA 2</title>
<microhd>143f619a9e5e29d03419ddd8148fcd105db60f80</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/pOMqelYclqbnj4f7mL5KgSN5rJj.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/yfS366U7NYqiZYGLZguJwqx3rUF.jpg</fanart>
<date>2019</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Después de cinco años de tranquilidad aparece una nueva amenaza: invasores de LEGO DUPLO del espacio exterior que lo destrozan todo antes de que dé tiempo a reconstruirlo. Emmet, Lucy, Batman y sus amigos unirán fuerzas para librar una batalla que les llevará a mundos inexplorados.</info>
 
 
<page>16</page><title>LA LEYENDA DE REDBAD</title>
<microhd>89b54a1702a139a395188c23bf887d5d673f61cc</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/soE1emWKttbkx17shYWg4fNKikJ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/aEmW8KkAgAe0JAhMAFm9EzKmseL.jpg</fanart>
<date>2018</date>
<genre>Aventuras</genre>
<extra>NA</extra>
<info>En el año 754 d.C. Europa del Norte se encuentra dividida. Por un lado, está el grupo formado por los frisones, los sajones y los daneses. El otro lado está ocupado por los francos. Los últimos pretenden realizar aquello que los romanos no pudieron: conquistar toda Europa mediante su nueva arma, el cristianismo. Su primer objetivo es apoderarse del mayor centro de comercio de Europa gobernado por el rey frisón Aldegisel.</info>
 
 
<page>16</page><title>LA LEYENDA DEL INDOMABLE</title>
<microhd>9c875b743930d7657c3615b240bacc7ea25d2fe4</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/n2Jbb0iBmZmwEdkRNNC9IvF9vZS.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/aEeLXQ78ChkHcEEZS2jijdiH0As.jpg</fanart>
<date>1967</date>
<genre>Drama</genre>
<extra>Mis</extra>
<info>Luke Jackson, un joven rebelde e impulsivo, es condenado a dos años de prisión tras causar graves destrozos estando borracho. En la cárcel, su indomable carácter chocará de frente con las rígidas normas de la institución, así como con el de otros presos, especialmente el brutal Dragline, que era el líder de los convictos hasta su llegada. Pero Luke es un veterano de guerra que no está dispuesto a ceder, y tendrá que pagar un alto precio por seguir siendo quien es.</info>
 
 
<page>16</page><title>LA LIGA DE LA JUSTICIA DE ZACK SNYDER</title>
<microhd>d9dc35ae6419cdcb9f766cb514067e38189a5010</microhd>
<fullhd>75213fbbbea08ad9ed33cd8dffe85af1e7432efa</fullhd>
<tresd>NA</tresd>
<cuatrok>ede305cfa07c3d62846fbbc0cd070efc7c72d755</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/rkuvJnamPl3xW9wKJsIS6qkmOCW.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/9WoLTsRgrupkvCgs3bhsl2EjLhs.jpg</fanart>
<date>2021</date>
<genre>Fantástico. Acción </genre>
<extra>NA</extra>
<info>Con la determinación de asegurar que el sacrificio definitivo de Superman (Henry Cavill) no fue en vano, Bruce Wayne (Ben Affleck) une fuerzas con Diana Prince (Gal Gadot) para reclutar a un equipo de metahumanos que protejan el mundo de una amenaza inminente de proporciones catastróficas. La tarea es más difícil de lo que Bruce imaginaba, ya que cada uno de los reclutas deberá enfrentarse a sus propios demonios para trascender aquello que los detenía, para unirse y formar de manera definitiva una liga de héroes sin precedentes. Ahora unidos, Batman, la Mujer Maravilla, Aquaman (Jason Momoa), Cyborg (Ray Fisher) y Flash (Ezra Miller) deberán salvar al planeta de la amenaza de Steppenwolf, DeSaad y Darkseid, antes de que sea demasiado tarde.Versión extendida de "Justice League" (2017), que representará una versión fiel a la visión original de la obra de Zack Snyder, que fue apartado de la producción de la misma tras una tragedia personal y reemplazado por Joss Whedon.</info>
 
 
<page>16</page><title>LA LLAMADA DE LO SALVAJE</title>
<microhd>411fa08ed451b6437c74e62e262ba5feb0b9f722</microhd>
<fullhd>b91541565ab8fd7c1f0388ee5317801b563b100e</fullhd>
<tresd>NA</tresd>
<cuatrok>a296c1c7c655783eb5778eb3eb47b8ac3f6951ee</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/fp26T4ZR4WC5iInDGHKPrPdKQTL.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/lSOpD4uDLi7vuyiogeONtUXOooy.jpg</fanart>
<date>2020</date>
<genre>Aventuras</genre>
<extra>NA</extra>
<info>Cuenta la historia de Buck, un perro bonachón cuya vida cambia de la noche a la mañana cuando su dueño se muda de California a los exóticos parajes de Alaska, durante la fiebre del oro, a finales del siglo XIX. Como novato de un grupo de perros de trineo (y posteriormente su líder), Buck experimenta una aventura que jamás olvidará, encontrando su lugar en el mundo y convirtiéndose así en su propio amo</info>
 
 
<page>16</page><title>LA MALDICION</title>
<microhd>3b451a591523c85236d94b4d6db66967d60a4cca</microhd>
<fullhd>5a38d0442d27085d2ee37c43dba78faa7d668cdb</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/62gIJdks9agqdYavMHJx3FlRXp9.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/s0w7rbXDFzf5qs5irLIN8glZbY7.jpg</fanart>
<date>2020</date>
<genre>Terror</genre>
<extra>NA</extra>
<info>Una casa encantada por un espíritu vengativo maldice a todos aquellos que entran en el lugar, llevándolos a tener una muerte violenta.</info>
 
 
<page>16</page><title>LA MALDICION DE LAKE MANOR</title>
<microhd>615e8a340489346cb552d40c6dbfed5b68ee9dac</microhd>
<fullhd>059e31f4279d65c36906b861abf328f23ff39ac4</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/nwPku1PfyeKonma5lM3kFccs8fN.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/gWE2VGDx5FyVxSMv7tH38P6PFPm.jpg</fanart>
<date>2019</date>
<genre>Terror</genre>
<extra>NA</extra>
<info>Samuel es un niño parapléjico que vive junto a su madre Elena en Lake Manor (Torino, Italia), dentro de una mansión alejada de la civilización. El chico ha recibido órdenes de no abandonar nunca su casa, por lo que se siente seguro pero algo frustrado y oprimido. Su situación cambia cuando llega Denise, una ayudante adolescente que le ayuda a cambiar su punto de vista sobre la relación que le une con su madre. Sin embargo, Elena hará todo lo posible por impedir que su hijo se vaya de su lado, algo que confunde tanto a Denise como al propio Samuel, que sospecha que su madre oculta algo. </info>
 
 
<page>16</page><title>LA MALDICION DEL HOMBRE LOBO</title>
<microhd>281b1be33e945b15addf01bac42807bb002ab5d4</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>55be36772e8ec86343e91735da6c9b65669841ca</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/nrFXmNQgC1drbnthFcuMWazosLG.jpg</thumbnail>
<fanart>https://i.imgur.com/mfzJzID.jpg</fanart>
<date>2022</date>
<genre>Terror. Fantástico. Acción</genre>
<extra>NA</extra>
<info>Jack Russell es un descendiente de la rama de humanos alterados místicamente conocidos como Lycantropes. Durante la noche de la luna llena y las dos noches que lo rodean, se ve obligado a mutar en un hombre lobo, una forma grande y poderosa que es un híbrido de humano y lobo, y pierde su intelecto humano. A través de una serie de eventos, también es capaz de mutar voluntariamente fuera de la luna llena, momento en el que permanece en control.</info>
 
 
<page>16</page><title>LA MARAVILLOSA HISTORIA DE HENRY SUGAR</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>88989a7a55d43edb2a4ba229085e45aebf0e45ba</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/5oeLyShu9JK7C8U57xF00zuTovf.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/dS4JR8gmj8UnamjuLEyAkEFoppy.jpg</fanart>
<date>2023</date>
<genre>Aventuras. Comedia. Fantástico</genre>
<extra>Estreno</extra>
<info>Una historia de Roald Dahl sobre un hombre rico que se entera de la existencia de un gurú que puede ver sin usar los ojos y se propone dominar esa habilidad para hacer trampas en el juego.</info>
 
 
<page>16</page><title>LA MATANZA DE TEXAS</title>
<microhd>28C3162CCD2C5EF27994528C122716EF6B9DFE1B</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/lvwrg14ovXnAHgsmmlINnC3Oj13.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/aTSA5zMWlVFTYBIZxTCMbLkfOtb.jpg</fanart>
<date>2022</date>
<genre>Terror</genre>
<extra>NA</extra>
<info>Melody (Sarah Yarkin), su hermana adolescente Lila (Elsie Fisher) y sus amigos Dante (Jacob Latimore) y Ruth (Nell Hudson) viajan al remoto pueblo de Harlow (Texas) para montar un negocio muy idealista. Pero su sueño se convierte en una auténtica pesadilla cuando molestan sin querer a Leatherface, el desquiciado asesino en serie cuyo sangriento legado sigue acechando a los habitantes de la zona, entre ellos Sally Hardesty (Olwen Fouéré), la única superviviente de su masacre de 1973, decidida a vengarse a muerte.</info>
 
 
<page>16</page><title>LA MEMORIA DE UN ASESINO</title>
<microhd>NA</microhd>
<fullhd>925014c700b94150acb21d6a211f7c978d6d35ba</fullhd>
<tresd>NA</tresd>
<cuatrok>86128db32764480f91261f7cbf68698adb7e67d1</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/zfC8bfoMZhaCxswi41CjesysfhZ.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/9zXPnbVpaDfTniLBuc5vgXGfzAP.jpg</fanart>
<date>2022</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>Un asesino a sueldo descubre que se ha convertido en un objetivo después de que se niega a completar un trabajo para una peligrosa organización criminal... Remake de la película belga "De zaak alzheimer" (2003).</info>
 
 
<page>16</page><title>LA MENSAJERA</title>
<microhd>726a6176818b82b5523ad2f0de6e68433b606a8c</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/qobimZ1TkVJYiJDAOzVRMdBX3a8.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ne4QPHn56Z1vxk6iKJKvDnmNI9Y.jpg</fanart>
<date>2019</date>
<genre>Accion. Thriller</genre>
<extra>NA</extra>
<info>Ezequiel Mannings (Gary Oldman), es un criminal que busca matar a Nick, la única persona dispuesta a testificar en su contra y para ello contrata a un misterioso mensajero para que sin saberlo entregue una bomba y lo mate, pero después de que ella rescata a Nick de una muerte segura, el dúo debe enfrentarse a un ejército de asesinos despiadados para sobrevivir durante la noche.</info>
 
 
<page>16</page><title>LA MILLA VERDE</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>692c0fb195171a2fb7634ea006a3ca2c2857f320</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/1EFS40uFzv5ZVLSpu3xqYqnou67.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/l6hQWH9eDksNJNiXWYRkWqikOdu.jpg</fanart>
<date>1999</date>
<genre>Drama. Fantastico</genre>
<extra>Mis</extra>
<info>Ambientada en el sur de los Estados Unidos, en plena Depresión. Paul Edgecomb es un funcionario de prisiones encargado de vigilar la "Milla Verde", un pasillo que separa las celdas de los reclusos condenados a la silla eléctrica. John Coffey, un gigantesco hombre negro acusado de asesinar brutalmente a dos hermanas de nueve años, está esperando su inminente ejecución. Tras una personalidad ingenua e infantil, Coffey esconde un prodigioso don sobrenatural.</info>
 
 
<page>16</page><title>LA MISION</title>
<microhd>NA</microhd>
<fullhd>95075b4ce991e1774722e06a0f26d03c34770e6d</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/oTU13XXz4WHisDWKX3X6dFWEjC0.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/xTXXc9TLkBAekldDcUQSirD5E1w.jpg</fanart>
<date>1986</date>
<genre>Aventuras. Drama</genre>
<extra>Culto</extra>
<info>Hispanoamérica, siglo XVIII. En plena jungla tropical junto a las cataratas de Iguazú un misionero jesuita, el padre Gabriel (Jeremy Irons), sigue el ejemplo de un jesuita crucificado, sin más armas que su fe y un oboe. Al ser aceptado por los indios guaraníes, Gabriel crea la misión de San Carlos. Entre sus seguidores está Rodrigo Mendoza (Robert De Niro), ex-traficante de esclavos, mercenario y asesino, que buscando el perdón se hace jesuita y encuentra la redención entre sus antiguas víctimas. Después de luchar juntos durante años, se enfrentan a causa de la independencia de los nativos: Gabriel confía en el poder de la oración; Rodrigo, en la fuerza de la espada. </info>
 
 
<page>16</page><title>LA MOMIA</title>
<microhd>e057fd603e349b2799c8c3458e40ddcb8e045bf4</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/6JEbMNds0ZRBD5AYcIIhCqRVgQU.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/5TC6gKrmtnoGE3c3lHeNPevkn4Z.jpg</fanart>
<date>2017</date>
<genre>Accion. Aventuras. Fantastico</genre>
<extra>NA</extra>
<info>A pesar de estar enterrada en una tumba en lo más profundo del desierto, una antigua princesa (Sofia Boutella) cuyo destino le fue arrebatado injustamente, se despierta en la época actual, trayendo consigo una maldición que ha crecido hasta límites insospechados con el paso de miles de años.</info>
 
 
<page>16</page><title>LA MONJA II</title>
<microhd>NA</microhd>
<fullhd>9577ab34b3ccf2e4aff16de25a9031d9c7784598</fullhd>
<tresd>NA</tresd>
<cuatrok>c196fb81f49e57c456d9703fdd1758a1390fa70c</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/x0ryPlzZjpOojEAuGrre2lFuBv6.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/8Ahp89AbwLu8rmWhR6yhpTt6uTj.jpg</fanart>
<date>2023</date>
<genre>Terror</genre>
<extra>Estreno</extra>
<info>1956 – Francia. Un sacerdote es asesinado. Un mal se está extendiendo. La hermana Irene una vez más se encuentra cara a cara con Valak, la monja demonio. Secuela de "La monja" (2018).</info>
 
 
<page>16</page><title>LA MUERTE TENIA UN PRECIO</title>
<microhd>NA</microhd>
<fullhd>95eab2b75089cff7c9eb8c6a8f91f91571d50e92</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/uz6jPSxKkR4gW8JfuzjPv599QV2.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/hs6OPto38fF589ScpGhl7uSU4MI.jpg</fanart>
<date>1965</date>
<genre>Accion</genre>
<extra>Culto</extra>
<info>Dos cazadores de recompensas que buscan al mismo hombre deciden unir sus fuerzas para encontrarlo, aunque las razones que los mueven son completamente diferentes. Su título original ("Per qualche dollaro in più") ya sugiere que es la continuación natural de "Por un puñado de dólares" ("Per un pugno di dollari"), dirigida por Leone un año antes.</info>
 
 
<page>16</page><title>LA MUJER EN LA VENTANA</title>
<microhd>b8e4d1c74065b2916c9a9c83597769a596ed3b3f</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/bNjRktID2PmoVuatC3hLw9Dla5J.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/6P5vjT157AqjXDfTzmEXmOL2CIf.jpg</fanart>
<date>2021</date>
<genre>Intriga. Thriller </genre>
<extra>NA</extra>
<info>La Dra. Anna Fox, que sufre de agorafobia, pasa sus días encerrada en su casa de Nueva York, bebiendo vino mientras ve viejas películas y espía a sus vecinos. Un día, mientras mira por la ventana, ve algo que sucede enfrente de su casa, en el hogar de los Russell, una familia a la que todo el barrio toma por ejemplar... Nueva adaptación del bestseller de A.J. Finn. Guion escrito por el actor y escritor -ganador del premio Pulitzer- Tracy Letts.</info>
 
 
<page>16</page><title>LA MUJER REY</title>
<microhd>NA</microhd>
<fullhd>cb312e697b64ae1c6b5389c0ea517507ac6538f9</fullhd>
<tresd>NA</tresd>
<cuatrok>fa9e1e9f4144df56dd03511cc9149992669509b8</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/g7H3cg6aM11JOdlOLqaUp4BmMvM.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/p5kb4VwnK0bkxsOmME3SBKAcBoe.jpg</fanart>
<date>2022</date>
<genre>Acción. Drama</genre>
<extra>NA</extra>
<info>Una epopeya histórica inspirada en los hechos reales que sucedieron en el Reino de Dahomey, uno de los estados más poderosos de África en los siglos XVIII y XIX. La historia sigue a Nanisca (Davis), general de la unidad militar exclusivamente femenina y a Nawi (Mbedu), una recluta ambiciosa. Juntas lucharon contra enemigos que violaron su honor, esclavizaron a su gente y amenazaron con destruir todo por lo que habían vivido.</info>
 
 
<page>16</page><title>LA NARANJA MECANICA</title>
<microhd>E84AA7342D95013EF4009AA84BA62918BC86B5B5</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>0ee13b76cf9a1ddfd3accc0454e091ebe9a6f361</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/nUDNrudVKnulzKK4Aa0CyQJ6ef8.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/PdOlrT3TyQDMDgYkyhkktqn7dP.jpg</fanart>
<date>1971</date>
<genre>Drama</genre>
<extra>Culto</extra>
<info>Gran Bretaña, en un futuro indeterminado. Alex (Malcolm McDowell) es un joven muy agresivo que tiene dos pasiones: la violencia desaforada y Beethoven. Es el jefe de la banda de los drugos, que dan rienda suelta a sus instintos más salvajes apaleando, violando y aterrorizando a la población. Cuando esa escalada de terror llega hasta el asesinato, Alex es detenido y, en prisión, se someterá voluntariamente a una innovadora experiencia de reeducación que pretende anular drásticamente cualquier atisbo de conducta antisocial. </info>
 
 
<page>16</page><title>LA NAVIDAD MAGICA DE LOS JANGLE</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>aec2eaab87b436adb16670080049c75c76826d73</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ecpe029lbZf0VOfVp9DK8Voy8Vc.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/zAb4tFIXFI47M9C6WLTCIqzgsB0.jpg</fanart>
<date>2020</date>
<genre>Fantástico. Musical</genre>
<extra>NA</extra>
<info>Un mundo imaginario cobra vida en la historia de un excéntrico creador de juguetes, su aventurera nieta y una invención mágica que tiene el poder de cambiar sus vidas para siempre.</info>
 
 
<page>16</page><title>LA NIEBLA Y LA DONCELLA</title>
<microhd>003175890fe18a2c2de5739c0f85be3e1805eb7a</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>9cdf2d29f07ba85dacadded6fb8bfdea630bfa70</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/bEhIJIvNPbZfHrfvwctMZY7Xktu.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/fzNBm9WcD3vvztgPZG3fZKmA5Vo.jpg</fanart>
<date>2017</date>
<genre>Thriller. Intriga </genre>
<extra>NA</extra>
<info>El sargento Rubén Bevilacqua y la cabo Virginia Chamorro son dos investigadores de la Guardia Civil que deben trabajar en un complicado y antiguo caso. Dos años antes un político local fue el principal sospechoso de la muerte de un joven, cuyo cuerpo apareció degollado en la isla de la Gomera, aunque finalmente fue absuelto. Ahora nuevas pistas llegan a la mesa de Bevilacqua, que se embarca en un viaje a la isla junto a su compañera, donde se verán envueltos en una confusa historia de amores prohibidos y corrupción que pondrá a prueba su capacidad profesional y sus motivaciones más íntimas.</info>
 
 
<page>16</page><title>LA NOCHE DE HALLOWEEN</title>
<microhd>e39a8c3cb2b769dc40369f05538b918d7882ac03</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>7cbd08d3ca995e815a251ad26ddeb1a55da1aae1</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/3zENNQfzTYiMx6Ud9TKKkvIzLeB.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/crn09DUYgmFUPbOroxt1tQSoi0r.jpg</fanart>
<date>2018</date>
<genre>Terror</genre>
<extra>NA</extra>
<info>Jamie Lee Curtis regresa a su icónico personaje Laurie Strode, quien llega a la confrontación final con Michael Myers, la figura enmascarada que la ha perseguido desde que escapó de la matanza que él cometió la noche de Halloween de hace cuatro décadas. Nueva entrega de "Halloween", secuela directa de la original de 1978. John Carpenter (creador de la original) es el productor ejecutivo.</info>
 
 
<page>16</page><title>LA NOCHE DE LOS MUERTOS VIVIENTES</title>
<microhd>46826e610ae3b81757d3ce57cdce130f2628e29f</microhd>
<fullhd>db52be4594f409db4987e28c78693e2dac34c8dd</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/eWenMLMSeZcn8zUDKEf3DeRIaAO.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/hCrWbI2hquneBPnSKpslrOHk7Fb.jpg</fanart>
<date>1968</date>
<genre>Terror</genre>
<extra>Culto</extra>
<info>Las radiaciones procedentes de un satélite provocan un fenómeno terrorífico: los muertos salen de sus tumbas y atacan a los hombres para alimentarse. La acción comienza en un cementerio de Pennsylvania, donde Barbara, después de ser atacada por un muerto viviente, huye hacia una granja. Allí también se ha refugiado Ben. Ambos construirán barricadas para defenderse de una multitud de despiadados zombies que sólo pueden ser vencidos con un golpe en la cabeza.</info>
 
 
<page>16</page><title>LA NOVIA CADAVER</title>
<microhd>f0e94d1c1aa143e11b95ecfdf7fc0fcac598252c</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/3ALM0VeZjGUryAqWo6pqohzbLDh.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/lMnz3uAHGf7r0pcfoEjaifo4du4.jpg</fanart>
<date>2005</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Un hombre pone en el dedo de una mujer muerta, como broma, un anillo de compromiso. Pero lo que no sabe el pobre mortal es que la muerta reclamará sus derechos como "prometida"</info>
 
 
<page>16</page><title>LA OTRA MISSY</title>
<microhd>34ac9cff636fc809c04dc20762c05f004c12dbd8</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/5MQScaFf8Uu8Hj9omCQju4KsWAA.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/1xQtvgay8rDwSaZPwyhcecqV8UD.jpg</fanart>
<date>2020</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Tim Morris conoce a la chica de sus sueños, y la relación por SMS no podría ir mejor, así que decide dejarse llevar e invitarla al retiro que organiza su compañía en un resort en Hawái. Sin embargo, la que aparece en el aeropuerto lista para viajar es una chica con la que tuvo una cita a ciegas para el olvido. Ya es demasiado tarde: se ha estado escribiendo con “La otra Missy”.</info>
 
 
<page>16</page><title>LA PIEDAD</title>
<microhd>NA</microhd>
<fullhd>549f755e44f4516b79419545179e616553c0d724</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/psoGuNBrjzVqFz9xAwmjsAoiAAQ.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/fMrUyy3EpAQKu3jCjbj4QXYFjur.jpg</fanart>
<date>2023</date>
<genre>Drama. Comedia</genre>
<extra>NA</extra>
<info>Mateo vive con su madre, Libertad, en un mundo color de rosa; un microcosmos que tiene precisamente dos habitantes, madre e hijo. Un día a Mateo le diagnostican cáncer…</info>
 
 
<page>16</page><title>LA PIEL DEL TAMBOR</title>
<microhd>NA</microhd>
<fullhd>c34c983a0d8cba4b608de77ed98578a43e52152c</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/m1UiIK8ENvvcjuZHwo0X4cs8uGz.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/3ZhYEFXO0JlLEGAgOV45QGe6gGA.jpg</fanart>
<date>2022</date>
<genre>Thriller. Intriga</genre>
<extra>NA</extra>
<info>Ciudad del Vaticano, 1995. Un hacker informático irrumpe en el ordenador personal del Papa dejándole un mensaje en el que asegura que la Iglesia de Nuestra Señora de las Lágrimas mata para defenderse. Intrigado, el Santo Padre ordena que se investigue el asunto. Monseñor Spada (Paul Guilfoyle) asignará la misión a su mejor agente, el sacerdote Quart (Richard Armitage), poseedor de una larga experiencia en los asuntos oscuros de la Santa Sede. Entre tanto, en Sevilla, la aristócrata Macarena Bruner (Amaia Salamanca), una hermosa divorciada que es dueña de los derechos sobre el terreno donde se alza Nuestra Señora de las Lágrimas, escandaliza a la ciudad con sus amores inapropiados con un bailaor flamenco, mientras coquetea por Sevilla y hace parte principal de un grupo que se resiste a dejar demoler la Iglesia.</info>
 
 
<page>16</page><title>LA POSESION DE MARY</title>
<microhd>d67d6de15ea508c45c9a56588777696b6a9a8568</microhd>
<fullhd>4e7b20abd850e60db9d16b5d42cb182270eb1cca</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/4js1nTDelM4dEtJNUDmUOZMEM9K.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/3R4Oro4z904rWOecd0Qjo22PqII.jpg</fanart>
<date>2019</date>
<genre>Terror. Fantástico</genre>
<extra>NA</extra>
<info>Una familia que pretende abrir un negocio de transporte compra un barco que encierra un terrorífico secreto en su interior. Por desgracia para ellos, éste sólo sale a la luz cuando el buque se encuentra mar adentro. </info>
 
 
<page>16</page><title>LA PROFECIA</title>
<microhd>7d7176d8ad4eb8dd994e1304ac6c9695fb992e2c</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/p0LcWxOIoBx0MEZMn8tFcrvDXR1.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/lOshblIfomMbZClGRZ83lSU8bN7.jpg</fanart>
<date>1976</date>
<genre>Terror. Intriga. Saga</genre>
<extra>Mis</extra>
<info>Cuando Kathy Thorn da a luz a un bebé muerto, su esposo Robert le oculta la verdad y sustituye a su hijo por un niño huérfano, ignorando su origen satánico. El horror empieza cuando, en el quinto cumpleaños de Damien, inesperadamente, su niñera se suicida. Un sacerdote que trata de advertir a Robert del peligro que corre, muere en un inesperado accidente. El creciente número de muertes hace que Robert, por fin, se dé cuenta de que el niño que han adoptado es el Anticristo y que hay que eliminarlo para impedir que se cumpla una terrible profecía.</info>
 
 
<page>16</page><title>LA PROFECIA II: LA MALDICION DE DAMIEN</title>
<microhd>c4628f5e50e7548c6b2ae91275b658c7b45c6a17</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ocsi8SlODWkVX6O4E8Ncfgnretw.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/m4lhZ4QjPffAk66tXWo4rawsJAe.jpg</fanart>
<date>1978</date>
<genre>Terror. Saga</genre>
<extra>Mis</extra>
<info>Desde la repentina y sospechosa muerte de sus padres, Damien, vive con sus adinerados tíos (Lee Grant y William Holden). El chico, a quien muchos consideran el Anticristo, planea fríamente apoderarse del imperio empresarial de su tío como primer paso para dominar el mundo. Mientras tanto, todos los que intentan desvelar los secretos de su siniestro pasado, o de su pérfido futuro, encuentran la muerte de forma rápida y cruel... </info>
 
 
<page>16</page><title>LA PROFECIA III: EL FINAL DE DAMIEN</title>
<microhd>b904620a8f52a82ec5d0cd8f21e543168d87af48</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/7j2SO97oTveDctwq0mAB7Dn7f07.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/93N65nfw1PV9q3MaJ7PGiuaoJqm.jpg</fanart>
<date>1981</date>
<genre>Terror. Saga</genre>
<extra>Mis</extra>
<info>Damien Thorn (Sam Neill), de 32 años, es un ser cruel, frío y calculador que ha sido engendrado por el diablo y cuyo único objetivo es dominar el mundo. Con tal de conseguir sus propósitos está dispuesto a matara todo el que se cruce en su camino. Sólo un abnegado sacerdote (Brazzi), cuya misión es destruir al Anticristo y que tiene en su poder las siete dagas sagradas de Megiddo, se interpone entre Damien y su deseo de sembrar el caos y la destrucción</info>
 
 
<page>16</page><title>LA PROTECTORA</title>
<microhd>4b9b2cc3219ec89d7aabd6bee7bba877bd366857</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/rdah0UpjZmy8OAsSuYWVz1aMH7L.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/j7n2zYUODG8ViBSq7VyXyGlwJfH.jpg</fanart>
<date>2019</date>
<genre>Accion. Thriller</genre>
<extra>NA</extra>
<info>A Sam (Noomi Rapace), una experta guardaespaldas y agente contra-terrorista acostumbrada a trabajar en zonas de guerra, le asignan una misión que ella presupone rápida y fácil: la protección de Zoe (Sophie Nélisse), una joven y rica heredera. A Sam no se le da bien tratar con jóvenes consentidas ni a Zoe le atrae la idea de tener una niñera, pero un violento intento de secuestro obligará a ambas a salir huyendo. En el camino tendrán que acabar con algunas vidas o, de lo contrario, perder las suyas. </info>
 
 
<page>16</page><title>LA PROTEGIDA</title>
<microhd>NA</microhd>
<fullhd>F927276A2D7F4DBC50C6FE69C6CAD49D558B42CD</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/IRdFT8ikE5HDWwQjEpZqnY9o7P.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/fbTxsnJcQwuwzCEu9VEiU9lV75Y.jpg</fanart>
<date>2021</date>
<genre>Thriller. Acción</genre>
<extra>NA</extra>
<info>Cuando no era más que una niña, Anna (Maggie Q) fue recogida por Moody (Samuel L. Jackson), un legendario asesino, su mentor y figura paterna. 20 años después, Anna se ha convertido en una de las asesinas a sueldo más hábiles del planeta. Cuando Moody es brutalmente asesinado, Anna jura venganza y, para ello, se alía con Rembrandt (Michael Keaton), un enigmático asesino. A medida que crece el acercamiento entre ambos, la confrontación se va volviendo más y más peligrosa.</info>
 
 
<page>16</page><title>LA PURGA. LA NOCHE DE LA BESTIAS: THE PURGUE</title>
<microhd>b935c8946934a2d1111761fb14c9b4810d114c38</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/gn1sNctXEHzAhTXxfSLYBTeYeuM.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/anFRs7Xi7u2nqqBpmhqvXINAPix.jpg</fanart>
<date>2013</date>
<genre>Saga.Thriller. Terror</genre>
<extra>NA</extra>
<info>Año 2022. En una futura sociedad distópica, el régimen político, llamado Nueva Fundación de los padres de América, ha implantado una medida catárquica ante la violencia campante y las cárceles saturadas: la "purga anual", según la cual una noche al año se puede cometer cualquier clase de crimen, incluso el asesinato, sin tener que responder ante la justicia. La violencia se desata durante esas 12 horas, y los individuos se desfogan, imperando la calma el resto del año. Horas antes de que comience la purga anual, el vendedor de sistemas de seguridad James Sandin regresa a su casa en un lujoso suburbio de Los Angeles junto a su esposa María y sus dos hijos, Zoey y Charlie. Lo que no contaba esa famillia es que poco tiempo después de comenzar la noche un extraño llame a la puerta. </info>
 
 
<page>16</page><title>LA PURGA. LA NOCHE DE LAS BESTIAS: ANARCHY</title>
<microhd>d8069ce4d4cfa1d8bb84d46896a81d4918570ec1</microhd>
<fullhd>3e70780b11556e0453bd2f2f909aa27ca194ecaf</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ynDDbzVge9AzuTkeyw8JcKr58Cq.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/zWGAnbxjjwY3xFGuOeR26LGbBlG.jpg</fanart>
<date>2014</date>
<genre>Saga.Thriller. Terror. Accion</genre>
<extra>NA</extra>
<info>El crimen hace estragos en Estados Unidos y las cárceles están llenas. El gobierno decide que una noche al año, durante doce horas, cualquier actividad criminal, incluso el asesinato, será legal. No se puede llamar a la policía, los hospitales no admiten pacientes, los ciudadanos deben arreglárselas solos porque cometer un delito no está castigado. Durante esa noche plagada de peligros, 5 personas que se han quedado fuera de sus casas deciden juntarse para poder enfrentarse mejor a los ciudadanos salvajes que buscan calmar sus ansias de violencia.</info>
 
 
<page>16</page><title>LA PURGA. LA NOCHE DE LAS BESTIAS: ELECTION</title>
<microhd>4d2defc26b16f9d44bd1f0a74cae5a2a47ae90e0</microhd>
<fullhd>1722ec5bae0cf41308ea1ee709e94cd8757b9208</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/wEpLdiISE6mLHXOc1iDnd3edfal.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/a7V3ZXCDAk3P7GjpgfN0IInjh0r.jpg</fanart>
<date>2016</date>
<genre>Saga.Thriller. Terror. Accion</genre>
<extra>NA</extra>
<info>Han pasado dos años desde que el ex-sargento de policía Leo Barnes (Frank Grillo) decidió no vengarse del hombre que mató a su hijo. Ahora Barnes dirige al equipo de seguridad que se encarga de proteger a la senadora Charlie Roan (Elizabeth Mitchell), una candidata a la Presidencia que reivindica la supresión de La Purga anual, que consiste en permitir, una noche al año, cualquier actividad criminal, incluído el asesinato. Roan considera que esta práctica perjudica sobre todo a los necesitados y a los pobres.</info>
 
 
<page>16</page><title>LA PURGA. LA NOCHE DE LAS BESTIAS: INFINITA</title>
<microhd>ce0a33aab87248fce30cd80771755d9cdce2663b</microhd>
<fullhd>2bf26ae5b072ef2b11ea7d5615774ac710c0d12a</fullhd>
<tresd>NA</tresd>
<cuatrok>7c1ff2d520afb02bf2ca6c90d1452512bcec487f</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/zWiXxOTM3NeYcj3dzbtoGqIIJPz.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/tehpKMsls621GT9WUQie2Ft6LmP.jpg</fanart>
<date>2021</date>
<genre>Thriller. Terror. Acción</genre>
<extra>NA</extra>
<info>Para los miembros de un movimiento clandestino, ya no es suficiente una noche anual de anarquía y asesinatos, así que deciden devolver a Estados Unidos el caos y las masacres sin fin, donde nadie volverá a estar a salvo... jamás. Adela (Ana de la Reguera) y su esposo Juan (Tenoch Huerta) viven en Texas, donde él trabaja como peón en un rancho para la adinerada familia Tucker. En la mañana después de la tradicional purga, una banda de asesinos enmascarados ataca violenta e ilegalmente a la familia Tucker, Tras este hecho, la familia texana se ve obligada a unirse con Juan y su esposa para enfrentar un país entero a punto de colapsar, que se hunde en un mar de caos y de sangre. Quinta y última entrega de la saga 'The Purge'.</info>
 
 
<page>16</page><title>LA PURGA. LA NOCHE DE LAS BESTIAS: LA PRIMERA PURGA</title>
<microhd>50ca9a50b377b0393b6cedb390d1eaf7e532a99a</microhd>
<fullhd>3be0c142e70456ed72b1f4b0688d15056bdf9d95</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/aO1DumNAMId7aYZyWScDRx4AQfu.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/r4clWIxKeWoqrHspQP0XunuUmB7.jpg</fanart>
<date>2018</date>
<genre>Saga. Thriller. Terror. Accion</genre>
<extra>NA</extra>
<info>Para mantener durante el resto del año la tasa de criminalidad por debajo del 1%, los Nuevos Padres Fundadores de América ponen a prueba una teoría sociológica que da rienda suelta a todo tipo de agresiones durante una noche en una comunidad aislada. Pero cuando la violencia de los opresores se encuentra con la ira de los marginados, el vandalismo explota más allá de esas fronteras “experimentales” para extenderse por todo el país. Detrás de cada tradición hay una revolución. Descubre cómo toda una nación entera abrazó una brutal celebración anual: 12 horas de impunidad criminal. Bienvenidos a un movimiento que comenzó como un simple experimento.</info>
 
 
<page>16</page><title>LA REINA DE AFRICA</title>
<microhd>4EAC0B8A95E1A08D485B39459BBF3AE2FBCBE370</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/kKFqbL0Sz26hwTXC792TybDlCdi.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/gwe1k68ZFxEo8ZqqR73oobiQwK4.jpg</fanart>
<date>1951</date>
<genre>Aventuras. Romance</genre>
<extra>Mis</extra>
<info>Al estallar la Primera Guerra Mundial (1914-1918), Charlie Allnut (Bogart), un rudo capitán de barco con tendencia a la bebida, y Rose Sayer (Hepburn), una estirada y puritana misionera, huyen de las tropas alemanas en una ruinosa embarcación, con la que deben descender un peligroso río. Son, a primera vista, dos seres antagónicos, incompatibles, pero la convivencia y, sobre todo, las penalidades que tendrán que afrontar juntos para sobrevivir harán cambiar radicalmente su relación.</info>
 
 
<page>16</page><title>LA SEMILLA DEL DIABLO</title>
<microhd>b1b5cfbd03953047900ab96271ed1f722dce7f02</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/rDUxRQDN2rFxsKayKJzozHqE33U.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/9IGQkvnVOZa9Ruwdu2BVl6kAQvS.jpg</fanart>
<date>1968</date>
<genre>Terror. Drama</genre>
<extra>Culto</extra>
<info>Los Woodhouse, un matrimonio neoyorquino, se mudan a un edificio situado frente a Central Park, sobre el cual, según un amigo, pesa una maldición. Una vez instalados, se hacen amigos de Minnie y Roman Castevet, unos vecinos que los colman de atenciones. Ante la perspectiva de un buen futuro, los Woodhouse deciden tener un hijo; pero, cuando Rosemary se queda embarazada, lo único que recuerda es haber hecho el amor con una extraña criatura que le ha dejado el cuerpo lleno de marcas. Con el paso del tiempo, Rosemary empieza a sospechar que su embarazo no es normal.</info>
 
 
<page>16</page><title>LA SERPIENTE BLANCA</title>
<microhd>b8d0e1d1d9db3e90320a09e02cd282f19f8ecc67</microhd>
<fullhd>aef03413ec9caf037f3c838a92ebb268c58c0d88</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/t4ikAfRDiwaD8lZITW7oF9RxY30.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/jIAyjJMrEIDjojxQDdZemDwqr82.jpg</fanart>
<date>2019</date>
<genre>Fantástico</genre>
<extra>NA</extra>
<info>Una joven mujer amnésica llamada Blanca es rescatada in extremis de la muerte por un atrapa-serpientes. Sin embargo, está lejos de haberse salvado por completo. Ambos deciden trabajar juntos para buscar pistas sobre el pasado de Blanca. A medida que superan un sinfín de obstáculos, comienzan a sentir algo el otro por el otro. Justo cuando están a punto de descubrir quién es ella realmente sucede una terrible desgracia.</info>
 
 
<page>16</page><title>LA SIRENITA</title>
<microhd>NA</microhd>
<fullhd>26fe398554006baf19ad4427a7f4ad2dde9a0afa</fullhd>
<tresd>NA</tresd>
<cuatrok>18d755ef3b4fb81bace821d3003a8d1974151256</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/mdszPVnIY7cWgbgJ8zbwu1PiU5V.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/rlahuHj5Yesn3xsaF9YRFlH4HMg.jpg</fanart>
<date>2023</date>
<genre>Infantil</genre>
<extra>Estreno</extra>
<info>Ariel es una joven y soñadora sirena, hija del Rey Tritón, que está cansada de su vida como princesa bajo el mar. Su mayor deseo es abandonar el fondo marino y tener una vida normal y apacible en la superficie. La bruja Úrsula descubre su anhelo y, por eso, a cambio de su preciosa voz le ofrece un cuerpo totalmente humano. Ariel acepta, movida por su pasión por conocer el mundo terrestre y porque está enamorada de un apuesto príncipe humano. Pero las cosas no serán fáciles para Ariel, ya que la bruja Úrsula esconde oscuras intenciones.</info>
 
 
<page>16</page><title>LA SIRENITA ( TRILOGIA )</title>
<microhd>f143220f191d74c68ec28cca55894162e90ab987</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/2ZucaVMGVqtmi7MD4adOq51kdAX.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/2ze42e0l0bPYEcJXInUukvNfZKk.jpg</fanart>
<date>1989</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Ariel, hija del rey Tritón, es la princesa de las sirenas. Está a punto de celebrarse su fiesta de cumpleaños y su mayor ilusión es poder conocer a los seres humanos. Con la ayuda de la bruja Úrsula, Ariel consigue subir a la superficie, donde salva de morir ahogado a un hermoso príncipe, cuyo barco acaba de naufragar, del que se enamora perdidamente.</info>
 
 
<page>16</page><title>LA SOMBRA DE LA LEY</title>
<microhd>c3dba8e4af63f2577d8acc0e8653c89183a1c886</microhd>
<fullhd>94d71d5b5f03fcc95b393d593c9cf1674c1ce204</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/1oC6UwQQm79VxHJUoSi8vcE6Jsm.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/9kAKaGPydPIhtY7X3n4rF1bchmS.jpg</fanart>
<date>2018</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Año 1921. España vive un momento agitado y caótico: son los años del plomo, fruto de los violentos enfrentamientos callejeros entre matones y anarquistas. El gansterismo y los negocios ilegales están instalados en la sociedad. En esta situación de disturbios, Aníbal Uriarte es un policía enviado a Barcelona para colaborar en la detención de los culpables del robo a un tren militar. Aníbal y sus formas no encuentran mucho apoyo entre sus compañeros, y enseguida comienzan los enfrentamientos y desconfianzas con el inspector Rediú, un superior corrupto. Aníbal entrará en contacto no sólo con los bajos fondos de la sociedad barcelonesa, sino también con el mundo anarquista más radical, dispuesto a todo para conseguir sus objetivos. Allí conocerá a Sara, una joven luchadora y temperamental, cuyo encuentro tendrá consecuencias inesperadas para ambos.</info>
 
 
<page>16</page><title>LA SONRISA ETRUSCA</title>
<microhd>46be99fc88ed0c0cfd930e81c088630fe808f679</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/b9X5a7bOWWouNBRyp307xiazXMH.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/8MnKYV9Z0xcPeMxwippykuVDhOM.jpg</fanart>
<date>2018</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Rory MacNeil (Brian Cox) es un escocés cascarrabias que abandona a regañadientes su querida y apartada isla de Vallasay para viajar a San Francisco en busca de tratamiento médico para su enfermedad terminal. Al mudarse con su hijo, al que hace quince años que no ve, la vida de Rory sufrirá una transformación a través del vínculo que establece con su nieto, justo cuando menos se lo espera.</info>
 
 
<page>16</page><title>LA SUITE NUPCIAL</title>
<microhd>acaad6f0d26b404686ecfecfc0eb60a368b7a4ff</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/7AZjSEnFqTngyebB6MYilV7C1y9.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/acpuA0E3qXVOFPJ8HsMYdWNOXhe.jpg</fanart>
<date>2020</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Fidel (Carlos Iglesias) es un hombre normal y corriente de mediana edad que intenta subirse al último tren que pasará por su aburrida y monótona vida, aunque esto implique un fin de semana a todo lujo y completamente fuera de su alcance en la Suite Nupcial de un hotel en Toledo. Allí tratará de tener una aventura con Marisa (Ana Arias), aunque las cosas no pasarán exactamente como las tenía pensadas. Casualidades de la vida, su mujer también tenía planes para ese mismo fin de semana… Se masca la tragedia.</info>
 
 
<page>16</page><title>LA TORRE OSCURA</title>
<microhd>f652c4526cb0399ada470d9d102f42c5ea282520</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>c45bd7f865d123a7be26dcafa6d673952ddb079b</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/rNwemYHVqgG5xFGFJpMtCWe3GVM.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/pVVobDO8cezhVPvwD6EBUN0g3mt.jpg</fanart>
<date>2017</date>
<genre>Fantastico. Western. Ciencia ficcion</genre>
<extra>NA</extra>
<info>En un mundo extrañamente parecido al nuestro un cowboy de nombre Roland Deschain de Gilead persigue a su eterno enemigo, "el hombre de negro". Roland, solitario, quizá maldito, anda sin descanso a través de un paisaje triste y abandonado. Conoce a Jake, un chico de Nueva York pero venido de otro tiempo, y ambos unen sus destinos. Ante ellos están las montañas. Y mucho más allá, la Torre Oscura... Adaptación de la saga literaria de Stephen King.</info>
 
 
<page>16</page><title>LA TRIBU</title>
<microhd>af0b0685b3d2e58349ddbe82f92514eb61797e72</microhd>
<fullhd>6097e5d6fe9a1c76904cda695f83f9898b9f9e83</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/pASeM9pyZtqDqTkXD1mNvqqh9hK.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/kn9mkIS9Wawan5F18RpHl2lQer3.jpg</fanart>
<date>2018</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Virginia (Carmen Machi), limpiadora de profesión y “streetdancer” vocacional, recupera al hijo que dio en adopción: Fidel (Paco León), un ejecutivo que lo ha perdido todo, incluida la memoria. Junto a “Las Mamis”, el extravagante grupo de baile que forman las compañeras de Virginia, madre e hijo descubrirán que a pesar de venir de mundos muy diferentes, ambos llevan el ritmo en la sangre.</info>
 
 
<page>16</page><title>LA TRINCHERA INFINITA</title>
<microhd>6494e6b2fb142637c2763cd7d1ffd81e336a86c5</microhd>
<fullhd>7eb67f4b4fac65da0cb18835e22fcd4931791e2b</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/4l28g28lT2yv1eIX0XNqNP4osPw.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/r73j0zjtTOsOCStHLby23NNDJyI.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Higinio y Rosa llevan pocos meses casados cuando estalla la Guerra Civil, y la vida de él pasa a estar seriamente amenazada. Con ayuda de su mujer, decidirá utilizar un agujero cavado en su propia casa como escondite provisional. El miedo a las posibles represalias, así como el amor que sienten el uno por el otro, les condenará a un encierro que se prolongará durante más de 30 años.</info>
 
<page>17</page><title>LA ULTIMA GRAN ESTAFA</title>
<microhd>NA</microhd>
<fullhd>679bebf288c4e655d57f4ed6367ec7d5b8f69060</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/t8MS3MdQZa9UOdAcXx6lfCD7TiY.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/wRrhiUWQSrmCzuVRTUfrei6p3VY.jpg</fanart>
<date>2020</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Max Barber (Robert de Niro) es un productor de cine de Hollywood de serie B que necesita encontrar un nuevo proyecto que le permita saldar su deuda con un jefe de la mafia local (Morgan Freeman). Decide emprender la producción de una película con escenas de acción de alto riesgo, con el fin de provocar la muerte de su actor protagonista y poder cobrar así el altísimo seguro, solucionando definitivamente sus problemas económicos. El actor elegido es Duke Montana (Tommy Lee Jones), una vieja estrella deprimida con problemas con la bebida: el blanco perfecto para los planes de Max. Sin embargo, los días de rodaje pasan y no todo sale según lo planeado. </info>
 
 
<page>17</page><title>LA VENTANA INDISCRETA</title>
<microhd>08e84ef770b309c63f7691a7694ca4bd03607af2</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>059533f65c92024c3fc13d21a53823477f29f755</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/fH1MipE8PXGg0rlI5cUdzxKnyA2.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/mP72QWmpidCjalXiIc4ChUxoukc.jpg</fanart>
<date>1954</date>
<genre>Intriga</genre>
<extra>Culto</extra>
<info>Un reportero fotográfico (Stewart) se ve obligado a permanecer en reposo con una pierna escayolada. A pesar de la compañía de su novia (Kelly) y de su enfermera (Ritter), procura escapar al tedio observando desde la ventana de su apartamento con unos prismáticos lo que ocurre en las viviendas de enfrente. Debido a una serie de extrañas circunstancias empieza a sospechar de un vecino cuya mujer ha desaparecido</info>
 
 
<page>17</page><title>LA VERDADERA HISTORIA DE LA BANDA DE KELLY</title>
<microhd>f0d81d5c221e566afeab1ecc114328763522ed08</microhd>
<fullhd>cb4c06db7d33613b416759321cec95f8f9da8867</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/2v0Hq1BJGeR8sit8j86Vnt01IqI.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/aNXmc2cpxNkQX1IaHU6bXH56WbT.jpg</fanart>
<date>2019</date>
<genre>Western. Acción</genre>
<extra>NA</extra>
<info>Basada en la novela de Peter Carey, ganadora del Premio Booker, narra la vida del bandido australiano Ned Kelly. Tras la muerte de su padre, el joven Ned tuvo que tratar de hacer lo posible para asegurarse de que su madre sobreviviera. Sin embargo, esta no era una tarea fácil en la Australia de 1870, asolada por la pobreza y por el hambre. Ned comenzará, entonces, un viaje que le convertirá en una leyenda.</info>
 
 
<page>17</page><title>LA VIDA DE BRIAN</title>
<microhd>d3c16a559c993346299888eaa4245bfaf02089b5</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/jWN2e7abSXfYmjMTkFzNnkrHn88.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/qkyB1HaOSc9mo651QryMn7OAdUZ.jpg</fanart>
<date>1979</date>
<genre>Comedia</genre>
<extra>Culto</extra>
<info>Brian nace en un pesebre de Belén el mismo día que Jesucristo. Un cúmulo de desgraciados y tronchantes equívocos le harán llevar una vida paralela a la del verdadero Hijo de Dios. Sus pocas luces y el ambiente de decadencia y caos absoluto en que se haya sumergida la Galilea de aquellos días, le harán vivir en manos de su madre, de una feminista revolucionaria y del mismísimo Poncio Pilatos, su propia versión del calvario.</info>
 
 
<page>17</page><title>LA VIDA ES BELLA</title>
<microhd>5b8d23ceabea7ff05cbefee1b4eedb2b01015848</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/qok7mdjAycemPkc5BD7VexRke77.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/uPhy27HEw7OKNY9DGh1ddNMC9MR.jpg</fanart>
<date>1997</date>
<genre>Comedia. Drama </genre>
<extra>Mis</extra>
<info>En 1939, a punto de estallar la Segunda Guerra Mundial (1939-1945), el extravagante Guido llega a Arezzo, en la Toscana, con la intención de abrir una librería. Allí conoce a la encantadora Dora y, a pesar de que es la prometida del fascista Rodolfo, se casa con ella y tiene un hijo. Al estallar la guerra, los tres son internados en un campo de exterminio, donde Guido hará lo imposible para hacer creer a su hijo que la terrible situación que están padeciendo es tan sólo un juego.</info>
 
 
<page>17</page><title>LA VIEJA GUARDIA</title>
<microhd>d2ec869e935bed88acd63e3083d099bc000c85ec</microhd>
<fullhd>0e3a97962c08bfa0f745151289d5cc62b20ff23c</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/qzYP4eiDmBoVztMUmWtKqMZcUGN.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/m0ObOaJBerZ3Unc74l471ar8Iiy.jpg</fanart>
<date>2020</date>
<genre>Fantástico. Acción</genre>
<extra>NA</extra>
<info>Un grupo de mercenarios inmortales liderado por una guerrera llamada Andy (Charlize Theron) ha luchado para proteger el mundo durante siglos. Pero cuando el equipo es reclutado para ejecutar una misión de emergencia y sus extraordinarias habilidades salen a la luz, Andy y Nile (Kiki Lane), la soldado más joven en unirse al grupo, tendrán que ayudar a sus compañeros a eliminar la amenaza de aquellos que buscan replicar y beneficiarse de sus poderes a toda costa...</info>
 
 
<page>17</page><title>LADRONES DE ELITE</title>
<microhd>49ef69849f606277de80a40c30b73a1d2d91ce40</microhd>
<fullhd>e887493ce4cba23170fc2ca69b59402b967c1541</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/y3pmIbrIFhLfTplrxpAo0S1tY1b.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/6FzuNrApEc71aJ3CwwkpadbNled.jpg</fanart>
<date>2021</date>
<genre>Acción. Thriller</genre>
<extra>NA</extra>
<info>Después de ser reclutado por un grupo peculiar de ladrones, el conocido criminal Richard Pace se ve envuelto en un complicado robo de oro que puede cambiarle la vida.</info>
 
 
<page>17</page><title>LADY BIRD</title>
<microhd>de2443f2e00cf815b95f29561dd2bd8af2f046ef</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/En0kxUKuSkOPYYa7RBbgeL7TvQ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/2ByWxoMbfE3pxztCJn5qTJ5Ui2Y.jpg</fanart>
<date>2017</date>
<genre>Comedia. Drama</genre>
<extra>NA</extra>
<info>Christine (Saoirse Ronan), que se hace llamar "Lady Bird", es una adolescente de Sacramento en su último año de instituto. La joven, con inclinaciones artísticas y que sueña con vivir en la costa Este, trata de ese modo encontrar su propio camino y definirse fuera de la sombra protectora de su madre (Laurie Metcalf).</info>
 
 
<page>17</page><title>LADY MACBETH</title>
<microhd>978b63b8ae1c6a5fca56b4fcea64167578168dcd</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/56wRThVBA2cGv9ek3BbnLQssW0v.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/wztoDoglCOuiEfzl1FS6mglBioF.jpg</fanart>
<date>2017</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>La Inglaterra rural de 1865. Katherine (Florence Pugh) vive angustiada por culpa de su matrimonio con un hombre amargado al que no quiere y que le dobla la edad, y de su fría y despiadada familia. Cuando se embarca en un apasionado idilio con un joven trabajador de la finca de su marido, en su interior se desata una fuerza tan poderosa que nada le impedirá intentar conseguir lo que desea.</info>
 
 
<page>17</page><title>LAGRIMAS DEL SOL</title>
<microhd>c4f6888a60aaa96edfac875fdb3b22b8126bd9f2</microhd>
<fullhd>f0c81cf467a8679b23c44c5310d997a409a0a41b</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/qRDZQkcp8Hu0WlTlWL7ivauPh5g.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/14SEmHUzZ2XsKtGtiGA1C6dn6lf.jpg</fanart>
<date>2003</date>
<genre>Bélico. Acción</genre>
<extra>Mis</extra>
<info>Un equipo comandado por el teniente Waters (Bruce Willis), un veterano oficial de la unidad especial SEAL, se adentra en el corazón de África con la misión de rescatar a Lena Kendrick (Monica Bellucci), una doctora americana que trabaja en una zona muy conflictiva. Pero, cuando la encuentran, ella se niega a abandonar a los refugiados y exige a Waters que los escolte a todos a través de la peligrosa jungla nigeriana. El oficial tendrá que enfrentarse a un grave dilema: obedecer las órdenes recibidas o, por el contrario, seguir los dictados de su propia conciencia.</info>
 
 
<page>17</page><title>LAS AVENTURAS DE TADEO JONES</title>
<microhd>NA</microhd>
<fullhd>b1ade3d56b6082a0f5bf8589fdcf53e0740e3746</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/mXM3Ou7D7LErxmEQLMwcVVcJkCf.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/1dY5eevEiLpnRfUpbk4rzpJTZP2.jpg</fanart>
<date>2012</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Debido a un malentendido, a Tadeo, un albañil soñador, lo confunden con un famoso arqueólogo y lo envían en una expedición al Perú. Con la ayuda de su fiel perro Jeff, una intrépida profesora, un loro mudo y un buscavidas, intentará salvar la mítica ciudad perdida de los Incas de una malvada empresa de cazatesoros. Adaptación del corto que ganó un Goya en 2004</info>
 
 
<page>17</page><title>LAS AVENTURAS DEL DOCTOR DOLITTLE</title>
<microhd>e5a3d47b494295d277911f1b0d1644611fb0aa82</microhd>
<fullhd>a4fd11cf49a8c368a3a432ec1cd67f8d8de8e158</fullhd>
<tresd>NA</tresd>
<cuatrok>ce963e18d46895b8cbe9f55d09524c4e24223089</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/12UGnmb4YdovJZ2QKjRMIsAJ16E.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/xcUf6yIheo78btFqihlRLftdR3M.jpg</fanart>
<date>2020</date>
<genre>Aventuras. Comedia.Infantil</genre>
<extra>NA</extra>
<info>Después de perder a su mujer hace siete años, el excéntrico Dr. John Dolittle , un reputado doctor y veterinario, se encerró en sí mismo tras los muros de su mansión con la única compañía de su colección de animales exóticos. Pero cuando la joven reina cae gravemente enferma, Dolittle tendrá que dejar, muy a su pesar, su vida de ermitaño para embarcarse en una épica aventura a una mítica isla en busca de una cura, recuperando su sentido del humor y su coraje a medida que se cruza con viejos adversarios y mientras descubre maravillosas criaturas.</info>
 
 
<page>17</page><title>LAS BRUJAS (DE ROALD DAHL)</title>
<microhd>2448af3696e3b7d631a6638a92ab3f0f01b0df5e</microhd>
<fullhd>26440af4bc37aad8a2dbcdc87ec69cf49012bd68</fullhd>
<tresd>NA</tresd>
<cuatrok>2eb9bc6f2dfea94684edd8ddbafbef4f2e37838f</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/s1y92244iBlKW04rEnaEK8i8tPY.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/8rIoyM6zYXJNjzGseT3MRusMPWl.jpg</fanart>
<date>2020</date>
<genre>Fantástico. Comedia. Terror</genre>
<extra>NA</extra>
<info>Cuenta la conmovedora historia de un pequeño huérfano (Bruno) que, a finales de 1967, se va a vivir con su querida abuela (Spencer) a Demopolis, un pueblo de Alabama. El niño y su abuela tienen extraños encuentros con algunas brujas aparentemente glamurosas pero absolutamente diabólicas, así que ella decide quitarse de en medio y llevarse al chico a un hotel de lujo en la costa. Por desgracia, llegan exactamente al mismo tiempo que la Gran Bruja (Hathaway) que ha reunido a sus compinches de todo el planeta -que van de incógnito- para llevar a cabo sus horribles planes. Adaptación del libro "Las brujas" de Roald Dahl.</info>
 
 
<page>17</page><title>LAS CONSECUENCIAS</title>
<microhd>NA</microhd>
<fullhd>a234d58aed8cb1aa57e62d8e3c1afd8c110e0a2b</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/zOfDFwYFQok5dhgjk0WCClh7b1Q.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/zOfDFwYFQok5dhgjk0WCClh7b1Q.jpg</fanart>
<date>2021</date>
<genre>Drama. Thriller. Intriga</genre>
<extra>NA</extra>
<info>En un viaje a una pequeña isla volcánica, Fabiola se convierte en espía de su hogar. No tiene evidencias ni certezas, pero su intuición le dice que no todo es lo que parece. Se debate entre el miedo a lo que puede encontrar y la necesidad de obtener respuestas. ¿Hasta dónde hurgar en la intimidad de los demás?, ¿hasta dónde mentir para proteger a la gente que quieres?</info>
 
 
<page>17</page><title>LAS DISTANCIAS</title>
<microhd>NA</microhd>
<fullhd>5c6e24b23bef4c0ea4200c6c72f5e9391e47588d</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ayf3UnmBoUQlJAxbM9gLhdMqem4.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/tNgMtrR9RR4HOxiDjcKMveAiCaV.jpg</fanart>
<date>2018</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Olivia, Eloy, Guille y Anna viajan a Berlín para visitar por sorpresa a su amigo Comas que cumple 35 años. Este no los recibe como ellos esperaban y durante el fin de semana sus contradicciones afloran y la amistad se pone a prueba. Juntos descubrirán que el tiempo y la distancia pueden cambiarlo todo.</info>
 
 
<page>17</page><title>LAS ESPIAS DE CHURCHILL</title>
<microhd>72a438450dc1664a2dd5a6ef6bca8ecf60ec5376</microhd>
<fullhd>80bf509a9677179141ad3316470e8f6413e5b7e2</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/pZuMaKAQjwcvPVs2WDdMuJU3mbN.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/udc1W1WEqVrZl4iyddFzYBut54W.jpg</fanart>
<date>2019</date>
<genre>Drama. Thriller </genre>
<extra>NA</extra>
<info>A comienzos de la Segunda Guerra Mundial, con Reino Unido llegando a la desesperación, Churchill ordena a su nueva agencia de espionaje que reclute y entrene a mujeres espía.</info>
 
 
<page>17</page><title>LAS ILUSIONES PERDIDAS</title>
<microhd>NA</microhd>
<fullhd>e5e3f65bb76eda292ab029d310a5f4220803136f</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ruDJwvNAyDPzc4tYAJFpFXiAfAf.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/hNYWEgCDlQQo9jAclDmwLmH4sAo.jpg</fanart>
<date>2021</date>
<genre>Drama. Comedia</genre>
<extra>NA</extra>
<info>Lucien es un joven poeta desconocido en la Francia del siglo XIX. Tiene grandes esperanzas y quiere forjar su destino. Deja la imprenta familiar de su provincia natal para probar suerte en París del brazo de su mecenas. Rápidamente abandonado a su suerte en esta fabulosa ciudad, el joven descubrirá lo que ocurre entre bastidores en este mundo consagrado a la ley del beneficio y el fingimiento. Una adaptación de la novela de Honoré de Balzac "Les illusions perdues".</info>
 
 
<page>17</page><title>LAS LEYES DE LA FRONTERA</title>
<microhd>fde2a5e718a310b981287eb4e244e9d5e08ba523</microhd>
<fullhd>A8EB4A15146384AFB985D409E621724F238452E6</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ySYXzjfJfHfn7nYdTYx15UiHNjN.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/trw18pk1UV0JvTDkCnvHhWe3j3h.jpg</fanart>
<date>2021</date>
<genre>Drama. Thriller</genre>
<extra>NA</extra>
<info>Verano de 1978. Ignacio Cañas (Marcos Ruiz) es un estudiante de 17 años introvertido y algo inadaptado que vive en Girona. Al conocer al Zarco (Chechu Salgado) y a Tere (Begoña Vargas), dos jóvenes delincuentes del barrio chino de la ciudad, se ve inmerso en una carrera imparable de hurtos, robos y atracos. Es la historia en la que Nacho se hace mayor, cruzando la línea que hay entre el bien y el mal, entre la justicia y la injusticia... Adaptación de la novela homónima de Javier Cercas. </info>
 
 
<page>17</page><title>LAS NIÑAS</title>
<microhd>f99bcebab9d63d5fc58d89525262f5c2880f3b03</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/xVBDEeY2qvNh5m0SAgBywLNXLjp.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/qqZqwLgCabEYfU7X1wSnBTRd5ud.jpg</fanart>
<date>2020</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Año 1992. Celia, una niña de 11 años, vive con su madre y estudia en un colegio de monjas en Zaragoza. Brisa, una nueva compañera recién llegada de Barcelona, la empuja hacia una nueva etapa en su vida: la adolescencia. En este viaje, en la España de la Expo y de las Olimpiadas del 92, Celia descubre que la vida está hecha de muchas verdades y algunas mentiras.</info>
 
 
<page>17</page><title>LAST CHRISTMAS</title>
<microhd>17a356f799fa06670dad8c227a989c8ee1f23960</microhd>
<fullhd>21d28566827f6f77c4b68c03104b71b23ab97a83</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/AcVAqTnPSiOtuWIj0zauw5WyjYp.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/eZ9wYTk9Gy2zYEv8rhRG3IoPuXG.jpg</fanart>
<date>2019</date>
<genre>Romance. Comedia. Drama </genre>
<extra>NA</extra>
<info>Kate (Emilia Clarke) es una mujer joven que suele tomar siempre la decisión menos acertada. ¿Su último error? Haber aceptado un trabajo como elfo de Santa Claus en un centro comercial. Sin embargo, el destino la lleva a conocer a Tom (Henry Golding), circunstancia que cambia por completo su vida. Para Kate, todo es demasiado bueno para ser cierto.</info>
 
 
<page>17</page><title>LE MANS 66</title>
<microhd>12dfd55c932bd65b711fb1261f6f944b6c6e2d77</microhd>
<fullhd>be60070c9f03ef0688893d1bf9bce53009d22568</fullhd>
<tresd>NA</tresd>
<cuatrok>c9e258bec4d8c1c7bdf7b696a945b5cf0e8878b1</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/kFkZFPBU7vnDDu4m9eOgk7w9W7O.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/n3UanIvmnBlH531pykuzNs4LbH6.jpg</fanart>
<date>2019</date>
<genre>Drama. Accion</genre>
<extra>NA</extra>
<info>Se centra en un excéntrico y decidido equipo americano de ingenieros y diseñadores, liderados por el visionario automovilístico Carroll Shelby (Damon) y su conductor británico Ken Miles (Bale). Henry Ford II y Lee Iacocca les dan la misión de construir desde cero un nuevo automóvil con el fin de derrocar el dominio de Ferrari en el Campeonato del Mundo de Le Mans de 1966.</info>
 
 
<page>17</page><title>LEONES POR CORDEROS</title>
<microhd>NA</microhd>
<fullhd>963d0b040d9b484735536e5717f35e7b860bfbae</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/gd2WNIOGfopWYnXdSXw46ZgEHIw.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/poFDHgqiCNSQ5Z1kYMTHbgBMQH4.jpg</fanart>
<date>2007</date>
<genre>Drama. Bélico</genre>
<extra>Mis</extra>
<info>Narra tres historias vinculadas entre sí: en Washington, un congresista (Tom Cruise) concede una exclusiva a una periodista (Meryl Streep). Al mismo tiempo, un idealista profesor (Robert Redford) de una universidad de California trata de motivar a un alumno aventajado de su clase. Por otro lado, dos soldados americanos destinados en Afganistán, antiguos alumnos del profesor, resultan heridos en acción y quedan aislados mientras esperan ser rescatados.</info>
 
 
<page>17</page><title>LICORICE PIZZA</title>
<microhd>a2c1e47841e33e4b2d6d2c72f9e8b6e1de4be84d</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/5ASO5vN6tImSLqw2BhLBxnrwVPd.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/836PkC9ZyqSaptT1HePg2eTK27g.jpg</fanart>
<date>2021</date>
<genre>Comedia. Drama. Romance</genre>
<extra>NA</extra>
<info>Es la historia de Alana Kane y Gary Valentine, de cómo se conocen, pasan el tiempo juntos y acaban enamorándose en el Valle de San Fernando en 1973.</info>
 
 
<page>17</page><title>LIFE</title>
<microhd>37b0ca40240825a6968de6af644e36f2fea67029</microhd>
<fullhd>3ed479dcc098adf82a676a2e9e619e3d372e31c4</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/1CGjrpF2HoqecZWO9FBUiBw2MLp.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/nNh7vHHISVAaziJEqAq0P9iL52w.jpg</fanart>
<date>2017</date>
<genre>Ciencia ficcion. Thriller. Terror</genre>
<extra>NA</extra>
<info>Seis miembros de la tripulación de la Estación Espacial Internacional están a punto de lograr uno de los descubrimientos más importantes en la historia humana: la primera evidencia de vida extraterrestre en Marte. A medida que el equipo comienza a investigar y sus métodos tienen consecuencias inesperadas, la forma viviente demostrará ser más inteligente de lo que cualquiera esperaba.</info>
 
 
<page>17</page><title>LIFECHANGER</title>
<microhd>372a3adf14a78e3a3b6c1293150ad0f518868644</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ocNctwT6ePRtzWPi3Tqqog5h7NM.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/vnU60ruIOvTYRJ2WI9pWOrIwBFS.jpg</fanart>
<date>2018</date>
<genre>Terror. Thriller</genre>
<extra>NA</extra>
<info>Un asesino con capacidad para cambiar de forma se embarca en una sangrienta misión para enderezar las cosas con la mujer a la que ama.</info>
 
 
<page>17</page><title>LIGA DE LA JUSTICIA</title>
<microhd>e3d30e037696003de7ad0e05becce9236daa5224</microhd>
<fullhd>063e5419f707538ee64f77b5efaef92a6013c186</fullhd>
<tresd>96293427a023e55e8003b580a2ce8d41771c1355</tresd>
<cuatrok>5573849f47422fcbdb0f72a75b96a791ab100333</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/lR11JiTpl76YNnH65pSVE1XSm8Q.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/pyxkLUfC5TG1pPoB4bwjCz8Z5iG.jpg</fanart>
<date>2017</date>
<genre>Accion. Fantastico</genre>
<extra>NA</extra>
<info>Motivado por la fe que había recuperado en la humanidad e inspirado por la acción altruista de Superman, Bruce Wayne recluta la ayuda de su nueva aliada, Diana Prince, para enfrentarse a un enemigo aún mayor. Juntos, Batman y Wonder Woman se mueven rápidamente para intentar encontrar y reclutar un equipo de metahumanos que combata esta nueva amenaza. El problema es que a pesar de la formación de esta liga de héroes sin precedentes –Batman, Wonder Woman, Aquaman, Cyborg y Flash– puede que sea demasiado tarde para salvar el planeta de una amenaza de proporciones catastróficas</info>
 
 
<page>17</page><title>LIGHTYEAR</title>
<microhd>NA</microhd>
<fullhd>2a09be49365726171914995ff1711ab10990abea</fullhd>
<tresd>NA</tresd>
<cuatrok>92004aa69a70a05219fe039e439dedf2241ac2ca</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/1z6Sx90tBUsRgSRrfqr4bEaUeod.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/hwUxHPkuUleJeoick4uZsrKDXxF.jpg</fanart>
<date>2022</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>La historia del origen de Buzz Lightyear, el héroe que inspiró el juguete, y que nos da a conocer al legendario Guardián Espacial que acabaría contando con generaciones de fans. </info>
 
 
<page>17</page><title>LILO, MI AMIGO EL COCODRILO</title>
<microhd>NA</microhd>
<fullhd>ac6bb4556cccd908fa514657f02a272088c8c5af</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/n1dyvKYZrwtcaLJThFeBkxEKaZW.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/c1bz69r0v065TGFA5nqBiKzPDys.jpg</fanart>
<date>2022</date>
<genre>Fantástico. Musical. Infantil. Comedia</genre>
<extra>NA</extra>
<info>Cuando la familia Primm se muda a Nueva York, su hijo adolescente Josh lucha por adaptarse a su nueva escuela y nuevos amigos. Todo eso cambia cuando descubre a Lilo, un cocodrilo cantante, a quien le gusta darse baños, el caviar y la buena música viviendo en el ático de su nueva casa. Los dos se convierten en mejores amigos, pero cuando la existencia de Lilo se ve amenazada por su malvado vecino Mr. Grumps, los Primm deberán aliarse con el carismático dueño de Lilo, Hector P. Valenti, para mostrar al mundo que la familia puede surgir de los lugares más inesperados y que no hay nada malo con un gran cocodrilo cantante con una personalidad aún mayor. Adaptación del libro infantil homónimo sobre un cocodrilo que vive en Nueva York. En versión original, el protagonista tendrá la voz del cantante Shawn Mendes.</info>
 
 
<page>17</page><title>LINCESSA LOS SILENCIOS DEL BOSQUE</title>
<microhd>NA</microhd>
<fullhd>b535f958941532fa82f87d0e449f0b65ff5cf510</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/gKh8TL2nSSV0LHv4T79ZpMQHfYT.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/i7fSBBueVkiThPZV5waT894YXDi.jpg</fanart>
<date>2023</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Lincessa, una cachorro de lince boreal, independiente, coqueta y aventurera, nos cuenta en primera persona sus andanzas, descubriéndonos los secretos y misterios de la vida en el interior del bosque, junto con su madre y su hermano.</info>
 
 
<page>17</page><title>LITTLE MONSTERS</title>
<microhd>09aa1130eda87e37631eead4640795d99720abe2</microhd>
<fullhd>3af11d5dcc37699adf25bff48b0f0292e8da1b8c</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/eUA0NDoHRF7z2lnjotFjdYeNdqp.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/oOihs7jNggpXiR3Wm122quMyubx.jpg</fanart>
<date>2019</date>
<genre>Comedia. Terror</genre>
<extra>NA</extra>
<info>Un músico fracasado, Dave (Alexander England), se une a la profesora infantil Miss Caroline (Lupita Nyong'o) y a una celebridad para los niños, Teddy McGiggle (Josh Gad), para intentar proteger a los pequeños escolares de un repentino brote de muertos vivientes.</info>
 
 
<page>17</page><title>LIVE IS LIFE. LA GRAN AVENTURA</title>
<microhd>c49931530e46012934235d2b7e303ba0ab868f8e</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/gG8XLenHvOx2vC0hgSKOXROj34b.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/9FFz0ZYuNmBmKQBaZ5ibSqGvnI3.jpg</fanart>
<date>2021</date>
<genre>Aventuras. Drama. Comedia</genre>
<extra>NA</extra>
<info>Verano 1985. Como cada año, Rodri deja Cataluña y viaja con su familia al pueblo gallego de sus padres para reencontrarse con su pandilla. Sin embargo, este año es distinto para él y sus amigos. Los problemas del mundo real empiezan a aparecer en sus vidas, amenazando con separarles. Aferrándose a la amistad que les une, los cinco amigos planean escaparse la noche de San Juan en busca de una flor mágica que, según cuenta la leyenda, crece en lo alto de una montaña y puede hacer que los deseos se hagan realidad. Porque su único deseo ahora es resolver el problema de su amigo en apuros, y con ello poder seguir juntos.</info>
 
 
<page>17</page><title>LLAMAN A LA PUERTA </title>
<microhd>NA</microhd>
<fullhd>42745c651bcfe88479839997d5cc238cfca8cb14</fullhd>
<tresd>NA</tresd>
<cuatrok>71aff43d555452fa38b46cbeaf9f875ca37898dd</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/4Zna9CvyzaAgejsErmyXmHC64Dd.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/22z44LPkMyf5nyyXvv8qQLsbom.jpg</fanart>
<date>2023</date>
<genre>Intriga. Thriller. Terror. Drama</genre>
<extra>NA</extra>
<info>Durante unas vacaciones en una cabaña en un bosque alejada de todo, una niña y sus padres se convierten en rehenes de cuatro desconocidos armados que obligan a la familia a tomar una decisión imposible para evitar el apocalipsis. Con acceso limitado al mundo exterior, la familia deberá decidir qué creer antes de que todo esté perdido. </info>
 
 
<page>17</page><title>LLEGA DE NOCHE</title>
<microhd>8c725ad3cb79297b801c9b8a0bd4a7ea2ab5cef7</microhd>
<fullhd>e338a1d027f843404bde81f4f56ff706adce9e27</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/6Qr6KPW7Z1FXAAMbV0SEOImBctd.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/6h9pjTsWzaUWuUszHHbeI1JWE1.jpg</fanart>
<date>2017</date>
<genre>Intriga. Terror. Thriller. Drama</genre>
<extra>NA</extra>
<info>Paul (Joel Edgerton) es un padre de familia que vive en una casa de madera con su esposa Sarah (Carmen Ejogo) y su hijo Travis (Kelvin Harrison Jr.), y que no se frenará ante nada para proteger a su familia de una amenaza que aterroriza al mundo exterior.</info>
 
 
<page>17</page><title>LO QUE EL VIENTO SE LLEVO</title>
<microhd>2a131db2e09b5933af65cdfb2478518e0ecd10c1</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/3bAYXjK0xCAizMVixG9mSW5FDun.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/4FHcxMtVruMr7ltU2cItWssLjTP.jpg</fanart>
<date>1939</date>
<genre>Drama. Romance. Aventuras</genre>
<extra>Mis</extra>
<info>Georgia, 1861. En la elegante mansión sureña de Tara, vive Scarlett O'Hara (Vivien Leigh), la joven más bella, caprichosa y egoísta de la región. Ella suspira por el amor de Ashley (Leslie Howard), pero él está prometido con su prima, la dulce y bondadosa Melanie (Olivia de Havilland). En la última fiesta antes del estallido de la Guerra de Secesión (1861-1865), Scarlett conoce al cínico y apuesto Rhett Butler (Clark Gable), un vividor arrogante y aventurero, que sólo piensa en sí mismo y que no tiene ninguna intención de participar en la contienda. Lo único que él desea es hacerse rico y conquistar el corazón de la hermosa Scarlett.</info>
 
 
<page>17</page><title>LOCK AND STOCK</title>
<microhd>66b4a7e3d79817208be00f8d48dcb94501c60093</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/5DrsSGAS0EcrClOwAcDIziIwzFR.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/nGG2SjHisYQT0E9a1sQbw6x9aPX.jpg</fanart>
<date>1998</date>
<genre>Thriller</genre>
<extra>Culto</extra>
<info>Eddie convence a tres amigos para jugarse sus ahorros en una partida de cartas contra Harry el Hacha, un mafioso del barrio. La partida está amañada, y Eddie no sólo pierde todo el dinero sino que contrae una deuda de medio millón de libras, que debe pagar en el plazo de una semana. El mafioso pretende quedarse con el local de su padre para resarcirse de la deuda, pero los cuatro amigos planean saldarla de una forma mucho más arriesgada.</info>
 
 
<page>17</page><title>LOCO POR ELLA</title>
<microhd>0c54536909017e3b0ce07236d3df9874204db237</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/mVcOR5f1bKhqSWWxViOfmOJ7aqc.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/y7b78soqvJq4aQ8G0Zt72inSQoE.jpg</fanart>
<date>2021</date>
<genre>Romance. Comedia. Drama</genre>
<extra>NA</extra>
<info>Tras conocer y pasar una noche con Carla (Susana Abaitua), la chica de sus sueños, Adri (Álvaro Cervantes) descubre que se ha escapado de un hospital psiquiátrico. Locamente enamorado y dispuesto a hacer lo que sea para volver a verla, Adri se interna como un paciente más con el fin de conquistar a Carla y, de paso, escribir el mejor reportaje de su vida para la revista Load. Pero, una vez dentro, no solo descubre que su historia de amor no es como él esperaba, sino que no va a ser tan fácil salir de allí. Con la ayuda de Saúl (Luis Zahera), su compañero de habitación, Adri comenzará un viaje de aprendizaje que lo acercará al resto de pacientes y a Carla, y lo obligará a replantearse no solo su objetivo inicial sino su visión del mundo.</info>
 
 
<page>17</page><title>LOCURA PADRE</title>
<microhd>ffbad2aec790a00a3e284470ddccb583788a3995</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/7N7jnxIdXJZ5EesRl6rCAbXUW2n.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/fcao76tMnc428FLcC5X2zJNOkbl.jpg</fanart>
<date>2017</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Dos hermanos gemelos descubren que su padre no está muerto y deciden emprender un viaje en carretera en su búsqueda.</info>
 
 
<page>17</page><title>LOOPER</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>d82ce09f3993924b7e596353ff5f281de8ae6857</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/nKOXar8RFjwit8jwfYnNdqPeQU0.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/lVTYM5m7ZDnEyhxcYrkxJaYqExh.jpg</fanart>
<date>2012</date>
<genre>Ciencia ficción. Acción</genre>
<extra>Mis</extra>
<info>Como en 2072 los asesinatos están terminantemente prohibidos, las víctimas son enviadas a través de una máquina del tiempo al pasado (2042), donde los Loopers, un grupo de asesinos a sueldo, se encargan de eliminarlas y deshacerse rápidamente de sus cuerpos. El problema surge cuando Joe (Gordon-Levitt), uno de los Loopers, recibe desde el futuro un encargo muy especial: eliminarse a sí mismo (Bruce Willis).</info>
 
 
<page>17</page><title>LOS ANGELES DE CHARLIE</title>
<microhd>f7b75aff8598cc71be7ab8ac9357e16c27692749</microhd>
<fullhd>316973db7692940ee38569f8b6ecd5feadff8fa0</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/egCHwgAZ5UWkQl6HI0Ih8u9QulF.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/a0xTB1vBxMGt6LGG4N7k1wO9lfL.jpg</fanart>
<date>2019</date>
<genre>Accion. Thriller. Comedia</genre>
<extra>NA</extra>
<info>Los Ángeles de Charlie siempre han proporcionado servicios de seguridad e investigación a clientes privados, y ahora la Agencia Townsend se ha expandido internacionalmente con las mujeres más inteligentes, valientes y mejor entrenadas a lo largo y ancho del planeta – varios equipos de Ángeles guiados por otros tantos Bosleys llevando a cabo los trabajos más duros por todo el mundo. Cuando un joven ingeniero de sistemas llama la atención sobre una peligrosa tecnología, los Ángeles son llamados a la acción, arriesgando sus vidas por protegernos a todos.</info>
 
 
<page>17</page><title>LOS ARCHIVOS DEL PENTAGONO</title>
<microhd>NA</microhd>
<fullhd>f58a54d9b44bbd5a04dcd36190675d70e043a352</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/1y2vRXsH3LS51XZajrKSYTlfMLe.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/8sb4aBST28vN3rBz704XJczS0Ld.jpg</fanart>
<date>2017</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>En junio de 1971 The New York Times y The Washington Post tomaron una valiente posición en favor de la libertad de expresión, informando sobre los documentos del Pentágono y el encubrimiento masivo de secretos por parte del gobierno, que había durado cuatro décadas y cuatro presidencias estadounidenses. En ese momento, Katherine Graham (Meryl Streep), primera mujer editora del Post, y el director Ben Bradlee (Tom Hanks) intentaban relanzar un periódico local en decadencia. Juntos decidieron tomar la audaz decisión de apoyar al The New York Times y luchar contra el intento de la Administración Nixon de restringir la primera enmienda... Historia basada en los documentos del Post que recogían información clasificada sobre la Guerra de Vietnam.</info>
 
 
<page>17</page><title>LOS ARISTOGATOS</title>
<microhd>0eb03744bcba5fbe73349865f74d99931e24f397</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/s9DsxlbVE1HvHnCz50GdHDBeZRy.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/s9DsxlbVE1HvHnCz50GdHDBeZRy.jpg</fanart>
<date>1970</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Año 1910. En París, una anciana millonaria y excéntrica lleva una vida feliz con sus gatos en su enorme mansión, con la única ayuda de su fiel mayordomo Edgar. Allí, la gatita Duquesa vive con sus tres traviesos cachorros, sin saber que Edgar planea acabar con todos los gatos para convertirse en el único heredero de la enorme fortuna de la anciana, Madame Bonfamille. Con este propósito, los abandona en un lugar remoto, pero quiere la suerte que Duquesa se encuentren con Thomas O’Malley, un gato vagabundo y mundano. Desde el principio, O’Malley, atraído por la elegancia de Duquesa, se propone ayudarla a regresar a casa de Madame Bonfamille, aunque para ello tenga que enfrentarse con todos los peligros de la gran ciudad.</info>
 
 
<page>17</page><title>LOS BUENOS MODALES</title>
<microhd>NA</microhd>
<fullhd>a8135e696ee1c91cdc3933bc98abb78fe8231474</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/k7fAmDcHwfru0dQdJOVF2RzOEvN.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/Wk7Ls7acluQGLRc4Ca6pIbcQOA.jpg</fanart>
<date>2023</date>
<genre>Comedia. Drama</genre>
<extra>Estreno</extra>
<info>Manuela y Rosario son dos hermanas que, tras años sin hablarse a causa de un secreto familiar, se reencuentran inesperadamente en el cumpleaños de sus nietos. Los niños se han conocido gracias a sus cuidadoras, Trini y Milagros, amigas inseparables, vecinas de toda la vida. Cuando descubren el conflicto que separa a los pequeños, las entrometidas limpiadoras intentarán, con torpeza, mucho amor y un poco de disparate, reconciliar a sus familias.</info>
 
 
<page>17</page><title>LOS CABALLEROS DEL ZODIACO</title>
<microhd>NA</microhd>
<fullhd>qfejzirjuysjdzrzt3igishmjdyk6van</fullhd>
<tresd>NA</tresd>
<cuatrok>vnhvwbjsbimfresuj6c6ave5ur4din2i</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/kGdB6jUuqs1RlVkjt4q7EvgbIqu.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/u17VLZqWFbeJsj1HpvB6QOOHvlC.jpg</fanart>
<date>2023</date>
<genre>Acción. Fantástico. Aventuras. Drama</genre>
<extra>NA</extra>
<info>Seiya (Mackenyu), un testarudo adolescente callejero, se pasa el tiempo luchando por dinero mientras busca a su hermana secuestrada. Cuando en una de sus peleas se le revelan poderes místicos que desconocía, Seiya se ve inmerso en un mundo de santos en guerra, antiguos entrenamientos mágicos y una diosa reencarnada que necesita su protección. Para sobrevivir, tendrá que aceptar su destino y sacrificarlo todo para ocupar el lugar que le corresponde entre Los Caballeros del Zodiaco.</info>
 
 
<page>17</page><title>LOS CRIMENES DE LA ACADEMIA</title>
<microhd>1d5aaa0be96023e340b4daad912bb0d2f2a93717</microhd>
<fullhd>f7c9972a6f7743c3610c49ca4e26e25867023b6a</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/90Tle5e429OdpMmvuRrcQLrCRiT.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/wwECpJRpM7GhLjWOVza4w0vAfij.jpg</fanart>
<date>2022</date>
<genre>Intriga. Thriller. Terror</genre>
<extra>NA</extra>
<info>Historia ambientada en 1830 sobre un veterano detective, Augustus Landor, que intenta resolver unos asesinatos cometidos en West Point con la ayuda de un joven cadete al que el mundo conocería luego como Edgar Allan Poe.</info>
 
 
<page>17</page><title>LOS CROODS:  UNA AVENTURA PREHISTORICA</title>
<microhd>27738397e1ba6957a1c664f6ed08a557b1b32cae</microhd>
<fullhd>57a5e4fb29bba9530862ec2d84b6c4784403844e</fullhd>
<tresd>ee05715d3421b0835eb46bd7f71866fe94d19fb0</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/3X3qtBTgKt5mCB30RJwbIjgjzdw.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/bNgqt819qpHcszjCzLCG5y16ldF.jpg</fanart>
<date>2013</date>
<genre>Infantil. Saga</genre>
<extra>NA</extra>
<info>Prehistoria. Tras la destrucción de su hogar por un gran terremoto, Grug se ve obligado a emigrar con su familia. Mientras avanzan por un mundo desconocido y aterrador, se encuentran con un nómada de mente abierta que los deja a todos fascinados, sobre todo a la hija de Grug.</info>
 
 
<page>17</page><title>LOS CROODS: UNA NUEVA ERA</title>
<microhd>f6ca6238cb2ed793aca71fc2502d95cc4c999969</microhd>
<fullhd>a7ba08cc8b3534bb7bc5f5b582300aa6b129b84c</fullhd>
<tresd>92b74ee581b3448880166db301da60b07f76f95b</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/5uMWKEmegf5aTJnp1u98JF4QerP.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/rCxdJkk5PMCWIzRWcpqIxUaWnf1.jpg</fanart>
<date>2020</date>
<genre>Infantil. Saga</genre>
<extra>NA</extra>
<info>Obligados a partir en busca de un nuevo lugar donde vivir, la primera familia prehistórica se aventura a explorar el mundo con el objetivo de localizar un lugar más seguro al que llamar hogar. Pero cuando encuentran un idílico paraíso amurallado que cumple con todas sus necesidades, se encuentran con que ese lugar esconde una sorpresa: hay otra familia que ya vive allí, los Masmejor. Y es que los Masmejor, con su elaborada casa en un árbol, sus asombrosas invenciones y sus enormes huertos, van un par de pasos por delante de los Croods en la escala evolutiva. Y cuando les reciben como sus primeros invitados, no tardan en surgir tensiones entre nuestros queridos cavernícolas y sus nuevos y modernos semejantes. Cuando todo parece perdido, una nueva amenaza embarca a las dos familias en una aventura épica más allá de la seguridad de la muralla, que les obligará a aparcar sus diferencias, aunar fuerzas y luchar por un futuro juntos.</info>
 
 
<page>17</page><title>LOS DIEZ MANDAMIENTOS</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>8e49d0122cb839659034701d5911caa89c26da2d</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/kUfNxdftpiADE92Ch2Z0IRZjwwm.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/vEcEXc3lnS0mWPLiOLiFhAS67Oj.jpg</fanart>
<date>1956</date>
<genre>Drama</genre>
<extra>Culto</extra>
<info>Drama bíblico ambientado en el Antiguo Egipto que narra la historia de Moisés (Charlton Heston), favorito de la familia del faraón, que decide renunciar a su vida de privilegios para conducir a su pueblo, los hebreos esclavizados en Egipto, hacia la libertad. </info>
 
 
<page>17</page><title>LOS DOS PAPAS</title>
<microhd>efced8482fff02f06da3defd53af99ef31140618</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/1KrH8cM2KudyhjJK8NtTgf2lMIP.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/mHz65gYQ3SmkQH3GKWSKWUkK5zW.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Explora la relación que mantuvieron el Papa Benedicto XVI y su sucesor, el Papa Francisco, dos de los líderes más poderosos de la Iglesia Católica, que abordan sus propios pasados ​​y las demandas del mundo moderno para que la institución avance.</info>
 
 
<page>17</page><title>LOS ESTADOS UNIDOS CONTRA BILLIE HOLIDAY</title>
<microhd>7efd97336377426086b412435783915862091e37</microhd>
<fullhd>7e2c80c03fb23a46cd299ac74ac1c1e096618532</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ErczuRvIF4cd7IJNc04RO1RB5B.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/3ntWQMZKZjYR0YFWjyTntUu8i6B.jpg</fanart>
<date>2021</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>La legendaria Billie Holiday (Andra Day), una de las mejores intérpretes de jazz de todos los tiempos, pasó la mayor parte de su carrera siendo adorada por sus fans alrededor del globo. Todo mientras el Departamento Federal de Narcóticos de Estados Unidos la fijó como objetivo mediante una operación encubierta liderada por el Agente Federal Jimmy Fletcher (Trevante Rhodes), con quien Billie había tenido un tumultuoso romance.</info>
 
 
<page>17</page><title>LOS EXTRAÑOS CACERIA NOCTURNA</title>
<microhd>624e2b0f8dc0574c267d61373e7b5d492c05baa0</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/gjUqYcAAJNTgnyyVoZQoZqYhAUx.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/6ThZWrEom8EaFJtRg31JR3OYe4b.jpg</fanart>
<date>2018</date>
<genre>Terror. Thriller</genre>
<extra>NA</extra>
<info>Cindy (Christina Hendricks) y Mike (Martin Henderson) se embarcan en un viaje por carretera y planean pasar una temporada en un campamento de remolques antes de dejar a su problemática hija Kinsey (Bailee Madison) en un internado. Pero el viaje se ve interrumpido por la llegada de tres psicópatas enmascarados que atacan y matan sin piedad.</info>
 
 
 
<page>18</page><title>LOS FABELMAN</title>
<microhd>NA</microhd>
<fullhd>wvtgvaikiyoth5ug4ghvouygn647le7u</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/wcgPDyvKkgo2agNXcdE1eXLGzmf.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/xQyGkQ8ICa4lgifGr3oZjkm3AJ2.jpg</fanart>
<date>2022</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Film semiautobiográfico de la propia infancia y juventud de Spielberg. Ambientada a finales de la década de 1950 y principios de los años 60, un niño de Arizona llamado Sammy Fabelman, influido por su excéntrica madre, artista (Michelle Williams), y su pragmático padre, ingeniero informático (Paul Dano), descubre un secreto familiar devastador y explora cómo el poder de las películas puede ayudarlo a contar historias y a forjar su propia identidad.</info>
 
 
<page>18</page><title>LOS FUTBOLISIMOS</title>
<microhd>6e0cf88fb58d6baa017c63a6b3b4e6d3f1d878fe</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ddIpRudVq0kzq5LAiML8i5wB8x3.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/5BDjrsN7aMmhzJ9zGcEmSbAStLz.jpg</fanart>
<date>2018</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Comedia de aventuras protagonizada por una pandilla de niños que, por salvar a su equipo de fútbol, se meterán en todo tipo de problemas en los que su ingenio y su amistad serán puestos a prueba. El Soto Alto F.C. es el nombre del equipo, y en él juegan, entre otros, Pakete, Camuñas, Angustias, Helena y Toni. Todos tienen una misión fundamental: ganar los dos próximos partidos para evitar que la asociación de madres y padres de alumnos cierren el club. Si bajan a segunda, se acabó el equipo.</info>
 
 
<page>18</page><title>LOS GOONIES</title>
<microhd>da2bbd9a8bb2b7eb85d16fba1119a0b74eabe7a4</microhd>
<fullhd>899b5702f44ec690b5dc5c9ef7a888aa896d3308</fullhd>
<tresd>NA</tresd>
<cuatrok>b22b8a3bca52e6a238149ab66ce7083af18b16b3</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/h0QoQSodfdd3VTBqlFb2QP7wIbL.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/h4FuqnkBrZ6Wxi4TEeB99QvL590.jpg</fanart>
<date>1985</date>
<genre>Aventuras. Infantil</genre>
<extra>Culto</extra>
<info>Mikey es un niño de trece años que junto con su hermano mayor y sus amigos forman un grupo que se hacen llamar "los Goonies". Un día deciden subir al desván de su casa, donde su padre guarda antigüedades. Allí encuentran el mapa de un tesoro perdido que data del siglo XVII, de la época de los piratas, y deciden salir a buscarlo repletos de espíritu aventurero.</info>
 
 
<page>18</page><title>LOS HERMANOS SISTERS</title>
<microhd>96bc2d04f093c1afc85430bc13d2e3163f7e1e3c</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/v8wlFbdZZZ1oSyVE8Vbl7MdNNSJ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/iGAQZPnHLeXTaxZdKVUjLegWDom.jpg</fanart>
<date>2018</date>
<genre>Accion.Western</genre>
<extra>NA</extra>
<info>1850. Los hermanos Charlie y Eli Sisters viven en un mundo salvaje y hostil, en plena fiebre del oro. Tienen las manos manchadas de sangre, tanto de criminales como de personas inocentes. No tienen escrúpulos a la hora de matar. Es su trabajo. Charlie (Joaquin Phoenix), el hermano pequeño, nació para matar. Eli (John C. Reilly), sin embargo, sueña con llevar una vida normal. Ambos son contratados por el Comodoro para encontrar y matar a Hermann Kermit Warm (Riz Ahmed), un buscador de oro. De Oregón a California arranca una caza despiadada, un viaje que pondrá a prueba el demencial vínculo entre los dos hermanos.</info>
 
 
<page>18</page><title>LOS HERMANOS WILLOUGHBY</title>
<microhd>420bc144c0d421a399f845decd7fee5c8a6c7e14</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/v1W9uSS3W7oil0Lo9QUJesey0i5.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/v0vTo9Na2TXF5k6pSfsxI8nKQIo.jpg</fanart>
<date>2020</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Los hermanos Willoughby están convencidos de que sin sus (egoístas) padres les iría mucho mejor. Cuando consiguen mandarlos de vacaciones, los cuatro niños se embarcan en una aventura en busca del verdadero significado de la familia.</info>
 
 
<page>18</page><title>LOS INCREIBLES</title>
<microhd>c22ef341ddf143bfe88bb97e82c691000469e308</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/al1jusd4T7JPatZlj4BuYkDDOzr.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/wiDGnsn9RtNglgKQy4J1jZQBG5v.jpg</fanart>
<date>2004</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Bob Parr era uno de los más grandes superhéroes del mundo (también se le conocía como "Mr. Increíble"), salvaba vidas y luchaba contra villanos a diario. Han pasado 15 años, y Bob y su mujer (una famosa ex-superheroína por derecho propio) han adoptado una identidad civil y se han retirado a la periferia para llevar una vida normal con sus tres hijos. Bob se dedica a comprobar los plazos de las reclamaciones de seguros y lucha contra el aburrimiento y los michelines. Está deseando volver a entrar en acción, así que cuando recibe una misteriosa comunicación que le ordena dirigirse a una remota isla para cumplir una misión de alto secreto, no se lo piensa dos veces.</info>
 
 
<page>18</page><title>LOS INCREIBLES 2</title>
<microhd>90e8a665ef71c89ba562d9d25e2cfcc11fdd70bc</microhd>
<fullhd>b1bfda1e3c03337bd6553b7d7c3d481a13f7944b</fullhd>
<tresd>5d237933b2fdc5e78b15d980b9c10daba47268dc</tresd>
<cuatrok>d06f71ed787bedeedc8904f5eb78255adc81fc16</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/iNIKq8r155eRFlsoaxgfRYs71qY.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/mabuNsGJgRuCTuGqjFkWe1xdu19.jpg</fanart>
<date>2018</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Secuela de "Los increíbles". Helen tiene que liderar una campaña para que los superhéroes regresen, mientras Bob vive su vida "normal" con Violet, Dash y el bebé Jack-Jack —cuyos superpoderes descubriremos—. Su misión se va a pique cuando aparece un nuevo villano con un brillante plan que lo amenaza todo. Pero los Parr no se amedrentarán y menos teniendo a Frozone de su parte.</info>
 
 
<page>18</page><title>LOS INTOCABLES DE ELIOT NESS</title>
<microhd>NA</microhd>
<fullhd>0d6c0be9dc010ba2c99caef7cf0d4aefbd88a9ed</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/pVcWaUbFDL76ObIsReIBQ5ILMHv.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/k0oT1Vj9dlkwk5Vuuz7pvnG8iJ6.jpg</fanart>
<date>1987</date>
<genre>Acción. Thriller. Drama</genre>
<extra>Mis</extra>
<info>Chicago, años 30. Época de la Ley Seca. El idealista agente federal Eliot Ness (Kevin Costner) persigue implacablemente al gángster Al Capone (Robert De Niro), amo absoluto del crimen organizado en la ciudad. La falta de pruebas le impide acusarlo de asesinato, extorsión y comercio ilegal de alcohol, pero Ness, con la ayuda de un par de intachables policías (Andy Garcia y Charles Martin Smith) reclutados con la ayuda de un astuto agente (Sean Connery), intentará encontrar algún medio para inculparlo por otra clase de delitos. </info>
 
 
<page>18</page><title>LOS JAPON</title>
<microhd>5d9d80d83ee6dc68cd95d917991a584057669057</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/r2Th4u8f74JRPJd0LpC8i0L0ZmI.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/tNcCDeHpC8t1jpGuCssUAxApffr.jpg</fanart>
<date>2019</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>En 1614 una expedición japonesa encabezada por el nieto del emperador desembarcó en el pueblo sevillano de Coria del Río, y el heredero al trono se enamoró de una bella sevillana, formó una familia y nunca regresó a Japón. 400 años después, el Emperador Satohito muere y el heredero legítimo resulta ser Paco Japón, vecino de 37 años de Coria del Río. La vida de Paco y de su familia cambiará drásticamente al verse de la noche a la mañana viviendo en el Palacio Imperial y preparándose para ser los nuevos Emperadores de Japón. El choque cultural y lingüístico entre japoneses y andaluces provocará situaciones cómicas y disparatadas en esta aventura donde Oriente y Occidente se dan la mano.</info>
 
 
<page>18</page><title>LOS JUEGOS DEL HAMBRE</title>
<microhd>8944928d80758cac88b7e5a65edc171df3ce850b</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>35851ae619ce2a3749b5429194456455d70bacde</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/jJUB1AXsHTZoGyN7CIagZh9JwiN.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/tlKw7QLNky3iyVHDHFtMFbPP4Es.jpg</fanart>
<date>2012</date>
<genre>Ciencia ficción. Aventuras. Thriller</genre>
<extra>Saga</extra>
<info>Lo que en el pasado fueron los Estados Unidos, ahora es una nación llamada Panem; un imponente Capitolio ejerce un control riguroso sobre los 12 distritos que lo rodean y que están aislados entre sí. Cada distrito se ve obligado a enviar anualmente un chico y una chica entre los doce y los dieciocho años para que participen en los Hunger Games, unos juegos que son transmitidos en directo por la televisión. Se trata de una lucha a muerte, en la que sólo puede haber un superviviente. Katniss Everdeen, una joven de dieciséis años, decide sustituir a su hermana en los juegos; pero para ella, que ya ha visto la muerte de cerca, la lucha por la supervivencia es su segunda naturaleza.</info>
 
 
<page>18</page><title>LOS JUEGOS DEL HAMBRE : EN LLAMAS</title>
<microhd>94ce426af64c7dbd5e2d3c688099e202b0f425d6</microhd>
<fullhd>07a2b4584970a31332acd50af3c6694d36a05ab1</fullhd>
<tresd>NA</tresd>
<cuatrok>234f1856c9fa5c7011d1d81fba0f240cc88633ab</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/290CyZsscncYKPwRv32PzpdcAZG.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/omoWcwdARBsGHdMdHUk0AQhS2Lp.jpg</fanart>
<date>2013</date>
<genre>Ciencia ficción. Aventuras</genre>
<extra>Saga</extra>
<info>Tras vencer en los 74º Juegos del Hambre anuales, la joven Katniss Everdeen y su compañero Peeta Mellark regresan a casa. Atrás dejaron a sus amigos y su familia, teniendo ambos que participar en un 'tour de la victoria' por todos los distritos. Durante ese largo viaje, Katniss se da cuenta de que se está gestando una rebelión, pero en el Capitolio todo sigue igual y bajo estricto control del Presidente Snow, que está organizando los 75º Juegos del Hambre, denominados 'El Vasallaje': una competición especial con una inesperada novedad que cambiará Panem para siempre.</info>
 
 
<page>18</page><title>LOS JUEGOS DEL HAMBRE: SINSAJO. PARTE 1</title>
<microhd>2fb761ae6e893fdd7dc2df68b605dce58c753e73</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>f87973dda8458571866b67ae78c935b1cc19810f</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/vLKhOBKno7mKXbf4K2IavkZ0xqo.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/rYsdoUqkltpKZQWwCWXf55csXvL.jpg</fanart>
<date>2014</date>
<genre>Ciencia ficción. Aventuras. Acción</genre>
<extra>Saga</extra>
<info>Katniss Everdeen se encuentra en el Distrito 13 después de destrozar los Juegos para siempre. Bajo el liderazgo de la comandante Coin y el consejo de sus amigos más leales, Katniss extiende sus alas mientras lucha por salvar a Peeta Mellark y a una nación alentada por su valentía... Tercera y última entrega de la saga literaria "Los juegos del hambre" de Suzanne Collins, que se divide en dos películas.</info>
 
 
<page>18</page><title>LOS JUEGOS DEL HAMBRE: SINSAJO. PARTE 2</title>
<microhd>d006b6bad01f4b7552c54206366d9cdbd5263f42</microhd>
<fullhd>NA</fullhd>
<tresd>db5194f809b3b2012ca2893b0aee0a57d7677dc7</tresd>
<cuatrok>4ff793e8884035c92f75680f5b5012e9e6fd0a03</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/imnl352kImciLMuEEyCedIwlag1.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/8wGpjCDAlwpOm1F1onhkhJq16lY.jpg</fanart>
<date>2015</date>
<genre>Ciencia ficción. Aventuras. Acción. Thriller</genre>
<extra>Saga</extra>
<info>La última entrega de 'Los juegos del hambre' nos muestra a una nación en guerra, en la que Katniss se enfrenta con uñas y dientes al presidente Snow (Donald Sutherland). Con la ayuda de algunos amigos, entre ellos Gale (Liam Hemsworth), Finnick (Sam Claflin) y Peeta (Josh Hutcherson), arriesgará la vida para salir del Distrito 13 y eliminar al presidente Snow.</info>
 
 
<page>18</page><title>LOS MERCENARIOS</title>
<microhd>4a1e2a12f8dcf4700e7c17925bce0b534cf4d197</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>d8c20013e5e05092b227b537a05cf04cd107217c</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/gmbMhNLsa0cEJIbsGUmTTtmLSPT.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/2dCpB96kuC8gPUvEwNGrr91cwmK.jpg</fanart>
<date>2010</date>
<genre>Acción.Saga</genre>
<extra>Mis</extra>
<info>Un grupo de mercenarios es contratado para infiltrarse en un país sudamericano y derrocar a su despiadado y corrupto dictador. Una vez allí, se verán atrapados en una telaraña de engaño y traición. Una vez fracasada la misión, tendrán que enfrentarse a un reto aún más difícil; salvar la unidad del grupo y la amistad que los ha unido durante largos años.</info>
 
 
<page>18</page><title>LOS MERCENARIOS 2</title>
<microhd>11b62765e530301effc8bd96f50f7e6a5db0ab6b</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>8328f4eb61aa6f38662e71acf7a0aa36e73bc028</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/yvLdUaaRrOK42VAoTeUXD3PspiC.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ihKsZZYMbyHXC1L7s90WDLITdC5.jpg</fanart>
<date>2012</date>
<genre>Acción. Saga</genre>
<extra>Mis</extra>
<info>Barney Ross (Sylvester Stallone), Lee Christmas (Jason Statham), Yin Yang (Jet Li), Gunner Jensen (Dolph Lundgren), Toll Road (Randy Couture) y Hale Caesar (Terry Crews) y Billy (Liam Hemsworth), un nuevo colega, se vuelven a reunir cuando el señor Church (Bruce Willis) les encarga un trabajo aparentemente sencillo y muy lucrativo. Sin embargo, el plan se tuerce cuando un peligroso terrorista llamado Villain (Jean-Claude Van Damme) les tiende una emboscada. Entonces su único deseo será vengarse. Así es como van sembrando a su paso la destrucción y el caos entre sus enemigos hasta que se encuentran con una amenaza inesperada: cinco toneladas de plutonio apto para uso militar, una cantidad más que suficiente para cambiar el equilibrio de poder en el mundo.</info>
 
 
<page>18</page><title>LOS MERCENARIOS 3</title>
<microhd>769f78d5301af769bdf99328a5414ab539d27695</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>4568b252fd921f2db169bfc929310930e7f53e2d</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/llcZUaonpQ0aYLE30xenjL1pIr.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/sJDBJxKWG7h3VpG6jc9Mm9bw4kG.jpg</fanart>
<date>2014</date>
<genre>Acción.Saga</genre>
<extra>Mis</extra>
<info>Gira en torno a la aparición de Stonebanks, el otro fundador del grupo conocido como Los Mercenarios, en la vida de Barney. Sus caminos se separaron cuando este se convirtió en traficante de armas, por lo que Barney se vio obligado a intentar matarle. Los Mercenarios se enfrentarán a este villano mientras se debaten entre las viejas y las nuevas tácticas de combate.</info>
 
 
<page>18</page><title>LOS NIÑOS DE WINDERMERE</title>
<microhd>ce8bff32386455c8ee7e9dd2ef368dc18b4593e6</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/4RnQMwVVxhJlZDJybptAOrrpt87.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/xP8LFoQamEa3IiEFPN9bKNCZFJq.jpg</fanart>
<date>2020</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Tras el final de la Segunda Guerra Mundial, el gobierno británico acogió a centenares de niños huérfanos, la mayoría judíos, supervivientes de los campos de exterminio naSis. Muchos de ellos vivieron en cabañas cerca del lago de Windermere. Esta es la historia cruda y redentora de los lazos que estos niños forjaron entre sí, y de cómo las amistades surgidas allí se convirtieron a veces en un salvavidas para su futuro. </info>
 
 
<page>18</page><title>LOS NUEVOS MUTANTES</title>
<microhd>8181e7fc8f92b4793899db045c53ba85d99dcf44</microhd>
<fullhd>489992e7a9f8b71255e5ce1bac991457b60a943c</fullhd>
<tresd>NA</tresd>
<cuatrok>59343ab7a4d4e62e5e04aae83277dd5dab58e8db</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/r5dpKWXzEElDaQ5odw8oGXSEXct.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/qYElKFSiqR5W2SivmHq5QZucje8.jpg</fanart>
<date>2020</date>
<genre>Ciencia ficción. Acción. Terror</genre>
<extra>NA</extra>
<info>Cinco jóvenes mutantes que acaban de descubrir sus habilidades, son encerrados en unas instalaciones secretas contra su voluntad y luchan por escapar de su pasado y salvarse a sí mismos</info>
 
 
<page>18</page><title>LOS ODIOSOS OCHO</title>
<microhd>e82171ef6dc57e81e6840e921745faa9f713fb3e</microhd>
<fullhd>33e307c1bbd969e227f2d8df8157c43bc72092a1</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/7DYGdFT4gtiD5BI6xyOU6v0XXvi.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/7gfDVfaw0VaIkUGiEH13o3TIC7A.jpg</fanart>
<date>2015</date>
<genre>Intriga. Accion</genre>
<extra>Mis</extra>
<info>Pocos años después de la Guerra de Secesión, una diligencia avanza por el invernal paisaje de Wyoming. Los pasajeros, el cazarrecompensas John Ruth (Kurt Russell) y su fugitiva Daisy Domergue (Jennifer Jason Leigh), intentan llegar al pueblo de Red Rock, donde Ruth entregará a Domergue a la justicia. Por el camino, se encuentran con dos desconocidos: el mayor Marquis Warren (Samuel L. Jackson), un antiguo soldado de la Unión convertido en cazarrecompensas de mala reputación, y Chris Mannix (Walton Goggins), un renegado sureño que afirma ser el nuevo sheriff del pueblo. Como se aproxima una ventisca, los cuatro se refugian en la Mercería de Minnie, una parada para diligencias de un puerto de montaña. Cuando llegan al local se topan con cuatro rostros desconocidos: el mexicano Bob (Demian Bichir), Oswaldo Mobray (Tim Roth), verdugo de Red Rock, el vaquero Joe Gage (Michael Madsen) y el general confederado Sanford Smithers (Bruce Dern). Mientras la tormenta cae sobre la parada de montaña, los ocho viajeros descubren que tal vez no lleguen hasta Red Rock después de todo.</info>
 
 
<page>18</page><title>LOS OJOS DE TAMMY FAYE</title>
<microhd>3ad8830f39e169e8748a4e16d5c0f23440729426</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>d6c3c8172acc2807cf65d5987fb798440dd0b946</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/iBjkm6oxTPrvNkzr63cmnrpsQPR.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/rskVKmBb83u7qbaASCxyJyLevkq.jpg</fanart>
<date>2021</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Biopic del extraordinario ascenso, caída y redención de la telepredicadora evangelista Tammy Faye Bakker. En los años 70 y 80, Tammy Faye y su marido, Jim Bakker, pusieron en pie prácticamente de la nada la red de cadenas religiosas más grande del mundo, así como un parque temático, y gozaron de una inmensa popularidad gracias a sus mensajes de amor, aceptación y prosperidad. Tammy Faye era legendaria por sus pestañas indestructibles, su original forma de cantar y su generosidad a la hora de acoger a personas de todo tipo. Pero no pasó mucho tiempo antes de que las irregularidades financieras, las rivalidades e intrigas y los escándalos derrocaran un imperio construido con gran meticulosidad.</info>
 
 
<page>18</page><title>LOS PERDONADOS</title>
<microhd>NA</microhd>
<fullhd>f848b442ae8df8314f5280dd122c900d66fab8aa</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/f37G0xxPdWw6IeeuMHxU05mMU2A.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/rPUiOzvoG4KdhRmNCoazBPVbBON.jpg</fanart>
<date>2022</date>
<genre>Drama. Thriller</genre>
<extra>NA</extra>
<info>Una pareja adinerada al borde del divorcio, David y Jo Henninger, viajan desde Londres a Marruecos para asistir a un fin de semana a todo lujo en el suntuoso hogar sahariano de unos amigos. Tras una comida acompañada de demasiado alcohol, sucede una tragedia. Lo que prometía ser una gran festividad acabará convirtiéndose en un fin de semana que, en el peor de los sentidos, ninguno olvidará jamás.</info>
 
 
<page>18</page><title>LOS PITUFOS 2</title>
<microhd>a82a3450e385ec8d89c8f3a5074422595e6a4363</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/vjb5WNkwZJDIcmVrzR4GyPmPRZo.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/fJz0UA5dHDerwW8vcY6xPoSbwOF.jpg</fanart>
<date>2013</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>El malvado brujo Gargamel crea una pareja de criaturas revoltosas, una copia de los pitufos, llamados los Malotes, y que espera le permitan aprovecharse de su poderosa y mágica esencia. Pero cuando descubre que solo un auténtico Pitufo le puede dar lo que quiere -y que solamente un secreto que conoce Pitufina puede transformar a los Malotes en verdaderos pitufos- Gargamel secuestra a Pitufina y la lleva a París. Por lo que está en manos de Papá Pitufo, Torpe, Gruñón y Vanidoso regresar a nuestro mundo, y reunirse con sus amigos humanos Patrick y Grace Winslow, ¡y rescatarla!</info>
 
 
<page>18</page><title>LOS PUENTES DE MADISON</title>
<microhd>NA</microhd>
<fullhd>bc65561413fe0c322dff05a3b2a4805fe500d035</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/aeNvfQ6kyfH2l2LKtFQ6XYxFIsm.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/4TnGcUsyd6ixuLWHaaEG5BGkAIp.jpg</fanart>
<date>1995</date>
<genre>Romance. Drama</genre>
<extra>Culto</extra>
<info>La apacible pero anodina vida de Francesca Johnson (Meryl Streep), un ama de casa que vive en una granja con su familia, se ve alterada con la llegada de Robert Kincaid (Clint Eastwood), un veterano fotógrafo de la revista National Geographic, que visita el condado de Madison (Iowa) para fotografiar sus viejos puentes. Cuando Francesca invita a Robert a cenar, un amor verdadero y una pasión desconocida nacerá entre ellos.</info>
 
 
<page>18</page><title>LOS RENGLONES TORCIDOS DE DIOS</title>
<microhd>ee8d0a59bdb5e8550a2e9c5434c791d641e20a89</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/hB2VqiLDiHAyLdZ1dlXIGqDJy06.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/bl3zwLEK1tMr8fCAuQ54paRFYXm.jpg</fanart>
<date>2022</date>
<genre>Intriga. Thriller. Drama</genre>
<extra>NA</extra>
<info>Alice, investigadora privada, ingresa en un hospital psiquiátrico simulando una paranoia. Su objetivo es recabar pruebas del caso en el que trabaja: la muerte de un interno en circunstancias poco claras. Sin embargo, la realidad a la que se enfrentará en su encierro superará sus expectativas y pondrá en duda su propia cordura. Un mundo desconocido y apasionante se mostrará ante sus ojos. Adaptación de la novela homónima de Torcuato Luca de Tena.</info>
 
 
<page>18</page><title>LOS RODRIGUEZ Y EL MAS ALLA</title>
<microhd>e5c803d23804c34d3c787aa45a5be9f5ae28f5b6</microhd>
<fullhd>df08afd8accebe39061be9141687da90b7dcefda</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/1U1x6ZDqCSlJVqUXKm58bhdFfsW.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/n6OkZL3n4ZM9zcPpf67ItCUVzYC.jpg</fanart>
<date>2019</date>
<genre>Comedia. Fantástico. Ciencia ficción. Infantil</genre>
<extra>NA</extra>
<info>Los Rodríguez son una familia como otra cualquiera, o al menos eso creían. Todo cambiará para ellos cuando descubran que el difunto abuelo era, en realidad, de otro planeta. Su nieto, Nicolás, abre en el trastero de la casa familiar una puerta cósmica por donde acceder al “más allá”. Y a partir de ahí, el caos se apoderará de esta familia, que deberá aprender a controlar los superpoderes que ha recibido.</info>
 
 
<page>18</page><title>LOS SECRETOS QUE OCULTAMOS</title>
<microhd>711df3b54e2fcdfe4b787db84b2ab6d4012ec04c</microhd>
<fullhd>15aac64a177ea492f91736406ad014795e4abb74</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/kTIAVoqJ8mKamOew1YNNf1vQlN8.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/7ajFPsJ0QWTElz8JO6K5P0G8n3R.jpg</fanart>
<date>2020</date>
<genre>Drama. Thriller </genre>
<extra>NA</extra>
<info>En los Estados Unidos post Segunda Guerra Mundial, una mujer (Rapace) intenta reconstruir su vida en los suburbios con su esposo (Messina), y secuestra a su vecino (Kinnaman) en busca de venganza por los atroces crímenes de guerra que cree que cometió contra ella.</info>
 
 
<page>18</page><title>LOS SIETE MAGNIFICOS</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>98d7575e94d16ce7ddabc85101e565a8c3106b86</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/v6fvtfBKs83iVT9EZDcRPWLXCRU.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/4LqxE6DDj6KTmt2r1Q5CFYkJEM6.jpg</fanart>
<date>2016</date>
<genre>Acción</genre>
<extra>Mis</extra>
<info>Los habitantes de Rose Creek, atemorizados bajo el control del industrial Bartholomew Bogue, deciden contratar a siete forajidos para terminar con la amenaza: Sam Chisolm (Denzel Washington), Josh Faraday (Chris Pratt), Goodnight Robicheaux (Ethan Hawke), Jack Home (Vincent D´Onofrio), Billy Rocks (Byung-Hun Lee), Vasquez (Manuel García Rulfo) y Red Harvest (Martin Sensmeier). Sin embargo, pronto se darán cuenta estos siete que están luchando por algo más que el simple dinero. Remake del western homónimo de John Sturges, a su vez remake de 'Los siete samuráis' de Akira Kurosawa</info>
 
 
<page>18</page><title>LOS SIETE SAMURAIS</title>
<microhd>80ad83d67525478157373405c3b3442e1c2ec9aa</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/zr4DkzWIMjaWtj6hlsbN6dnNeTX.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/mzI1JXm8sX5fia3ifPS2jGPUh2n.jpg</fanart>
<date>1954</date>
<genre>Aventuras. Drama</genre>
<extra>Culto</extra>
<info>Japón, Siglo XVI. Una aldea de campesinos indefensos es repetidamente atacada y saqueada por una banda de forajidos. Aconsejados por el anciano de la aldea, unos aldeanos acuden a la ciudad con el objetivo de contratar a un grupo de samuráis para protegerlos. A pesar de que el único salario es comida y techo, varios samuráis se van incorporando uno a uno al singular grupo que finalmente se dirige a la aldea.</info>
 
 
<page>18</page><title>LOS TIPOS MALOS</title>
<microhd>NA</microhd>
<fullhd>B4CF065EC013B2CEC2A307F9BC27BB4B8E8C664C</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/wFdwJh3fbhp5aiRbQelVz1mbbwP.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/9IDJpHROaC0S1ZlIxrvzOcOX5yC.jpg</fanart>
<date>2022</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Cuenta la historia de cinco famosos delincuentes que tratan de realizar su empresa más complicada hasta la fecha: portarse bien. Se basa en los libros ilustrados homónimos de Aaron Blabey.</info>
 
 
<page>18</page><title>LOVING PABLO</title>
<microhd>1ebdeb47952f74d0b37da04d26c01d01f217b851</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/cjgO5nJTC0E3TxicrHk5XCd27tB.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/pcIXV6Od8KeDsLWKu7Wzthaur4V.jpg</fanart>
<date>2017</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Drama sobre el narcotraficante colombiano Pablo Escobar, basado en el libro 'Amando a Pablo, odiando a Escobar', escrito por la periodista colombiana Virginia Vallejo, donde cuenta la relación amorosa que mantuvo con el narcotraficante en la década de los 80.</info>
 
 
<page>18</page><title>LUCA</title>
<microhd>NA</microhd>
<fullhd>adf9575d5cd2cd6e149ee136df554b84cc200b79</fullhd>
<tresd>NA</tresd>
<cuatrok>5c9c191c43e728387b67b9b37ac13e3ac5cdb1c0</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/jTswp6KyDYKtvC52GbHagrZbGvD.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/aI5S2WMoTFVgBznYi2DP3WRojCl.jpg</fanart>
<date>2021</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>La historia se desarrolla en un hermoso pueblo al lado del mar en la Riviera Italiana. Es la del crecimiento de un niño que experimenta un verano inolvidable con helado, pasta e interminables viajes en scooter. Luca comparte estas aventuras con su nuevo mejor amigo, pero toda la diversión se ve amenazada por un gran secreto: es un monstruo marino de otro mundo ubicado justo debajo de la superficie del agua.</info>
 
 
<page>18</page><title>LUTHER: CAE LA NOCHE</title>
<microhd>NA</microhd>
<fullhd>da3227f31de4e5b50245f2d207956a593c9ee395</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/chgYU3vF3P4AwXQKpWNjp9KBp2e.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/eN6R6mb3ntHwA3y3MhSwpP78ljN.jpg</fanart>
<date>2023</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Atormentado por un caso sin resolver, el brillante pero caído en desgracia inspector londinense John Luther se fuga de la cárcel para intentar atrapar a un sádico asesino en serie... Largometraje inspirado en la serie de televisión "Luther".</info>
 
 
<page>18</page><title>M3GAN</title>
<microhd>NA</microhd>
<fullhd>cfbbba81f71444e15679dc4601a1a41ff6bac3ab</fullhd>
<tresd>NA</tresd>
<cuatrok>fc2c58f6dcb97a4a92d919ce17ff3b1aee774028</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/rxDPzExeovcBZY2IVWdYs87AzVE.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/A077VsMIBBXNvlI5mohGnSiIrfI.jpg</fanart>
<date>2023</date>
<genre>Terror. Thriller. Ciencia ficción </genre>
<extra>NA</extra>
<info>M3GAN es una maravilla de la inteligencia artificial, una muñeca realista programada para ser la mejor compañera de los niños y la mayor aliada de los padres. Diseñada por Gemma, M3GAN es capaz de escuchar, observar y aprender mientras se convierte en amiga, profesora, compañera de juegos y protectora del niño al que se vincule. Cuando, inesperadamente, Gemma se convierte en la tutora legal de Cady, su sobrina huérfana de 9 años, no sabe muy bien qué hacer ni se siente preparada para ejercer de madre. Sometida a un intenso estrés laboral, Gemma decide vincular su prototipo de M3GAN a Cady en un intento de resolver ambos problemas, pero no tardará en descubrir las inimaginables consecuencias de su decisión.</info>
 
 
<page>18</page><title>MAD MAX 2 :  EL GUERRERO DE LA CARRETERA</title>
<microhd>c6f92fb64f93e997d1a2b6a20f982ca5ae8607e3</microhd>
<fullhd>7a0998549982e5c64ccff253bfb161c746dacb10</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/dcWUhNODxu7bYvoobo1IkKuFROL.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/uTbIoazfviVXmM4hYlC95lOlIDw.jpg</fanart>
<date>1981</date>
<genre>Acción. Ciencia ficción.Saga</genre>
<extra>Culto</extra>
<info>Tras el holocausto nuclear, la gasolina se ha convertido en un bien escaso y muy codiciado. Mad Max, héroe solitario, inicia una lucha sin cuartel para ayudar a una colonia de supervivientes constantemente atacada por un grupo de violentos guerreros que intenta arrebatarle un tanque de gasolina. Max decide ayudar a los defensores del tanque... </info>
 
 
<page>18</page><title>MAD MAX 3: MAS ALLÁ DE LA CUPULA DEL TRUENO</title>
<microhd>NA</microhd>
<fullhd>612fd4e10dbc358d4015cffc3665e4b7dfad4df4</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/pgmOX4PQl5xPAdAx18zPPnQjQKs.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/yIx1Wm8bEFO5Qmz7YHX9ZbiZm24.jpg</fanart>
<date>1985</date>
<genre>Acción. Ciencia ficción.Saga</genre>
<extra>Mis</extra>
<info>Tras la catástrofe nuclear, Mad Max cruza un desierto donde pierde su caravana de camellos. Llega a una ciudad donde le proponen cambiárselos a cambio de que ataque al tirano de la ciudad subterránea, un enano que fabrica gas metano con excrementos de cerdo.</info>
 
 
<page>18</page><title>MAD MAX:  SALVAJES DE AUTOPISTA</title>
<microhd>NA</microhd>
<fullhd>5c3b6e78fc04e903f1f6eee62d4994e4f2bf3e7f</fullhd>
<tresd>NA</tresd>
<cuatrok>b40c06317eedc76a71d6439444516b094d0c84e4</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/pgTGAi2z6hqI2llYvlJjZeP8GuE.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/iojuitFQi3Rb05ps7rBhJTdnYGs.jpg</fanart>
<date>1979</date>
<genre>Acción. Ciencia ficción.Saga</genre>
<extra>Culto</extra>
<info>En un futuro posnuclear, Max Rockatansky, un policía encargado de la vigilancia de una autopista, tendrá que vérselas con unos criminales que actúan como vándalos, sembrando el pánico por las carreteras. Cuando, durante una persecución, Max acaba con Nightrider, el líder del violento grupo, el resto de la banda jura vengar su muerte. </info>
 
 
<page>18</page><title>MAD MAX: FURIA EN LA CARRETERA</title>
<microhd>3236f30e5be60929437e13a195ec9f7e6efdd9a3</microhd>
<fullhd>NA</fullhd>
<tresd>bae393393840ed9da122cec3b475d05588585129</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/mnVZLdo9C4X80sJmgcGNpMNCbgk.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/nlCHUWjY9XWbuEUQauCBgnY8ymF.jpg</fanart>
<date>2015</date>
<genre>Acción. Ciencia ficción.Saga</genre>
<extra>NA</extra>
<info>Perseguido por su turbulento pasado, Mad Max cree que la mejor forma de sobrevivir es ir solo por el mundo. Sin embargo, se ve arrastrado a formar parte de un grupo que huye a través del desierto en un War Rig conducido por una Emperatriz de élite: Furiosa. Escapan de una Ciudadela tiranizada por Immortan Joe, a quien han arrebatado algo irreemplazable. Enfurecido, el Señor de la Guerra moviliza a todas sus bandas y persigue implacablemente a los rebeldes en una "guerra de la carretera" de altas revoluciones... Cuarta entrega de la saga post-apocalíptica que resucita la trilogía que a principios de los ochenta protagonizó Mel Gibson. </info>
 
 
<page>18</page><title>MADAME CURIE</title>
<microhd>bbf5f225e28505eff0120d9a97347e5c5d9344ac</microhd>
<fullhd>fa6e71512292c26281ade623aa8e7608bb743540</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/v57nuQ0ZFv22EHOJHGQucMB1w9N.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/e7tjpKP36Xd6BzyTMZo4AUe86VM.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Pionera. Rebelde. Genio. Radioactive es la historia real e increíble de Marie Sklodowska-Curie y su trabajo ganador del Premio Nobel que cambió el mundo para siempre. A medida que descubre elementos radiactivos previamente desconocidos, pronto se vuelve terriblemente evidente que su investigación podría conducir a aplicaciones en medicina que permitirían salvar miles de vidas, pero también a usos en la guerra que podrían destruir millones de ellas.</info>
 
 
<page>18</page><title>MADELEINE COLLINS</title>
<microhd>9215f3d8079031171ec43af58ac84751f0c22dc8</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/n12GwPNwr3pG0F5ytFRVtfQmcOl.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/nRx0bDFOLzt0RYrP61WJJs4Yno4.jpg</fanart>
<date>2021</date>
<genre>Thriller. Drama</genre>
<extra>NA</extra>
<info>Judith lleva una doble vida entre Suiza y Francia. Por un lado, Abdel, con quien está criando a su hija en común. Por el otro, Melvil, con quien tiene dos hijos un poco mayores. Poco a poco, este frágil equilibrio hecho de mentiras, secretos e idas y venidas se va fracturando. Ante el desenmascaramiento, Judith elige la huída hacia adelante. </info>
 
 
<page>18</page><title>MADRE</title>
<microhd>343892a308ad1ce35e989a622f9198351ba82370</microhd>
<fullhd>77d975d8454f5da308a44f4712e18844cd0051b9</fullhd>
<tresd>NA</tresd>
<cuatrok>660a9887b7e6668c9c1f3070baafed89092eca54</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/j8wRmze5yJ9uN9zrvXpx9ZcfoO2.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/sZcF7Y4npaByM9P9hFfxHqr3WGE.jpg</fanart>
<date>2019</date>
<genre>Drama. Intriga</genre>
<extra>NA</extra>
<info>Elena (Marta Nieto) perdió a su hijo Iván, de seis años, en una playa de Francia. Ahora Elena vive en esa playa y está empezando a salir de ese oscuro túnel donde ha permanecido anclada todo este tiempo. Secuela en formato largometraje del cortometraje homónimo del propio Sorogoyen.</info>
 
 
<page>18</page><title>MADRES PARALELAS</title>
<microhd>331398d7cecbaf9cf60d15eea59ec3ee95b2b60b</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/cxp4W9QtHOOdi2VfgUmB9AtljvA.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/kcUuumAlMWqw7SzXE4DEqd3M8Gc.jpg</fanart>
<date>2021</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Dos mujeres coinciden en una habitación de hospital donde van a dar a luz. Ambas están solteras y se quedaron embarazadas por accidente. Janis, de mediana edad, no se arrepiente y está exultante. La otra, Ana, una adolescente, está asustada, arrepentida y traumatizada. Janis intenta animarla mientras pasean por los pasillos del hospital. Las pocas palabras que intercambien en esas horas crearán un vínculo muy estrecho entre las dos, que por casualidad se desarrolla y se complica, afectando a sus vidas de forma decisiva.</info>
 
 
<page>18</page><title>MAFIA MAMMA</title>
<microhd>NA</microhd>
<fullhd>622523c23e18764d3d76871347b2d57500a5637d</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/ezJ9Jsiy8opl5WKJCTdQcu63pfr.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/gGZxCy9keK0D6TxbmS8GJj85Ut4.jpg</fanart>
<date>2023</date>
<genre>Comedia</genre>
<extra>Estreno</extra>
<info>Kristin Balbano es una ejecutiva y madre de familia estadounidense que viaja a Roma para asistir al funeral de su abuelo. Allí descubre que el negocio familiar es una organización criminal y sus rivales ahora tienen a los Balbano en el punto de mira. Kristin tiene que hacerse con las riendas, siguiendo los deseos de su abuelo. Guiada por la consigliere de confianza, desafía las expectativas de todos, incluida la suya propia, como la nueva jefa del negocio familiar.</info>
 
 
<page>18</page><title>MAGIC CAMP</title>
<microhd>91b04b0fa0c13f9bf57a08bfa1fbc34940a65de2</microhd>
<fullhd>c907f6f1a3a5680a98721cc87fa3e58b532b1e4c</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/1bYfyZXe50PzcMvHl1sFKFmKfTS.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/mZgbq4Zpxz7ozWXl4asj6vNdlIi.jpg</fanart>
<date>2020</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Un banquero regresa al Magic Camp al que asistió cuando era niño.</info>
 
 
<page>18</page><title>MAIGRET</title>
<microhd>e880398a26bcf8614788a4c29ebf7378f896fdfa</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/hJGm8gqeFVd5MkOZa6HEHDi1te7.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/nhakN8pulc348fXw4sMCDRab3g9.jpg</fanart>
<date>2022</date>
<genre>Intriga</genre>
<extra>NA</extra>
<info>París, década de 1950, el cuerpo de una hermosa joven vestida con un elegante traje de noche aparece en mitad de una plaza. El célebre inspector Jules Maigret se encarga de investigar el caso, pero le resulta imposible identificar a la víctima, porque absolutamente nadie parece haberla conocido ni recordarla. En el transcurso de sus indagaciones, el camino de Maigret se cruza con el de Betty, una joven delincuente con un parecido sorprendente con la víctima. Todo ello despierta en Maigret el recuerdo de otra desaparición mucho más antigua e íntima.</info>
 
 
<page>18</page><title>MAIXABEL</title>
<microhd>4CDD465A755D16EE71692306326683631E60F9A5</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/lQfKSDS7f1rsmg20B1YPmLBWgiw.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/7P8MrCJfAeKrzbfrv5AEU1tvJfE.jpg</fanart>
<date>2021</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Maixabel Lasa pierde en el año 2000 a su marido, Juan María Jaúregui, asesinado por ETA. Once años más tarde, recibe una petición insólita: uno de los asesinos ha pedido entrevistarse con ella en la cárcel de Nanclares de la Oca /Álava), en la que cumple condena tras haber roto sus lazos con la banda terrorista. A pesar de las dudas y del inmenso dolor, Maixabel accede a encontrarse cara a cara con las personas que acabaron a sangre fría con la vida de quien había sido su compañero desde los dieciséis años.</info>
 
 
<page>18</page><title>MALASAÑA 32</title>
<microhd>8f15638a16d5de3c627745e49a920212ed1d6f0e</microhd>
<fullhd>23a684f7267cf11730bd5c6d72c4a8406a65d56b</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/mJTIkMOaEM74ChVBX9WVxyufLrm.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/mJTIkMOaEM74ChVBX9WVxyufLrm.jpg</fanart>
<date>2020</date>
<genre>Terror</genre>
<extra>NA</extra>
<info>Manolo y Candela se instalan en el madrileño barrio de Malasaña, junto a sus tres hijos y el abuelo Fermín. Atrás dejan el pueblo en busca de la prosperidad que parece ofrecerles la capital de un país que se encuentra en plena transición política. Pero hay algo que la familia Olmedo no sabe: en la casa que han comprado, no están solos...</info>
 
 
<page>18</page><title>MALCOLM AND MARIE</title>
<microhd>5a78bcc5f2d47f58e2d0f0bc2f06ca5884461705</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/svwb7yGCbjHppmwhsdJbDH0xhUI.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/kiruQFdPZDph4NiMYQNW5hiD41G.jpg</fanart>
<date>2021</date>
<genre>Drama. Romance </genre>
<extra>NA</extra>
<info>La relación de un director de cine y su novia es puesta a prueba cuando regresan a casa tras el estreno de su película y esperan las reacciones de los críticos.</info>
 
 
<page>18</page><title>MALDITOS BASTARDOS</title>
<microhd>NA</microhd>
<fullhd>cfdfbc964a6227facc177bcba013a90c5d1bfef7</fullhd>
<tresd>NA</tresd>
<cuatrok>c053be8817e2fdba9b30373f475ee07da296eb1e</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/dqu7nUtKTLdpM7DaJvD4zcSXhn1.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/bPnPsyo09tLRnTOb7AlsBsgnbhP.jpg</fanart>
<date>2009</date>
<genre>Bélico. Acción. Comedia</genre>
<extra>Mis</extra>
<info>Segunda Guerra Mundial (1939-1945). En la Francia ocupada por los alemanes, Shosanna Dreyfus (Mélanie Laurent) presencia la ejecución de su familia por orden del coronel Hans Landa (Christoph Waltz). Después de huir a París, adopta una nueva identidad como propietaria de un cine. En otro lugar de Europa, el teniente Aldo Raine (Brad Pitt) adiestra a un grupo de soldados judíos ("The Basterds") para atacar objetivos concretos. Los hombres de Raine y una actriz alemana (Diane Kruger), que trabaja para los aliados, deben llevar a cabo una misión para hacer caer a los jefes del Tercer Reicj. El destino quiere que todos se encuentren bajo la marquesina de un cine donde Shosanna espera para vengarse.</info>
 
 
<page>18</page><title>MALEFICA</title>
<microhd>53d60e37f97ccbb65693e23b5e1998e194c5dc2e</microhd>
<fullhd>824b8b8615325983886f5c98d806abf89f00d47d</fullhd>
<tresd>4c932f690e0bdee9a3ce5ac9645bfe904a327180</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/2d4lnLtysk2jrLdJaAtUL6CuXYE.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/xjotE7aFdZ0D8aGriYjFOtDayct.jpg</fanart>
<date>2014</date>
<genre>Fantástico. Aventuras.Saga</genre>
<extra>NA</extra>
<info>Maléfica es una bella hada con un corazón puro y unas asombrosas alas negras. Crece en un entorno idílico, un apacible reino en el bosque limítrofe con el mundo de los hombres, hasta que un día un ejército de invasores humanos amenaza la armonía de su país. Maléfica se erige entonces en la protectora de su reino, pero un día es objeto de una despiadada e inesperada traición, un hecho triste y doloroso que endurecerá su corazón hasta convertirlo en piedra, y que la llevará a lanzar una temible maldición.</info>
 
 
<page>18</page><title>MALEFICA: MAESTRA DEL MAL</title>
<microhd>2d5b40ab922109496572e3722c5bb797d5d2b09f</microhd>
<fullhd>37c25ba7e1e55e79e5bc8d6434fb7207d844fd79</fullhd>
<tresd>489972a9a6696d89f72aa7aeb870b3560a37c613</tresd>
<cuatrok>671c6c528c56077bc5662c92ac4af644b142d24d</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/cXOGXpfKSbUD6E3Qewsj4hKy9Jx.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/HmEXofYSc56wBHIqY6tJDroCSw.jpg</fanart>
<date>2019</date>
<genre>Saga. Fantastico. Aventuras</genre>
<extra>NA</extra>
<info>Tiene lugar varios años después de los acontecimientos narrados en la primera película, y explora la relación entre Maléfica y Aurora y las alianzas que se forman para sobrevivir a las amenazas del mágico mundo en el que habitan. Secuela de "Maléfica" (2014).</info>
 
<page>19</page><title>MALEVOLENT</title>
<microhd>e51a2be775665e24f039679af893dbdb6d5988cd</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/lm29Sf2uI69CmbCsUkWFn7sbvL6.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/9St4dPHHgG2XaLv7tMCVJV09p5c.jpg</fanart>
<date>2018</date>
<genre>Thriller. Terror</genre>
<extra>NA</extra>
<info>Dos hermanos especializados en engañar a la gente con "problemas paranormales" se topan con un caso que resulta ser pero que muy real.</info>
 
 
<page>19</page><title>MALNASIDOS</title>
<microhd>c5e5d72871cd9c17634bf46f2b09d554892e66f5</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/4z9fsQTypbwzaWa4kLERtJjjM83.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/9XtMjrgfZJvonFUhpKpWjSmXjzr.jpg</fanart>
<date>2022</date>
<genre>Terror. Bélico. Acción</genre>
<extra>NA</extra>
<info>Durante la Guerra Civil Española, meses de sangrientos combates han dejado tras de sí miles de muertos en las trincheras. Jan Lozano, capitán de la quinta brigada del bando nacional, cae prisionero junto a un joven soldado cuando los rodea en el bosque un grupo de republicanos. La única posibilidad de escapar a la sentencia de muerte es hacer frente a una misión imposible en campo enemigo. Pero un peligro mayor del esperado obligará a los bandos rivales a unirse contra un nuevo y desconocido adversario. Tendrán que dejar de lado el odio mutuo y así evitar convertirse en infectados.</info>
 
 
<page>19</page><title>MALOS TIEMPOS EN EL ROYALE</title>
<microhd>57f3655f360a8813853153e49607b990219d88ef</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/iNtFgXqXPRMkm1QO8CHn5sHfUgE.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/gb3TVVZNNxVGNfS1NxGaiWZfwnW.jpg</fanart>
<date>2018</date>
<genre>Thriller</genre>
<extra>NA</extra>
<info>Siete desconocidos, cada uno con un secreto, se reúnen en el hotel El Royale, en el lago Tahoe, un sitio ruinoso con un oscuro pasado. En el transcurso de una fatídica noche, todos tendrán una última oportunidad de redención ... antes de que todo se vaya al infierno.</info>
 
 
<page>19</page><title>MAMA NO ENREDES</title>
<microhd>NA</microhd>
<fullhd>7f21edc0849251f8698fbff33b2f2cc322049971</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/aR1bh2DCg4NUzNbGavFo5jxpHWl.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/pfDaIuKoRVLirTPHUfiEV3MkpdL.jpg</fanart>
<date>2022</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Clara, madre de dos adolescentes, es una mujer abierta a nuevas experiencias que decide crearse un perfil en TILINK, una app de citas. ¿Qué pasa por la mente de un veinteañero cuando se encuentra el perfil de su propia madre en Tilink, la aplicación de citas de moda? Sí, su madre aún es joven. Sí, está divorciada de su padre y tiene derecho a rehacer su vida, pero... ¿no sabe lo que es Tilink? Allí sólo hay salidos que la van a utilizar para luego dejarla tirada, francotiradores del sexo... como él mismo. Dani y su hermana Milena se embarcarán en la delirante aventura de boicotear los ligues de su madre, e irán a saco cuando la mujer inicie una relación más seria con un yogurín argentino, convencidos de que sólo la va a hacer sufrir. Sólo que las estrategias de los hermanos tendrán el efecto opuesto al deseado, cada vez habrá más hombres en la vida de su madre y nada saldrá como ellos esperan.</info>
 
 
<page>19</page><title>MAMA O PAPA</title>
<microhd>138AD0716D65A70F9570990AF7C08D1AB07B9EC5</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/yo4aBPuyWh8uB2VHPwK1O7R6WwO.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/4VL5C8s3y2efpB03SvCD3eFxg1i.jpg</fanart>
<date>2021</date>
<genre>Infantil. Comedia</genre>
<extra>NA</extra>
<info>Flora y Víctor son los padres que todo niño querría tener: modernos, divertidos y cariñosos. Sin embargo, todo se tuerce el día que deciden divorciarse y aparece la oportunidad laboral con la que llevan soñando toda la vida. Solo hay un problema: la custodia. Ninguno está dispuesto a ceder. Así que, por orden de la jueza, los niños tendrán que decidir con quién se quedan: con mamá, o con papá. A partir de este momento, los padres modélicos se declaran la guerra y no habrá tregua. Ambos harán todo lo posible por no obtener la custodia de sus hijos.</info>
 
 
<page>19</page><title>MAMA TE QUIERE</title>
<microhd>b4b545437d68de8caa78ed5263a4c48ccb765325</microhd>
<fullhd>819c581df172ec57743cfaa45e6ff9be6aabae2e</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/rEeTuQSsEyM2bSG7rwvm23WJjfg.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/DnjwdbjdR2c5MFhKv5ZSRf8rDU.jpg</fanart>
<date>2020</date>
<genre>Terror. Thriller</genre>
<extra>NA</extra>
<info>Dicen que el amor de una madre es para siempre... pero para Chloe esto no es un consuelo, sino una amenaza. Hay algo extraño, incluso siniestro en la relación de Chloe (Kiera Allen) y su madre, Diane (Sarah Paulson). Diane ha criado a su hija completamente aislada, controlando cada uno de sus movimientos, pero Chloe pronto empezará a descubrir los oscuros secretos que guarda su madre.</info>
 
 
<page>19</page><title>MANHATTAN SIN SALIDA</title>
<microhd>57d1d1e235f52b0dd32bec9275af701c67296e6a</microhd>
<fullhd>bbad606158455c23693f8fdd53d911dd7eb44482</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/lvd6UqDqoYv0gMSqzFyiZkC8eny.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/5KmhjlR5CEarB8mKtpjcjHRYIu9.jpg</fanart>
<date>2019</date>
<genre>Thriller. Drama. Acción</genre>
<extra>NA</extra>
<info>Andre Davis (Chadwick Boseman) es un policía de Nueva york al que le encargan la investigación del asesinato de varios policías. Durante la búsqueda contrarreloj de los responsables, en la que se cierran por primera vez en la historia de Manhattan todos los puentes que acceden a ella, el detective descubre una conspiración en la que tendrá que discernir entre aquellos a los que caza y los sospechosos que están tratando de cazarle a él.</info>
 
 
<page>19</page><title>MANK</title>
<microhd>72267d9f0b25da0f108c1cf080128bd8c36d51bd</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/Atd7UaGgwOhcyHggqchYLqSJ6VO.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/hBOH4PNnhcGPgZbZjBwkx9gnNxI.jpg</fanart>
<date>2020</date>
<genre>Drama. Comedia</genre>
<extra>NA</extra>
<info>Biopic sobre Herman Mankiewicz, guionista de 'Ciudadano Kane', que repasa el proceso de rodaje de la obra maestra de Orson Welles, dirigida y estrenada en 1941. La película toma como base un guión escrito por Jack Fincher, padre de David Fincher, antes de morir en 2003.</info>
 
 
<page>19</page><title>MANTEN LA CALMA</title>
<microhd>b8655833697ee7772fda5575aba8eac8d326645b</microhd>
<fullhd>4a558887825a20619597239202bd540e1d98f512</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/haadA0Dg9TK5ya3v4leItQYENUb.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/iYDPorAGIDLrFC2nPV2nId7fDH7.jpg</fanart>
<date>2019</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>En la zona rural más oscura de Irlanda, el ex-boxeador Douglas 'Arm' Armstrong se ha convertido en el brazo fuerte de la familia Devers, dedicada al negocio de la venta de droga. Mientras tanto, también trata de ser un buen padre para Jack, un niño autista de cinco años. Dividido entre sus dos familias, la lealtad de Arm se pone a prueba cuando los Devers le piden por primera vez que asesine a un hombre.</info>
 
 
<page>19</page><title>MAR ADENTRO</title>
<microhd>3c387256f2c709ce78c1f7a0bdc3eb39a9bf9be9</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/tupNYmu4H2h0G7doPa0OI0T4zk9.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/uogzGsu5cVUYAl9Xf9JAVvElNLK.jpg</fanart>
<date>2004</date>
<genre>Drama</genre>
<extra>Mis</extra>
<info>Ramón (Javier Bardem) lleva casi treinta años postrado en una cama al cuidado de su familia. Su única ventana al mundo es la de su habitación, que da al mar, donde sufrió el accidente que interrumpió su juventud. Desde entonces, su único deseo es morir dignamente. En su vida ejercen una gran influencia dos mujeres: Julia (Belén Rueda), una abogada que apoya su causa, y Rosa (Lola Dueñas), una vecina que intenta convencerlo de que vivir merece la pena. Pero también ellas, cautivadas por la luminosa personalidad de Ramón, se replantearán los principios que rigen sus vidas. Él sabe que sólo quien de verdad le ame le ayudará a emprender el último viaje.</info>
 
 
<page>19</page><title>MARIA REINA DE ESCOCIA</title>
<microhd>85a63db3a4d7c9496d4cc31d36788eb7c76cbee0</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>ac014ed7823ca4ff12db7546bd5607218a2dabd6</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/sWjZjCZlFHy4C12JU3IIOj3lW22.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/iv5N169nQIzrS0pZ8MBEiemTtVt.jpg</fanart>
<date>2018</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Relato biográfico de una etapa de la vida de la reina María Estuardo de Escocia, que se enfrentó a su prima Isabel I cuando, al volver de Francia tras haber enviudado, reclamaba su derecho a la corona de Inglaterra.</info>
 
 
<page>19</page><title>MARIDOS</title>
<microhd>NA</microhd>
<fullhd>37242733cd6edf625a0b6a0a5a0203a36170167b</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/aJQx8Ba9NTWfMqWngoMDhIOiDCN.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/rE4Av8pKVObC872MsbMCcN2UHux.jpg</fanart>
<date>2023</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Toni (Paco León) y Emilio (Ernesto Alterio) reciben la misma trágica llamada: sus mujeres están en coma tras un alud en una estación de esquí. Cuando se presentan en el mostrador de admisiones del hospital de montaña hacen un sorprendente descubrimiento: sus mujeres son, en realidad, la misma persona, Laura (Celia Freijeiro). Durante años, Laura ha llevado en secreto vidas paralelas, una salvaje montaña rusa a caballo entre sus dos familias. Obligados a convivir hasta que Laura despierte y pueda ser trasladada, Emilio y Toni luchan por demostrar quién de los dos es el único y auténtico marido.</info>
 
 
<page>19</page><title>MARLOWE</title>
<microhd>NA</microhd>
<fullhd>4ad92a7cfdee01c8b919504f2d670b3d4c06f969</fullhd>
<tresd>NA</tresd>
<cuatrok>93646e4dc7053fb47242e8468c9a87c919aa7490</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/gKWPzgKwNqexpZP7LkIngLHbzaq.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/ipy6YEHhqwsEsLX61i6aidaPNOl.jpg</fanart>
<date>2022</date>
<genre>Thriller. Intriga</genre>
<extra>NA</extra>
<info>A finales de los años 30, en los bajos fondos de Los Ángeles, el detective privado Philip Marlowe (Liam Neeson) es contratado para encontrar al ex amante de una glamurosa heredera (Diane Kruger), hija de una conocida estrella de cine (Jessica Lange). La desaparición desentierra una red de mentiras y Marlowe se verá envuelto en una investigación peligrosa y mortal en la que todos los implicados tienen algo que ocultar.</info>
 
 
<page>19</page><title>MARVEL: A - CAPITAN AMERICA EL PRIMER VENGADOR</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>059a673eacb88437a1ea58d8db528b39ef62b6ed</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/3AZTlCFbe6vBcbzJ6fDLg0PTFVs.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/d5rx7OeKnBLYL0PhDRofzqWXQvU.jpg</fanart>
<date>2016</date>
<genre>Saga.Accion. Thriller. Fantastico</genre>
<extra>NA</extra>
<info>Después de que otro incidente internacional involucre a Los Vengadores, causando varios daños colaterales, aumentan las presiones políticas para instaurar un sistema que exija más responsabilidades y que determine cuándo deben contratar los servicios del grupo de superhéroes. Esta nueva situación dividirá a Los Vengadores, mientras intentan proteger al mundo de un nuevo y terrible villano. Tercera entrega de la saga Capitán América.</info>
 
 
<page>19</page><title>MARVEL: B - CAPITANA MARVEL</title>
<microhd>e8ddd048dada141e3ed737ef4ec1afde6fc6359f</microhd>
<fullhd>96c3f2165f47c8c88a570df667b02843c256eadc</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/gQIQqdrhFdKBFEaZuZ117wkpqOJ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/o3FYGEKUJHKGngfWI5mDneUUnzV.jpg</fanart>
<date>2019</date>
<genre>Saga.Fantastico. Accion. Aventuras. Ciencia ficcion</genre>
<extra>NA</extra>
<info>La historia sigue a Carol Danvers mientras se convierte en una de las heroínas más poderosas del universo, cuando la Tierra se encuentra atrapada en medio de una guerra galáctica entre dos razas alienígenas... Situada en los años 90, 'Capitana Marvel' es una historia nueva de un período de tiempo nunca antes visto en la historia del Universo Cinematográfico de Marvel.</info>
 
 
<page>19</page><title>MARVEL: C - IRON MAN</title>
<microhd>ae718b0e12892194648437b58a58d3c200fdafa2</microhd>
<fullhd>3a33ca9df8151342e98902513e87e8641375a0fb</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/veJTO75R5aapilhmlcFuqoGCPe3.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/55RIH0MvyCRKLjx1RjEICF9SvAj.jpg</fanart>
<date>2008</date>
<genre>Saga.Fantastico. Accion. Aventuras. Ciencia ficcion</genre>
<extra>NA</extra>
<info>El multimillonario fabricante de armas Tony Stark (Robert Downey Jr.) debe enfrentarse a su turbio pasado después de sufrir un accidente con una de sus armas. Equipado con una armadura de última generación tecnológica, se convierte en "El hombre de hierro", un héroe que se dedica a combatir el mal en todo el mundo.</info>
 
 
<page>19</page><title>MARVEL: D - EL INCREIBLE HULK</title>
<microhd>1a287a3db37144c7ab5cad8b5fea51c9ca2a156e</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/p4NYZXVtgKf6aiH65QzzkUVJcsd.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/cVpBJxRBIwTjajKQ08TE6BINTEu.jpg</fanart>
<date>2008</date>
<genre>Saga.Fantastico. Accion. Ciencia ficcion</genre>
<extra>NA</extra>
<info>El científico Bruce Banner (Edward Norton) recorre el mundo en busca de un antídoto que le permita librarse de su Alter Ego. Perseguido por el ejército y dominado por su propia rabia, es incapaz de sacarse de la cabeza a Betty Ross (Liv Tyler), por lo que decide volver a la civilización. Mientras se enfrenta a una extraña criatura, el agente de la KGB Emil Blonsky (Tim Roth) se expone a una dosis de radiación más intensa que la que convirtió a Bruce en Hulk. Emil hace responsable a Hulk de su terrible situación, y la ciudad de Nueva York se convierte en el escenario de la última batalla entre las dos criaturas más poderosas que jamás pisaron la Tierra.</info>
 
 
<page>19</page><title>MARVEL: E - IRON MAN 2</title>
<microhd>50d17d2caa8dd787380e4a58da453428e82b1a61</microhd>
<fullhd>c058e59395744b179ef7d799b79feb7e2a1667ae</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/5tCA3oZXLRPHmS5DIDnlu7hY4Ab.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ijrl82hn5jlEXSBz5iIdf2y4wWb.jpg</fanart>
<date>2010</date>
<genre>Saga.Fantastico. Ciencia ficcion. Accion</genre>
<extra>NA</extra>
<info>El mundo ya sabe que el multimillonario Tony Stark (Robert Downey Jr.) es Iron Man, el superhéroe enmascarado. A pesar de las presiones del gobierno, la prensa y la opinión pública para que comparta su tecnología con el ejército, Tony es reacio a desvelar los secretos de la armadura de Iron Man, porque teme que esa información caiga en en manos de irresponsables. Con Pepper Potts (Gwyneth Paltrow) y James “Rhodey” Rhodes (Don Cheadle) a su lado, Tony forja alianzas nuevas y se enfrenta a nuevas y poderosas fuerzas. </info>
 
 
<page>19</page><title>MARVEL: F - THOR</title>
<microhd>b297ba207f3464876eaba4a5f23d18986448c6b5</microhd>
<fullhd>8613567e898c5b7682e756b11fe72964aea484b5</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/4TCnYa9GPactNYEVc5TnGrLqzXD.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/cDJ61O1STtbWNBwefuqVrRe3d7l.jpg</fanart>
<date>2011</date>
<genre>Saga.Fantastico. Accion. Aventuras</genre>
<extra>NA</extra>
<info>El mundo ya sabe que el multimillonario Tony Stark (Robert Downey Jr.) es Iron Man, el superhéroe enmascarado. A pesar de las presiones del gobierno, la prensa y la opinión pública para que comparta su tecnología con el ejército, Tony es reacio a desvelar los secretos de la armadura de Iron Man, porque teme que esa información caiga en en manos de irresponsables. Con Pepper Potts (Gwyneth Paltrow) y James “Rhodey” Rhodes (Don Cheadle) a su lado, Tony forja alianzas nuevas y se enfrenta a nuevas y poderosas fuerzas.</info>
 
 
<page>19</page><title>MARVEL: G - LOS VENGADORES</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>114a6f7c5eda19e8860d85aeb5aae8902aa33168</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/tyKKKSvjEgDVQ6vtm8vzL5Zx06c.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/v3A0T4fAz8xRugAkfUVkxGLd377.jpg</fanart>
<date>2012</date>
<genre>Saga.Fantastico. Accion. Ciencia ficcion</genre>
<extra>NA</extra>
<info>Cuando un enemigo inesperado surge como una gran amenaza para la seguridad mundial, Nick Fury, director de la Agencia SHIELD, decide reclutar a un equipo para salvar al mundo de un desastre casi seguro. Adaptación del cómic de Marvel "Los Vengadores", el legendario grupo de superhéroes formado por Ironman, Hulk, Thor y el Capitán América entre otros.</info>
 
 
<page>19</page><title>MARVEL: H - IRON MAN 3</title>
<microhd>6017a4f03f0ef5a9408c8a122f32793ecdaff62d</microhd>
<fullhd>f3c2be9cc161ec7a34ce2c96040cfa1d274bba6d</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/jcs4B2AiNPbVm8GFXNaTmvymR7x.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/n9X2DKItL3V0yq1q1jrk8z5UAki.jpg</fanart>
<date>2013</date>
<genre>Saga.Fantastico. Ciencia ficcion. Accion</genre>
<extra>NA</extra>
<info>El descarado y brillante empresario Tony Stark/Iron Man se enfrentará a un enemigo cuyo poder no conoce límites. Cuando Stark comprende que su enemigo ha destruido su universo personal, se embarca en una angustiosa búsqueda para encontrar a los responsables. Este viaje pondrá a prueba su entereza una y otra vez. Acorralado, Stark tendrá que sobrevivir por sus propios medios, confiando en su ingenio y su instinto para proteger a las personas que quiere. </info>
 
 
<page>19</page><title>MARVEL: I - THOR EL MUNDO OSCURO</title>
<microhd>17f300921f110a2801c34f3d9e744d4144c5b5c1</microhd>
<fullhd>f3c2be9cc161ec7a34ce2c96040cfa1d274bba6d</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/A1BiaYVCTSSQpSI4mLFQqgR6oHr.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/5QEOy0QEpad9QsXeMxuGHPXMale.jpg</fanart>
<date>2013</date>
<genre>Saga.Fantastico. Ciencia ficcion. Accion</genre>
<extra>NA</extra>
<info>Thor lucha por restablecer el orden en el cosmos, pero una antigua raza liderada por el vengativo Malekith regresa con el propósito de volver a sumir el universo en la oscuridad. Se trata de un villano con el que ni siquiera Odín y Asgard se atreven a enfrentarse; por esa razón, Thor tendrá que emprender un viaje muy peligroso, durante el cual se reunirá con Jane Foster y la obligará a sacrificarlo todo para salvar el mundo. </info>
 
 
<page>19</page><title>MARVEL: J - CAPITAN AMERICA EL SOLDADO DE INVIERNO</title>
<microhd>7f1273925b6d2d998f201ceed722c927b8199fbc</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>98634848bd8a8da24db6a82b81e30e7160e9dca0</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/6QBRnyvJHD7slOlX6aukvMwcEu.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/yHB0eNR8rvCpn0VdghEwBsXAC0N.jpg</fanart>
<date>2014</date>
<genre>Saga.Fantastico. Ciencia ficcion. Accion</genre>
<extra>NA</extra>
<info>Tras los devastadores acontecimientos acaecidos en Nueva York con Los Vengadores, Steve Rogers, alias el Capitán América, vive tranquilamente en Washington D.C. intentando adaptarse al mundo moderno. Pero cuando atacan a un colega de S.H.I.E.L.D., Steve se ve envuelto en una trama de intrigas que representa una amenaza para el mundo. Se unirá entonces a la Viuda Negra para desenmascarar a los conspiradores. Cuando por fin descubren la magnitud de la trama, se unirá a ellos el Halcón. Los tres tendrán que enfrentarse a un enemigo inesperado y extraordinario: el Soldado de Invierno.</info>
 
 
<page>19</page><title>MARVEL: K - GUARDIANES DE LA GALAXIA</title>
<microhd>6a323bb06053b636bde1b6e910ccac5c271605b7</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/lGR9yjcPctrIxNrnGBiXlEbM6nT.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/mZSAu5acXueGC4Z3S5iLSWx8AEp.jpg</fanart>
<date>2014</date>
<genre>Saga.Fantastico. Ciencia ficcion. Accion</genre>
<extra>NA</extra>
<info>El temerario aventurero Peter Quill es objeto de un implacable cazarrecompensas después de robar una misteriosa esfera codiciada por Ronan, un poderoso villano cuya ambición amenaza todo el universo. Para poder escapar del incansable Ronan, Quill se ve obligado a pactar una complicada tregua con un cuarteto de disparatados inadaptados: Rocket, un mapache armado con un rifle, Groot, un humanoide con forma de árbol, la letal y enigmática Gamora y el vengativo Drax the Destroyer. Pero cuando Quill descubre el verdadero poder de la esfera, deberá hacer todo lo posible para derrotar a sus extravagantes rivales en un intento desesperado de salvar el destino de la galaxia. </info>
 
 
<page>19</page><title>MARVEL: L- GUARDIANES DE LA GALAXIA VOL. 2</title>
<microhd>156715ea1b116c4ac4b20aa1b198563fe4dc286f</microhd>
<fullhd>61ad23d81ae726ea3ae27c7d4d52243fe4265e23</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/kdg6Y06jfq9FV7qknWNcKLYtBJn.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/xpFIaHL7Rm5kKLcEPDEMpjh4Xq6.jpg</fanart>
<date>2017</date>
<genre>Saga.Fantastico. Ciencia ficcion. Accion</genre>
<extra>NA</extra>
<info>Continúan las aventuras del equipo en su travesía por los confines del cosmos. Los Guardianes deberán luchar para mantener unida a su nueva familia mientras intentan resolver el misterio de los verdaderos orígenes de Peter Quill. Viejos rivales se convertirán en nuevos aliados, y queridos personajes de los cómics clásicos acudirán en ayuda de nuestros héroes a medida que el universo cinematográfico de Marvel continúa expandiéndose.</info>
 
 
<page>19</page><title>MARVEL: M - VENGADORES LA ERA DE ULTR0N</title>
<microhd>c6cf54ac9a2bebd0a586b61069560ccb66f36a9e</microhd>
<fullhd>533602d0be8107eb0f7c3b6e5885e9ceaf1051e5</fullhd>
<tresd>NA</tresd>
<cuatrok>0e8d321a6a57c9708a28a233d913e5dc16406458</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/SWfuMs4doCHcrz0InTh785SpQU.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/8i6ZDk97Vyh0jHohMG4vFeFuKJh.jpg</fanart>
<date>2015</date>
<genre>Saga.Fantastico. Ciencia ficcion. Accion</genre>
<extra>NA</extra>
<info>Cuando Tony Stark intenta reactivar un programa caído en desuso cuyo objetivo es mantener la paz, las cosas empiezan a torcerse y los héroes más poderosos de la Tierra, incluyendo a Iron Man, Capitán América, Thor, El Increíble Hulk, Viuda Negra y Ojo de Halcón, tendrán que afrontar la prueba definitiva cuando el destino del planeta se ponga en juego. Cuando el villano Ultrón emerge, le corresponderá a Los Vengadores detener sus terribles planes, que junto a incómodas alianzas llevarán a una inesperada acción que allanará el camino para una épica y única aventura.</info>
 
 
<page>19</page><title>MARVEL: N - ANT MAN</title>
<microhd>ce6f8243da5b98fb3a006908822c22c21473999f</microhd>
<fullhd>e5afc402f57015262b07dd87d37f5606b5ab6845</fullhd>
<tresd>1279c5137c5c7f4b9891623dbc7c2790b48fc857</tresd>
<cuatrok>307ef0f2c7d5de0f768d57296fb1a138645a83f2</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/2TOeUo6ugj0OxoVCYOiQggRN6pQ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/1K3JmSNUN8OpjYsCjc0Hy0SYxAb.jpg</fanart>
<date>2015</date>
<genre>Saga.Ciencia ficcion. Fantastico. Accion. Comedia</genre>
<extra>NA</extra>
<info>Tras abandonar la cárcel, el ladrón y estafador Scott Lang (Paul Rudd) recibe la llamada del misterioso Dr. Hank Pym (Michael Douglas) para realizar un trabajo especial. El científico suministra al joven un traje especial, que le otorgar la capacidad de reducir su tamaño al de un insecto, al tiempo que aumenta considerablemente su fuerza. Con esta nueva arma en su poder, capaz de comunicarse con los insectos, Lang deberá abrazar su héroe interior, olvidar su pasado de delincuente y tratar de salvar al mundo de una terrible amenaza.</info>
 
 
<page>19</page><title>MARVEL: Ñ - CAPITAN AMERICA CIVIL WAR</title>
<microhd>8df6a89689c4592e86dd6aa1220ef3f05018c860</microhd>
<fullhd>be6b19dec850bffe406202d1f510bed5a811e304</fullhd>
<tresd>b4e80db505ce77ba1c3b6457b966cfd2d3c68ef8</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ltvmEnNvgnPs9YPCyblC00OSzTi.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/uXzQDq63nkrRbj2sl0OFJWmOPZg.jpg</fanart>
<date>2016</date>
<genre>Accion. Thriller. Fantastico.Saga</genre>
<extra>NA</extra>
<info>Después de que otro incidente internacional involucre a Los Vengadores, causando varios daños colaterales, aumentan las presiones políticas para instaurar un sistema que exija más responsabilidades y que determine cuándo deben contratar los servicios del grupo de superhéroes. Esta nueva situación dividirá a Los Vengadores, mientras intentan proteger al mundo de un nuevo y terrible villano. Tercera entrega de la saga Capitán América.</info>
 
 
<page>19</page><title>MARVEL: O - BLACK PANTER</title>
<microhd>40574a5c68ce501ffa76235c021b1ad3d05f2111</microhd>
<fullhd>b20578f608d942de480b6f97e316f87f54010f9c</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/udd8VinUWwLIiTYn3wdOEpCk9Fq.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ykKKcW2wSlazLd2Zc5AslSBFhJH.jpg</fanart>
<date>2018</date>
<genre>Saga.Accion. Fantastico. Aventuras</genre>
<extra>NA</extra>
<info>“Black Panther" cuenta la historia de T'Challa quien, después de los acontecimientos de "Capitán América: Civil War", vuelve a casa, a la nación de Wakanda, aislada y muy avanzada tecnológicamente, para ser proclamado Rey. Pero la reaparición de un viejo enemigo pone a prueba el temple de T'Challa como Rey y Black Panther ya que se ve arrastrado a un conflicto que pone en peligro todo el destino de Wakanda y del mundo.</info>
 
 
<page>19</page><title>MARVEL: P - SPIDER MAN HOMECOMING</title>
<microhd>2cdbc89e1de428a0e5c60bc6f509dbf3a67f61fc</microhd>
<fullhd>577218134ae24514a6ba9487376ec6ed26b8cb14</fullhd>
<tresd>047dc871fe0b2f00334a93b3205402bc573dad95</tresd>
<cuatrok>9f69b0ae576f0562a9db76d135c4759c5788061f</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/bRl6C6FwzTndk0MBGZZ68nRFlw3.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/vc8bCGjdVp0UbMNLzHnHSLRbBWQ.jpg</fanart>
<date>2017</date>
<genre>Saga.Fantastico. Accion. Comedia. Ciencia ficcion</genre>
<extra>NA</extra>
<info>Peter Parker (Tom Holland) comienza a experimentar su recién descubierta identidad como el superhéroe Spider-Man. Después de la experiencia vivida con los Vengadores, Peter regresa a casa, donde vive con su tía (Marisa Tomei). Bajo la atenta mirada de su mentor Tony Stark (Robert Downey Jr.), Peter intenta mantener una vida normal como cualquier joven de su edad, pero interrumpe en su rutina diaria el nuevo villano Vulture (Michael Keaton) y, con él, lo más importante de la vida de Peter comenzará a verse amenazado.</info>
 
 
<page>19</page><title>MARVEL: Q - ANT MAN Y LA AVISPA</title>
<microhd>6c597df0a22b044d196c4f894ff27aff2d92a50a</microhd>
<fullhd>e8b9ce94c8bea2cf81b1271c3405b8f344278829</fullhd>
<tresd>010cff9bf32aaff7e1d67eb3cd4444485b03ba0b</tresd>
<cuatrok>magnet:?xt=urn:b89ba0720c461ac53f298cada3d71d26d8665c5f</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/bvYI6i9lQ3bsup9PgnMF3YYr8ZR.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/6P3c80EOm7BodndGBUAJHHsHKrp.jpg</fanart>
<date>2018</date>
<genre>Saga.Fantastico. Ciencia ficcion. Accion. Comedia</genre>
<extra>NA</extra>
<info>Después de "Capitán América: Guerra Civil", Scott Lang lidia con las consecuencias de sus elecciones como superhéroe y padre. Mientras lucha por mantener un equilibrio entre su vida hogareña y sus responsabilidades como Ant-Man, se enfrenta a Hope van Dyne y al Dr. Hank Pym con una nueva misión urgente. Scott debe volver a ponerse el traje y aprender a luchar junto con La Avispa mientras el equipo trabaja en conjunto para descubrir secretos del pasado.</info>
 
 
<page>19</page><title>MARVEL: R - DOCTOR STRANGE</title>
<microhd>e69efe7ff0deaa8bfb4042417bc19586d0841e0d</microhd>
<fullhd>063b4b7ac06b703013ba95ccf0365691aa176a3b</fullhd>
<tresd>c109018db45d703e4b4ac6b8538f4a9fa3d0c22e</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/lxZRdSYXPLxnAQZmlG8civ2sfJr.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/7qI7HPrw3hD2toqYQ62vOdkLx3I.jpg</fanart>
<date>2016</date>
<genre>Saga.Fantastico. Accion. Comedia</genre>
<extra>NA</extra>
<info>La vida del Dr. Stephen Strange cambia para siempre tras un accidente automovilístico que le deja muy malheridas sus manos. Cuando la medicina tradicional falla, se ve obligado a buscar esperanza y una cura en un lugar impensable: una comunidad aislada en Nepal llamada Kamar-Taj. Rápidamente descubre que éste no es sólo un centro de recuperación, sino también la primera línea de una batalla en contra de fuerzas oscuras y ocultas empeñadas en destruir nuestra realidad. En poco tiempo, Strange, armado con sus poderes mágicos recientemente adquiridos, se ve obligado a elegir entre volver a su antigua vida de riqueza y prestigio o dejarlo todo, para defender el mundo como el mago más poderoso del planeta. Adaptación del cómic creado por Stan Lee y Steve Ditko</info>
 
 
<page>19</page><title>MARVEL: S - THOR RAGNAROK</title>
<microhd>1c7628ac841168f977679dc1893668a7319d4d1e</microhd>
<fullhd>1403948e0a3a85c37b819f6abe33497f81afe973</fullhd>
<tresd>3a402b74363ba1f3303ab545a27fa457e03f1077</tresd>
<cuatrok>0b834e90c1f2284e18c862743cb63abd5277b730</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/u6LNWz05yOe5qli6IUpShfnU34N.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/vLmHH8jAy8Jq8uBsLucd3592WGh.jpg</fanart>
<date>2017</date>
<genre>Saga.Fantastico. Ciencia ficcion. Accion. Comedia</genre>
<extra>NA</extra>
<info>Thor está preso al otro lado del universo sin su poderoso martillo y se enfrenta a una carrera contra el tiempo. Su objetivo es volver a Asgard y parar el Ragnarok porque significaría la destrucción de su planeta natal y el fin de la civilización Asgardiana a manos de una todopoderosa y nueva amenaza, la implacable Hela. Pero, primero deberá sobrevivir a una competición letal de gladiadores que lo enfrentará a su aliado y compañero en los Vengadores, ¡el Increíble Hulk!.</info>
 
 
<page>19</page><title>MARVEL: T - VENGADORES INFINITY WAR</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>55779f8648172931d25b6b48d965f6a656ede2d5</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/q6Q81fP4qPvfQTH2Anlgy12jzO2.jpg</thumbnail>
<fanart>https://i.imgur.com/JbcGvyF.jpg</fanart>
<date>2018</date>
<genre>Saga.Fantastico. Ciencia ficcion. Accion</genre>
<extra>NA</extra>
<info>El todopoderoso Thanos ha despertado con la promesa de arrasar con todo a su paso, portando el Guantelete del Infinito, que le confiere un poder incalculable. Los únicos capaces de pararle los pies son los Vengadores y el resto de superhéroes de la galaxia, que deberán estar dispuestos a sacrificarlo todo por un bien mayor. Capitán América e Ironman deberán limar sus diferencias, Black Panther apoyará con sus tropas desde Wakanda, Thor y los Guardianes de la Galaxia e incluso Spider-Man se unirán antes de que los planes de devastación y ruina pongan fin al universo. ¿Serán capaces de frenar el avance del titán del caos?</info>
 
 
<page>19</page><title>MARVEL: U - VENGADORES ENDGAME</title>
<microhd>cfe85cace4a894e11ee9863be6644bce43b47460</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>ea61411a36575e7fc1008a18e67dab065f5fb725</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/1ELa9bm6zxIADz8u71VZtvNzx3k.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/r0jfRKIIKl34kZCeTxInYu5bVPf.jpg</fanart>
<date>2019</date>
<genre>Saga.Ciencia ficcion. Fantastico. Accion</genre>
<extra>NA</extra>
<info>Después de los eventos devastadores de 'Avengers: Infinity War', el universo está en ruinas debido a las acciones de Thanos, el Titán Loco. Con la ayuda de los aliados que quedaron, los Vengadores deberán reunirse una vez más para intentar deshacer sus acciones y restaurar el orden en el universo de una vez por todas, sin importar cuáles son las consecuencias... Cuarta y última entrega de la saga "Vengadores".</info>
 
 
<page>19</page><title>MARVEL: V - SPIDER-MAN LEJOS DE CASA</title>
<microhd>NA</microhd>
<fullhd>53c65267df31c9e08d844812ce39937946e8a5f2</fullhd>
<tresd>NA</tresd>
<cuatrok>1d33a2de151274b214a9c1f975062dcebb56dcf1</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/iKVR1ba3W1wCm9bVCcpnNvxQUWX.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/dihW2yTsvQlust7mSuAqJDtqW7k.jpg</fanart>
<date>2019</date>
<genre>Saga.Fantastico. Accion. Ciencia ficcion</genre>
<extra>NA</extra>
<info>Peter Parker decide irse junto a MJ, Ned y el resto de sus amigos a pasar unas vacaciones a Europa. Sin embargo, el plan de Parker por dejar de lado sus superpoderes durante unas semanas se ven truncados cuando Nick Fury contacta con él para solicitarle ayuda para frenar el ataque de unas criaturas elementales que están causando el caos en el continente. En ese momento, Parker vuelve a ponerse el traje de Spider-Man para cumplir con su labor.</info>
 
 
<page>19</page><title>MARVEL: W- VIUDA NEGRA</title>
<microhd>883592ad9d28e1f493aa0d20e53202d3a7ba609a</microhd>
<fullhd>47c059419b48adc38f3cdb1e994c1bf71104f0e2</fullhd>
<tresd>NA</tresd>
<cuatrok>f67abe7d968147a0975be01c1647e4682358e5da</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/oggEsESyLjVGD7jbIdBfa2hFUrx.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/keIxh0wPr2Ymj0Btjh4gW7JJ89e.jpg</fanart>
<date>2021</date>
<genre>Acción. Aventuras. Saga</genre>
<extra>NA</extra>
<info>Natasha Romanoff, alias Viuda Negra, se enfrenta a los capítulos más oscuros de su historia cuando surge una peligrosa conspiración relacionada con su pasado. Perseguida por una fuerza que no se detendrá ante nada para acabar con ella, Natasha debe lidiar con su historia como espía y con la estela de relaciones destruidas que dejó atrás mucho antes de convertirse en Vengadora.</info>
 
 
<page>19</page><title>MARVEL: X- ANT-MAN Y LA AVISPA: QUANTUMANIA</title>
<microhd>NA</microhd>
<fullhd>5f4579a305d74d8aefddf4cd28a7096e56b1e091</fullhd>
<tresd>NA</tresd>
<cuatrok>49c846eb53d29d0c30d605df2c2ad1e3cf9f1ea2</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/jTNYlTEijZ6c8Mn4gvINOeB2HWM.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/ve7ABy1y0b19gcExYz1z9cDFPGB.jpg</fanart>
<date>2023</date>
<genre>Ciencia ficción. Fantástico. Acción. Comedia. Sagas</genre>
<extra>NA</extra>
<info>Scott Lang y Hope Van Dyne, junto con Hank Pym y Janet Van Dyne, exploran el Reino Cuántico, donde interactúan con extrañas criaturas y se embarcan en una aventura que va más allá de los límites de lo que creían posible. </info>
 
 
<page>19</page><title>MARY SHELLEY</title>
<microhd>e634f1fed25003f457c1a7d0ba0ace4be4579cb1</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/suxu8fjLWcxAqm3bXtJjJxbuc2c.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/3vp5dMXAzbl5JUoGgVXEBZMJn7i.jpg</fanart>
<date>2018</date>
<genre>Drama. Romance</genre>
<extra>NA</extra>
<info>Será siempre recordada por ser la escritora que creó a Frankenstein. Criada por un filósofo de renombre (Stephen Dillane) en el Londres del siglo XVIII, Mary Wollstonecraft Godwin (Elle Fanning) es una adolescente soñadora decidida a dejar huella en el mundo. Un día conoce al brillante poeta Percy Shelley (Douglas Booth) con el que empezara una aventura amorosa marcada por la pasión y la tragedia, algo que transformará a Mary y la impulsará a escribir su obra maestra gótica.</info>
 
 
<page>19</page><title>MASCOTAS</title>
<microhd>9edf8ce1503ae972c6ac7a32bea565af13d4f505</microhd>
<fullhd>89fc5d55f221b96a8f89cdc369fb260298372676</fullhd>
<tresd>9a0893404615beb12cd5b8c24f2299175b55ffcc</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/kfMiEhfmsAxTUoldgR4mnKndDVv.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/i83cF7Y6TRsafDmUpbgkvBuXj6k.jpg</fanart>
<date>2016</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>En un edificio de apartamentos de Manhattan, la vida de Max como mascota favorita corre peligro cuando su dueña trae a casa a un otro perro llamado Duke, con quien Max pronto tiene sus diferencias. Pero ambas mascotas tienen que dejar atrás su rivalidad cuando se enteran de que un adorable conejito blanco llamado Snowball está reclutando a un ejército de animales domésticos que han sido abandonados, decididos a vengarse de todos los animales domésticos felices y de sus dueños.</info>
 
 
<page>19</page><title>MASCOTAS 2</title>
<microhd>6a1342a2964774ed7ba73d2cecbc0fb0ebcdafef</microhd>
<fullhd>2c0012c6a5e3be1084197e6b8081f120b404ae2e</fullhd>
<tresd>0c4fe2d693103cee4852f6199fe603d5c9655d65</tresd>
<cuatrok>cb3d0062b94c1abcf4bddaec7ebbfd3ea3492491</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/dqGsG2uNV9JGgrfODbTafPUukci.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/q5g35ZsAGm6HOsDc6p8XAPOyB3J.jpg</fanart>
<date>2019</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Max se enfrenta a nuevos e importantes cambios en su vida: su dueña Katie no sólo se ha casado, sino que también ha sido madre por primera vez. En un viaje familiar al campo conoce a un perro granjero llamado Rooster, con el que aprende a dominar sus miedos. Mientras tanto, Gidget trata de recuperar el juguete favorito de Max de un apartamento repleto de gatos. Snowball, por otro lado, se embarca en una peligrosa misión para liberar a un tigre blanco, Hu, de sus captures en un circo de animales. Secuela de 'Mascotas'</info>
 
 
<page>19</page><title>MASCOTAS UNIDAS</title>
<microhd>43dd72944899077cbb09146d23cc6fd2669574b6</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/aLu16Bk5uJD7XyeWLDgrlugWQV8.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/ePkagE11fpUbTasJ2udxTJHuaWU.jpg</fanart>
<date>2019</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Un astuto perro y una gatita consentida lideran a un equipo de héroes inesperados después de que su ciudad se vea asediada por el malvado alcalde y su ejército de robots.</info>
 
 
<page>19</page><title>MATAR A SANTA</title>
<microhd>0d1d520732b37eb3c131c70a2500239a9982afe0</microhd>
<fullhd>a9f34d58774fbf10ea5feed3705a43055a528696</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/w85cLr1btxfTExmch6L1vKMH0ti.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/bWv6AlBqwT32fEXfeHgMGA7bEXw.jpg</fanart>
<date>2020</date>
<genre>Comedia. Thriller</genre>
<extra>NA</extra>
<info>La historia se centra en un Papá Noel poco ortodoxo llamado Gibson que lucha por evitar que su negocio se hunda, al mismo tiempo que evita ser asesinado por un matón contratado por un niño de 12 años tras recibir carbón en Navidad.</info>
 
 
<page>19</page><title>MATAR O MORIR  (PEPPERMINT)</title>
<microhd>2eef070b476e5f6467d39aa5527e9947d383273c</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/53cCw1weALlERwAieFQs5yvarFt.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/14HPHJnNUVAdbYKnG46WnBh4OD6.jpg</fanart>
<date>2018</date>
<genre>Thriller. Accion</genre>
<extra>NA</extra>
<info>Riley North (Jennifer Garner) despierta de un coma y se entera de que su marido y su hija han sido brutalmente asesinados. Cuando años después el sistema que debe juzgar a los asesinos demuestra su fracaso, Riley decide tomarse la justicia por su mano.</info>
 
 
<page>19</page><title>MATRIX</title>
<microhd>341b3345df1673cc18c082e429028cf9a3dfc40a</microhd>
<fullhd>f321dc4e7696c5fa4e431f9f86de7cc0c8fc168c</fullhd>
<tresd>NA</tresd>
<cuatrok>47c28ebebe2e69f6540ae722473ea6e54cb938ae</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ejmTPNAkgZaVJ4AI9zb9SehAYU0.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ByDf0zjLSumz1MP1cDEo2JWVtU.jpg</fanart>
<date>1999</date>
<genre>Ciencia ficcion. Fantastico. Accion. Thriller.Saga</genre>
<extra>Culto</extra>
<info>Thomas Anderson es un brillante programador de una respetable compañía de software. Pero fuera del trabajo es Neo, un hacker que un día recibe una misteriosa visita...</info>
 
 
<page>19</page><title>MATRIX RELOADED</title>
<microhd>250548445a3a14452d6e0e2606da21363e93edce</microhd>
<fullhd>d28d71f17517d49b928f91abdcc2bd76591e2e4f</fullhd>
<tresd>NA</tresd>
<cuatrok>6c04e692191d2bca8a9ac57788b97a277e8a775f</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ukUGBeKTZ9XUtTP9W3ebwM5av4r.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ihOJcrorpDQtHTzIOrobbOhpJsj.jpg</fanart>
<date>2003</date>
<genre>Ciencia ficcion. Fantastico. Accion. Thriller .Saga</genre>
<extra>NA</extra>
<info>Neo, Morpheus, Trinity y el resto de la tripulación continúan en la lucha contra las máquinas que han esclavizado a la raza humana. Ahora más humanos han sido despertados e intentan vivir en el mundo real. A medida que aumentan en número, la batalla se acerca a Sion, la última ciudad real en el mundo y centro de la resistencia humana. Y tiene poco tiempo, muy poco tiempo...</info>
 
 
<page>19</page><title>MATRIX RESURRECTIONS</title>
<microhd>6FB6752E63D72578AC8E782EFF2733A2A7BA422A</microhd>
<fullhd>d3b42cd3b297373e59afc5cc66c70194d034df61</fullhd>
<tresd>NA</tresd>
<cuatrok>4bf029e8a96433be8c1ffd12b8b15ce5a67d727e</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/aWsGPmTGRpzGXZ34EZZNMwFw6iH.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/rWZC1dOapXyhNfJ7xUU1P6giTwc.jpg</fanart>
<date>2021</date>
<genre>Ciencia ficción. Acción. Drama. Saga</genre>
<extra>NA</extra>
<info>Neo vive una vida normal y corriente en San Francisco mientras su terapeuta le prescribe pastillas azules. Hasta que Morfeo le ofrece la pastilla roja y vuelve a abrir su mente al mundo de Matrix.</info>
 
 
<page>19</page><title>MATRIX REVOLUTION</title>
<microhd>2e84aa851eeda3f9dd892fc3bc881a238eddd535</microhd>
<fullhd>32e90e6bdbf0c80c349b4efb80bd5931aefdf61f</fullhd>
<tresd>NA</tresd>
<cuatrok>b9346e5e226f00bf4622047bada3d3c29902f5c3</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ynMQ4nVwkoP2gLxXXDgcdltihTD.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/kJYPag1YrA4PsXIftFdq3QEaanV.jpg</fanart>
<date>2003</date>
<genre>Ciencia ficcion. Fantastico. Accion. Thriller.Saga</genre>
<extra>NA</extra>
<info>Todo lo que tiene un comienzo tiene un final. La guerra estalla en la superficie de la tierra mientras las máquinas invaden Zion. Allí donde Reloaded significaba vida, Revolutions apunta a la muerte...</info>
 
 
<page>19</page><title>MECHANIC: RESURRECTION</title>
<microhd>a20ae7ff71a2e9b9bd13c5f7706380161bf738e9</microhd>
<fullhd>c482e73ec5780724b1bf69781f1dbcdea261ed45</fullhd>
<tresd>NA</tresd>
<cuatrok>facb6de4f690771c6ca2c73d57b3b7df3275cff5</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/bG3Q6EFcngpPn7uPuQTzTuq9DgV.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/3f5rBEc6ioRBO2CATDt4sKjEJnY.jpg</fanart>
<date>2016</date>
<genre>Acción. Thriller </genre>
<extra>Mis</extra>
<info>Cuando Arthur Bishop (Jason Statham) creía que ya había dejado atrás su pasado criminal, se encuentra de nuevo con él al ser secuestrada la mujer de su vida por uno de sus mayores enemigos. Se ve así obligado a viajar por el mundo para ejecutar tres asesinatos imposibles que deben parecer accidentes. </info>
 
 
<page>19</page><title>MEDITERRANEO</title>
<microhd>6ed90f4fdb14a84c0f3cd8369148f3d6b614bf94</microhd>
<fullhd>020c48beab3238d13a82ffca9313025774be68f2</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/nyEljHUdujmIsi8w87Dc4QSBiD.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/o8oyEaFiYPQNQiRin4fxbNUZv0y.jpg</fanart>
<date>2021</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Otoño de 2015. Dos socorristas, Óscar (Eduard Fernández) y Gerard (Dani Rovira), viajan hasta la isla de Lesbos, en Grecia, impactados por la fotografía de un niño ahogado en las aguas del Mediterráneo. Al llegar descubren una realidad sobrecogedora: miles de personas arriesgan sus vidas cada día cruzando el mar en precarias embarcaciones y huyendo de conflictos armados. Sin embargo, nadie ejerce labores de rescate. Junto a Esther (Anna Castillo), Nico (Sergi López) y otros miembros del equipo, lucharán por cumplir un cometido, dando apoyo a personas que lo necesitan. Para todos ellos, este viaje inicial supondrá una odisea que marcará sus vidas.</info>
 
 
<page>20</page><title>MEGALODON</title>
<microhd>5ea2be97bb7ed0f968a389a2b3d3c24b39b7b5df</microhd>
<fullhd>c22691f95d82f34875978a19713080430fead08a</fullhd>
<tresd>428667295b32036f411bdad27bdb37d7ffc74370</tresd>
<cuatrok>d14ead6c536742635531adeeb1c84d23d8aa5e41</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/6ruqBOf0agpyK45N0IEvr7miIF8.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/fXFQnOSbybaU9h2kJ3jtRUU4OpA.jpg</fanart>
<date>2018</date>
<genre>Accion. Terror. Ciencia ficcion</genre>
<extra>NA</extra>
<info>Un submarino de aguas profundas, que forma parte de un programa internacional, es atacado por una enorme criatura y queda averiado en el fondo de la fosa oceánica más profunda del Pacífico, con su tripulación atrapada en el interior. El tiempo se acaba y un oceanógrafo chino, el Dr. Chang, recluta a Jonas Taylor (Jason Statham), un especialista en rescate en aguas profundas, en contra de los deseos de su hija Suyin (Li Bingbing) que cree que puede rescatar a la tripulación por sus propios medios. Pero ambos deberán unir sus fuerzas para salvar a la tripulación y también al océano de una amenaza imparable: un tiburón prehistórico de 23 metros conocido con el nombre de Megalodón. Aunque se creía que estaba extinguido, el Meg está vivo y coleando... y está de caza. Cinco años antes, Jonas se había encontrado con esta misma criatura aterradora, pero nadie le creyó en ese momento. Ahora Jonas debe enfrentarse a sus miedos para volver a las profundidades marinas... donde volverá a verse cara a cara con el depredador más temible de todos los tiempos.</info>
 
 
<page>20</page><title>MEGALODON 2 : LA FOSA</title>
<microhd>NA</microhd>
<fullhd>14b46865b9e3662050003a571aedc1839871a132</fullhd>
<tresd>NA</tresd>
<cuatrok>aa928081f304c01d0f89266b7d97a93e1e3e0c65</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/mBgynPDplmo5JTY9VfGqY35OjDu.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/Aukfa8dk6B5OxuelbaPBOJYXaBI.jpg</fanart>
<date>2023</date>
<genre>Ciencia ficción. Terror. Acción</genre>
<extra>Estreno</extra>
<info>Un equipo de investigación inicia una misión que va a explorar las profundidades más abismales del mar. Pero su viaje se convierte en caos cuando un malévolo operativo minero amenaza su misión y los obliga a librar una batalla de alto riesgo por la supervivencia. Enfrentados a colosales Megalodones y a implacables saqueadores medioambientales, nuestros héroes deben correr más rápido, ser más astutos y nadar a mayor velocidad que sus despiadados depredadores en una trepidante carrera contra el tiempo. Secuela de 'The Meg' (2018).</info>
 
 
<page>20</page><title>MELODIA DE SEDUCCION</title>
<microhd>NA</microhd>
<fullhd>2f8a688c450ace65dc0c246cfc8bdb246c4d8529</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/p9FecqQhylTZEgzl1nRkoD7LV5O.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/f2BSeNo3erIc9m8m4cjHBWRB7Te.jpg</fanart>
<date>1989</date>
<genre>Intriga. Thriller. Drama</genre>
<extra>Mis</extra>
<info>Un hombre aparece muerto en la cama de un tiro en la nuca. Frank (Al Pacino), el policía encargado del caso, sospecha que lo ha asesinado una mujer. Un compañero (John Goodman) que se ocupa de un caso idéntico coincide con su teoría. Las dos víctimas habían puesto sendos anuncios en verso en una revista de contactos. Poco después, otro hombre, que también publicó un anuncio similar en la misma revista, muere del mismo modo que los otros dos. Obviamente sólo hay un camino para llegar hasta el criminal: anunciarse en verso en la misma revista.</info>
 
 
<page>20</page><title>MEMENTO</title>
<microhd>d621da82a987bf65d4421033a0897eed2fe2aac8</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/neHrQLVfT3KPqvv30pNPyRb6chQ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/nIAJjSD9PX9yR01coWMhQ3SXzIg.jpg</fanart>
<date>2000</date>
<genre>Thriller. Intriga</genre>
<extra>Culto</extra>
<info>La memoria de Leonard, un investigador de una agencia de seguros, está irreversiblemente dañada debido a un golpe sufrido en la cabeza, cuando intentaba evitar el asesinato de su mujer: éste es el último hecho que recuerda del pasado. La memoria reciente la ha perdido: los hechos cotidianos desaparecen de su mente en unos minutos. Así pues, para investigar y vengar el asesinato de su esposa tiene que recurrir a la ayuda de una cámara instantánea y a las notas tatuadas en su cuerpo. </info>
 
 
<page>20</page><title>MEN IN BLACK (HOMBRES DE NEGRO)</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>d9f015a24b51251a239e673206a78d144acaa1b9</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/oEjjM8XSPHkhTgPz7go3mEfzysh.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/1GJvBE7UWU1WOVi0XREl4JQc7f8.jpg</fanart>
<date>1997</date>
<genre>Ciencia ficción. Fantástico. Comedia. Acción. Saga</genre>
<extra>Mis</extra>
<info>Durante muchos años los extraterrestres han vivido en la Tierra, mezclados con los seres humanos, sin que nadie lo supiese. Los Hombres de Negro son agentes especiales que forman parte de una unidad altamente secreta del gobierno; su misión consiste en controlar a los alienígenas. Dos de estos agentes (uno veterano y otro recién incorporado), cuya misión consiste en vigilar a los alienígenas que viven en Nueva York, descubren a un terrorista galáctico que pretende acabar con la Humanidad.</info>
 
 
<page>20</page><title>MEN IN BLACK 3 (HOMBRES DE NEGRO III)</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>4238d87c3b83154e75b3b4e5227e5d4ca32e61c3</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/tgS52JXKh04tODt43ieT2pZsLE6.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/44mAbdjGrJJQ4hzkKx2Z2EsninZ.jpg</fanart>
<date>2012</date>
<genre>Ciencia ficción. Comedia. Acción . Saga</genre>
<extra>Mis</extra>
<info>Cuando el MIB recibe la información de que el Agente K podría morir a manos de un alienígena, lo que cambiaría la historia para siempre, el Agente J es enviado a los años 60 para evitarlo. Tercera entrega de la popular saga Men in Black.</info>
 
 
<page>20</page><title>MEN IN BLACK INTERNATIONAL</title>
<microhd>c5cd54a4314a9087c85df8c71e21b5f88d0b9da3</microhd>
<fullhd>493f68ad1d7195e2310a2e15bd73b50ceb418cff</fullhd>
<tresd>NA</tresd>
<cuatrok>fc986c547a9d50cf01f870672d3e0a739bb2f2a7</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ozbfxtelyJNtbgVJAoQKLmDl7ZW.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/uK9uFbAwQ1s2JHKkJ5l0obPTcXI.jpg</fanart>
<date>2019</date>
<genre>Ciencia ficcion. Accion. Comedia</genre>
<extra>NA</extra>
<info>Los Hombres de Negro siempre han protegido la Tierra de la escoria del universo. En esta nueva aventura, se enfrentarán a su mayor amenaza hasta la fecha: un topo en la organización MIB. Adaptación cinematográfica de los cómics de Lowell Cunningham</info>
 
 
<page>20</page><title>MENTES PODEROSAS</title>
<microhd>41b59afe96e4e8838ecd8b6e84333f9c37957507</microhd>
<fullhd>d6b12e7a651d0181998a43b7207bd991cead3c30</fullhd>
<tresd>NA</tresd>
<cuatrok>ca076119c871ac9781927e5fccc0d2b3e49333cf</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/yI2vJ8enBBivKhzh0KvkglE9Loh.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/5BxrMNGl3YDiWgHCVJu8iLQoJDM.jpg</fanart>
<date>2018</date>
<genre>Ciencia ficcion. Romance. Fantastico</genre>
<extra>NA</extra>
<info>Después de que una enfermedad matase al 98% de los niños y jóvenes de los Estados Unidos, el 2% que consiguió sobrevivir ha desarrollado superpoderes, pero se los ha encerrado en campos de internamiento al ser declarados una amenaza. Una de esas niñas de 16 años, Ruby, consigue escapar de su campamento y se une a un grupo de adolescentes que huyen de las fuerzas del gobierno.</info>
 
 
<page>20</page><title>MERCENARIOS DE ELITE</title>
<microhd>be86afedf2923c692b5f27a4066438562203a2aa</microhd>
<fullhd>cc1ae0506e4181d416fb57571a29cff9b1e4e662</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/x7Dc6FTBYOfwMDEHSGW6EOitidk.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/86L8wqGMDbwURPni2t7FQ0nDjsH.jpg</fanart>
<date>2020</date>
<genre>Acción. Thriller</genre>
<extra>NA</extra>
<info>El trabajo del multimillonario Donovan Chalmers (Bruce Willis) es tan valioso que contrata a mercenarios para protegerlo. Pese a ello, un grupo terrorista logra secuestrar a su hija para conseguir acercarse a él. </info>
 
 
<page>20</page><title>METROPOLIS</title>
<microhd>NA</microhd>
<fullhd>MYCRJ4YPVGUS57REMVYYTAMWEY5JIB52</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/pxbGZewX327IbTvrCVRJgcLJTSQ.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/r8pUcom5Mw8igtBpj3AHSAUvH0R.jpg</fanart>
<date>1927</date>
<genre>Ciencia ficción. Drama</genre>
<extra>Culto</extra>
<info>Futuro, año 2000. En la megalópolis de Metrópolis la sociedad se divide en dos clases, los ricos que tienen el poder y los medios de producción, rodeados de lujos, espacios amplios y jardines, y los obreros, condenados a vivir en condiciones dramáticas recluidos en un gueto subterráneo, donde se encuentra el corazón industrial de la ciudad. Un día Freder (Alfred Abel), el hijo del todopoderoso Joh Fredersen (Gustav Frohlich), el hombre que controla la ciudad, descubre los duros aspectos laborales de los obreros tras enamorarse de María (Brigitte Helm), una muchacha de origen humilde, venerada por las clases bajas y que predica los buenos sentimientos y al amor. El hijo entonces advierte a su padre que los trabajadores podrían rebelarse.</info>
 
 
<page>20</page><title>MI QUERIDA COFRADIA</title>
<microhd>a160e673c5990460e698727abb39ad5f97ea93cd</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/mkIVyoqmv14QEwym3QMuWaGFj9I.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/vCY4AaBBumWAEwCeiJy8tt0YT08.jpg</fanart>
<date>2018</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Carmen lleva más de 30 años desviviéndose por la hermandad de su pueblo y su sueño es convertirse en hermana mayor de la misma, siendo esto algo utópico en un círculo social aún en la actualidad representado por hombres principalmente. Después de producirse una votación, Ignacio es elegido hermano mayor y, tras un desafortunado accidente, Carmen lo deja inconsciente y tiene que esconderlo en su casa. Todo se complica cuando su hija discute con su marido, actual alcalde del pueblo, y decide volver a casa de su madre.</info>
 
 
<page>20</page><title>MIA Y EL LEON BLANCO</title>
<microhd>be0020ee96f90dff1f30ec9662834e25aa4e4399</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/qlP3Ci2d2A3HuZFzl3OHlDGQrQt.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/5LUIyd7lgkKbl5PgP8so9Eil3tC.jpg</fanart>
<date>2018</date>
<genre>Aventuras</genre>
<extra>NA</extra>
<info>Una joven que se ha trasladado con sus padres desde Londres a África desarrolla un vínculo sorprendente y especial con un león salvaje. Su increíble amistad la impulsa a viajar por la sabana para salvar a su mejor amigo.</info>
 
 
<page>20</page><title>MIAMOR PERDIDO</title>
<microhd>4b73c10a0576adcf4fc77db3df21ab81d1b7e5a8</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/kULYM5oKad5qn3lsxeBIIHtkcEw.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/y0E7cQlEZyk6KC1PsqFPiAfNgSn.jpg</fanart>
<date>2018</date>
<genre>Romance. Comedia</genre>
<extra>NA</extra>
<info>Mario y Olivia se enamoran locamente. Una tarde, planteándose que toda relación se acaba, terminan rompiendo. Durante la bronca, Miamor, el gato callejero adoptado por ambos y que sólo atiende si le hablan en valenciano, se escapa y desaparece, como el amor que les unía. Durante un tiempo, Olivia dará por muerto al amor y al minino, mientras Mario lo mantiene con vida a escondidas. Como el gato de Schrödinger, el amor entre los dos permanece vivo y muerto al mismo tiempo, hasta que un buen día, en el que parece haber resucitado definitivamente, Olivia se siente víctima de un engaño y decide declararle a Mario la guerra; una en la que, como en el amor que aún respira, todo puede valer.</info>
 
 
<page>20</page><title>MIAU</title>
<microhd>1ab2a9e24c62e9471d1ae2d8fab75b2adbf8a291</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/wZcMQ2M5qiEVVQdyPH4OGs4TyWq.jpg</thumbnail>
<fanart>https://imgur.com/cqzybw0.jpg</fanart>
<date>2018</date>
<genre>Comedia</genre>
<extra>NA</extra>
<info>Cuatro jubilados trazan un plan para sentirse vivos: van a robarle al aburrimiento y a la vida. Telmo ocupa su tiempo escribiendo un ensayo sobre el origen de su chiste favorito cuando se reencuentra casualmente con dos amigos de juventud. Un tercero, Monreal, sale de la cárcel con la intención de revolucionar al grupo, proponiéndoles un rocambolesco plan y poniendo patas arriba su monótona rutina. Sin nada que perder, el variopinto grupo se verá inmerso en el robo a un museo... Adaptación de la novela "Hilo musical para una piscifactoría", de Juan Luis Saldaña.</info>
 
 
<page>20</page><title>MIDSOMMAR</title>
<microhd>f03657f5ba4fb070d16b0cfcc0746419d28eb87f</microhd>
<fullhd>782c99122967b96a6a869f1ec2f73cd1953281b2</fullhd>
<tresd>NA</tresd>
<cuatrok>5a40162e4b5c118717cb61770b0fdd5a850f4f33</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/km0ta4N96yjoZPVy5A3gbcE3Ne8.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/g6GtOfXtzDpY73ef7wludoorTti.jpg</fanart>
<date>2019</date>
<genre>Terror. Intriga. Drama</genre>
<extra>NA</extra>
<info>Una pareja estadounidense que no está pasando por su mejor momento acude con unos amigos al Midsommar, un festival de verano que se celebra cada 90 años en una aldea remota de Suecia. Lo que comienza como unas vacaciones de ensueño en un lugar en el que el sol no se pone nunca, poco a poco se convierte en una oscura pesadilla cuando los misteriosos aldeanos les invitan a participar en sus perturbadoras actividades festivas.</info>
 
 
<page>20</page><title>MIDWAY: BATALLA EN EL PACIFICO</title>
<microhd>297ddef71f2d1962e078ef80485ab8619dac8d17</microhd>
<fullhd>bef926de118de12baada9b5ef2b731126518cf27</fullhd>
<tresd>NA</tresd>
<cuatrok>371da2ce37d46d44361fc0a75cb24d275a2225ec</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/zeWzsPPUxJPr08ZJNCiArSe11fQ.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/4rEiz5E3LjxtFa7GYUXJDdiuOBE.jpg</fanart>
<date>2019</date>
<genre>Belico. Drama. Accion</genre>
<extra>NA</extra>
<info>1942, Segunda Guerra Mundial. La historia de la batalla de Midway contada por los líderes y soldados que la lucharon.</info>
 
 
<page>20</page><title>MIENTRAS DURE LA GUERRA</title>
<microhd>6d8326b0fe550a61110658a2d43f5e1765f4f392</microhd>
<fullhd>53eb149f1a3116a37486f398ed2afd28bb7506ce</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/3FjfGLlanl6fhdJCapZ75pYURSK.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/tsihdadFxcOiI2NdTxmMmwnfrG3.jpg</fanart>
<date>2019</date>
<genre>Drama </genre>
<extra>NA</extra>
<info>España. Verano de 1936. El célebre escritor Miguel de Unamuno decide apoyar públicamente la sublevación militar que promete traer orden a la convulsa situación del país. Inmediatamente es destituido por el gobierno republicano como rector de la Universidad de Salamanca. Mientras, el general Franco consigue sumar sus tropas al frente sublevado e inicia una exitosa campaña con la secreta esperanza de hacerse con el mando único de la guerra. La deriva sangrienta del conflicto y el encarcelamiento de algunos de sus compañeros provoca que Unamuno empiece a cuestionar su postura inicial y a sopesar sus principios. Cuando Franco traslada su cuartel a Salamanca y es nombrado Jefe del Estado en la zona nacional, Unamuno acude a su Palacio, decidido a hacerle una petición. </info>
 
 
<page>20</page><title>MILLA 22</title>
<microhd>2350157a4f91779684950023e43aa53aacfbb964</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/butXolfzlkbVL07HuIZeBmjuTyn.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/2nOMUnQ4OtsKVTmpp3og7MYt4oG.jpg</fanart>
<date>2018</date>
<genre>Accion. Thriller</genre>
<extra>NA</extra>
<info>James Silva (Mark Wahlberg) es un experimentado agente de la CIA, que es enviado a un país sospechoso de actividad nuclear ilegal. El funcionario Li (Iko Uwais), llega a la embajada estadounidense buscando intercambiar información sobre material radioactivo robado a cambio de su paso seguro a los Estados Unidos. A Silva le asignan la peligrosa misión de transportarlo desde el centro de una ciudad hasta una pista de aterrizaje a 22 millas de distancia (unos 35 kilómetros).</info>
 
 
<page>20</page><title>MILLENNIUM LO QUE NO TE MATA TE HACE MAS FUERTE</title>
<microhd>97b9cb48744375120a545d1f03a657b0a25c8a77</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/c156owEtfOYVIOl0LSrqjRewZdg.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/r0ljJIPBGsGdpeDdlHpzCiNnxiB.jpg</fanart>
<date>2018</date>
<genre>Thriller. Drama. Intriga</genre>
<extra>NA</extra>
<info>La joven hacker Lisbeth Salander y el periodista Mikael Blomkvist se encuentran atrapados en una red de espías, ciberdelincuentes y funcionarios corruptos del gobierno. Adaptación de la novela "Lo que no te mata te hace más fuerte" de David Lagercrantz, una continuación literaria de la saga 'Millennium' creada originalmente por Stieg Larsson.</info>
 
 
<page>20</page><title>MILLION DOLLAR BABY</title>
<microhd>NA</microhd>
<fullhd>a1b019d6a199cb8ed523fa3a8987ba3421756d18</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/corahhcHlrDUx6WJPLuRZvMeS0H.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/3rSEhIjfjh7xMq1BnNtAF2nbXCi.jpg</fanart>
<date>2004</date>
<genre>Drama</genre>
<extra>Mis</extra>
<info>Después de haber entrenado y representado a los mejores púgiles, Frankie Dunn (Eastwood) regenta un gimnasio con la ayuda de Scrap (Freeman), un ex-boxeador que es además su único amigo. Frankie es un hombre solitario y adusto que se refugia desde hace años en la religión buscando una redención que no llega. Un día, entra en su gimnasio Maggie Fitzgerald (Swank), una voluntariosa chica que quiere boxear y que está dispuesta a luchar denodadamente para conseguirlo. Frankie la rechaza alegando que él no entrena chicas y que, además, es demasiado mayor. Pero Maggie no se rinde y se machaca cada día en el gimnasio, con el único apoyo de Scrap. </info>
 
 
<page>20</page><title>MINIONS: EL ORIGEN DE GRU</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>5c7dd5182ff4ba7b56154bdd5dfd9d8a3c82d68c</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/zCdvIdb6SvoaOzTqCWFQdCEmOXq.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/uMSxXLfH7v30gRNBqsQaSP3yqX5.jpg</fanart>
<date>2022</date>
<genre>Infantil</genre>
<extra>NA</extra>
<info>Son los años 70 y Gru crece en un barrio residencial, en pleno boom de los peinados cardados y los pantalones de campana. Como fan incondicional de un famoso supergrupo de villanos, 'Los salvajes 6', Gru idea un plan para demostrarles que es lo suficientemente malvado como para trabajar con ellos. Por suerte, cuenta con la ayuda de sus fieles seguidores, los Minions, siempre dispuestos a sembrar el caos por donde pasan. Juntos, Kevin, Stuart, Bob, y Otto -un nuevo Minion con aparato en los dientes y desesperado por sentirse aceptado- desplegarán su potencial para construir junto a Gru su primera guarida, experimentar con sus primeras armas y llevar a cabo sus primeras misiones.</info>
 
 
<page>20</page><title>MINIONS: GRU MI VILLANO FAVORITO</title>
<microhd>e51ceef7d22645c1ba1a91875803a31f8dd4040d</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/waCZ8OtOrBXQ6DeW0QCcSkUcxc3.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/euCnMxNRlHNxA4f9BMnWbmxPOse.jpg</fanart>
<date>2010</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Gru (voz de Steve Carell) es un hombre deplorable que planea el acto criminal más increíble de la Historia: robar la Luna. Incitado por una madre malvada (voz de Julie Andrews), sólo encontrará un obstáculo en su camino: tres niñas huérfanas a las que tendá que cuidar temporalmente.</info>
 
 
<page>20</page><title>MINIONS: GRU MI VILLANO FAVORITO 2</title>
<microhd>7b6713fac16c4cf19e0c6b2e9f4ca3a2521d0a29</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/w600_and_h900_bestv2/9lUBTFYaDmW1KqEQ0vZaWHXw1Ib.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/fOipappvgVtUbbHOtmCkHzcwJjC.jpg</fanart>
<date>NA</date>
<genre>NA</genre>
<extra>NA</extra>
<info>NA</info>
 
 
<page>20</page><title>MINIONS: GRU MI VILLANO FAVORITO 3</title>
<microhd>724ca07068ea9d7c777dd1ebe5e724597422deb7</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/bPI2Wcmyxmc17qqiIi1Ww7HU6nS.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/ftRkFtAGuHngHnLiOxktq0aCVMF.jpg</fanart>
<date>2017</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Balthazar Pratt, un antiguo niño estrella obsesionado con el personaje que encarnó en los años ochenta, demuestra ser el peor enemigo al que Gru ha debido enfrentarse hasta la fecha.</info>
 
 
<page>20</page><title>MIRA COMO CORREN</title>
<microhd>NA</microhd>
<fullhd>e5508ea248dd8aa41591691f9a68fbd8f4eb527e</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/vzjCNklQDb84d3AdH9oLlTssHsM.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/cn9GQzCaeYylEV2hymVR8bskRMJ.jpg</fanart>
<date>2022</date>
<genre>Thriller. Intriga. Comedia</genre>
<extra>NA</extra>
<info>Un desesperado productor de cine de Hollywood se propone convertir en película una popular obra de teatro. Cuando miembros de la producción son asesinados, el cansado inspector Stoppard y la agente novata Stalker se encontrarán en medio de una intrigante novela policíaca.</info>
 
 
<page>20</page><title>MISION IMPOSIBLE</title>
<microhd>NA</microhd>
<fullhd>53a536004e7d18ab590af1e53c3ee1ba2faee245</fullhd>
<tresd>NA</tresd>
<cuatrok>3c0aec1b023c939fa5017777ec611a33d8ceb9ff</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/xCpmxw3UUjv4PGzbIPOHeoKAV0l.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/z0Hz9LIcAm8yhodffZhXuc0wK1M.jpg</fanart>
<date>1996</date>
<genre>Accion. Intriga. Thriller.Saga</genre>
<extra>NA</extra>
<info>Ethan Hunt es un superespía capaz de llevar a cabo la misión más peligrosa con la máxima eficacia y elegancia. Forma parte de un competente equipo dirigido por el agente Jim Phelps, que ha vuelto a reunir a sus hombres para participar en una dificilísima misión: evitar la venta de un disco robado que contiene información secreta de vital importancia.</info>
 
 
<page>20</page><title>MISION IMPOSIBLE 2</title>
<microhd>NA</microhd>
<fullhd>53a536004e7d18ab590af1e53c3ee1ba2faee245</fullhd>
<tresd>NA</tresd>
<cuatrok>bf5204118f9d6f0ba9e5060f6aad3b3e36461465</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/mskE3W88cjMRrnKQye8pjmJu3O1.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/qGjrmG0auvmt5KENxrsujnXDmgO.jpg</fanart>
<date>2000</date>
<genre>Saga.Accion</genre>
<extra>NA</extra>
<info>La nueva misión del agente especial Ethan Hunt consiste en impedir que un despiadado ex-agente que se ha convertido en terrorista internacional se apodere de un virus mortal y lo introduzca en Australia causando millones de víctimas. Para ello, cuenta de nuevo con la inestimable ayuda del genio informático Luther Stickell, con el experto conductor Billy Baird y con la sensual y exótica ladrona internacional Nhye, que mantuvo una relación sentimental con el terrorista, pero que ahora se siente atraída por Hunt.</info>
 
 
<page>20</page><title>MISION IMPOSIBLE 3</title>
<microhd>NA</microhd>
<fullhd>53a536004e7d18ab590af1e53c3ee1ba2faee245</fullhd>
<tresd>NA</tresd>
<cuatrok>502e21f93ded2fe613f67cc0704742773fcd1294</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ea30Xn4rUKON2qsZj1D5lRCtyPN.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/oZaovOU7ODohMkNsl8Tf2q0FxUR.jpg</fanart>
<date>2006</date>
<genre>Saga. Accion</genre>
<extra>NA</extra>
<info>Tras haber llevado a cabo diversas misiones, el agente especial Ethan Hunt (Tom Cruise) se ha retirado del servicio activo y se ha prometido con su amada Julia (Michelle Monaghan). Pero, cuando es secuestrado uno de los agentes entrenados por él, volverá de nuevo a la acción. También tendrá que enfrentarse a Owen Davian (Philip Seymour Hoffman), un individuo sin escrúpulos que trafica con armas y con información.</info>
 
 
<page>20</page><title>MISION IMPOSIBLE FALLOUT</title>
<microhd>NA</microhd>
<fullhd>6de947b28545c433f4e3dd3a96286969e94a3ded</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/cUlWqDLEYMN3OWlOZ0U18WcIw5F.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/aw4FOsWr2FY373nKSxbpNi3fz4F.jpg</fanart>
<date>2018</date>
<genre>Thriller. Accion. Sagas</genre>
<extra>NA</extra>
<info>Sexta entrega de la saga. En esta ocasión presenta a Ethan Hunt (Tom Cruise) y su equipo IMF (Alec Baldwin, Simon Pegg, Ving Rhames), con algunos aliados conocidos (Rebecca Ferguson, Michelle Monaghan), en una lucha contrarreloj después de que una misión salga mal.</info>
 
 
<page>20</page><title>MISION IMPOSIBLE: NACION SECRETA</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>840f746ff4a0d45fd867514d4919f8fd9c3e1967</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ww0IX7Xla6tHgrwYSupfXcksV3n.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/lBw9DFzSZIBVZNEWotEZ7P0A9po.jpg</fanart>
<date>2015</date>
<genre>Accion. Thriller. Saga</genre>
<extra>NA</extra>
<info>Con la FMI disuelta y Ethan Hunt (Tom Cruise) abandonado a su suerte, el equipo tiene que enfrentarse contra el Sindicato, una red de agentes especiales altamente preparados y entrenados. Estos grupos están empeñados en crear un nuevo orden mundial mediante una serie de ataques terroristas cada vez más graves. Ethan reúne a su equipo y une sus fuerzas con la agente británica renegada Ilsa Faust (Rebecca Ferguson), quien puede que sea o no miembro de esta nación secreta, mientras el grupo se va enfrentando a su misión más imposible hasta la fecha... Quinta entrega de la saga Misión Imposible.</info>
 
 
<page>20</page><title>MISION IMPOSIBLE: PROTOCOLO FANTASMA</title>
<microhd>NA</microhd>
<fullhd>53a536004e7d18ab590af1e53c3ee1ba2faee245</fullhd>
<tresd>NA</tresd>
<cuatrok>d2ea1b18cba289a9d359d393fe4d9dc07a1f7d2a</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/y0hjsPyieqxcunEcAxb9mrrE09c.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/hqyjzDRCs1N5gEsh2gklzPdsEFD.jpg</fanart>
<date>2011</date>
<genre>Accion. Thriller. Saga</genre>
<extra>NA</extra>
<info>Cuarta entrega de la serie cinematográfica Misión imposible. El agente Ethan Hunt, acusado de un atentado terrorista con bombas contra el Kremlin, es desautorizado junto con toda la organización, al poner en marcha el Presidente el “Protocolo Fantasma”. Abandonado a su suerte y sin recursos, el objetivo de Ethan es rehabilitar el buen nombre de su agencia e impedir un nuevo ataque. Pero Ethan emprende esta misión con un equipo formado por fugitivos, cuyos motivos personales no conoce bien.</info>
 
 
<page>20</page><title>MISION IMPOSIBLE: SENTENCIA MORTAL - PARTE 1</title>
<microhd>NA</microhd>
<fullhd>2399e970173728758d7f668ce50525ca2fc1dcf7</fullhd>
<tresd>NA</tresd>
<cuatrok>ee12cc6982ba3f25e90ea7a96e89778385a0640b</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/83sGKvCv2T2CulYbd40Aeduc7n2.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/628Dep6AxEtDxjZoGP78TsOxYbK.jpg</fanart>
<date>2023</date>
<genre>Thriller. Acción</genre>
<extra>Estreno</extra>
<info>Ethan Hunt y su equipo del FMI, se embarcan en su misión más peligrosa hasta la fecha: localizar, antes de que caiga en las manos equivocadas, una nueva y terrorífica arma que amenaza a toda la humanidad. En esta tesitura, y con unas fuerzas oscuras del pasado de Ethan acechando, comienza una carrera mortal alrededor del mundo en la que está en juego el control del futuro y el destino del planeta. Enfrentado a un enemigo misterioso y todopoderoso, Ethan se ve obligado a considerar que nada puede anteponerse a su misión, ni siquiera las vidas de aquellos que más le importan.</info>
 
 
<page>20</page><title>MISION PANAMA</title>
<microhd>NA</microhd>
<fullhd>d094afc6651011b06e57dd0f5b95982712318002</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/u9DzgsmssidygWuORgYzhi317vj.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/mK8cLOafmAMAJjg9Md53VC7C8wi.jpg</fanart>
<date>2022</date>
<genre>Thriller. Acción</genre>
<extra>NA</extra>
<info>Un ex marine es contratado por un contratista de defensa para viajar a Panamá para completar un trato de armas. En el proceso, se involucra con la invasión estadounidense de Panamá y aprende una lección importante sobre la verdadera naturaleza del poder político.</info>
 
 
<page>20</page><title>MISTERIO EN VENECIA</title>
<microhd>NA</microhd>
<fullhd>3c4ebca2468a9fa3cbd15e5da81107b23601a618</fullhd>
<tresd>NA</tresd>
<cuatrok>ecf089e7db9e210de8ddbb6576d5e56107019345</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/kWz6Aa2chxcNUgAQNooYS4yyLkC.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/pUTpUOMnFn7zmANje5jkFTmYNn9.jpg</fanart>
<date>2023</date>
<genre>Intriga. Thriller. Terror</genre>
<extra>Estreno</extra>
<info>En la Venecia posterior a la Segunda Guerra Mundial, Poirot, ahora retirado y viviendo en su propio exilio, asiste a regañadientes a una sesión de espiritismo, cuando uno de los invitados es asesinado, por lo que depende del ex detective descubrir una vez más al asesino.</info>
 
 
<page>20</page><title>MODELO 77</title>
<microhd>NA</microhd>
<fullhd>acefbe1bbcf0d57472540a0352b3bcc8e35203b9</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/2ZbWaMQEGlVJsCSt73XPkZpqKZF.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/pZkhtaqZzcTSz2e5GxUj3CM6jz6.jpg</fanart>
<date>2022</date>
<genre>Drama. Thriller</genre>
<extra>NA</extra>
<info>Cárcel Modelo. Barcelona, 1977. Manuel (Miguel Herrán), un joven contable, encarcelado y pendiente de juicio por cometer un desfalco, se enfrenta a una posible pena de entre 10 y 20 años, un castigo desproporcionado para la cuantía de su delito. Pronto, junto a su compañero de celda, Pino (Javier Gutiérrez), se une a un grupo de presos comunes que se está organizando para exigir una amnistía. Se inicia una guerra por la libertad que hará tambalearse al sistema penitenciario español. Si las cosas están cambiando fuera, dentro también tendrán que hacerlo.</info>
 
 
<page>20</page><title>MOLLYS GAME</title>
<microhd>d534f455e39be5743a642c7803aaa9a2b9a34455</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/sxV1wlobArp7paz16QuC8n1dPwj.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/yvbXGWjg30sj7rohEZvSe90jSJC.jpg</fanart>
<date>2017</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Se centra en la vida de Molly Bloom, una esquiadora de talla mundial que llegó a ser millonaria antes de los 21 años. Tras perderse los Juegos Olímpicos, Molly se trasladó a vivir a Los Ángeles, donde incluso trabajó de camarera. Gracias a su inteligencia y sus dotes empresariales, la joven acabó ganando millones de dólares organizando partidas póker antes de que el FBI la investigara.</info>
 
 
<page>20</page><title>MONSTER HUNTER</title>
<microhd>635e121a50d883593afe61b55287fc9895699d96</microhd>
<fullhd>838ef58db61026aaf5c8240595625fe299ed2a72</fullhd>
<tresd>NA</tresd>
<cuatrok>635e121a50d883593afe61b55287fc9895699d96</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/dZOYV74OQsmBkoKPIRu0n1sYHZb.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/aUCSimits9Ipk4Pyoy8mMaIE0Iu.jpg</fanart>
<date>2020</date>
<genre>Fantástico. Aventuras. Acción </genre>
<extra>NA</extra>
<info>Tras nuestro mundo hay otro: un mundo de monstruos peligrosos y poderosos que gobiernan sus dominios con una ferocidad mortal. Cuando la teniente Artemis (Milla Jovovovich) y sus leales soldados son transportados de nuestro mundo al nuevo mundo, la imperturbable teniente recibe el golpe de su vida. En su desesperada batalla por sobrevivir contra enormes enemigos con poderes increíbles y ataques imparables y repugnantes, Artemis se unirá a un hombre misterioso que ha encontrado la forma de defenderse.</info>
 
 
<page>20</page><title>MONSTRUOS UNIVERSITY</title>
<microhd>33177f20b05538151117e15f64e63f7c5ac75feb</microhd>
<fullhd>8f7c5bff584ef635299e0034882ad5323f60e98e</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/4rLfeHGIyDEQsqR22iajGSLFNME.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/lo7qlO5gx7oz5l5K3RgilmaPoBp.jpg</fanart>
<date>2013</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Mike Wazowski (Billy Crystal) y James P. Sullivan (John Goodman) son amigos inseparables, pero no fue siempre así. Cuando se conocieron en la Universidad, no se podían soportar, pero acabaron superando sus diferencias y se convirtieron en los mejores amigos. Precuela de la exitosa "Monstruos, S.A.".</info>
 
 
<page>20</page><title>MOONFALL</title>
<microhd>NA</microhd>
<fullhd>ac6871c64937a2d4ed4b720eb1c561e8e5a1db5f</fullhd>
<tresd>NA</tresd>
<cuatrok>30c5ac66b323d984396b1cbe80837f8eba24d223</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/noe4dNuEbqj7xIjgOFkTOohRsNC.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/x747ZvF0CcYYTTpPRCoUrxA2cYy.jpg</fanart>
<date>2022</date>
<genre>Ciencia ficción</genre>
<extra>NA</extra>
<info>Una fuerza misteriosa golpea a la Luna fuera de su órbita y la envía en choque directo contra la Tierra a toda velocidad. Unas semanas antes del impacto con el mundo al borde de la aniquilación, la ejecutiva de la NASA y ex astronauta Jo Fowler (Halle Berry) está convencida de tener la clave para salvar nuestro planeta. Pero solo el astronauta Brian Harper (Patrick Wilson) y el teórico conspiranoico KC Houseman (John Bradley) la creen. Estos héroes inverosímiles montarán una misión imposible al espacio, dejando atrás a todos sus seres queridos, para aterrizar en la superficie lunar e intentar salvar a la humanidad, enfrentándose a un misterio de proporciones cósmicas.</info>
 
 
<page>20</page><title>MORBIUS</title>
<microhd>34218b9e1c24495b77606ce7d1806e57ff4296a2</microhd>
<fullhd>1b4579dbb29a3cb7083e3f0c621cd36e829213ae</fullhd>
<tresd>NA</tresd>
<cuatrok>fdd5b5dc7b1f171a48ef325a2eb8df9777d2fb4a</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/6WmTdYNoSinBAXs0AfTTCSaV5lw.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/rl49Qr4KbfTSYjXYAN8fMDIt2xS.jpg</fanart>
<date>2022</date>
<genre>Terror. Fantástico. Ciencia ficción. Acción</genre>
<extra>NA</extra>
<info>El Doctor Michael Morbius (Jared Leto) es un bioquímico que sufre una extraña enfermedad en la sangre. Al intentar curarse y dar una respuesta a su trastorno se infecta sin darse cuenta con una forma de vampirismo. Tras la cura, Michael se siente más vivo que nunca y adquiere varios dones como fuerza y velocidad, además de una necesidad irresistible de consumir sangre. Trágicamente convertido en un imperfecto antihéroe, el Doctor Morbius tendrá una última oportunidad, pero sin saber a qué precio.</info>
 
 
<page>20</page><title>MORTAL KOMBAT</title>
<microhd>3b7c3746d9c21534e6561d7bfa2635e246c49abb</microhd>
<fullhd>69ba1a46c4f1561e7a032dacbdc6bae737ec20a3</fullhd>
<tresd>NA</tresd>
<cuatrok>e0f60c3402809b4c7c1cae42b9984957ea7bbe3a</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/2xmx8oPlbDaxTjHsIOZvOt5L3aJ.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/6ELCZlTA5lGUops70hKdB83WJxH.jpg</fanart>
<date>2021</date>
<genre>Acción. Aventuras. Fantástico</genre>
<extra>NA</extra>
<info>En “Mortal Kombat”, Cole Young, el luchador de MMA (Artes Marciales Mixtas), acostumbrado a recibir palizas por dinero, desconoce su ascendencia, y tampoco sabe por qué el emperador Shang Tsung de Outworld ha enviado a su mejor guerrero, Sub-Zero, un Cryomancer sobrenatural, para dar caza a Cole. Cole teme por la seguridad de su familia y busca a Sonya Blade siguiendo las indicaciones de Jax, un comandante de las Fuerzas Especiales que tiene la misma extraña marca de dragón con la que nació Cole. No tarda en llegar al templo de Lord Raiden, un Dios Anciano y el protector de Earthrealm, que ofrece refugio a los que llevan la marca. Aquí, Cole entrena con los experimentados guerreros Liu Kang, Kung Lao y el mercenario rebelde Kano, mientras se prepara para enfrentarse a los mayores campeones de la Tierra contra los enemigos de Outworld (El Mundo Exterior) en una batalla de enorme envergadura por el universo. Veremos si los esfuerzos de Cole se ven recompensados y consigue desbloquear su arcana -ese inmenso poder que surge del interior de su alma— a tiempo de salvar no solo a su familia, sino para detener Outworld de una vez por todas.</info>
 
 
<page>20</page><title>MOWGLY: LA LEYENDA DE LA SELVA</title>
<microhd>eab27967bb091d73b3dcfbdc27793bf73000400e</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>4f71a171e355b8fe79477b4b41d5ee2fb6475ed7</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/yJ12iCaZqbr7683NYI3Nyav6C7g.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/remgeE1ak6CqU38EBLkFFFiQ5aQ.jpg</fanart>
<date>2018</date>
<genre>Infantil.</genre>
<extra>NA</extra>
<info>Trata sobre la educación de Mowgli, criado por una manada de lobos en las selvas de India. Mientras aprende las reglas, a menudo complicadas, de la jungla. Bajo la tutela de un oso llamado Baloo y una pantera llamada Bagheera, Mowgli llega a ser aceptado por los animales de la jungla como uno más de ellos. Por todos menos uno: el temible tigre Shere Khan. Pero puede haber peligros mayores acechando en la jungla, cuando Mowgli se enfrenta a sus orígenes humanos.</info>
 
 
<page>20</page><title>MR. WAIN</title>
<microhd>NA</microhd>
<fullhd>97966b24e8fbba874e6bd8bf24b0d8d21845a681</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/bwl987oVArEV7Ugvr4axCN5W10a.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/s7qE2YROM7pJkzilqpahrLuTTcE.jpg</fanart>
<date>2021</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>Cuenta la historia real del británico Louis Wain, un artista, inventor y empresario, que hizo todo lo posible para cuidar de sus cinco hermanas y su madre. Dos eventos cambiaron su vida para siempre: conocer a Emily, el amor de su vida, y adoptar a Peter, un gatito perdido. Los dos se convertirán tanto en su familia como en su inspiración para pintar las extraordinarias imágenes de gatos que lo hicieron mundialmente famoso.</info>
 
 
<page>20</page><title>MUERE OTRA VEZ</title>
<microhd>9461b3748d7de2d0313a7df6fc66d18071bc2068</microhd>
<fullhd>c4deaf3a29f48cc8e04ab909509e08e06d11b34d</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/hlHAeVkX5xdfSw17j9sR2vChULc.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/1Yc5hIRh3skhsEKLbCHE7v1FBOa.jpg</fanart>
<date>2021</date>
<genre>Ciencia ficción. Acción. Thriller. Comedia</genre>
<extra>NA</extra>
<info>Un oficial de las fuerzas especiales, ya retirado, se ve obligado a vivir una y otra vez en bucle el día de su muerte.</info>
 
 
<page>20</page><title>MUERE, HART</title>
<microhd>d57931b5e6c130190923574e15c9392bb0b2de8b</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>d57931b5e6c130190923574e15c9392bb0b2de8b</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/xz91Gre0w7tyl99jGEh5oGc8DQ7.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/un2kba7UHYrydCNL3OkLocU5mG.jpg</fanart>
<date>2023</date>
<genre>Acción. Comedia</genre>
<extra>NA</extra>
<info>Sigue una versión ficticia de Kevin Hart, mientras intenta convertirse en una estrella de cine de acción. Asiste a una escuela dirigida por Ron Wilcox, donde intenta aprender cómo convertirse en una de las estrellas de acción más codiciadas de la industria.</info>
 
 
<page>20</page><title>MUERTE EN EL NILO</title>
<microhd>5cc6a7f431b4457ac62b077ef53ea13c51ef5503</microhd>
<fullhd>533BEAFF3655C3C1DB4334F20B6C5304D7D72D7A</fullhd>
<tresd>NA</tresd>
<cuatrok>14982a430c54056a918bbff2ec8b775fa1fcb9f7</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/sWB5i8r8EprS75q7rSGCszOrOH0.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/515SYjj1FG4CgK6WF3FtI4eICm0.jpg</fanart>
<date>2022</date>
<genre>Intriga. Thriller</genre>
<extra>NA</extra>
<info>Basada en la novela de Agatha Christie, publicada en 1937. "Muerte en el Nilo" es un thriller de misterio dirigido por Kenneth Branagh sobre el caos emocional y las consecuencias letales que provocan los amores obsesivos. Las vacaciones egipcias del detective belga Hércules Poirot, a bordo de un glamuroso barco de vapor, se ven alteradas por la búsqueda de un asesino cuando la idílica luna de miel de una pareja perfecta se ve truncada de la forma más trágica. </info>
 
 
<page>20</page><title>MUJERCITAS</title>
<microhd>a0f9eb2f35e0fe9de40abe4571bc5ca45707575d</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/z2wBWFEQ9FE0RcjmWXz3vvSNPfT.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/A8EzeMnCrQfnNZVpULItCbYlK7C.jpg</fanart>
<date>2019</date>
<genre>Drama. Romance</genre>
<extra>NA</extra>
<info>Amy, Jo, Beth y Meg son cuatro hermanas en plena adolescencia, que viven con su madre en una Norteamérica que sufre lejanamente su Guerra Civil. Con sus variadas vocaciones artísticas y anhelos juveniles, descubrirán el amor y la importancia de los lazos familiares.</info>
 
 
<page>20</page><title>MULA</title>
<microhd>7ec0fc802544307f5358209c405fd6c2ae963d93</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>NA</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/r7tJH9dXbHUHFF9ziACg3ELc2TC.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/bTeRgkAavyw1eCtSkaww18wLYNP.jpg</fanart>
<date>2018</date>
<genre>Drama</genre>
<extra>NA</extra>
<info>A Earl Stone (Eastwood), un octogenario que está en quiebra, solo, y que se enfrenta a la ejecución hipotecaria de su negocio, se le ofrece un trabajo aparentemente facil: sólo requiere conducir. Pero, sin saberlo, Earl se convirte en traficante de drogas para un cártel mexicano, y pasa a estar bajo el radar del agente de la DEA Colin Bates (Cooper).</info>
 
 
<page>20</page><title>MULAN</title>
<microhd>a633cb0a5abe8449986b963b46eb1d35d864b502</microhd>
<fullhd>b8c20936307b2371145e361038d065d0f1180351</fullhd>
<tresd>NA</tresd>
<cuatrok>282017683937603b6d0876a0915b9e48b1f4539e</cuatrok>
<thumbnail>https://image.tmdb.org/t/p/original/ioOjEgaduadwBJiDKYkvD5T8kMY.jpg</thumbnail>
<fanart>https://image.tmdb.org/t/p/original/aoHiMjRt0Qs1dtkV61LyxTnQtJl.jpg</fanart>
<date>2020</date>
<genre>Aventuras. Acción. Bélico .Infantil</genre>
<extra>NA</extra>
<info>Narra la historia de Mulán, una intrépida joven lo arriesga todo por amor a su familia y a su país hasta convertirse en una de las mayores guerreras de la historia de China. Cuando el emperador de China decreta que un hombre de cada familia debe servir en el Ejército Imperial para defender al país de los invasores del norte, Hua Mulán, la hija mayor de un condecorado guerrero, decide ocupar el lugar de su padre enfermo. Haciéndose pasar por un hombre, Hua Jun, se enfrenta a constantes desafíos y deberá aprender a canalizar su fuerza interior y a aceptar su verdadero potencial. Un viaje épico que la convertirá en una reconocida guerrera y le permitirá ganarse el respeto de una nación agradecida... y de un padre orgulloso. Remake del clásico de Disney.</info>
 
 
<page>20</page><title>MY FAIR LADY</title>
<microhd>NA</microhd>
<fullhd>NA</fullhd>
<tresd>NA</tresd>
<cuatrok>cac67286d77ea328485d3ae0146907c69ac77311</cuatrok>
<thumbnail>https://www.themoviedb.org/t/p/original/iA1hDrGBtfJQuMiwJS1m7AA9LhR.jpg</thumbnail>
<fanart>https://www.themoviedb.org/t/p/original/5HpzmeVrvQKKhzIhMO1IFiB1UsI.jpg</fanart>
<date>1964</date>
<genre>Musical. Romance. Comedia</genre>
<extra>Mis</extra>
<info>Versión cinematográfica del mito de Pigmalión, inspirada en la obra teatral homónima del escritor irlandés G.B. Shaw (1856-1950). En una lluviosa noche de 1912, el excéntrico y snob lingüista Henry Higgins conoce a Eliza Doolittle, una harapienta y ordinaria vendedora de violetas. El vulgar lenguaje de la florista despierta tanto su interés que hace una arriesgada apuesta con su amigo el coronel Pickering: se compromete a enseñarle a hablar correctamente el inglés y a hacerla pasar por una dama de la alta sociedad en un plazo de seis meses.</info>
 
